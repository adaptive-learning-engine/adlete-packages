# Welcome to ADLETE!

ADLETE is a generalized, adaptive learning technology to analyze learners’ competence level and provide subsequent task/exercise/training recommendations with an appropriate difficulty level. We designed the ADLETE framework based on the methodological Evidence-Centered Design (ECD) framework, especially in its use of Dynamic Bayesian Networks for probabilistic competency models.

The ADLETE adaptive learning technology supports you in creating virtual learning environments (e.g., game-based learning applications, serious games, virtual reality simulations, moodle courses, etc..) for any learning domain to keep learners in a continuous, adaptive learning circle. The following figure illustrates this learning circle:![figure-learning-circle.png](/figure-learning-circle.png)**Figure 1.: Is a functionality diagram, illustrating the ADLETE continuos, adaptive, learning circle.**

The green stages of the circle are generalized and implemented through the ADLETE framework, while the blue stages can be implemented through your (existing) virtual learning environment.

## Overview

This mono-repository contains several software components to develop adaptive virtual learning environments (VLE). We designed and developed all software components based on the ideas and suggestions of the Evidence-Centered Design framework. Additionally, the ADLETE framework follows the concept of modeling learner competencies´ by using Bayesian Networks. Before you start using the ADLETE framework, we highly recommend getting familiar with those two topics in advance. To do so, here is a table with reading suggestions. We also add overtime a bibliography with further exciting resources. To help you further understand the ADLETE framework, we developed a simple prototype for the learning domain arithmetic. You can find this prototype here at [arithmetic-demo](https://gitlab.com/adaptive-learning-engine/arithmetic-demo).

| Topic                              | Author(s)                                       | Title                                       | Link                                                                     |
| ---------------------------------- | ----------------------------------------------- | ------------------------------------------- | ------------------------------------------------------------------------ |
| Evidence-Centered Design Framework | Shute                                           | ECD For Dummies                             | [LINK](https://myweb.fsu.edu/vshute/ECD.pdf)                             |
| ADLETE Framework| Gnadlinger, Selmanagić, Simbeck, and Kriglstein | Adapting Is Difficult! Introducing a Generic Adaptive Learning Framework for Learner Modeling and Task Recommendation Based on Dynamic Bayesian Networks. | [LINK](https://www.researchgate.net/publication/370060038_Adapting_is_difficult_Introducing_a_Generic_Adaptive_Learning_Framework_for_Learner_Modeling_and_Task_Recommendation_Based_on_Dynamic_Bayesian_Networks) |
| Bayesian Networks                  | Almond, Mislevy, Steinberg, Yan, and Williamson | Bayesian Networks in Educational Assessment | [LINK](https://books.google.at/books/about/?id=DgUyBwAAQBAJ&redir_esc=y) |

## Introduction

The following infographic (Figure 2) should give you a first impression of this repository. Please note that we implemented the ADLETE framework in a generalized way to (re-)use it for any learning domain. To achieve this, we built the ADLETE framework along different layers and split it into backend and frontend software components. Therefore you must extend your (existing) actual virtual learning environment (e.g., game-based learning application, serious game, virtual reality simulation, moodle course, etc..) to interact with the ADLETE framework.

![figure1](/figure-overview.png)**\
Figure 2.: Relationship between software components (\*\*VLE = virtual learning environment).**

## Table of Content

As the infographic (Figure 1) suggests, this repository consists of the following main software components. The following sections holds the documentation of the different software components.

- [ADLETE - Engine Building Blocks](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Engine-Building-Blocks "ADLETE - Engine Building Blocks")
- [ADLETE - Framework](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Framework "ADLETE - Framework")
- [ADLETE - Service](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Service "ADLETE - Service")
- [ADLETE - Visualizer]()
- [ADLETE - Client Plugin TypeScript for Web - Browser](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Client-Plugin-TypeScript "ADLETE - Client Plugin TypeScript for Web - Browser")
- [ADLETE - Client Plugin Unity for Game Development in Unity](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Client-Plugin-Unity "ADLETE Client Plugin Unity")
