/* eslint-disable max-classes-per-file */
/* eslint-disable class-methods-use-this */
import { PluginFactoryRegistry } from './PluginFactoryRegistry';
import { PluginManager } from './PluginManager';
import { IPlugin, IPluginFactory, IPluginFactoryRegistry, IPluginManager, IPluginMeta } from './specs';

interface Plugins {
  foobar: Foobar;
  moobar: Moobar;
}

const FoobarMeta: IPluginMeta<Plugins, 'foobar'> = {
  id: 'foobar',
  dependencies: ['moobar'],
};

const MoobarMeta: IPluginMeta<Plugins, 'moobar'> = {
  id: 'moobar',
};

class Moobar implements IPlugin<Plugins> {
  meta: IPluginMeta<Plugins>;

  initialize(pluginManager: IPluginManager<Plugins>): Promise<void> {
    const bar = pluginManager.get('foobar');
    return Promise.resolve();
  }

  terminate(): Promise<void> {
    throw new Error('Method not implemented.');
  }

  x: string;
}

class Foobar implements IPlugin<Plugins> {
  meta: IPluginMeta<Plugins>;

  initialize(pluginManager: IPluginManager<Plugins>): Promise<void> {
    const bar = pluginManager.get('foobar');
    return Promise.resolve();
  }

  terminate(): Promise<void> {
    throw new Error('Method not implemented.');
  }
}

const foobarFactory: IPluginFactory<Plugins, 'foobar'> = {
  meta: FoobarMeta,
  create: () => Promise.resolve(new Foobar()),
};

const moobarFactory: IPluginFactory<Plugins, 'moobar'> = {
  meta: MoobarMeta,
  create: () => Promise.resolve(new Moobar()),
};

interface OtherPlugins {
  sfsdfsfd: Foobar;
}

const pluginRegistry = new PluginFactoryRegistry<Plugins>();

pluginRegistry.add('foobar', moobarFactory);

const pluginMan = new PluginManager<Plugins>(pluginRegistry);

const foo = pluginMan.get('foobar');
foo.initialize(pluginMan);

const bar: IPluginFactoryRegistry<Plugins> = null;

const myplugin = bar.createPlugin('foobar');
