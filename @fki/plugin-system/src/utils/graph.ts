import { DirectedGraph } from 'graphology';
import { subgraph } from 'graphology-operators';
import type { Attributes } from 'graphology-types';

export type NodeKey = string | number;

export function nodesAndEdgesToGraph<
  TKey extends string | number | symbol,
  TNodeAttr = Attributes,
  TEdgeAttr = Attributes,
  TGraphAttr = Attributes
>(nodes: [TKey, TNodeAttr][], edges: [TKey, TKey, TEdgeAttr][]): DirectedGraph<TNodeAttr> {
  const graph = new DirectedGraph<TNodeAttr, TEdgeAttr, TGraphAttr>({ allowSelfLoops: false, multi: false });
  nodes.forEach(([nodeId, attr]) => graph.addNode(nodeId, attr));
  edges.forEach(([source, target, attr]) => graph.addDirectedEdge(source, target, attr));
  return graph;
}

export function getNodeRank<TKey extends string | number | symbol>(
  graph: DirectedGraph,
  nodeId: TKey,
  ranks: Record<TKey, number>
): number {
  let rank = 0;
  graph.forEachOutEdge(nodeId, (key, attr, source, target) => {
    const targetId = target as TKey;
    const targetRank = target in ranks ? ranks[targetId] : getNodeRank(graph, targetId, ranks);
    rank = Math.max(rank, targetRank + 1);
  });
  // eslint-disable-next-line no-param-reassign
  ranks[nodeId] = rank;
  return rank;
}

function collectDescendants<TNodeAttr = Attributes, TEdgeAttr = Attributes, TGraphAttr = Attributes>(
  graph: DirectedGraph<TNodeAttr, TEdgeAttr, TGraphAttr>,
  id: NodeKey,
  nodes: Set<NodeKey>
): void {
  graph.forEachOutEdge(id, (key, attr, source, target) => {
    if (!nodes.has(target)) {
      nodes.add(target);
      collectDescendants(graph, target, nodes);
    }
  });
}

export function getDescendantGraph<TNodeAttr = Attributes, TEdgeAttr = Attributes, TGraphAttr = Attributes>(
  graph: DirectedGraph<TNodeAttr, TEdgeAttr, TGraphAttr>,
  ids: NodeKey[]
): DirectedGraph<TNodeAttr, TEdgeAttr, TGraphAttr> {
  const subgraphNodes = new Set<NodeKey>();
  ids.forEach((id) => {
    subgraphNodes.add(id);
    collectDescendants(graph, id, subgraphNodes);
  });
  return subgraph(graph, subgraphNodes as Set<string>); // TODO: remove type conversion, once graphology-operators allows NodeKey
}

export function getNodeSequence<TKey extends string | number | symbol>(graph: DirectedGraph): TKey[][] {
  const sequence: TKey[][] = [];

  // calculating ranks from the leafs onward
  const ranks = {} as Record<TKey, number>;
  graph.forEachNode((id) => {
    if (graph.inDegree(id) === 0) getNodeRank<TKey>(graph, id as TKey, ranks);
  });

  graph.forEachNode((id) => {
    const rank = ranks[id as TKey];
    if (sequence.length <= rank) sequence.length = rank + 1;
    if (sequence[rank] == null) sequence[rank] = [];
    sequence[rank].push(id as TKey);
  });

  return sequence;
}
