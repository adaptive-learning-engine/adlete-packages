import { IPluginFactory, IPluginFactoryRegistry, Plugins } from './specs';

export class PluginFactoryRegistry<TPlugins extends Plugins<TPlugins> = unknown> implements IPluginFactoryRegistry<TPlugins> {
  protected factories: Partial<{ [Property in keyof TPlugins]: IPluginFactory<TPlugins, keyof TPlugins> }>;

  public constructor() {
    this.factories = {};
  }

  public add<K extends keyof TPlugins>(id: K, factory: IPluginFactory<TPlugins, K>): void {
    if (this.factories[id]) throw new Error(`PluginFactory with id '${id.toString()}' already exists!`);

    this.factories[id] = factory;
  }

  public get<K extends keyof TPlugins>(id: K): IPluginFactory<TPlugins, K> {
    return this.factories[id] as IPluginFactory<TPlugins, K>;
  }

  public remove(id: keyof TPlugins): void {
    if (!this.factories[id]) throw new Error(`PluginFactory with id '${id.toString()}' does not exist!`);

    this.factories[id] = null;
  }

  public createPlugin<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]> {
    if (!this.factories[id]) throw new Error(`PluginFactory with id '${id.toString()}' does not exist!`);

    return this.factories[id].create() as Promise<TPlugins[K]>;
  }

  public getIds(): (keyof TPlugins)[] {
    // TODO: what about symbols and integers?
    return Object.keys(this.factories) as (keyof TPlugins)[];
  }
}
