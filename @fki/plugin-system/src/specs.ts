export type PromiseState = 'unknown' | 'pending' | 'resolved' | 'rejected';

export interface IPromiseWithState<T> {
  promise: Promise<T>;
  state: PromiseState;
}

export type PluginId = string | number | symbol;

export type Plugins<TPlugins extends { [Property in keyof TPlugins]: IPlugin<TPlugins> }> = {
  [Property in keyof TPlugins]: IPlugin<TPlugins>;
};

export interface IPluginMeta<TPlugins extends Plugins<TPlugins>, TPluginKey extends keyof TPlugins = keyof TPlugins> {
  id: TPluginKey;
  dependencies?: (keyof TPlugins)[];
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IPlugin<TPlugins extends Plugins<TPlugins> = any> {
  meta: IPluginMeta<TPlugins>;
  initialize(pluginMan: IPluginManager<TPlugins>): Promise<void>;
  terminate(): Promise<void>;
}

export interface IPluginFactory<TPlugins extends Plugins<TPlugins>, TPluginKey extends keyof TPlugins> {
  meta: IPluginMeta<TPlugins, TPluginKey>;
  create(): Promise<TPlugins[TPluginKey]>;
}

export interface IPluginFactoryRegistry<TPlugins extends Plugins<TPlugins>> {
  getIds(): (keyof TPlugins)[];

  // TODO: use infer
  add<K extends keyof TPlugins>(id: K, factory: IPluginFactory<TPlugins, K>): void;
  get<K extends keyof TPlugins>(id: K): IPluginFactory<TPlugins, K>;

  remove(id: keyof TPlugins): void;
  createPlugin(id: keyof TPlugins): Promise<TPlugins[keyof TPlugins]>;
}

export type PluginPhase = 'creation' | 'initialization' | 'termination';

export type PluginState =
  | 'unknown'
  | 'creating'
  | 'created'
  | 'error-on-creation'
  | 'initializing'
  | 'initialized'
  | 'error-on-initialization'
  | 'terminating'
  | 'terminated'
  | 'error-on-termination';

export interface IPluginManager<TPlugins extends Plugins<TPlugins>> {
  getPhaseState(id: keyof TPlugins, phase: PluginPhase): PromiseState;
  getState(id: keyof TPlugins): PluginState;
  get<K extends keyof TPlugins>(id: K): TPlugins[K];
  create<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]>;
  initialize<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]>;
  terminate<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]>;
}
