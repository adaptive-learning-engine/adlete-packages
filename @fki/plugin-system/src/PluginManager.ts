import { PluginFactoryRegistry } from './PluginFactoryRegistry';
import { IPlugin, IPluginManager, IPromiseWithState, PluginPhase, Plugins, PluginState, PromiseState } from './specs';

export class PluginManager<
  TPlugins extends Plugins<TPlugins>,
  TPluginPromiseStates extends { [Property in keyof TPlugins]: IPromiseWithState<IPlugin<TPlugins>> } = {
    [Property in keyof TPlugins]: IPromiseWithState<IPlugin<TPlugins>>;
  }
> implements IPluginManager<TPlugins>
{
  protected factories: PluginFactoryRegistry<TPlugins>;

  protected plugins: Partial<TPlugins>;

  protected phasesWithStates: Record<PluginPhase, TPluginPromiseStates> = {
    creation: {} as TPluginPromiseStates,
    initialization: {} as TPluginPromiseStates,
    termination: {} as TPluginPromiseStates,
  };

  public constructor(pluginFactoryRegistry: PluginFactoryRegistry<TPlugins>) {
    this.factories = pluginFactoryRegistry;
    this.plugins = {};
  }

  public getPhaseState(id: keyof TPlugins, phase: PluginPhase): PromiseState {
    if (phase !== 'creation' && !this.plugins[id]) throw new Error(`Plugin with id '${id.toString()}' does not exist!`);

    if (!this.phasesWithStates[phase][id]) return 'unknown';

    return this.phasesWithStates[phase][id].state;
  }

  public getState(id: keyof TPlugins): PluginState {
    if (!this.plugins[id]) return 'unknown';

    const creationState = this.getPhaseState(id, 'creation');
    switch (creationState) {
      case 'pending':
        return 'creating';
      case 'resolved':
        return 'created';
      case 'rejected':
        return 'error-on-creation';
      default:
    }

    const initState = this.getPhaseState(id, 'initialization');
    switch (initState) {
      case 'pending':
        return 'initializing';
      case 'resolved':
        return 'initialized';
      case 'rejected':
        return 'error-on-initialization';
      default:
    }

    const termState = this.getPhaseState(id, 'termination');
    switch (termState) {
      case 'pending':
        return 'terminating';
      case 'resolved':
        return 'terminated';
      case 'rejected':
        return 'error-on-termination';
      default:
    }

    return 'unknown';
  }

  public get<K extends keyof TPlugins>(id: K): TPlugins[K] {
    return this.plugins[id];
  }

  public async create<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]> {
    const state = this.getPhaseState(id, 'creation');

    if (state === 'unknown') {
      const promise = this.factories.createPlugin(id);

      // TODO: find out why generics dont work well here
      const promiseWithState = { promise, state: 'pending' } as unknown as TPluginPromiseStates[K];
      this.phasesWithStates.creation[id] = promiseWithState;

      try {
        this.plugins[id] = await promise;
        promiseWithState.state = 'resolved';
      } catch (e) {
        promiseWithState.state = 'rejected';
        throw e;
      }

      return promise;
    }

    throw new Error(`Already tried to create plugin '${id.toString()}', which is currently in state '${state}'`);
  }

  public async initialize<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]> {
    const state = this.getPhaseState(id, 'initialization');

    if (state === 'unknown') {
      const plugin = this.plugins[id];
      const promise = plugin.initialize(this);

      // TODO: find out why generics dont work well here
      const promiseWithState = { promise, state: 'pending' } as unknown as TPluginPromiseStates[K];
      this.phasesWithStates.initialization[id] = promiseWithState;

      try {
        await promise;
        promiseWithState.state = 'resolved';
      } catch (e) {
        promiseWithState.state = 'rejected';
        throw e;
      }

      return promise.then(() => plugin);
    }

    throw new Error(`Already tried to initiaize plugin '${id.toString()}', which is currently in state '${state}'`);
  }

  public async terminate<K extends keyof TPlugins>(id: K): Promise<TPlugins[K]> {
    const state = this.getPhaseState(id, 'termination');

    if (state === 'unknown') {
      const plugin = this.plugins[id];
      const promise = plugin.terminate();

      // TODO: find out why generics dont work well here
      const promiseWithState = { promise, state: 'pending' } as unknown as TPluginPromiseStates[K];
      this.phasesWithStates.termination[id] = promiseWithState;

      try {
        await promise;
        promiseWithState.state = 'resolved';
      } catch (e) {
        promiseWithState.state = 'rejected';
        throw e;
      }

      return promise.then(() => plugin);
    }

    throw new Error(`Already tried to initiaize plugin '${id.toString()}', which is currently in state '${state}'`);
  }
}
