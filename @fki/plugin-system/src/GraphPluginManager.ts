/* eslint-disable no-plusplus */
import { PluginManager } from './PluginManager';
import { IPluginMeta, Plugins } from './specs';
import { getDescendantGraph, getNodeSequence, NodeKey, nodesAndEdgesToGraph } from './utils/graph';

export function extractPluginEdges<TPlugins extends Plugins<TPlugins>>(
  nodes: [keyof TPlugins, IPluginMeta<TPlugins>][]
): [keyof TPlugins, keyof TPlugins, null][] {
  return nodes.reduce((edges, [nodeId, meta]) => {
    if (meta.dependencies) meta.dependencies.forEach((dep) => edges.push([nodeId, dep, null]));
    return edges;
  }, []);
}

export class GraphPluginManager<TPlugins extends Plugins<TPlugins>> extends PluginManager<TPlugins> {
  public createSequence(ids: (keyof TPlugins)[] = null): (keyof TPlugins)[][] {
    const pluginIds = this.factories.getIds();
    const nodes = pluginIds.map((id) => [id, this.factories.get(id).meta] as [keyof TPlugins, IPluginMeta<TPlugins>]);
    const edges = extractPluginEdges(nodes);
    let graph = nodesAndEdgesToGraph(nodes, edges);

    if (ids && ids.length > 0) {
      graph = getDescendantGraph(graph, ids as NodeKey[]); // TODO: remove type conversion once graphology allows symbols
    }

    return getNodeSequence<keyof TPlugins>(graph);
  }

  public async executeCreationPhase(sequence: (keyof TPlugins)[][]): Promise<void> {
    for (let i = 0; i < sequence.length; ++i) {
      // eslint-disable-next-line no-await-in-loop
      await Promise.all(
        sequence[i].map((id) => {
          const shouldCreate = this.getPhaseState(id, 'creation') === 'unknown';
          return shouldCreate ? this.create(id) : this.phasesWithStates.creation[id].promise;
        })
      );
    }
  }

  public async executeInitializationPhase(sequence: (keyof TPlugins)[][]): Promise<void> {
    for (let i = 0; i < sequence.length; ++i) {
      // eslint-disable-next-line no-await-in-loop
      await Promise.all(
        sequence[i].map((id) => {
          const shouldInit = this.getPhaseState(id, 'initialization') === 'unknown';
          return shouldInit ? this.initialize(id) : this.phasesWithStates.initialization[id].promise;
        })
      );
    }
  }

  public async executeTerminationPhase(sequence: (keyof TPlugins)[][], reverse = true): Promise<void> {
    // eslint-disable-next-line no-param-reassign
    sequence = reverse ? [...sequence].reverse() : sequence;
    for (let i = 0; i < sequence.length; ++i) {
      // eslint-disable-next-line no-await-in-loop
      await Promise.all(
        sequence[i].map((id) => {
          const shouldTerminate = this.getPhaseState(id, 'termination') === 'unknown';
          return shouldTerminate ? this.terminate(id) : this.phasesWithStates.termination[id].promise;
        })
      );
    }
  }
}
