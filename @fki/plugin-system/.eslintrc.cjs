module.exports = {
  extends: ['@adlete/eslint-config/eslint-base'],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
};
