/* eslint-disable no-underscore-dangle */
import { SvgIcon } from '@mui/material';
import { IJsonModel, TabNode } from 'flexlayout-react';
import React, { memo, ReactElement } from 'react';

import { StatefulEventEmitter } from '../events/StatefulEventEmitter';
import { IRegistry } from '../utils/Registry';
import { shallowClone } from '../utils/shallowClone';

import { PanelErrorBoundary } from './PanelErrorBoundary';
import { PanelFactory } from './PanelFactory';

export const MemoPanelFactory = memo(PanelFactory);

export interface ILayout {
  name: string;
  isBuiltin?: boolean;
  model: IJsonModel;
}

export interface IPanelType {
  title: string;
  createPanel(): ReactElement;
  icon: typeof SvgIcon;
}

export interface ILayoutData {
  builtinLayouts: Record<string, ILayout>;
  userLayouts: Record<string, ILayout>;
  currLayout: string;
}

interface ILayoutSaveData {
  version: number;
  userLayouts: Record<string, ILayout>;
  currLayout: string;
}

export interface IAddPanelData {
  component: string;
}

const DB_CURRENT_VERSION = 2;
const DB_KEY = 'layout-data';

function loadedDataToLayoutData(builtinLayouts: Record<string, ILayout>, loadedData: ILayoutSaveData): ILayoutData {
  return { builtinLayouts, userLayouts: loadedData.userLayouts, currLayout: loadedData.currLayout };
}

function layoutDataToSaveData(layoutData: ILayoutData): ILayoutSaveData {
  return {
    version: DB_CURRENT_VERSION,
    userLayouts: layoutData.userLayouts,
    currLayout: layoutData.currLayout,
  };
}

interface ILayoutEventData {
  layoutDataChanged: ILayoutData;
  panelAdded: IAddPanelData;
}

export type IPanelTypeRegistry = IRegistry<Record<string, IPanelType>>;

export class LayoutManager {
  protected _events: StatefulEventEmitter<ILayoutEventData>;

  public get events(): StatefulEventEmitter<ILayoutEventData> {
    return this._events;
  }

  protected _panelTypes: IPanelTypeRegistry;

  public get panelTypes(): IPanelTypeRegistry {
    return this._panelTypes;
  }

  protected builtinLayouts: Record<string, ILayout>;

  public dbDefaultData: ILayoutSaveData;

  public constructor(panelTypeRegistry: IPanelTypeRegistry, builtinLayouts: Record<string, ILayout>) {
    this._panelTypes = panelTypeRegistry;
    this.builtinLayouts = builtinLayouts;
  }

  public initialize(): Promise<void> {
    // loading
    this.dbDefaultData = {
      version: DB_CURRENT_VERSION,
      userLayouts: {
        custom: { name: 'custom', model: this.builtinLayouts.default.model },
      },
      currLayout: 'default',
    };

    const item = window.localStorage.getItem(DB_KEY);
    let initialLayoutData: ILayoutData;
    if (item) {
      let loadedData = JSON.parse(item) as ILayoutSaveData;
      if (loadedData.version !== DB_CURRENT_VERSION) {
        loadedData = this.upgradeDatabase(loadedData);
      }
      initialLayoutData = loadedDataToLayoutData(this.builtinLayouts, loadedData);
    } else {
      initialLayoutData = loadedDataToLayoutData(this.builtinLayouts, this.dbDefaultData);
      this.saveLayoutData(initialLayoutData);
    }

    this._events = new StatefulEventEmitter<ILayoutEventData>({ layoutDataChanged: initialLayoutData });
    this._events.addListener('layoutDataChanged', (layoutData) => this.onLayoutDataChanged(layoutData));

    return Promise.resolve();
  }

  public onLayoutDataChanged(layoutData: ILayoutData): void {
    this.saveLayoutData(layoutData);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public upgradeDatabase(_loadedData: ILayoutSaveData): ILayoutSaveData {
    // currently we're just resetting to defaults
    window.localStorage.setItem(DB_KEY, JSON.stringify(this.dbDefaultData));
    return this.dbDefaultData;
  }

  public getLayout(name: string): ILayout {
    const currentLayoutData = this._events.getState('layoutDataChanged');

    if (name in currentLayoutData.builtinLayouts) {
      return currentLayoutData.builtinLayouts[name];
    }
    if (name in currentLayoutData.userLayouts) {
      return currentLayoutData.userLayouts[name];
    }
    return null;
  }

  public getAllLayouts(): ILayout[] {
    const currentLayoutData = this._events.getState('layoutDataChanged');

    return [...Object.values(currentLayoutData.builtinLayouts), ...Object.values(currentLayoutData.userLayouts)];
  }

  // eslint-disable-next-line class-methods-use-this
  protected saveLayoutData(data: ILayoutData): void {
    const saveData = layoutDataToSaveData(data);
    window.localStorage.setItem(DB_KEY, JSON.stringify(saveData));
  }

  public updateUserLayout(layout: ILayout, makeCurrLayout = false): void {
    const currentLayoutData = this._events.getState('layoutDataChanged');

    // TODO: throw error when overriding builtin layout
    currentLayoutData.userLayouts[layout.name] = layout;
    if (makeCurrLayout) {
      currentLayoutData.currLayout = layout.name;
    }

    this._events.emit('layoutDataChanged', currentLayoutData);
  }

  public setCurrentLayout(layoutName: string): void {
    const currentLayoutData = this._events.getState('layoutDataChanged');
    const newLayout = shallowClone(currentLayoutData);
    newLayout.currLayout = layoutName;
    this._events.emit('layoutDataChanged', newLayout);
  }

  public createPanelComponent(node: TabNode): ReactElement {
    const panelName = node.getComponent();
    if (this.panelTypes.has(panelName)) {
      // using MemoComponent, so panels aren't re-rendered on every click, which
      // especially made problems with BabylonRenderer
      return (
        <PanelErrorBoundary>
          <MemoPanelFactory node={node} factory={this.panelTypes.get(panelName).createPanel} />
        </PanelErrorBoundary>
      );
    }
    return (
      <div>
        Unknown Panel:
        {panelName}
      </div>
    );
  }

  public createIconComponent(node: TabNode): ReactElement {
    const panelName = node.getComponent();

    if (this.panelTypes.has(panelName)) {
      const IconType = this.panelTypes.get(panelName).icon;
      return <IconType style={{ fontSize: '14px' }} />;
    }
    return <div />;
  }

  public addPanel(addPanelData: IAddPanelData): void {
    this._events.emit('panelAdded', addPanelData);
  }
}
