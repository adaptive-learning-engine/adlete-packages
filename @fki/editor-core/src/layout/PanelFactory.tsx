import { TabNode } from 'flexlayout-react';
import { ReactElement } from 'react';

export interface IPanelFactoryProps {
  node: TabNode;
  factory: () => ReactElement;
}

export function PanelFactory({ factory }: IPanelFactoryProps): ReactElement {
  return factory();
}
