import { Box } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { Action, Actions, Layout, Model } from 'flexlayout-react';
import React, { ReactElement, useCallback, useEffect, useRef } from 'react';
// import { Close } from '@mui/icons-material';

import { useStatefulEvent } from '../events/hooks';

import { IAddPanelData, LayoutManager } from './LayoutManager';

interface IInternalData {
  lastActionType: string;
}

export interface IFlexibleLayoutProps extends CommonProps {
  layoutManager: LayoutManager;
}

export function FlexibleLayout({ layoutManager, className }: IFlexibleLayoutProps): ReactElement {
  const layoutData = useStatefulEvent(layoutManager.events, 'layoutDataChanged');
  const flexLayout = useRef<Layout>(null);

  // subscribing to panel events
  useEffect(() => {
    const listener = (addPanelData: IAddPanelData) => {
      flexLayout.current.addTabWithDragAndDropIndirect(`Add panel ${addPanelData.component}`, {
        component: addPanelData.component,
        name: layoutManager.panelTypes.get(addPanelData.component).title,
      });
    };
    layoutManager.events.addListener('panelAdded', listener);
    return () => {
      layoutManager.events.removeListener('panelAdded', listener);
    };
  });

  const internalData = useRef<IInternalData>({ lastActionType: '' });

  // TODO: dirty workaround until action is passed to onModelChange (again)
  // see https://github.com/caplin/FlexLayout/pull/64
  function handleAction(action: Action) {
    internalData.current.lastActionType = action.type;
    return action;
  }

  function handleModelChange(model: Model) {
    if (internalData.current.lastActionType === Actions.SET_ACTIVE_TABSET) {
      return;
    }
    // TODO: performance - this will trigger another re-render
    const userLayout = {
      name: 'custom',
      model: model.toJson(),
    };
    layoutManager.updateUserLayout(userLayout, true);
  }

  const layout = layoutManager.getLayout(layoutData.currLayout);
  const model = Model.fromJson(layout.model);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const panelFactory = useCallback(layoutManager.createPanelComponent.bind(layoutManager), [layoutManager]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const iconFactory = useCallback(layoutManager.createIconComponent.bind(layoutManager), [layoutManager]);

  const rootClassName = className ? `flexible-layout ${className}` : 'flexible-layout';

  // const closeIcon = <Close style={{ fontSize: '14px' }} />;
  return (
    <Box className={rootClassName}>
        <Layout
          ref={flexLayout}
          model={model}
          factory={panelFactory}
          iconFactory={iconFactory}
          onModelChange={handleModelChange}
          onAction={handleAction}
          // closeIcon={closeIcon}
          />
    </Box>
  );
}
