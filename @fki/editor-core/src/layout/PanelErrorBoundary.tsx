import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Alert, AlertTitle, Box, Typography } from '@mui/material';
import * as React from 'react';
import { ErrorInfo } from 'react';

/** @internal */
export interface IErrorBoundaryProps {
  children: React.ReactNode;
}
/** @internal */
export interface IErrorBoundaryState {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error: Error | any;
}

export interface IStackviewProps {
  error: Error;
}

export function StackView(props: IStackviewProps): React.ReactElement {
  const splitStack = props.error.stack ? props.error.stack.split('\n') : [];
  return (
    <Box>
      {splitStack.map((line) => (
        <div>{line}</div>
      ))}
    </Box>
  );
}

/** @internal */
export class PanelErrorBoundary extends React.Component<IErrorBoundaryProps, IErrorBoundaryState> {
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { error: null };
  }

  static getDerivedStateFromError(error: Error): IErrorBoundaryState {
    return { error };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    // eslint-disable-next-line no-console
    console.debug(error);
    // eslint-disable-next-line no-console
    console.debug(errorInfo);
  }

  render(): React.ReactNode {
    if (this.state.error) {
      if (this.state.error instanceof Error) {
        return (
          <Alert severity="error">
            <AlertTitle>
              {this.state.error.name}: {this.state.error.message}
            </AlertTitle>
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                <Typography>StackTrace</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <StackView error={this.state.error} />
              </AccordionDetails>
            </Accordion>
          </Alert>
        );
      }
      return <div>{this.state.error.toString()}</div>;
    }

    return this.props.children;
  }
}
