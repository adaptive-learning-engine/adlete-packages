import SaveIcon from '@mui/icons-material/Save';
import {
  alpha,
  Autocomplete,
  AutocompleteRenderInputParams,
  AutocompleteRenderOptionState,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import React, { ChangeEvent, ReactElement, useCallback, useState } from 'react';

import { useStatefulEvent } from '../events/hooks';

import { ILayout, LayoutManager } from './LayoutManager';

interface ISaveLayoutDialogProps {
  open: boolean;
  onClose: () => void;
  onSave: (name: string) => void;
}

function SaveLayoutDialog(props: ISaveLayoutDialogProps) {
  const { open, onClose, onSave } = props;

  const [name, setName] = useState('');

  function onInputChange(e: ChangeEvent<HTMLInputElement>) {
    setName(e.target.value);
  }

  function onSaveClick() {
    onSave(name);
  }

  // TODO: warn when overriding an already existing layout
  return (
    <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Save the current layout?</DialogTitle>
      <DialogContent>
        <TextField autoFocus margin="dense" id="name" label="Layout name" type="email" fullWidth onChange={onInputChange} />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Cancel
        </Button>
        <Button onClick={onSaveClick} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function renderInput(params: AutocompleteRenderInputParams) {
  return <TextField ref={params.InputProps.ref} label="Layout" inputProps={params.inputProps} autoFocus variant="outlined" />;
}

function getOptionLabel(option: ILayout) {
  return option.name;
}

function renderOption(
  openSaveDialog: () => void,
  props: React.HTMLAttributes<HTMLLIElement>,
  option: ILayout,
  { selected }: AutocompleteRenderOptionState
) {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <li {...props} className="layoutchooser-option">
      <Stack direction="row">
        <Box flexGrow="1">{option.name}</Box>
        {option.name === 'custom' ? <SaveIcon className="save-icon" fontSize="small" onClick={openSaveDialog} /> : <Box />}
      </Stack>
    </li>
  );
}

export interface ILayoutChooserProps {
  layoutManager: LayoutManager;
}

export function LayoutChooser({ layoutManager }: ILayoutChooserProps): ReactElement {
  const layoutData = useStatefulEvent(layoutManager.events, 'layoutDataChanged');
  const [saveDialogOpen, setSaveDialogOpen] = useState(false);

  const openSaveDialog = useCallback(() => {
    setSaveDialogOpen(true);
  }, [setSaveDialogOpen]);
  const onSaveDialogClose = useCallback(() => {
    setSaveDialogOpen(false);
  }, [setSaveDialogOpen]);

  const onSaveDialogSave = useCallback(
    (name: string) => {
      // TODO: prevent overriding default
      onSaveDialogClose();
      const userLayout = {
        name,
        model: layoutData.userLayouts.custom.model,
      };
      layoutManager.updateUserLayout(userLayout, true);
    },
    [layoutData, layoutManager, onSaveDialogClose]
  );

  const options = layoutManager.getAllLayouts();

  const setCurrentLayout = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (event: any, option: ILayout) => {
      layoutManager.setCurrentLayout(option.name);
    },
    [layoutManager]
  );

  const renderOptionBound = useCallback(
    (props: React.HTMLAttributes<HTMLLIElement>, layout: ILayout, state: AutocompleteRenderOptionState) =>
      renderOption(openSaveDialog, props, layout, state),
    [openSaveDialog]
  );

  return (
    <>
      <Autocomplete
        renderInput={renderInput}
        options={options}
        getOptionLabel={getOptionLabel}
        renderOption={renderOptionBound}
        value={layoutManager.getLayout(layoutData.currLayout)}
        onChange={setCurrentLayout}
      />
      <SaveLayoutDialog open={saveDialogOpen} onClose={onSaveDialogClose} onSave={onSaveDialogSave} />
    </>
  );
}

export const StyledLayoutChooser = styled(LayoutChooser)(({ theme }) => {
  const saveIconColor = alpha(theme.palette.text.primary, 0.26);
  return `
    & .save-icon: {
      color: ${saveIconColor};
      
      '&:hover': { 
        color: ${alpha(saveIconColor, 0.4)}
      }
    },
  }`;
});
