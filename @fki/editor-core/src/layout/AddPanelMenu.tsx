import { LibraryAdd } from '@mui/icons-material';
import { Button, ListItemIcon, ListItemText, Menu, MenuItem } from '@mui/material';
import React, { ReactElement, useCallback, useMemo } from 'react';

import { IPanelType, LayoutManager } from './LayoutManager';

type AddPanelCB = (panelName: string) => void;

function createPanelMenuItem(component: string, panelInfo: IPanelType, onAddPanel: AddPanelCB): ReactElement {
  // const onAddPanelBound = useCallback(onAddPanel.bind(null, component), [panelInfo]);

  const IconType = panelInfo.icon;
  return (
    <MenuItem key={component} onClick={() => onAddPanel(component)}>
      <ListItemIcon>
        <IconType style={{ fontSize: 'small' }} />
      </ListItemIcon>
      <ListItemText>{panelInfo.title}</ListItemText>
    </MenuItem>
  );
}

export interface IAddPanelMenuProps {
  layoutManager: LayoutManager;
}

export function AddPanelMenu({ layoutManager }: IAddPanelMenuProps): ReactElement {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuItemChosen = useCallback(
    (component: string) => {
      setAnchorEl(null);
      layoutManager.addPanel({ component });
    },
    [layoutManager]
  );

  const panelTypes = layoutManager.panelTypes.getAll();

  const panels = useMemo(
    () => Object.entries(panelTypes).map(([component, panelType]) => createPanelMenuItem(component, panelType, handleMenuItemChosen)),
    [panelTypes, handleMenuItemChosen]
  );

  return (
    <div>
      <Button aria-controls="add-panel-menu" aria-haspopup="true" onClick={handleClick} startIcon={<LibraryAdd />}>
        Add Panel
      </Button>
      <Menu id="add-panel-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        {panels}
      </Menu>
    </div>
  );
}
