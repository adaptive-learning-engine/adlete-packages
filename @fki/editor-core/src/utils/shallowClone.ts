export function shallowClone<T>(obj: T): T {
  return { ...obj };
}
