/* eslint-disable @typescript-eslint/no-explicit-any */
export type PropAsArray<Obj, K extends keyof Obj> = Obj[K] extends any[] ? Obj[K] : never;
export type PropNotAsArray<Obj, K extends keyof Obj> = Obj[K] extends any[] ? never : Obj[K];

export type ArgumentTypes<T> = T extends (...args: infer U) => unknown ? U : never;
export type ConstructorArg1<T> = T extends new (first: infer U) => unknown ? U : undefined;

// type NeverIfUndefined<Obj, K extends keyof Obj> = Obj[K] extends undefined ? boolean : never;
// type NeverIfNotArray<Obj, K extends keyof Obj> = Obj[K] extends any[] ? boolean : never;

// export type TypeAsTuple<T> = [T];
export type TypeAsTupleIfNot<T> = T extends [infer F, ...infer R] ? T : [T];

// export type TypeAsTupleIfNotUndefined<T> = T extends undefined ? undefined : [T];

// export type TypeAsTupleOrNeverIfNotUndefined<T> = T extends undefined ? never : [T];
// export type TypeAsTupleOrUndefinedIfNot<T> = T extends [infer F, ...infer R] ? T : TypeAsTupleIfNotUndefined<T>;

// export type TypeAsTupleOrNeverIfNot<T> = T extends [infer F, ...infer R] ? T : TypeAsTupleOrNeverIfNotUndefined<T>;
export type NeverIfUndefined<T, Else> = T extends undefined ? never : Else;
// export type NeverIfNotUndefined<T> = T extends undefined ? boolean : never;

export type AsTupleExceptUndefined<T> = NeverIfUndefined<T, TypeAsTupleIfNot<T>>;

export type RestTuple<T> = AsTupleExceptUndefined<T>;
