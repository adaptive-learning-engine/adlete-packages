export interface IRegistry<TObjects> {
  add<K extends keyof TObjects>(id: K, obj: TObjects[K]): void;
  has<K extends keyof TObjects>(id: K): boolean;
  get<K extends keyof TObjects>(id: K): TObjects[K];
  remove(id: keyof TObjects): void;
  getIds(): (keyof TObjects)[];
  getAll(): TObjects;
}

export class Registry<TObjects> implements IRegistry<TObjects> {
  protected objects: Partial<TObjects>;

  public constructor() {
    this.objects = {};
  }

  public add<K extends keyof TObjects>(id: K, obj: TObjects[K]): void {
    if (this.objects[id]) throw new Error(`Object with id '${id.toString()}' already exists!`);

    this.objects[id] = obj;
  }

  public get<K extends keyof TObjects>(id: K): TObjects[K] {
    return this.objects[id];
  }

  public has<K extends keyof TObjects>(id: K): boolean {
    return this.objects[id] !== undefined;
  }

  public remove(id: keyof TObjects): void {
    if (!this.objects[id]) throw new Error(`Object with id '${id.toString()}' does not exist!`);

    this.objects[id] = null;
  }

  public getIds(): (keyof TObjects)[] {
    // TODO: what about symbols and integers?
    return Object.keys(this.objects) as (keyof TObjects)[];
  }

  public getAll(): TObjects {
    return { ...this.objects } as TObjects;
  }
}
