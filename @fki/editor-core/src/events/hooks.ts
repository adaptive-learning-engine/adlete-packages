import { useEffect, useState } from 'react';

import { StatefulEventEmitter } from './StatefulEventEmitter';

export function useStatefulEvent<EventData, K extends keyof EventData>(
  emitter: StatefulEventEmitter<EventData>,
  eventName: K
): EventData[K] {
  const initialState = emitter.hasState(eventName) ? emitter.getState(eventName) : undefined;
  const [state, setState] = useState<EventData[K]>(initialState);

  useEffect(() => {
    // forcing current state in case it changed between the call and useEffect
    const currState = emitter.getState(eventName);
    setState(currState);

    // its easier to call getState in the listener, because of how storing single values works
    const listener = () => {
      setState(emitter.getState(eventName));
    };
    emitter.addListener(eventName, listener);

    return () => {
      emitter.removeListener(eventName, listener);
    };
  }, [emitter, eventName]);

  return state;
}
