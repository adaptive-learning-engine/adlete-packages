import { RestTuple } from '../utils/types';

import { EventEmitterOptions, ITypedEventEmitter, ITypedEventEmitterNoEmit, TypedEventEmitter } from './TypedEventEmitter';

export interface IStatefulEventEmitterNoEmit<EventData extends Record<keyof EventData, unknown>>
  extends ITypedEventEmitterNoEmit<EventData> {
  hasState<K extends keyof EventData>(eventName: K): boolean;
  getState<K extends keyof EventData>(eventName: K): EventData[K];
  // getStateAsSingle<K extends keyof Events>(eventName: K): Events[K][0];
  clearState<K extends keyof EventData>(eventName?: K): void;
}

export interface IStatefulEventEmitter<EventData extends Record<keyof EventData, unknown>> extends ITypedEventEmitter<EventData> {
  setState<K extends keyof EventData>(eventName: K, ...args: RestTuple<EventData[K]>): void;
}

export class StatefulEventEmitter<EventData extends Record<keyof EventData, unknown>>
  extends TypedEventEmitter<EventData>
  implements IStatefulEventEmitter<EventData>
{
  protected states: Partial<EventData>;

  // TODO: put initialStates into Options!
  public constructor(initialStates?: Partial<EventData>, options?: EventEmitterOptions) {
    super(options);

    this.states = initialStates !== undefined ? initialStates : {};
  }

  public emit<K extends keyof EventData>(eventName: K, ...args: RestTuple<EventData[K]>): boolean {
    // assuming single argument should be saved directly and not as tuple
    // TODO: as save args directly? --> need a RestTuple type for `states`
    const argsAsArray = args as unknown[];
    if (argsAsArray.length === 1) {
      // eslint-disable-next-line prefer-destructuring
      this.states[eventName] = argsAsArray[0] as EventData[K];
    } else if (argsAsArray.length > 1) {
      this.states[eventName] = args as EventData[K];
    }
    return super.emit(eventName, ...args);
  }

  public hasState<K extends keyof EventData>(eventName: K): boolean {
    return this.states[eventName] != null;
  }

  public getState<K extends keyof EventData>(eventName: K): EventData[K] {
    return this.states[eventName];
  }

  // public getStateAsSingle<K extends keyof Events>(eventName: K): Events[K][0] {
  //   if (this.states[eventName]) return this.states[eventName][0];
  //   return undefined;
  // }

  public setState<K extends keyof EventData>(eventName: K, ...args: RestTuple<EventData[K]>): void {
    this.states[eventName] = args as EventData[K];
  }

  public clearState<K extends keyof EventData>(eventName?: K): void {
    if (eventName === undefined) this.states = {};
    else this.states[eventName] = undefined;
  }
}
