import EventEmitter from 'events';

import { ConstructorArg1, RestTuple } from '../utils/types';

// EventEmitterOptions is not exported from 'events', so we need to infer it
export type EventEmitterOptions = ConstructorArg1<typeof EventEmitter>;

export interface ITypedEventEmitterNoEmit<EventData extends Record<keyof EventData, unknown>> {
  addListener<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  on<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  once<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  removeListener<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  off<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  removeAllListeners<K extends keyof EventData>(eventName: K): this;
  listeners<K extends keyof EventData>(eventName: K): (...args: RestTuple<EventData[K]>) => void[];
  rawListeners<K extends keyof EventData>(eventName: K): (...args: RestTuple<EventData[K]>) => void[];
  listenerCount<K extends keyof EventData>(eventName: K): number;
  prependListener<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  prependOnceListener<K extends keyof EventData>(eventName: K, listener: (...args: RestTuple<EventData[K]>) => void): this;
  eventNames(): (keyof EventData)[];
  setMaxListeners(max: number): void;
}

export interface ITypedEventEmitter<EventData extends Record<keyof EventData, unknown>> extends ITypedEventEmitterNoEmit<EventData> {
  emit<K extends keyof EventData>(eventName: K, ...args: RestTuple<EventData[K]>): boolean;
}

export const TypedEventEmitter: new <EventData extends Record<keyof EventData, unknown>>(
  options?: EventEmitterOptions
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) => ITypedEventEmitter<EventData> = EventEmitter as any;

export const TypedEventEmitterNoEmit: new <EventData extends Record<keyof EventData, unknown>>(
  options?: EventEmitterOptions
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) => ITypedEventEmitterNoEmit<EventData> = EventEmitter as any;

// interface MyEvents {
//   single: string; // 1
//   arr: string[]; // 1
//   tuple: [string, number]; // 2
//   rest: (string | number)[]; // X
//   rest2: [...string[]]; // X
//   empty: [];
//   undef: undefined;
// }

// type Foo = IsTuple<string>;
// type Bar = IsTuple<string[]>;
// type Moo = IsTuple<[string]>;
// type Boo = IsTuple<[string, number, number]>;

// const myEmitter: ITypedEventEmitter<MyEvents> = null;

// function foo(bar: string[]) {}

// myEmitter.addListener('arr', foo);

// myEmitter.emit('single');
// myEmitter.emit('single', 'sdfsdf');
// myEmitter.emit('single', ['sdfsdf']);

// myEmitter.emit('arr', 'sdfsdf');
// myEmitter.emit('arr', ['sdfsdf']);

// myEmitter.emit('tuple');
// myEmitter.emit('tuple', 'sdfsdf');
// myEmitter.emit('tuple', ['sdfsdf']);
// myEmitter.emit('tuple', 'sdfsdf', 34);
// myEmitter.emit('tuple', 'sdfsdf', 34, 34);

// myEmitter.emit('rest', ['sdfsdf']);
// myEmitter.emit('rest', 'sdfsdf');

// myEmitter.emit('rest2', 'sdfsdf', 'sdfsdf');

// myEmitter.emit('empty', 34);
// myEmitter.emit('undef');

// type Test<T> = T extends [infer F, ...infer R] ? T : boolean;
// type Test2<T> = T extends undefined ? number : T extends [infer F, ...infer R] ? T : boolean;

// type X = Test2<undefined>;
// type fooo = Test2<MyEvents['undef']>;
