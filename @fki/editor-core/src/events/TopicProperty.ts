/* eslint-disable no-param-reassign */
import { Observable, Subject } from 'rxjs';

interface ValueSubject<T> extends Subject<T> {
  current: T;
}

function convertToValueSubject<T>(subject: ValueSubject<T>, startValue: T) {
  if (subject.current === undefined) {
    subject.current = startValue;
    const { next } = subject;
    subject.next = function nextWithValue(value?: T) {
      if (value !== undefined) this.current = value;
      next.call(subject, value);
    };
  } else {
    // eslint-disable-next-line no-console
    console.warn('Trying to convert subject to ValueSubject again!');
  }
}

export class TopicProperty<T> {
  public subject$: Subject<T>;

  public observable$: Observable<T>;

  // TODO: make startValue optional
  public constructor(subject$: Subject<T>, startValue: T) {
    convertToValueSubject(subject$ as ValueSubject<T>, startValue);
    this.subject$ = subject$;
    this.observable$ = subject$;
  }

  public next(value: T): void {
    this.subject$.next(value);
  }

  public get current(): T {
    return (this.subject$ as ValueSubject<T>).current;
  }
}
