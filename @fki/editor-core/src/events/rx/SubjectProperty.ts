/* eslint-disable no-underscore-dangle */
/* eslint-disable max-classes-per-file */
import { Observable, Subject } from 'rxjs';

export class SubjectProperty<T> {
  public subject$: Subject<T>;

  public observable$: Observable<T>;

  public constructor(subject$: Subject<T>) {
    this.subject$ = subject$;
    this.observable$ = subject$;
  }

  public next(value: T): void {
    this.subject$.next(value);
  }
}

function injectCallback<T>(subject: Subject<T>, cb: (value: T) => void) {
  const { next } = subject;
  // eslint-disable-next-line no-param-reassign
  subject.next = function nextWithCallback(value?: T) {
    cb(value);
    next.call(subject, value);
  };
}

export class StatefulSubjectProperty<T> extends SubjectProperty<T> {
  public subject$: Subject<T>;

  public observable$: Observable<T>;

  // TODO: allow multiple values
  protected _value: T;

  protected _hasValue = false;

  public constructor(subject$: Subject<T>, initialValue?: T) {
    super(subject$);
    injectCallback(subject$, (value) => {
      this._value = value;
      this._hasValue = true;
    });
    this._value = initialValue;
    if (this._value !== undefined) this._hasValue = true;
  }

  public get value(): T {
    return this._value;
  }

  public get hasValue(): boolean {
    return this._hasValue;
  }
}
