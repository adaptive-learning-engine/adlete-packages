/* eslint-disable max-classes-per-file */
import { Observable, Subject } from 'rxjs';

export interface IRxEventEmitterOptions<Events> {
  errorOnMissing?: boolean;
  initialSubjects?: Partial<Record<keyof Events, Subject<Events[keyof Events]>>>;
  createSubject?(eventName: keyof Events): Subject<Events[keyof Events]>;
}

export interface IRxReadOnlyEventEmitter<Events> {
  getObservable<K extends keyof Events>(eventName: K): Observable<Events[K]>;
}

export interface IRxEventEmitter<Events> extends IRxReadOnlyEventEmitter<Events> {
  getSubject<K extends keyof Events>(eventName: K): Subject<Events[K]>;
  next<K extends keyof Events>(eventName: K, value: Events[K]): void;
}

export class RxEventEmitter<Events> implements IRxEventEmitter<Events> {
  protected subjects: Partial<Record<keyof Events, Subject<Events[keyof Events]>>>;

  protected options: IRxEventEmitterOptions<Events>;

  public constructor(options?: IRxEventEmitterOptions<Events>) {
    this.options = options != null ? options : {};
    this.subjects = options.initialSubjects != null ? options.initialSubjects : {};
  }

  protected tryGetSubject<K extends keyof Events>(eventName: K): Subject<Events[K]> {
    if (this.subjects[eventName]) return this.subjects[eventName] as Subject<Events[K]>;

    if (this.options.createSubject) {
      const subject = this.options.createSubject(eventName) as Subject<Events[K]>;
      this.subjects[eventName] = subject;
      return subject;
    }

    if (this.options.errorOnMissing) throw new Error(`No Subject exists or could be created for event '${eventName.toString()}'!`);

    return null;
  }

  public getSubject<K extends keyof Events>(eventName: K): Subject<Events[K]> {
    return this.tryGetSubject(eventName);
  }

  public getObservable<K extends keyof Events>(eventName: K): Observable<Events[K]> {
    return this.tryGetSubject(eventName) as unknown as Observable<Events[K]>;
  }

  public next<K extends keyof Events>(eventName: K, value: Events[K]): void {
    const subject = this.tryGetSubject(eventName);
    subject.next(value);
  }
}
