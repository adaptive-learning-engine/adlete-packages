import { Observable, Subject } from 'rxjs';

import { IRxEventEmitter, IRxReadOnlyEventEmitter } from './RxEventEmitter';
import { StatefulSubjectProperty } from './SubjectProperty';

export interface IRxStatefulEventEmitterOptions<Events> {
  errorOnMissing?: boolean;
  initialSubjectProperties?: Partial<Record<keyof Events, StatefulSubjectProperty<Events[keyof Events]>>>;
  createSubjectProperty?(eventName: keyof Events): StatefulSubjectProperty<Events[keyof Events]>;
}

export interface IRxReadOnlyStatefulEventEmitter<Events> extends IRxReadOnlyEventEmitter<Events> {
  getValue<K extends keyof Events>(eventName: K): Events[K];
  hasValue<K extends keyof Events>(eventName: K): boolean;
}

export interface IRxStatefulEventEmitter<Events> extends IRxEventEmitter<Events>, IRxReadOnlyStatefulEventEmitter<Events> {}

export class RxStatefulEventEmitter<Events> implements IRxStatefulEventEmitter<Events> {
  protected options: IRxStatefulEventEmitterOptions<Events>;

  protected subjectProps: Partial<Record<keyof Events, StatefulSubjectProperty<Events[keyof Events]>>>;

  public constructor(options?: IRxStatefulEventEmitterOptions<Events>) {
    this.options = options != null ? options : {};
    this.subjectProps = options.initialSubjectProperties != null ? options.initialSubjectProperties : {};
  }

  protected tryGetProperty<K extends keyof Events>(eventName: K): StatefulSubjectProperty<Events[K]> {
    if (this.subjectProps[eventName]) return this.subjectProps[eventName] as StatefulSubjectProperty<Events[K]>;

    if (this.options.createSubjectProperty) {
      const subjectProperty = this.options.createSubjectProperty(eventName) as StatefulSubjectProperty<Events[K]>;
      this.subjectProps[eventName] = subjectProperty;
      return subjectProperty;
    }

    if (this.options.errorOnMissing) throw new Error(`No StatefulSubjectProperty exists or could be created for event '${eventName.toString()}'!`);

    return null;
  }

  public getSubject<K extends keyof Events>(eventName: K): Subject<Events[K]> {
    const subjectProp = this.tryGetProperty(eventName);
    return subjectProp.subject$ as Subject<Events[K]>;
  }

  public getObservable<K extends keyof Events>(eventName: K): Observable<Events[K]> {
    const subjectProp = this.tryGetProperty(eventName);
    return subjectProp.observable$ as Observable<Events[K]>;
  }

  public next<K extends keyof Events>(eventName: K, value: Events[K]): void {
    const subjectProp = this.tryGetProperty(eventName);
    subjectProp.next(value);
  }

  public getValue<K extends keyof Events>(eventName: K): Events[K] {
    const subjectProp = this.tryGetProperty(eventName);
    return subjectProp.value as Events[K];
  }

  public hasValue<K extends keyof Events>(eventName: K): boolean {
    const subjectProp = this.tryGetProperty(eventName);
    return subjectProp.hasValue;
  }
}
