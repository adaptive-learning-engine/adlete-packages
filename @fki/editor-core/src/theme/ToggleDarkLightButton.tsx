import Brightness4Icon from '@mui/icons-material/Brightness4';
import { IconButton } from '@mui/material';
import React, { ReactElement, useCallback } from 'react';

import { ThemeManager } from './ThemeManager';

export interface IToggleDarkLightButtonProps {
  themeManager: ThemeManager;
}

export function ToggleDarkLightButton({ themeManager }: IToggleDarkLightButtonProps): ReactElement {
  const toggleDarkLight = useCallback(() => themeManager.toggleDarkLight(), [themeManager]);

  return (
    <IconButton onClick={toggleDarkLight}>
      <Brightness4Icon />
    </IconButton>
  );
}
