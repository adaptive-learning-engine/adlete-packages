import { createTheme as createMuiTheme, Theme, ThemeOptions } from '@mui/material';
import { blue, deepPurple } from '@mui/material/colors';
import merge from 'lodash.merge';

import { densityOptions } from './density';

// due to current bug in Material-UI V4
// https://github.com/mui-org/material-ui/issues/18442
// workaround doesn't work, when changing fontSize
// thus using implementation info from
// https://github.com/mui-org/material-ui/blob/a1018937b3df5aaa1c6f11bfcafc662a8249f7e2/packages/material-ui/src/styles/createTypography.js
const htmlFontSize = 16;
function createFontSizeOptions(fontSize: number) {
  const coef = fontSize / 14;
  const pxToRem = (size: number) => `${(size / htmlFontSize) * coef}rem`;
  return {
    typography: {
      fontSize,

      body1: {
        fontSize: pxToRem(fontSize),
      },

      body2: {
        fontSize: pxToRem(fontSize),
      },
    },
  };
}

const paletteOptions: ThemeOptions = {
  palette: {
    mode: 'light',
    primary: blue,
    secondary: deepPurple,
  },
};

export function createTheme(customOptions: ThemeOptions, fontSize = 13): Theme {
  const fontSizeOptions = createFontSizeOptions(fontSize);

  // TODO: use deep merge
  const typography = merge({}, fontSizeOptions.typography, customOptions.typography);

  const options = merge({}, paletteOptions, densityOptions, customOptions, { typography });
  return createMuiTheme(options);
}
