// import createSpacing from '@mui/material/styles/createSpacing';

// TODO: createSpacing
import { createSpacing } from '@mui/system';

export const densityOptions = {
  // raw spacing doesn't work with the deepmerging of `createMuiTheme`
  spacing: createSpacing(4),
  props: {
    MuiButton: {
      size: 'small',
    },
    MuiFilledInput: {
      margin: 'dense',
    },
    MuiFormControl: {
      margin: 'dense',
    },
    MuiFormHelperText: {
      margin: 'dense',
    },
    MuiIconButton: {
      size: 'small',
    },
    MuiInputBase: {
      margin: 'dense',
    },
    MuiInputLabel: {
      margin: 'dense',
    },
    MuiListItem: {
      dense: true,
    },
    MuiOutlinedInput: {
      margin: 'dense',
    },
    MuiFab: {
      size: 'small',
    },
    MuiTable: {
      size: 'small',
    },
    MuiTextField: {
      margin: 'dense',
    },
    MuiToolbar: {
      variant: 'dense',
    },
  },
  overrides: {
    MuiExpansionPanel: {
      root: {
        '&$expanded': {
          margin: '0px',
        },
      },
    },
    MuiExpansionPanelSummary: {
      root: {
        minHeight: 'initial',
        '&$expanded': {
          minHeight: 'initial',
        },
      },
      content: {
        margin: '6px 0',
        '&$expanded': {
          margin: '6px 0',
        },
      },
      expandIcon: {
        padding: '0px 6px',
      },
    },
    MuiIconButton: {
      sizeSmall: {
        // Adjust spacing to reach minimal touch target hitbox
        marginLeft: 4,
        marginRight: 4,
        padding: 12,
      },
    },
  },
};
