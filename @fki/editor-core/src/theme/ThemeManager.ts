/* eslint-disable no-underscore-dangle */
import { Theme, ThemeOptions } from '@mui/material';
import merge from 'lodash.merge';

import { StatefulEventEmitter } from '../events/StatefulEventEmitter';
import { shallowClone } from '../utils/shallowClone';

import { createTheme } from './createTheme';

interface INamedTheme {
  name: string;
  themeOptions: ThemeOptions;
}

interface IThemeSaveData {
  version: number;
  userThemes: Record<string, INamedTheme>;
  currTheme: string;
}

const DARK: ThemeOptions = { palette: { mode: 'dark' } };
const LIGHT: ThemeOptions = { palette: { mode: 'light' } };

const DB_CURRENT_VERSION = 2;
const DB_KEY = 'theme-data';
const DB_DEFAULT_DATA: IThemeSaveData = {
  version: DB_CURRENT_VERSION,
  userThemes: {
    custom: { name: 'custom', themeOptions: {} },
  },
  currTheme: 'custom',
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function upgradeDatabase(_loadedData: IThemeSaveData) {
  // currently we're just resetting to defaults
  window.localStorage.setItem(DB_KEY, JSON.stringify(DB_DEFAULT_DATA));
  return DB_DEFAULT_DATA;
}

interface IThemeEventData {
  themeDataChanged: IThemeSaveData;
  themeChanged: Theme;
}

export class ThemeManager {
  protected _events: StatefulEventEmitter<IThemeEventData>;

  public get events(): StatefulEventEmitter<IThemeEventData> {
    return this._events;
  }

  public initialize(): Promise<void> {
    const item = window.localStorage.getItem(DB_KEY);
    let initialThemeData: IThemeSaveData = null;
    if (item) {
      let loadedData = JSON.parse(item) as IThemeSaveData;
      if (loadedData.version !== DB_CURRENT_VERSION) {
        loadedData = upgradeDatabase(loadedData);
      }
      initialThemeData = loadedData;
    } else {
      initialThemeData = DB_DEFAULT_DATA; // TODO: create clone
    }

    const initialTheme = createTheme(initialThemeData.userThemes[initialThemeData.currTheme].themeOptions);

    this._events = new StatefulEventEmitter<IThemeEventData>({ themeDataChanged: initialThemeData, themeChanged: initialTheme });
    this._events.addListener('themeDataChanged', (data) => this.onThemeDataChanged(data));

    return Promise.resolve();
  }

  protected onThemeDataChanged(themeData: IThemeSaveData): void {
    window.localStorage.setItem(DB_KEY, JSON.stringify(themeData));
    const theme = createTheme(themeData.userThemes[themeData.currTheme].themeOptions);
    this._events.emit('themeChanged', theme);
  }

  public toggleDarkLight(): void {
    // TODO: clone currThemeData
    const currThemeData = this._events.getState('themeDataChanged');
    const currTheme = this._events.getState('themeChanged');
    const currThemeOptions = currThemeData.userThemes[currThemeData.currTheme].themeOptions;
    const darkLightOptions = currTheme.palette.mode === 'dark' ? LIGHT : DARK;

    const newOptions = merge({}, currThemeOptions, darkLightOptions);
    const newThemeData = shallowClone(currThemeData);

    // dark and light will be saved in the theme itself
    newThemeData.userThemes[newThemeData.currTheme].themeOptions = newOptions;

    this._events.emit('themeDataChanged', newThemeData);
  }
}
