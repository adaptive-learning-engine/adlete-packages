import { emphasize, makeStyles, Theme } from '@mui/material';
// import merge from 'lodash.merge';

/**
 * Flexlayout Class Tree
 *
 * Optional classes or elements are in parentheses
 *
 * - flexlayout__layout
 *   - flexlayout__tabset
 *     - (flexlayout__tabset_header)
 *       - flexlayout__tab_toolbar
 *         - custom buttons
 *         - flexlayout__tab_toolbar_button-min || flexlayout__tab_toolbar_button-min
 *     - flexlayout__tab_header_outer (flexlayout__tabset-selected)
 *       - flexlayout__tab_header_inner
 *         - flexlayout__tab_button && (flexlayout__tab_button--selected || flexlayout__tab_button--unselected)
 *           - (flexlayout__tab_button_leading) (when iconFactory)
 *           - (flexlayout__tab_button_content) (when not in edit mode)
 *           - (flexlayout__tab_button_textbox) (when in edit mode)
 *           - (flexlayout__tab_button_trailing) (when closable)
 *       - (flexlayout__tab_toolbar) (only if no header, see above)
 *   - flexlayout__tab
 *   - flexlayout__border_left (same for top, right and bottom)
 *     - flexlayout__border_inner_left
 *       - flexlayout__border_button flexlayout__border_button_left && (flexlayout__border_button--selected || flexlayout__border_button--unselected)
 *         - (flexlayout__tab_button_leading) (when iconFactory)
 *         - (flexlayout__tab_button_content) (when not in edit mode)
 *         - (flexlayout__tab_button_textbox) (when in edit mode)
 *         - (flexlayout__tab_button_trailing) (when closable)
 *     - flexlayout__border_toolbar_left
 *       - custom buttons
 *   - flexlayout__splitter
 *   - flexlayout__splitter_drag
 *   - flexlayout__outline_rect
 *   - flexlayout__outline_rect_edge
 *   - flexlayout__edge_rect
 *   - flexlayout__drag_rect
 */

// Converted using
// https://www.sassmeister.com/
// https://transform.tools/css-to-js

// Variables

// .foobar {
//   color_text: $color_text;
//   color_background: $color_background;
//   color_base: $color_base;
//   color_1: $color_1;
//   color_2: $color_2;
//   color_3: $color_3;
//   color_4: $color_4;
//   color_5: $color_5;
//   color_6: $color_6;
//   color_drag1: $color_drag1;
//   color_drag2: $color_drag2;

//   font-size: $font-size;
//   font-family: $font-family;
// }

// $color_text: white !default;
// $color_background: black !default;
// $color_base: black !default;
// $color_1: scale_color($color_base, $lightness: 7%) !default;
// $color_2: scale_color($color_base, $lightness: 10%) !default;
// $color_3: scale_color($color_base, $lightness: 15%) !default;
// $color_4: scale_color($color_base, $lightness: 20%) !default;
// $color_5: scale_color($color_base, $lightness: 25%) !default;
// $color_6: scale_color($color_base, $lightness: 30%) !default;
// $color_drag1: #cfe8ff !default;
// $color_drag2: #b7d1b5 !default;
// $font-size: medium !default;
// $font-family: Roboto, Arial, sans-serif !default;

// color_text: white;
// color_background: black;
// color_base: black;
// color_1: #121212;
// color_2: #1a1a1a;
// color_3: #262626;
// color_4: #333333;
// color_5: #404040;
// color_6: #4d4d4d;
// color_drag1: #cfe8ff;
// color_drag2: #b7d1b5;
// font-size: medium;
// font-family: Roboto, Arial, sans-serif;

interface IDesignVariables {
  colorText: string;
  colorBackground: string;
  colorBase: string;
  color1: string;
  color2: string;
  color3: string;
  color4: string;
  color5: string;
  color6: string;
  colorDrag1: string;
  colorDrag2: string;

  fontSize: string;
  fontFamily: string;

  borderRadiusSmall: string | number;
  borderRadiusBig: string | number;
}

function createDesignVariables(theme: Theme): IDesignVariables {
  const { palette, shape } = theme;

  return {
    colorText: palette.text.primary,
    colorBackground: palette.background.default,
    colorBase: palette.background.default,
    color1: emphasize(palette.background.default, 0.07),
    color2: emphasize(palette.background.default, 0.1),
    color3: emphasize(palette.background.default, 0.15),
    color4: emphasize(palette.background.default, 0.2),
    color5: emphasize(palette.background.default, 0.25),
    color6: emphasize(palette.background.default, 0.3),
    colorDrag1: palette.primary.main,
    colorDrag2: palette.primary.main,

    fontSize: '10px',
    fontFamily: 'Roboto, Arial, sans-serif',

    borderRadiusSmall: shape.borderRadius,
    borderRadiusBig: shape.borderRadius,
  };
}

function createStyles(theme: Theme) {
  const design = createDesignVariables(theme);
  return {
    root: {
      '& .flexlayout__layout': {
        left: '0',
        top: '0',
        right: '0',
        bottom: '0',
        position: 'absolute',
        overflow: 'hidden',
      },
      '& .flexlayout__splitter': { backgroundColor: design.color2 },
      '@media (hover: hover)': [
        { '& .flexlayout__splitter:hover': { backgroundColor: design.color4 } },
        {
          '& .flexlayout__tab_button:hover': {
            backgroundColor: design.color3,
            color: design.colorText,
          },
        },
        {
          '& .flexlayout__tab_button:hover .flexlayout__tab_button_trailing': {
            background: 'transparent url("../images/close.png") no-repeat center',
          },
        },
        {
          '& .flexlayout__border_button:hover': {
            backgroundColor: design.color3,
            color: design.colorText,
          },
        },
        {
          '& .flexlayout__border_button:hover .flexlayout__border_button_trailing': {
            background: 'transparent url("../images/close.png") no-repeat center',
          },
        },
        { '& .flexlayout__popup_menu_item:hover': { backgroundColor: design.color6 } },
      ],
      '& .flexlayout__splitter_border': { zIndex: 10 },
      '& .flexlayout__splitter_drag': {
        zIndex: 1000,
        borderRadius: design.borderRadiusBig,
        backgroundColor: design.color5,
      },
      '& .flexlayout__outline_rect': {
        position: 'absolute',
        cursor: 'move',
        boxSizing: 'border-box',
        border: `2px solid ${design.colorDrag1}`,
        boxShadow: 'inset 0 0 60px rgba(0, 0, 0, 0.2)',
        borderRadius: design.borderRadiusBig,
        zIndex: 1000,
      },
      '& .flexlayout__outline_rect_edge': {
        cursor: 'move',
        border: `2px solid ${design.colorDrag2}`,
        boxShadow: 'inset 0 0 60px rgba(0, 0, 0, 0.2)',
        borderRadius: design.borderRadiusBig,
        zIndex: 1000,
        boxSizing: 'border-box',
      },
      '& .flexlayout__edge_rect': {
        position: 'absolute',
        zIndex: 1000,
        boxShadow: 'inset 0 0 5px rgba(0, 0, 0, 0.2)',
        backgroundColor: 'gray',
      },
      '& .flexlayout__drag_rect': {
        position: 'absolute',
        cursor: 'move',
        color: design.colorText,
        backgroundColor: design.color1,
        border: `2px solid ${design.color4}`,
        boxShadow: 'inset 0 0 60px rgba(0, 0, 0, 0.3)',
        borderRadius: design.borderRadiusBig,
        zIndex: 1000,
        boxSizing: 'border-box',
        opacity: 0.9,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        overflow: 'hidden',
        padding: '10px',
        wordWrap: 'break-word',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
      '& .flexlayout__tabset': {
        overflow: 'hidden',
        backgroundColor: [design.color1, design.color1],
        boxSizing: 'border-box',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
      '& .flexlayout__tabset_header': {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        left: '0',
        right: '0',
        padding: '3px 3px 3px 5px',
        boxSizing: 'border-box',
        borderBottom: `1px solid ${design.color3}`,
        color: design.colorText,
        backgroundColor: design.color1,
        boxShadow: 'inset 0 0 3px 0 rgba(136, 136, 136, 0.54)',
      },
      '& .flexlayout__tabset_header_content': { flexGrow: 1 },
      '& .flexlayout__tabset_tabbar_outer': {
        boxSizing: 'border-box',
        backgroundColor: [design.color1, design.color1],
        position: 'absolute',
        left: '0',
        right: '0',
        overflow: 'hidden',
        display: 'flex',
      },
      '& .flexlayout__tabset_tabbar_outer_top': { borderBottom: `1px solid ${design.color3}` },
      '& .flexlayout__tabset_tabbar_outer_bottom': { borderTop: `1px solid ${design.color3}` },
      '& .flexlayout__tabset_tabbar_inner': {
        position: 'relative',
        boxSizing: 'border-box',
        display: 'flex',
        flexGrow: 1,
        overflow: 'hidden',
      },
      '& .flexlayout__tabset_tabbar_inner_tab_container': {
        display: 'flex',
        boxSizing: 'border-box',
        position: 'absolute',
        top: '0',
        bottom: '0',
        width: '10000px',
      },
      '& .flexlayout__tabset_tabbar_inner_tab_container_top': {
        borderTop: '2px solid transparent',
      },
      '& .flexlayout__tabset_tabbar_inner_tab_container_bottom': {
        borderBottom: '2px solid transparent',
      },
      '& .flexlayout__tabset-selected': {
        backgroundImage: `linear-gradient(${design.colorBackground}, ${design.color4})`,
      },
      '& .flexlayout__tabset-maximized': {
        backgroundImage: `linear-gradient(${design.color6}, ${design.color2})`,
      },
      '& .flexlayout__tab': {
        overflow: 'auto',
        position: 'absolute',
        boxSizing: 'border-box',
        color: design.colorText,
        backgroundColor: design.colorBackground,
      },
      '& .flexlayout__tab_button': {
        display: 'inline-flex',
        alignItems: 'center',
        boxSizing: 'border-box',
        padding: '3px 8px',
        margin: '0px 2px',
        cursor: 'pointer',
      },
      '& .flexlayout__tab_button--selected': {
        backgroundColor: design.color3,
        color: design.colorText,
      },
      '& .flexlayout__tab_button--unselected': { color: 'gray' },
      '& .flexlayout__tab_button_top': {
        boxShadow: 'inset -2px 0px 5px rgba(0, 0, 0, 0.1)',
        borderTopLeftRadius: '3px',
        borderTopRightRadius: '3px',
      },
      '& .flexlayout__tab_button_bottom': {
        boxShadow: 'inset -2px 0px 5px rgba(0, 0, 0, 0.1)',
        borderBottomLeftRadius: '3px',
        borderBottomRightRadius: '3px',
      },
      '& .flexlayout__tab_button_leading': { display: 'inline-block' },
      '& .flexlayout__tab_button_content': { display: 'inline-block' },
      '& .flexlayout__tab_button_textbox': {
        border: 'none',
        color: 'green',
        backgroundColor: design.color3,
      },
      '& .flexlayout__tab_button_textbox:focus': { outline: 'none' },
      '& .flexlayout__tab_button_trailing': {
        display: 'inline-block',
        marginLeft: '8px',
        minWidth: '8px',
        minHeight: '8px',
      },
      '@media (pointer: coarse)': [
        {
          '& .flexlayout__tab_button_trailing': {
            minWidth: '20px',
            minHeight: '20px',
          },
        },
        {
          '& .flexlayout__border_button_trailing': {
            minWidth: '20px',
            minHeight: '20px',
          },
        },
      ],
      '& .flexlayout__tab_button--selected .flexlayout__tab_button_trailing': {
        background: 'transparent url("../images/close.png") no-repeat center',
      },
      '& .flexlayout__tab_button_overflow': {
        marginLeft: '10px',
        paddingLeft: '12px',
        border: 'none',
        color: 'gray',
        fontSize: 'inherit',
        background: 'transparent url("../images/more2.png") no-repeat left',
      },
      '& .flexlayout__tab_toolbar': { display: 'flex', alignItems: 'center' },
      '& .flexlayout__tab_toolbar_button': {
        minWidth: '20px',
        minHeight: '20px',
        border: 'none',
        outline: 'none',
      },
      '& .flexlayout__tab_toolbar_button-min': {
        background: 'transparent url("../images/maximize.png") no-repeat center',
      },
      '& .flexlayout__tab_toolbar_button-max': {
        background: 'transparent url("../images/restore.png") no-repeat center',
      },
      '& .flexlayout__tab_toolbar_button-float': {
        background: 'transparent url("../images/popout.png") no-repeat center',
      },
      '& .flexlayout__tab_floating': {
        overflow: 'auto',
        position: 'absolute',
        boxSizing: 'border-box',
        color: design.colorText,
        backgroundColor: design.colorBackground,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      '& .flexlayout__tab_floating_inner': {
        overflow: 'auto',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
      '& .flexlayout__tab_floating_inner div': {
        marginBottom: '5px',
        textAlign: 'center',
      },
      '& .flexlayout__tab_floating_inner div a': { color: 'royalblue' },
      '& .flexlayout__border': {
        boxSizing: 'border-box',
        overflow: 'hidden',
        display: 'flex',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
        backgroundColor: design.color2,
      },
      '& .flexlayout__border_top': {
        borderBottom: `1px solid ${design.color3}`,
        alignItems: 'center',
      },
      '& .flexlayout__border_bottom': {
        borderTop: `1px solid ${design.color3}`,
        alignItems: 'center',
      },
      '& .flexlayout__border_left': {
        borderRight: `1px solid ${design.color3}`,
        alignContent: 'center',
        flexDirection: 'column',
      },
      '& .flexlayout__border_right': {
        borderLeft: `1px solid ${design.color3}`,
        alignContent: 'center',
        flexDirection: 'column',
      },
      '& .flexlayout__border_inner': {
        position: 'relative',
        boxSizing: 'border-box',
        display: 'flex',
        overflow: 'hidden',
        flexGrow: 1,
      },
      '& .flexlayout__border_inner_tab_container': {
        whiteSpace: 'nowrap',
        display: 'flex',
        boxSizing: 'border-box',
        position: 'absolute',
        top: '0',
        bottom: '0',
        width: '10000px',
      },
      '& .flexlayout__border_inner_tab_container_right': {
        transformOrigin: 'top left',
        transform: 'rotate(90deg)',
      },
      '& .flexlayout__border_inner_tab_container_left': {
        flexDirection: 'row-reverse',
        transformOrigin: 'top right',
        transform: 'rotate(-90deg)',
      },
      '& .flexlayout__border_button': {
        display: 'flex',
        alignItems: 'center',
        cursor: 'pointer',
        padding: '3px 8px',
        margin: '2px',
        boxSizing: 'border-box',
        whiteSpace: 'nowrap',
        boxShadow: 'inset 0 0 5px rgba(0, 0, 0, 0.15)',
        borderRadius: design.borderRadiusSmall,
      },
      '& .flexlayout__border_button--selected': {
        backgroundColor: design.color3,
        color: design.colorText,
      },
      '& .flexlayout__border_button--unselected': { color: 'gray' },
      '& .flexlayout__border_button_leading': { display: 'inline' },
      '& .flexlayout__border_button_content': { display: 'inline-block' },
      '& .flexlayout__border_button_trailing': {
        display: 'inline-block',
        marginLeft: '8px',
        minWidth: '8px',
        minHeight: '8px',
      },
      '& .flexlayout__border_button--selected .flexlayout__border_button_trailing': {
        background: 'transparent url("../images/close.png") no-repeat center',
      },
      '& .flexlayout__border_toolbar': { display: 'flex', alignItems: 'center' },
      '& .flexlayout__border_toolbar_left': { flexDirection: 'column' },
      '& .flexlayout__border_toolbar_right': { flexDirection: 'column' },
      '& .flexlayout__border_toolbar_button': {
        minWidth: '20px',
        minHeight: '20px',
        border: 'none',
        outline: 'none',
      },
      '& .flexlayout__border_toolbar_button-float': {
        background: 'transparent url("../images/popout.png") no-repeat center',
      },
      '& .flexlayout__border_toolbar_button_overflow': {
        border: 'none',
        paddingLeft: '12px',
        color: 'gray',
        fontSize: 'inherit',
        background: 'transparent url("../images/more2.png") no-repeat left',
      },
      '& .flexlayout__border_toolbar_button_overflow_top, .flexlayout__border_toolbar_button_overflow_bottom': {
        marginLeft: '10px',
      },
      '& .flexlayout__border_toolbar_button_overflow_right, .flexlayout__border_toolbar_button_overflow_left': {
        paddingRight: '0px',
        marginTop: '5px',
      },
      '& .flexlayout__popup_menu': {
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
      '& .flexlayout__popup_menu_item': {
        padding: '2px 10px 2px 10px',
        whiteSpace: 'nowrap',
      },
      '& .flexlayout__popup_menu_container': {
        boxShadow: 'inset 0 0 5px rgba(0, 0, 0, 0.15)',
        border: `1px solid ${design.color6}`,
        color: design.colorText,
        background: design.colorBackground,
        borderRadius: design.borderRadiusSmall,
        position: 'absolute',
        zIndex: 1000,
        maxHeight: '50%',
        minWidth: '100px',
        overflow: 'auto',
      },
      '& .flexlayout__floating_window _body': { height: '100%' },
      '& .flexlayout__floating_window_content': {
        left: '0',
        top: '0',
        right: '0',
        bottom: '0',
        position: 'absolute',
      },
      '& .flexlayout__floating_window_tab': {
        overflow: 'auto',
        left: '0',
        top: '0',
        right: '0',
        bottom: '0',
        position: 'absolute',
        boxSizing: 'border-box',
        backgroundColor: design.colorBackground,
        color: design.colorText,
      },
      '& .flexlayout__error_boundary_container': {
        left: '0',
        top: '0',
        right: '0',
        bottom: '0',
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
      },
      '& .flexlayout__error_boundary_content': {
        display: 'flex',
        alignItems: 'center',
      },
      '& .flexlayout__tabset_sizer': {
        paddingTop: '5px',
        paddingBottom: '3px',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
      '& .flexlayout__tabset_header_sizer': {
        paddingTop: '3px',
        paddingBottom: '3px',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
      '& .flexlayout__border_sizer': {
        paddingTop: '6px',
        paddingBottom: '5px',
        fontSize: design.fontSize,
        fontFamily: design.fontFamily,
      },
    },
  };
}

export const useLayoutStyles = makeStyles(createStyles);
