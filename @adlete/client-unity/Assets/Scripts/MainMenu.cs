﻿using System;
using UnityEngine;
using UnityEngine.UI;

using Adlete;
using Newtonsoft.Json;

public class MainMenu : MonoBehaviour
{
    public GameObject panelRequests;
    public GameObject panelWaitingForRequests;

    public InputField learnerIdInput;
    public InputField outputField;


    private ModuleConnection moduleConnection;

    private void Awake()
    {
        moduleConnection = GameObject.FindObjectOfType<ModuleConnection>();
    }

    public void OnGetStatus()
    {
        this.BeforeFireRequest();

        moduleConnection.CheckStatus((StatusInfo result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
            {
                outputField.text = error;

                this.AfterFiredRequest();
            });
    }

    public void OnGetServiceConfiguration()
    {
        this.BeforeFireRequest();

        moduleConnection.FetchServiceConfiguration((ServiceConfiguration result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();
        });
    }

    public void OnLearnerQuery()
    {
        this.BeforeFireRequest();

        moduleConnection.FetchLearner(learnerIdInput.text, (Learner result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();
        });
    }

    public void OnLearnerAnalyticsQuery()
    {
        this.BeforeFireRequest();

        moduleConnection.LearnerAnalytics(learnerIdInput.text, (LearnerAnalyticsData result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();
        });
    }

    public void OnCreateLearner()
    {
        this.BeforeFireRequest();

        moduleConnection.CreateLearner(learnerIdInput.text, "level_beginner", (Learner result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();
        });
    }

    public void OnDeleteLearner()
    {
        this.BeforeFireRequest();

        moduleConnection.DeleteLearner(learnerIdInput.text, (StatusInfo result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();
        });
    }

    public void OnStartSession()
    {
        this.BeforeFireRequest();

        moduleConnection.StartSession(learnerIdInput.text, (Session result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    public void OnFetchActiveSession()
    {
        this.BeforeFireRequest();

        moduleConnection.FetchActiveSession(learnerIdInput.text, (Session result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    public void OnEndSession()
    {
        this.BeforeFireRequest();

        moduleConnection.StopSession(learnerIdInput.text, (StatusInfo result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    public void OnFetchRecommendation()
    {
        this.BeforeFireRequest();

        moduleConnection.FetchNextActivityRecommendation(learnerIdInput.text, (Recommendation result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    public void OnSubmitSuccess()
    {
        this.BeforeFireRequest();

        Observation observation = new Observation();
        string learnerId = learnerIdInput.text;
        observation.activityName = "activityAddition";
        observation.activityCorrectness = 1.0f;
        observation.activityDifficulty = 0.5f;
        observation.timestamp = DateTime.Now;

        moduleConnection.SubmitActivityResult(learnerId, observation, (StatusInfo result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    public void OnSubmitFailure()
    {
        this.BeforeFireRequest();

        Observation observation = new Observation();
        string learnerId = learnerIdInput.text;
        observation.activityName = "activityAddition";
        observation.activityCorrectness = 0.0f;
        observation.activityDifficulty = 0.5f;
        observation.timestamp = DateTime.Now;

        moduleConnection.SubmitActivityResult(learnerId, observation, (StatusInfo result) =>
        {
            outputField.text = JsonConvert.SerializeObject(result);

            this.AfterFiredRequest();

        }, (string error) =>
        {
            outputField.text = error;

            this.AfterFiredRequest();

        });
    }

    private void BeforeFireRequest()
    {
        this.panelRequests.SetActive(false);
        this.panelWaitingForRequests.SetActive(true);
    }

    private void AfterFiredRequest()
    {
        this.panelRequests.SetActive(true);
        this.panelWaitingForRequests.SetActive(false);
    }
}
