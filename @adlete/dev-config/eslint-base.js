const rulesBase = require("./eslint-rules").base;

module.exports = {
  plugins: ["@typescript-eslint"],

  extends: [
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended",
    "prettier",
  ],
  rules: rulesBase,
};
