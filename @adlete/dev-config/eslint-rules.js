module.exports.base = {
  "class-methods-use-this": "off",
  "import/prefer-default-export": "off",
  "import/order": [
    "error",
    {
      groups: ["builtin", "external", "internal", "parent", "sibling", "index"],
      pathGroups: [
        {
          pattern: "@adlete/**",
          group: "internal",
          position: "before",
        },
      ],
      pathGroupsExcludedImportTypes: [],
      "newlines-between": "always",
      alphabetize: {
        order: "asc",
      },
    },
  ],
  "import/no-default-export": "error",
  "import/no-unresolved": "off",
  "max-classes-per-file": "off",
  "no-param-reassign": "off",
  "no-plusplus": "off",
  "no-underscore-dangle": "off",
  "prefer-destructuring": "off",
  "@typescript-eslint/no-use-before-define": "off",
  "sort-imports": [
    "error",
    {
      allowSeparatedGroups: true,
      ignoreCase: true,
      ignoreDeclarationSort: true,
      ignoreMemberSort: false,
      memberSyntaxSortOrder: ["none", "all", "multiple", "single"],
    },
  ],
  "@typescript-eslint/no-shadow": [
    "warn",
    {
      ignoreTypeValueShadow: true,
      ignoreFunctionTypeParameterNameValueShadow: true,
    },
  ],
};

module.exports.react = {
  "react/destructuring-assignment": "off",
  "react/sort-comp": "off",
  "react-hooks/rules-of-hooks": "error", // Checks rules of Hooks
  "react-hooks/exhaustive-deps": "warn", // Checks effect dependencies
};
