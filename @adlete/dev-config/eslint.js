const rulesBase = require("./eslint-rules").base;
const rulesReact = require("./eslint-rules").react;

module.exports = {
  plugins: ["@typescript-eslint", "react-hooks"],
  parser: "@typescript-eslint/parser",
  extends: [
    "eslint:recommended",
    "plugin:import/recommended",
    "plugin:import/typescript",
    "plugin:jsx-a11y/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "prettier",
  ],
  ignorePatterns: ["**/*.cjs", "**/*.js"],
  rules: Object.assign(rulesBase, rulesReact),
  settings: {
    "import/resolver": {
      typescript: true,
      node: true
    }
  }
};
