import { IRecommendationFilter } from '@adlete/engine-blocks/recommendation/IRecommendationFilter';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';

export interface IActivityRecommendationFilter extends IRecommendationFilter {
  statistics: Record<string, IAccumulatedActivityStatistics>;
  totalActivitiesCompleted: number;
  adleteConfig: IAdleteConfiguration;

  activitySubset?: string[];
}
