export class ActivityInputError extends Error {
  __proto__ = Error;
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, ActivityInputError.prototype);
  }
}
