import { IAdjusterFactory } from '@adlete/engine-blocks/adjusters/IAdjusterFactory';
import { IScalarEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { Adjuster, IAdjusterParams } from '@adlete/framework/Adjuster';
import { IObservationAdjusterParam, ObservationAdjuster } from '@adlete/framework/ObservationAdjuster';
import { IRecommenderAdjusterParams, RecommenderAdjuster } from '@adlete/framework/RecommenderAdjuster';

export class AdjusterFactory implements IAdjusterFactory<IScalarEvidenceBundle, IActivityRecommendation<number>> {
  createAdjuster(param: IAdjusterParams): Adjuster {
    return new Adjuster(param);
  }
  createRecommendationAdjuster(param: IRecommenderAdjusterParams): RecommenderAdjuster {
    return new RecommenderAdjuster(param);
  }
  createObservationAdjuster(param: IObservationAdjusterParam): ObservationAdjuster {
    return new ObservationAdjuster(param);
  }
}
