import { IBaseAdjusterParam } from '@adlete/engine-blocks/adjusters/IAdjusterFactory';
import { IAdleteConfiguration } from './IAdleteConfiguration';
import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { IRecommendationAdjuster } from '@adlete/engine-blocks/adjusters/IRecommendationAdjuster';
import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { ISerializableAdjuster } from './ISerializableAdjuster';
import { utilityValueLens } from '@adlete/engine-blocks/utilityAI/Utility';
import cloneDeep from 'lodash.clonedeep';
import { IActivityRecommendationFilter } from '@adlete/framework/IActivityRecommendationFilter';
import { ActivitiesChooser } from '@adlete/framework/ActivitiesChooser';
import { DifficultyGenerator } from '@adlete/framework/DifficultyGenerator';

export interface IRecommenderAdjusterParams extends IBaseAdjusterParam {
  config: IAdleteConfiguration;
  scalarBeliefs: IScalarBeliefBundle;
  tendencies: IScalarTendencyBundle;
}
export class RecommenderAdjuster implements IRecommendationAdjuster<IActivityRecommendation<number>>, ISerializableAdjuster {
  scalarBeliefBundle: IScalarBeliefBundle;

  protected config: IAdleteConfiguration;
  protected activitiesChooser: ActivitiesChooser;
  protected difficultyGenerator: DifficultyGenerator;
  protected m_tendencies: IScalarTendencyBundle;

  public set tendencies(tendencies: IScalarTendencyBundle) {
    this.m_tendencies = cloneDeep(tendencies);
  }

  public get tendencies(): IScalarTendencyBundle {
    return this.m_tendencies;
  }
  constructor(adjusterParams: IRecommenderAdjusterParams) {
    this.config = adjusterParams.config;
    this.m_tendencies = adjusterParams.tendencies;
    this.scalarBeliefBundle = adjusterParams.scalarBeliefs;
    this.activitiesChooser = new ActivitiesChooser(this.config.activityNames);
    this.difficultyGenerator = new DifficultyGenerator(this.config.activityNames);
  }

  serializeBeliefs(): { probabilisticBeliefs: string; scalarBeliefs: string; tendencies: string } {
    throw new Error('Method not implemented.');
  }

  recommend(filter?: IActivityRecommendationFilter): IActivityRecommendation<number> {
    const activityName = this.activitiesChooser.getSortedActivities({
      beliefModel: this.scalarBeliefBundle,
      tendencies: this.m_tendencies,
      statistics: filter.statistics,
      totalActivitiesCompleted: filter.totalActivitiesCompleted,
      adleteConfig: filter.adleteConfig,
      activitySubset: filter.activitySubset,
    })[0].action as string;

    // recommend the difficulty of the recommended activity
    const difficulty = utilityValueLens.get(
      this.difficultyGenerator.getDifficulty(activityName, {
        beliefModel: this.scalarBeliefBundle,
        tendencies: this.m_tendencies,
        statistics: filter.statistics,
        totalActivitiesCompleted: filter.totalActivitiesCompleted,
        adleteConfig: filter.adleteConfig,
        activitySubset: filter.activitySubset,
      })
    );

    // forward the trend of the recommended activity to allow the external learning environment make better task selection decisions
    let trend = 0;
    if (this.config.activityObservationWeights[activityName] !== null) {
      const activityObservationWeightsNodeNames = Object.keys(this.config.activityObservationWeights[activityName]);

      if (activityObservationWeightsNodeNames.length > 0) {
        Object.keys(this.config.activityObservationWeights[activityName]).forEach((nodeName) => {
          trend = trend + this.m_tendencies[nodeName].tendency;
        });

        trend = trend / activityObservationWeightsNodeNames.length;
      }
    }

    return {
      activityName,
      difficulty,
      trend,
    };
  }
}
