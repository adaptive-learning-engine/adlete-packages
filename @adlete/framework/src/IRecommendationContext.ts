import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';

/**
 * This interface is used to create objects which holds the context for the utility ai activity scoring.
 * Beware that the member @currActivityName is a placeholder and will be filled within the scoring functions.
 */
export interface IRecommendationContext {
  beliefModel: IScalarBeliefBundle;
  tendencies: IScalarTendencyBundle;
  statistics: Record<ActivityName, IAccumulatedActivityStatistics>;
  totalActivitiesCompleted: number;
  adleteConfig: IAdleteConfiguration;

  activityName?: ActivityName;
  activitySubset?: string[];
}
