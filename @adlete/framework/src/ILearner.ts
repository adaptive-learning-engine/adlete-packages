import { IBeliefBundle, IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendency, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';

export interface ILearner {
  learnerId: string;
  overAllFinishedActivities?: number;
  probabilisticBeliefs: IBeliefBundle<IProbabilityBelief>[];
  scalarBeliefs: IBeliefBundle<IScalarBelief>[];
  tendencies: ITendencyBundle<IScalarTendency>[];
  createdAt?: Date;
  updatedAt?: Date;
}
