import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IActivityObservationWeights } from '@adlete/engine-blocks/observation/IActivityObservationWeights';
import { SimulatedLearnerBehaviorsType } from '@adlete/framework/ISimulatedLearnerBehavior';

export type RecommendationTargets =
  | ActivityName[]
  | {
      baseDifficultyOn: ActivityName[];
      baseSelectionOn: Record<ActivityName, number>;
    };

export interface IAdleteConfiguration {
  competenceModel: IBayesNet;

  globalWeights: {
    generalEvidenceWeight: number;
    tendencyPositiveWeight: number;
    tendencyNegativeWeight: number;
    recommendationCompetenceWeaknessScoreWeight: number;
    recommendationActivityRepetitionScoreWeight: number;
    recommendationActivityCorrectIncorrectRatioScore: number;
    recommendationDifficultyFlowModifier: number;
    recommendationDifficultyConstantAddition: number;
  };
  activityNames: ActivityName[];

  activityObservationWeights: Record<ActivityName, IActivityObservationWeights>;
  activityRecommenderTargets: Record<ActivityName, RecommendationTargets>;
  initialScalarBeliefs: Record<string, IScalarBeliefBundle>;

  simulatedLearnerBehaviorTypes: SimulatedLearnerBehaviorsType[];
}

export interface IAdleteJSONConfiguration {
  competenceModel: string;
  globalWeights: string;
  activityNames: string;
  activityObservationWeights: string;
  activityRecommenderTargets: string;
  initialScalarBeliefs: string;
  simulatedLearnerBehaviorTypes: string;
}
