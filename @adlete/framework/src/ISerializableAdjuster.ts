export interface ISerializableAdjuster {
  serializeBeliefs(): { probabilisticBeliefs: string; scalarBeliefs: string; tendencies: string };
}
