import cloneDeep from 'lodash.clonedeep';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { createWeightedSumScorer } from '@adlete/engine-blocks/utilityAI/Combiners';
import { Scorer } from '@adlete/engine-blocks/utilityAI/Scoring';
import { applyContextModifier } from '@adlete/engine-blocks/utilityAI/ScoringModifiers';
import { IWeightedUtility, Utility } from '@adlete/engine-blocks/utilityAI/Utility';
import { IActionUtilityPair, sortActionsByValue } from '@adlete/engine-blocks/utilityAI/Evaluation';

import { ActivityInputError } from '@adlete/framework/errors/ActivityInputError';
import { IRecommendationContext } from '@adlete/framework/IRecommendationContext';

export class ActivitiesChooser {
  activities: ActivityName[];

  // data and long-living contexts
  activityScores: Record<string, Utility> = {};

  // scorers
  activityScorers: Record<string, Scorer<IRecommendationContext>> = {};

  constructor(activities: ActivityName[]) {
    this.activities = cloneDeep(activities);

    // defines the score-functions which will be used to calculated the score for a single activities type
    const activityScorer = createWeightedSumScorer<IRecommendationContext>([
      this.calcCompetenceWeaknessScore,
      this.calcActivityRepetitionScore,
      this.calcActivityCorrectIncorrectRatioScore,
      this.calcThresholdScore,
    ]);

    // assigning this score-functions to all existing activities types (defined in adleteConfig)
    activities.forEach((activityName) => {
      // the scorers need to know the activity name, thus we're modifying the context accordingly
      this.activityScorers[activityName] = applyContextModifier(activityScorer, (context) => {
        context.activityName = activityName;
      });
    });
  }

  public getSortedActivities(activityContext: IRecommendationContext): IActionUtilityPair[] {
    let tmpActivityScorer: Record<string, Scorer<IRecommendationContext>>;

    // if a client can not address all possible activities it can forward a subset list
    // of activities. Only one of the given activities should be recommended.
    if (activityContext.activitySubset) {
      tmpActivityScorer = {};
      activityContext.activitySubset.forEach((entry) => {
        tmpActivityScorer[entry] = this.activityScorers[entry];
        if (!this.activityScorers[entry]) {
          throw new ActivityInputError(`Could not match the activity ${entry}`);
        }
      });
    } else {
      tmpActivityScorer = this.activityScorers;
    }
    const sorted = sortActionsByValue(tmpActivityScorer, activityContext);
    return sorted;
  }

  private calcCompetenceWeaknessScore(context: IRecommendationContext): IWeightedUtility {
    let weakestCompetenceValue = Number.POSITIVE_INFINITY;
    let boldestCompetenceValue = Number.NEGATIVE_INFINITY;

    Object.keys(context.beliefModel).forEach((competenceName: string) => {
      const currentValue = context.beliefModel[competenceName].value;
      weakestCompetenceValue = currentValue < weakestCompetenceValue ? currentValue : weakestCompetenceValue;
      boldestCompetenceValue = currentValue > boldestCompetenceValue ? currentValue : boldestCompetenceValue;
    });

    const activityObservationWeight = context.adleteConfig.activityObservationWeights[context.activityName];
    const competencesWhichArePracticedByAnActivity = Object.keys(activityObservationWeight);
    let mappingFactor = 0;
    competencesWhichArePracticedByAnActivity.forEach((competenceName) => {
      if (context.beliefModel[competenceName] === undefined) {
        throw new Error(`ObservationWeight exists for a unknown competence:${competenceName}`);
      }

      const invertedCompetenceValue = boldestCompetenceValue - (context.beliefModel[competenceName].value - weakestCompetenceValue);

      if (boldestCompetenceValue - weakestCompetenceValue !== 0) {
        mappingFactor += 2 * ((invertedCompetenceValue - weakestCompetenceValue) / (boldestCompetenceValue - weakestCompetenceValue)) - 1;
      } else {
        mappingFactor += 2 * (invertedCompetenceValue - 1);
      }
    });

    mappingFactor /= competencesWhichArePracticedByAnActivity.length;

    return {
      value: mappingFactor,
      weight: context.adleteConfig.globalWeights.recommendationCompetenceWeaknessScoreWeight,
    };
  }

  private calcActivityRepetitionScore(context: IRecommendationContext): IWeightedUtility {
    const statistics = context.statistics[context.activityName];

    let activityPlayedRatio = 0;
    if (statistics !== undefined && statistics !== null && context.totalActivitiesCompleted > 0) {
      activityPlayedRatio = statistics.activitiesPlayed / context.totalActivitiesCompleted;
    }

    const invertedActivityPlayedRatio = 1 - activityPlayedRatio;
    const mappingFactor = 2 * invertedActivityPlayedRatio - 1; // mapping from the range 0 to 1 to the range -1 to 1
    return {
      value: mappingFactor,
      weight: context.adleteConfig.globalWeights.recommendationActivityRepetitionScoreWeight,
    };
  }

  /**
   * Calculates the Right-Wrong-Ratio-Score, which is the right wrong ratio of a specific activity type smoothed to a linear scale between -0.2 and 0.2.
   * The score became negative as soon as frustration factor (wrong ratio is higher than 65%) or the boredom factor (correct ratio is
   * higher than 65%).
   * @param activityType
   * @param state
   */
  private calcActivityCorrectIncorrectRatioScore(context: IRecommendationContext): IWeightedUtility {
    const statistics = context.statistics[context.activityName];

    let negativeResultRatio = 0;
    let positiveResultRatio = 0;
    if (statistics !== undefined && statistics !== null && statistics.activitiesPlayed > 0) {
      negativeResultRatio = 1 - statistics.avgAnswersCorrectness;
      positiveResultRatio = statistics.avgAnswersCorrectness;
    }

    let invertedHighestResultRatio = 0;
    if (negativeResultRatio > positiveResultRatio) {
      invertedHighestResultRatio = 1 - negativeResultRatio;
    } else {
      invertedHighestResultRatio = 1 - positiveResultRatio;
    }

    return {
      value: 2 * invertedHighestResultRatio - 1, // map from range 0 - 1 to range -1 - +1
      weight: context.adleteConfig.globalWeights.recommendationActivityCorrectIncorrectRatioScore,
    };
  }

  private calcThresholdScore(context: IRecommendationContext): IWeightedUtility {
    if (context.activityName == null) {
      throw new Error('Could not find activity Name to calculate weight');
    }

    const recommendationTarget = context.adleteConfig.activityRecommenderTargets[context.activityName];
    let weightedUtility: IWeightedUtility = {
      value: 0,
      weight: 0,
    };

    if (Array.isArray(recommendationTarget) || recommendationTarget.baseSelectionOn === null) {
      return weightedUtility;
    }

    for (let [key, value] of Object.entries(recommendationTarget.baseSelectionOn)) {
      let belief = context.beliefModel[key];
      if (belief.value < value) {
        weightedUtility = {
          value: -100,
          weight: 1,
        };
        break;
      }
    }

    return weightedUtility;
  }
}
