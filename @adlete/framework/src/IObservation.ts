import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IActivityObservation } from '@adlete/engine-blocks/observation/IObservation';

export interface IObservation extends IActivityObservation {
  activityName: ActivityName;
  activityCorrectness: number;
  activityDifficulty: number;
  timestamp?: Date;
  additionalInfos?: string; // use this field for additional tracking information
}
