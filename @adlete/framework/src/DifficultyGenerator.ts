import cloneDeep from 'lodash.clonedeep';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { doLinearPrediction } from '@adlete/engine-blocks/recommendation/TendencyModel';
import { createClampScorer, createWeightedSumScorer } from '@adlete/engine-blocks/utilityAI/Combiners';
import { applyContextModifier } from '@adlete/engine-blocks/utilityAI/ScoringModifiers';
import { ActionKey, getActionUtilities } from '@adlete/engine-blocks/utilityAI/Evaluation';
import { Scorer } from '@adlete/engine-blocks/utilityAI/Scoring';
import { Utility } from '@adlete/engine-blocks/utilityAI/Utility';
import { clamp01 } from '@adlete/engine-blocks/utils/Utils';

import { IRecommendationContext } from '@adlete/framework/IRecommendationContext';

export class DifficultyGenerator {
  activities: ActivityName[];

  // data and long-living contexts
  activityScores: Record<string, Utility> = {};

  // scorers
  activityScorers: Record<string, Scorer<IRecommendationContext>> = {};

  constructor(activities: ActivityName[]) {
    this.activities = cloneDeep(activities);

    // calculates the score of a single activity
    const activityScorer = createClampScorer<IRecommendationContext>(
      createWeightedSumScorer<IRecommendationContext>([this.calcExpectancyValue, this.calcFlowValue, this.calcConstantAddition]),
      0,
      1
    );

    // assigning this score-functions to all existing activities types (defined in adleteConfig)
    activities.forEach((activityName) => {
      // the scorers need to know the activity name, thus we're modifying the context accordingly
      this.activityScorers[activityName] = applyContextModifier(activityScorer, (context) => {
        context.activityName = activityName;
      });
    });
  }

  public getDifficulties(activityContext: IRecommendationContext): Record<ActionKey, Utility> {
    return getActionUtilities(this.activityScorers, activityContext); // updating the scores for the single activities.
  }

  public getDifficulty(activityName: ActivityName, activityContext: IRecommendationContext): Utility {
    const singleActivityScorer: Record<ActivityName, Scorer<IRecommendationContext>> = {};
    singleActivityScorer[activityName] = this.activityScorers[activityName];
    const result = getActionUtilities(singleActivityScorer, activityContext); // updating the scores for the single activities.

    return result[activityName];
  }

  private calcExpectancyValue(context: IRecommendationContext): number {
    const activityRecommenderTargets = context.adleteConfig.activityRecommenderTargets[context.activityName];
    if (activityRecommenderTargets === undefined || activityRecommenderTargets === null) {
      throw new Error(
        `Given adlete configuration does not include a activityRecommenderTargets definition for activity: "${context.activityName}".`
      );
    }

    let accumulatedBeliefValue = 0;
    let baseDifficultyOn: ActivityName[] = Array.isArray(activityRecommenderTargets)
      ? activityRecommenderTargets
      : activityRecommenderTargets.baseDifficultyOn;

    const competenceNames = context.adleteConfig.activityObservationWeights[context.activityName];
    let accumulatedPrediction = 0;
    let accumulatedTendencies = 0;
    let accumulatedDatapoints = 0;

    Object.entries(competenceNames).forEach(([key, value]) => {
      const tendencies = context.tendencies[key];
      let equationPrams = [0, 0];
      let dataPointCount = 0;
      if (tendencies) {
        equationPrams = tendencies.equationParams;
        dataPointCount = tendencies.dataPoints.length;
        accumulatedTendencies += tendencies.tendency * value;
      }
      accumulatedPrediction += clamp01(doLinearPrediction(dataPointCount, equationPrams)) * value;
      accumulatedDatapoints += dataPointCount * value;
    });
    baseDifficultyOn.forEach((competenceName) => {
      if (context.beliefModel[competenceName] === undefined || context.beliefModel[competenceName] === null) {
        throw new Error(`activityRecommenderTarget exists for a unknown competence: ${competenceName}`);
      }

      const belief = context.beliefModel[competenceName];
      // clamp01 regarding special case tendencies data points [0,1]
      accumulatedBeliefValue += belief.value;
    });

    accumulatedBeliefValue = accumulatedBeliefValue / baseDifficultyOn.length;
    if (accumulatedBeliefValue > accumulatedPrediction) {
      return accumulatedBeliefValue + (1 - 1 / accumulatedDatapoints) * accumulatedTendencies; // This will increase or decrease the value based on my tendencies.
    }
    // the expected difficulty must be a value between 0 - 1
    return accumulatedPrediction;
  }

  private calcFlowValue(context: IRecommendationContext): number {
    if (context.adleteConfig.globalWeights.recommendationDifficultyFlowModifier === 0) {
      return 0;
    }

    return (
      Math.sin((Math.PI / 20) * context.totalActivitiesCompleted + 2 * Math.PI) *
      context.adleteConfig.globalWeights.recommendationDifficultyFlowModifier
    );
  }

  private calcConstantAddition(context: IRecommendationContext): number {
    return context.adleteConfig.globalWeights.recommendationDifficultyConstantAddition;
  }
}
