import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';

export interface IAccumulatedActivityStatistics {
  activityName: ActivityName;

  activitiesPlayed: number;

  sumAnswersCorrectness: number;

  avgAnswersCorrectness: number;
}
