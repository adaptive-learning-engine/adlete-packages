/* use SimulatedLearnerBehaviorsType to give simulatedLearnerBehaviors a unique identifier */
export type SimulatedLearnerBehaviorsType = string;
