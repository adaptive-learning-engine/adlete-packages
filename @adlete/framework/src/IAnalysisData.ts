export interface IHeatmapData {
    x: string
    y: string
    value: number | null
    tooltip?: string
}

export interface ILearnerMasteryGridData {
    xLabel: string
    yLabel: string
    value: number | null
  }