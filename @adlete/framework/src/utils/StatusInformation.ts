/**
 * IStatusInfo is used for Error Handling it holds
 * @statusCode a status code number:
 *  200: ok
 *  401: client not logged. Use method login with a valid learnerId
 *  402: Input invalid.
 *  404: server connection failed.
 *  500: internal server error. See specific information in the @statusDescription
 *
 * @statusDescription holds specific information about the status
 */
export interface IStatusInfo {
    statusCode: number;
    statusDescription?: string;
    timestamp?: Date;
}

export interface IServerStatusInfo extends IStatusInfo {
    authenticated: boolean
}