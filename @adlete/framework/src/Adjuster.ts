import cloneDeep from 'lodash.clonedeep';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IAdjuster } from '@adlete/engine-blocks/adjusters/IAdjuster';
import { IBaseAdjusterParam } from '@adlete/engine-blocks/adjusters/IAdjusterFactory';
import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { bayesToScalarBeliefModel, probabilityToScalarBelief, probabilityToScalarBeliefBundle } from '@adlete/engine-blocks/belief/convert';
import { BayesBeliefSystem } from '@adlete/engine-blocks/evidence/BayesBeliefSystem';
import { IActivityEvidenceBundle, IScalarEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { ScalarBeliefSystem } from '@adlete/engine-blocks/evidence/ScalarBeliefSystem';
import { ScalarEvidenceInterpreter } from '@adlete/engine-blocks/evidence/ScalarEvidenceInterpreter';
import { scalarToProbabilityEvidence } from '@adlete/engine-blocks/evidence/convert';
import { IBeliefObservation } from '@adlete/engine-blocks/observation/IObservation';
import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { IScalarTendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { updateTendencies } from '@adlete/engine-blocks/recommendation/TendencyModel';
import { utilityValueLens } from '@adlete/engine-blocks/utilityAI/Utility';
import { ActivitiesChooser } from '@adlete/framework/ActivitiesChooser';
import { DifficultyGenerator } from '@adlete/framework/DifficultyGenerator';
import { IActivityRecommendationFilter } from '@adlete/framework/IActivityRecommendationFilter';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { IObservation } from '@adlete/framework/IObservation';
import { ISerializableAdjuster } from '@adlete/framework/ISerializableAdjuster';


export interface IAdjusterParams extends IBaseAdjusterParam {
  config: IAdleteConfiguration;
  beliefs: IProbabilityBeliefBundle;
  tendencies?: IScalarTendencyBundle;
}
/**
 * Engine that uses scalar values for evidence and beliefs
 * (depending on the BeliefSystem, probabilities might be used internally)
 */
export class Adjuster implements IAdjuster<IScalarEvidenceBundle, IActivityRecommendation<number>>, ISerializableAdjuster {
  public bayesBeliefSystem: BayesBeliefSystem;

  public scalarBeliefSystem: ScalarBeliefSystem;

  public evidenceInterpreter: ScalarEvidenceInterpreter;

  protected m_tendencies: IScalarTendencyBundle;

  public set tendencies(tendencies: IScalarTendencyBundle) {
    this.m_tendencies = cloneDeep(tendencies);
  }

  public get tendencies(): IScalarTendencyBundle {
    return this.m_tendencies;
  }

  protected activitiesChooser: ActivitiesChooser;

  protected difficultyGenerator: DifficultyGenerator;

  protected config: IAdleteConfiguration;

  constructor(adjusterParams: IAdjusterParams) {
    this.config = adjusterParams.config;
    let bayesBeliefModel: BayesBeliefModel;
    bayesBeliefModel = new BayesBeliefModel(adjusterParams.beliefs);
    /* generate probability and scalar belief model object from input competence model and restore values if given */

    let scalarBeliefModel;
    scalarBeliefModel = bayesToScalarBeliefModel(bayesBeliefModel);

    if (adjusterParams.tendencies !== undefined) {
      this.m_tendencies = cloneDeep(adjusterParams.tendencies);
    } else {
      const beliefModel = scalarBeliefModel.getBeliefs();
      this.m_tendencies = {};
      Object.keys(beliefModel).forEach((competenceName) => {
        this.m_tendencies[competenceName] = {
          tendency: 0,
          equationParams: [0, beliefModel[competenceName].value],
          dataPoints: [beliefModel[competenceName].value],
        };
      });
    }
    /* generate probability and scalar belief systems */
    this.bayesBeliefSystem = new BayesBeliefSystem(bayesBeliefModel, this.config.globalWeights.generalEvidenceWeight);
    this.scalarBeliefSystem = new ScalarBeliefSystem(scalarBeliefModel);

    /* initialize the interpreting evidence behaviour when updating the competence model */
    const evdInterpreterOpts = {
      scalarToProbabilityEvidence,
      probabilityToScalarBelief,
    };

    this.evidenceInterpreter = new ScalarEvidenceInterpreter(this.bayesBeliefSystem, this.scalarBeliefSystem, evdInterpreterOpts);

    this.activitiesChooser = new ActivitiesChooser(this.config.activityNames);
    this.difficultyGenerator = new DifficultyGenerator(this.config.activityNames);
  }

  serializeBeliefs(): { probabilisticBeliefs: string; scalarBeliefs: string; tendencies: string } {
    const probabilisticBeliefs = this.bayesBeliefSystem.getBeliefs();
    return {
      probabilisticBeliefs: JSON.stringify(probabilisticBeliefs),
      scalarBeliefs: JSON.stringify(probabilityToScalarBeliefBundle(probabilisticBeliefs)),
      tendencies: JSON.stringify(this.tendencies),
    };
  }

  /**
   * Converts an Observation into a activity evidence.
   *
   * @param observation
   */
  public convertObservationToActivityEvidence(observation: IObservation): IActivityEvidenceBundle {
    const activityObservationWeight = this.config.activityObservationWeights[observation.activityName as ActivityName];
    if (activityObservationWeight === undefined) {
      throw new Error(`The activities type does not exist, conversion failed. Is this activity type: ${observation.activityName} correct?`);
    }

    const activityEvidenceBundle: IActivityEvidenceBundle = {};
    Object.keys(activityObservationWeight).forEach((key) => {
      activityEvidenceBundle[key] = {
        correctness: observation.activityCorrectness,
        difficulty: observation.activityDifficulty,
        weight: activityObservationWeight[key],
      };
    });

    return activityEvidenceBundle;
  }

  /**
   * Converts an ActivityEvidence into a ScalarEvidence.
   * Instead of using default converter (convertEvidence.activityToScalarEvidenceBundle( activityEvidence );)
   * this converter manipulating only within a range +/- X because the observation correctness value
   * is only 0 or 1.
   *
   * @param activityEvidence
   */
  public convertActivityEvidenceToScalarEvidence(activityEvidence: IActivityEvidenceBundle): IScalarEvidenceBundle {
    //return activityToScalarEvidenceBundle(activityEvidence);

    const result: IScalarEvidenceBundle = {};

    Object.entries(activityEvidence).forEach(([competenceName, evidence]) => {
      const beliefValue = this.scalarBeliefSystem.getBelief(competenceName).value;

      let value: number;
      // PFA (Performance Factor Analysis) Theta (SKill estimate) + (yc (change of skill) + ß (difficulty)(1-c (correctness)))
      // Since input is only 1 or 0 a specific interpretation is needed
      if (evidence.correctness === 1) {
        //value = clamp01(evidence.difficulty + evidence.difficulty * evidence.weight); // positive effect (+)
        value = Math.max(beliefValue, evidence.difficulty * evidence.weight);
      } else {
        value = Math.min(beliefValue, evidence.difficulty * evidence.weight); // negative effect
      }

      result[competenceName] = {
        weight: evidence.weight,
        value,
      };
    });

    return result;
  }

  /**
   * Interprets evidence and updates the beliefs about the player
   *
   * @param evidence
   * observation: IObservation
   */
  public interpretEvidence(evidence: IScalarEvidenceBundle, observation: IObservation): void {
    const activityWeights = this.config.activityObservationWeights[observation.activityName];
    const beliefWeights: Record<string, number> = {};

    /* store belief before the belief update to calculate tendencies later on */
    Object.entries(activityWeights).forEach(([key, value]) => {
      beliefWeights[key] = this.scalarBeliefSystem.getBelief(key).value;
    });

    const beliefObservation: IBeliefObservation = {
      beliefWeights: beliefWeights,
      correctness: observation.activityCorrectness,
      difficulty: observation.activityDifficulty,
    };

    this.evidenceInterpreter.interpret(evidence);

    updateTendencies(
      this.m_tendencies,
      this.scalarBeliefSystem,
      beliefObservation,
      this.config.globalWeights.tendencyPositiveWeight,
      this.config.globalWeights.tendencyNegativeWeight,
      5
    );
  }

  /**
   * Returns a scalar activity recommendation
   * @param filter
   */
  recommend(filter: IActivityRecommendationFilter): IActivityRecommendation<number> {
    // recommend the next activity
    const activityName = this.activitiesChooser.getSortedActivities({
      beliefModel: this.scalarBeliefSystem.beliefModel.getBeliefs(),
      tendencies: this.m_tendencies,
      statistics: filter.statistics,
      totalActivitiesCompleted: filter.totalActivitiesCompleted,
      adleteConfig: filter.adleteConfig,
      activitySubset: filter.activitySubset,
    })[0].action as string;

    // recommend the difficulty of the recommended activity
    const difficulty = utilityValueLens.get(
      this.difficultyGenerator.getDifficulty(activityName, {
        beliefModel: this.scalarBeliefSystem.beliefModel.getBeliefs(),
        tendencies: this.m_tendencies,
        statistics: filter.statistics,
        totalActivitiesCompleted: filter.totalActivitiesCompleted,
        adleteConfig: filter.adleteConfig,
        activitySubset: filter.activitySubset,
      })
    );

    // forward the trend of the recommended activity to allow the external learning environment make better task selection decisions
    let trend = 0;
    if (this.config.activityObservationWeights[activityName] !== null) {
      const activityObservationWeightsNodeNames = Object.keys(this.config.activityObservationWeights[activityName]);

      if (activityObservationWeightsNodeNames.length > 0) {
        Object.keys(this.config.activityObservationWeights[activityName]).forEach((nodeName) => {
          trend = trend + this.m_tendencies[nodeName].tendency;
        });

        trend = trend / activityObservationWeightsNodeNames.length;
      }
    }

    return {
      activityName,
      difficulty,
      trend,
    };
  }
}
