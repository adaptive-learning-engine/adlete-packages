import { IObservationAdjuster } from '@adlete/engine-blocks/adjusters/IObservationAdjuster';
import { BayesBeliefSystem } from '@adlete/engine-blocks/evidence/BayesBeliefSystem';
import { IActivityEvidenceBundle, IScalarEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { ScalarBeliefSystem } from '@adlete/engine-blocks/evidence/ScalarBeliefSystem';
import { IBeliefObservation } from '@adlete/engine-blocks/observation/IObservation';
import { IScalarTendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import cloneDeep from 'lodash.clonedeep';
import { IAdleteConfiguration } from './IAdleteConfiguration';
import { IObservation } from '@adlete/framework/IObservation';
import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { updateTendencies } from '@adlete/engine-blocks/recommendation/TendencyModel';
import { bayesToScalarBeliefModel, probabilityToScalarBelief, probabilityToScalarBeliefBundle } from '@adlete/engine-blocks/belief/convert';
import { IProbabilityBeliefBundle, IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { ISerializableAdjuster } from './ISerializableAdjuster';
import { IBaseAdjusterParam } from '@adlete/engine-blocks/adjusters/IAdjusterFactory';
import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { scalarToProbabilityEvidence, scalarToProbabilityEvidenceBundle } from '@adlete/engine-blocks/evidence/convert';

export interface IObservationAdjusterParam extends IBaseAdjusterParam {
  probabilisticBeliefs: IProbabilityBeliefBundle;
  config: IAdleteConfiguration;
  tendencies: IScalarTendencyBundle;
}

export class ObservationAdjuster implements IObservationAdjuster<IScalarEvidenceBundle>, ISerializableAdjuster {
  public bayesBeliefSystem: BayesBeliefSystem;

  protected oldBeliefs: IProbabilityBeliefBundle;
  protected scalarBeliefBundle: IScalarBeliefBundle;
  protected m_tendencies: IScalarTendencyBundle;

  protected config: IAdleteConfiguration;
  public set tendencies(tendencies: IScalarTendencyBundle) {
    this.m_tendencies = cloneDeep(tendencies);
  }

  public get tendencies(): IScalarTendencyBundle {
    return this.m_tendencies;
  }

  constructor(adjusterParameter: IObservationAdjusterParam) {
    this.config = adjusterParameter.config;
    this.oldBeliefs = adjusterParameter.probabilisticBeliefs;
    const bayesBeliefModel = new BayesBeliefModel(adjusterParameter.probabilisticBeliefs);
    this.bayesBeliefSystem = new BayesBeliefSystem(bayesBeliefModel, this.config.globalWeights.generalEvidenceWeight);
    this.tendencies = adjusterParameter.tendencies;
  }

  public serializeBeliefs(): { probabilisticBeliefs: string; scalarBeliefs: string; tendencies: string } {
    const probabilisticBeliefs = this.bayesBeliefSystem.getBeliefs();
    this.scalarBeliefBundle ?? probabilityToScalarBeliefBundle(probabilisticBeliefs);
    return {
      probabilisticBeliefs: JSON.stringify(probabilisticBeliefs),
      scalarBeliefs: JSON.stringify(this.scalarBeliefBundle),
      tendencies: JSON.stringify(this.tendencies),
    };
  }

  /**
   * Converts an Observation into a activity evidence.
   *
   * @param observation
   */
  public convertObservationToActivityEvidence(observation: IObservation): IActivityEvidenceBundle {
    const activityObservationWeight = this.config.activityObservationWeights[observation.activityName as ActivityName];
    if (activityObservationWeight === undefined) {
      throw new Error(`The activities type does not exist, conversion failed. Is this activity type: ${observation.activityName} correct?`);
    }

    const activityEvidenceBundle: IActivityEvidenceBundle = {};
    Object.keys(activityObservationWeight).forEach((key) => {
      activityEvidenceBundle[key] = {
        correctness: observation.activityCorrectness,
        difficulty: observation.activityDifficulty,
        weight: activityObservationWeight[key],
      };
    });

    return activityEvidenceBundle;
  }

  /**
   * Converts an ActivityEvidence into a ScalarEvidence.
   * Instead of using default converter (convertEvidence.activityToScalarEvidenceBundle( activityEvidence );)
   * this converter manipulating only within a range +/- X because the observation correctness value
   * is only 0 or 1.
   *
   * @param activityEvidence
   */
  public convertActivityEvidenceToScalarEvidence(activityEvidence: IActivityEvidenceBundle): IScalarEvidenceBundle {
    //return activityToScalarEvidenceBundle(activityEvidence);

    const result: IScalarEvidenceBundle = {};

    Object.entries(activityEvidence).forEach(([competenceName, evidence]) => {
      const beliefValue = probabilityToScalarBelief(this.bayesBeliefSystem.getBelief(competenceName)).value;

      let value: number;
      // PFA (Performance Factor Analysis) Theta (SKill estimate) + (yc (change of skill) + ß (difficulty)(1-c (correctness)))
      // Since input is only 1 or 0 a specific interpretation is needed
      if (evidence.correctness === 1) {
        //value = clamp01(evidence.difficulty + evidence.difficulty * evidence.weight); // positive effect (+)
        value = Math.max(beliefValue, evidence.difficulty * evidence.weight);
      } else {
        value = Math.min(beliefValue, evidence.difficulty * evidence.weight); // negative effect
      }

      result[competenceName] = {
        weight: evidence.weight,
        value,
      };
    });

    return result;
  }

  interpretEvidence(evidence: IScalarEvidenceBundle, observation: IObservation): void {
    const activityWeights = this.config.activityObservationWeights[observation.activityName];
    const beliefWeights: Record<string, number> = {};

    /* store belief before the belief update to calculate tendencies later on */
    Object.entries(activityWeights).forEach(([key, value]) => {
      const probabilityBelief = this.oldBeliefs[key];
      beliefWeights[key] = probabilityToScalarBelief(probabilityBelief).value;
    });

    const beliefObservation: IBeliefObservation = {
      beliefWeights: beliefWeights,
      correctness: observation.activityCorrectness,
      difficulty: observation.activityDifficulty,
    };

    const probEvidence = scalarToProbabilityEvidenceBundle(
      scalarToProbabilityEvidence,
      evidence,
      this.bayesBeliefSystem.beliefModel.getStates().length
    );
    this.bayesBeliefSystem.addEvidence(probEvidence);
    //this.evidenceInterpreter.interpret(evidence);
    const scalarBeliefModel = bayesToScalarBeliefModel(this.bayesBeliefSystem.beliefModel);
    const scalarBeliefSystem = new ScalarBeliefSystem(scalarBeliefModel);

    updateTendencies(
      this.m_tendencies,
      scalarBeliefSystem,
      beliefObservation,
      this.config.globalWeights.tendencyPositiveWeight,
      this.config.globalWeights.tendencyNegativeWeight,
      5
    );
    this.scalarBeliefBundle = scalarBeliefModel.getBeliefs();
  }
}
