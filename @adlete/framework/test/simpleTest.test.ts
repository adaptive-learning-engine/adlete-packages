import { scalarToProbabilityBelief } from '@adlete/engine-blocks/belief/convert';

// import '../src/Adjuster';

describe('engine-blocks', () => {
  test('test', () => {
    const a = scalarToProbabilityBelief({ value: 0, certainty: 1 }, 3);
    // const a = 1;
    expect(a).toEqual(2);
  });
});
