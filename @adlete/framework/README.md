# @adlete/framework

An adaptive learning engine assembled from engine building block which are provided by @adlete/engine-blocks

## Example

```
TODO
```

## Development

### Dependencies

- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

### Install package dependencies

```bash
yarn install
```

### Building code

This package uses [Typescript](http://typescriptlang.org/).

```bash
# build
yarn build

# watch and build
yarn watch:build
```

### Building documentation

```bash
yarn build:docs
```

### IDE

#### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/).

#### Linting

This package uses a prettier configuration to set a style format for all source code files, we ship this config in the package @adlete/dev-config.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).
