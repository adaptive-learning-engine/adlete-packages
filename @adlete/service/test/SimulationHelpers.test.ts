import { calculateDifficulty } from '../src/util/SimulationHelpers';

describe('SimulationHelpers', () => {
  test('calculate Difficulty correct', () => {
    const result = calculateDifficulty(0, 6, 0.432, 1);
    expect(result).toBe(3);
  });

  test('calculateDifficulty edge', () => {
    const result = calculateDifficulty(0, 6, 0.212, 1);
    expect(result).toBe(1);
  })

  test('calculate Difficulty small Step', () => {
    const result = calculateDifficulty(0, 6, 0.432, 0.01);
    expect(result).toBe(2.59);
  });

  test('calculate Difficulty another step', () => {
    const result = calculateDifficulty(0, 6, 0.17, 0.03);
    expect(result).toBe(1.02);
  });

  test('calculate Difficulty incorrect', () => {
    expect(() => calculateDifficulty(40, 6, 0.17, 0.03)).toThrowError('max Difficulty is <= minDifficulty');
  });
});
