You can use the following function to quickly test bayes net functionality within the index.ts file.
This should be a temporal file, it can be deleted any time.

import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { BayesNet, BayesNetFactory, getCPT, getDistributionFromPotential, LazyPropagation, Vector } from '@adlete/node-agrum';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function tmpTest(competenceModel: IBayesNet) {
// const bayesNet = new BayesBeliefModel(competenceModel);

const bayesNet = new BayesNet();
const bayesNetFactory = new BayesNetFactory(bayesNet);
// generate nodes
Object.values(['compA', 'compB', 'compC']).forEach((node) => {
bayesNetFactory.startVariableDeclaration();
bayesNetFactory.variableName(node);
['low', 'medium', 'high'].forEach((state) => {
bayesNetFactory.addModality(state);
});
bayesNetFactory.endVariableDeclaration();
});

// generate parents
bayesNetFactory.startParentsDeclaration('compA');
bayesNetFactory.addParent('compB');
bayesNetFactory.addParent('compC');
bayesNetFactory.endParentsDeclaration();

let vecCPT = new Vector();
// 1.0, 0.0, 0.0,
// 0.5, 0.5, 0.0,
// 0.5, 0.0, 0.5,
// 0.5, 0.5, 0.0,
// 0.0, 1.0, 0.0,
// 0.0, 0.5, 0.5,
// 0.5, 0.0, 0.5,
// 0.0, 0.5, 0.5,
// 0.0, 0.0, 1.0

[1, 0, 0, 0.5, 0.5, 0, 0.5, 0, 0.5, 0.5, 0.5, 0, 0, 1, 0, 0, 0.5, 0.5, 0.5, 0, 0.5, 0, 0.5, 0.5, 0, 0, 1].forEach((val) =>
vecCPT.add(val)
);
bayesNet.cpt(bayesNet.idFromName('compA')).fillWith(vecCPT);

vecCPT = new Vector();
[0, 0.3, 0.7].forEach((val) => vecCPT.add(val));
bayesNet.cpt(bayesNet.idFromName('compB')).fillWith(vecCPT);

vecCPT = new Vector();
[0.1, 0.9, 0.0].forEach((val) => vecCPT.add(val));
bayesNet.cpt(bayesNet.idFromName('compC')).fillWith(vecCPT);

const inferenceEngine = new LazyPropagation(bayesNet);

// if (this.inferenceEngine.hasEvidence(nodeId)) {
// this.inferenceEngine.chgEvidence(nodeId,s vec);
// } else {
console.log(inferenceEngine.posterior('compA').toString());
console.log(inferenceEngine.posterior('compB').toString());
console.log(inferenceEngine.posterior('compC').toString());

// inferenceEngine.eraseAllEvidence();
// inferenceEngine.eraseAllTargets();

// vecCPT = new Vector();
// [0, 0.1, 0.9].forEach((val) => vecCPT.add(val));
// inferenceEngine.addEvidence(bayesNet.idFromName('compB'), vecCPT);

// inferenceEngine.eraseAllEvidence();
// inferenceEngine.eraseTarget('compB');

vecCPT = new Vector();
[0, 0.1, 0.9].forEach((val) => vecCPT.add(val));
bayesNet.cpt(bayesNet.idFromName('compB')).fillWith(vecCPT);

vecCPT = new Vector();
[0, 0.1, 0.9].forEach((val) => vecCPT.add(val));
inferenceEngine.addEvidence(bayesNet.idFromName('compB'), vecCPT);

const inferenceEngine2 = new LazyPropagation(bayesNet);

inferenceEngine2.makeInference();

// inferenceEngine.makeInference();
console.log('---------------------------------');
console.log(inferenceEngine2.posterior('compA').toString());
console.log(inferenceEngine2.posterior('compB').toString());
console.log(inferenceEngine2.posterior('compC').toString());
}
