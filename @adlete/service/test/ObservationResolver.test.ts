import { graphql, GraphQLArgs } from 'graphql';

import { IObservation } from '@adlete/framework/IObservation';

import { Learner } from '../src/entities/Learner';
import { StatusInfo } from '../src/entities/StatusInfo';

import {
  closeDBConnection,
  createTestLearner,
  deleteTestLearner,
  generateUniqueKey,
  getContext,
  getSchema,
  MongooseConnector,
  openDBConnection,
  startTestSession,
  stopTestSession,
} from './GlobalTestSetup.test';

describe('ObservationResolver', () => {
  const learnerId = generateUniqueKey('test');

  /**
   * Starts the connection, the session and creates a new learner
   */
  let connector: MongooseConnector;
  beforeAll(async () => {
    connector = await openDBConnection();
    await createTestLearner(learnerId);
    await startTestSession(learnerId);
  }, 100000);

  /**
   * tests some general Setting values as status Code / description
   */
  it('observationSettings', async () => {
    const observationRequest = await generateSubmitActivityResultsGraphQLArgs(learnerId);
    const observationRequestResults = await graphql(observationRequest);

    /**
     * Test 01: The result should not contain any errors / or the learner id's existence
     * */
    expect(observationRequestResults.errors).toEqual(undefined);
    const { statusCode, statusDescription } = observationRequestResults.data.submitActivityResult as StatusInfo;

    /**
     * Test 02: code must be 200 as described in API documentation
     * */
    expect(statusCode).toEqual(200);

    /**
     * Test 03: checks if the Evidence has been interpreted successfully
     *  * */
    expect(statusDescription).toMatch('Interpreted evidence successfully!');
  }, 100000);

  /**
   * Check if incorrect input values throw any errors
   */
  it('observationInputValues', async () => {
    const args = await generateSubmitActivityResultsGraphQLArgs(learnerId);
    const observation: IObservation = args.variableValues.observation as IObservation;
    /**
     * Test 04: try if activityDifficulty is too high or too low
     */

    observation.activityDifficulty = 1.1;
    const resultDifficulty1 = await graphql(args);
    expect(resultDifficulty1.errors).not.toEqual(undefined);

    observation.activityDifficulty = -1;
    const resultDifficulty2 = await graphql(args);
    expect(resultDifficulty2.errors).not.toEqual(undefined);

    /**
     * Test 05: try if activityCorrectness is too high or too low
     */
    observation.activityCorrectness = 1.1;
    const resultactivityCorrectness1 = await graphql(args);
    expect(resultactivityCorrectness1.errors).not.toEqual(undefined);

    observation.activityCorrectness = -1;
    const resultactivityCorrectness2 = await graphql(args);
    expect(resultactivityCorrectness2.errors).not.toEqual(undefined);
  }, 100000);

  /**
   * check if timestamps make sense
   */
  it('observationTimeStamp', async () => {
    const observationRequest = await generateSubmitActivityResultsGraphQLArgs(learnerId);
    const learnerQuery: GraphQLArgs = {
      schema: await getSchema(),
      source: `query learner($learnerId:String!){
                learner(learnerId:$learnerId) {
                  _id,
                  overAllFinishedActivities
                  learnerId, 
                  probabilisticBeliefs, 
                  scalarBeliefs, 
                  tendencies,
                  createdAt,
                  updatedAt
                }
              }`,
      contextValue: getContext(),
      variableValues: { learnerId },
    };

    const learnerRequestResult = await graphql(learnerQuery);
    const observationRequestResult = await graphql(observationRequest);

    const learner = learnerRequestResult.data.learner as Learner;
    const submitActivityResult = observationRequestResult.data.submitActivityResult as StatusInfo;

    const createDate = new Date(learner.createdAt);
    const thisDate = new Date(submitActivityResult.timestamp);

    /**
     * Test 07: Try if date is older than learnerId timestamp
     */
    expect(thisDate.getTime()).toBeGreaterThan(createDate.getTime());
  }, 100000);

  /**
   * Stop the Session, the connection and deletes the created Learner
   */
  afterAll(async () => {
    await stopTestSession(learnerId);
    await deleteTestLearner(learnerId);
    await closeDBConnection(connector);
  }, 100000);
});

async function generateSubmitActivityResultsGraphQLArgs(id: string): Promise<GraphQLArgs> {
  const schema = await getSchema();

  return {
    schema,
    source: `mutation submitActivityResult($observation: ObservationInput!) {
              submitActivityResult(observation:$observation) {
                statusCode,
                statusDescription,
                timestamp
              }
            }`,
    contextValue: getContext(),
    variableValues: {
      observation: {
        learnerId: id,
        activityName: 'activityAddition', // test that only certain names can be used???
        activityCorrectness: 0.1, // try if number is below 0, NAN or over 1
        activityDifficulty: 0.2, // try if number is below 0, NAN or over 1
        timestamp: '2021-09-11T15:00:39.419+00:00', // try if Date is in the wrong format/ not from today
      },
    },
  };
}
