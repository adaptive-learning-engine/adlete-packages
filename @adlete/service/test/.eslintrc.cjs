module.exports = {
  extends: ['@adlete/eslint-config/eslint'],
  plugins: ['jest'],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
};
