import { calculateProbability } from '../src/util/LearnerSimulator';

describe('LearnerSimulator', () => {
  test('real learner correct equal', () => {
    const propability = calculateProbability(0.5, 0.5, 1, 0.1);
    expect(propability).toBeCloseTo(0.96, 2);
  });

  test('real learner correct higher', () => {
    const propability = calculateProbability(0.5, 0.6, 1, 0.1);
    expect(propability).toBeCloseTo(0.64, 2);
  });

  test('real learner correct lower', () => {
    const propability = calculateProbability(0.5, 0.4, 1, 0.1);
    expect(propability).toBeCloseTo(1.28, 2);
  });
});
