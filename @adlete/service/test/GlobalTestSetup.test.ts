/* eslint-disable no-console */
import { createRequire } from 'module';

/* eslint-disable @typescript-eslint/no-var-requires */
import 'reflect-metadata'; // must be the first import!

import dotenv from 'dotenv';

import * as fs from 'fs';

import { ExecutionResult, graphql, GraphQLArgs, GraphQLSchema } from 'graphql';
import { ObjMap } from 'graphql/jsutils/ObjMap';
import { ObjectId } from 'mongodb';
import mongoose from 'mongoose';
import { buildSchema } from 'type-graphql';

import { clientValidator } from '../src/resolvers/ClientValidator';
import { LearnerResolver } from '../src/resolvers/LearnerResolver';
import { ObservationResolver } from '../src/resolvers/ObservationResolver';
import { RecommendationResolver } from '../src/resolvers/RecommendationResolver';
import { SessionResolver } from '../src/resolvers/SessionResolver';
import { SessionStatisticsResolver } from '../src/resolvers/SessionStatisticsResolver';
import { StatusInfoResolver } from '../src/resolvers/StatusInfoResolver';
import { Context } from '../src/util/Context';
import { ObjectIdScalar } from '../src/util/ObjectIdScalar';
import { TypegooseMiddleware } from '../src/util/TypegooseMiddleware';

const require = createRequire(import.meta.url);

// load .env-File into
dotenv.config();
// db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
export class MongooseConnector {
  private mongoose: typeof import('mongoose');

  public async connect(): Promise<void> {
    this.mongoose = await this.getMongoose();
  }

  public disconnect(): void {
    this.mongoose.disconnect();
  }

  public async dropDatabase(): Promise<boolean> {
    if (this.mongoose == null) {
      throw new Error('Connect first');
    }

    return this.mongoose.connection.db.dropDatabase();
  }

  private async getMongoose(): Promise<typeof mongoose> {
    let db;
    if (process.env.mongURI_FILE) {
      db = fs.readFileSync(process.env.mongURI_FILE, 'utf8') as string;
    } else {
      db = process.env.mongoURI;
    }

    return mongoose.connect(db);
  }
}

export async function openDBConnection(): Promise<MongooseConnector> {
  try {
    const mongooseConnector: MongooseConnector = new MongooseConnector();
    await mongooseConnector.connect();
    return mongooseConnector;
  } catch (e) {
    console.error(`some error at the connection with DB${e}`);
    return null;
  }
}
export async function closeDBConnection(mongooseConnector: MongooseConnector): Promise<MongooseConnector> {
  // const mongooseConnector: MongooseConnector = new MongooseConnector();
  await mongooseConnector.disconnect();
  return mongooseConnector;
}

export async function dropDB(mongooseConnector: MongooseConnector): Promise<void> {
  await mongooseConnector.dropDatabase();
}

/**
 * Returns a graphql schema including all resolver compare to ./src/index.ts
 */
export async function getSchema(): Promise<GraphQLSchema> {
  return buildSchema({
    resolvers: [
      StatusInfoResolver,
      LearnerResolver,
      SessionResolver,
      SessionStatisticsResolver,
      ObservationResolver,
      RecommendationResolver,
    ],
    // use document converting middleware
    globalMiddlewares: [TypegooseMiddleware],
    // use ObjectId scalar mapping
    scalarsMap: [{ type: ObjectId, scalar: ObjectIdScalar }],
    authMode: 'null',
    authChecker: clientValidator,
  });
}

/**
 * Returns a service context
 * @param lectures
 */
export function getContext(): Context {
  const ctx: Context = {
    isAuthorized: true,
    version: require('../package.json').version,
    // eslint-disable-next-line import/no-dynamic-require
    adleteConfig: require(process.env.adleteConfigPath),
  };

  return ctx;
}

export function generateUniqueKey(pre: string): string {
  const newKey = `${pre}_${new Date().getTime()}`;
  return newKey;
}

export async function startTestSession(id: string): Promise<ExecutionResult<ObjMap<unknown>, ObjMap<unknown>>> {
  const startSession: GraphQLArgs = {
    schema: await getSchema(),
    source: `mutation startSession($learnerId:String!) 
      {
        startSession(learnerId:$learnerId) { 
          _id, 
          startTimestamp,
          activitiesStatistics {activityName}
        }
      }
      `,
    contextValue: getContext(),
    variableValues: { learnerId: id },
  };
  return graphql(startSession);
}

export async function createTestLearner(id: string): Promise<ExecutionResult<ObjMap<unknown>, ObjMap<unknown>>> {
  const learner: GraphQLArgs = {
    schema: await getSchema(),
    source: `mutation createLearner($learnerId:String!) {
          createLearner(learnerId:$learnerId) { 
            _id, 
            learnerId, 
            probabilisticBeliefs, 
            scalarBeliefs, 
            tendencies,
            createdAt,
            updatedAt
          }
        }
        `,
    contextValue: getContext(),
    variableValues: { learnerId: id },
  };
  return graphql(learner);
}

export async function deleteTestLearner(id: string): Promise<ExecutionResult<ObjMap<unknown>, ObjMap<unknown>>> {
  const deleteLearner: GraphQLArgs = {
    schema: await getSchema(),
    source: `mutation deleteLearner($learnerId:String!){
        deleteLearner(learnerId:$learnerId) {
          statusCode,
          statusDescription,
          timestamp
        }
      }
        `,
    contextValue: getContext(),
    variableValues: { learnerId: id },
  };
  return graphql(deleteLearner);
}

export async function stopTestSession(id: string): Promise<ExecutionResult<ObjMap<unknown>, ObjMap<unknown>>> {
  const session: GraphQLArgs = {
    schema: await getSchema(),
    source: `mutation stopSession($learnerId:String!) 
      {
        stopSession(learnerId:$learnerId) { 
         statusCode,
          statusDescription
        }
      }
      `,
    contextValue: getContext(),
    variableValues: { learnerId: id },
  };
  return graphql(session);
}
