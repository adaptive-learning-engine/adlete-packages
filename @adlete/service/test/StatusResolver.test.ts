/* eslint-disable import/first */
import { graphql, GraphQLArgs } from 'graphql';

import { StatusInfo } from '../src/entities/StatusInfo';

import { getContext, getSchema } from './GlobalTestSetup.test';

describe('StatusResolver', () => {
  /**
   * test query status
   */
  it('status', async () => {
    const args: GraphQLArgs = {
      schema: await getSchema(),
      source:
        'query status{                                    ' +
        '    status {                                     ' +
        '        statusCode,statusDescription,timestamp   ' +
        '    }                                            ' +
        '}                                                ',
      contextValue: getContext(),
      variableValues: {},
    };
    const result = await graphql(args);

    /**
     * Test 00: The result should not contain any errors
     * */
    expect(result.errors).toEqual(undefined);

    const { statusCode, statusDescription } = result.data.status as StatusInfo;

    /**
     * Test 01: code must be 200
     * */
    const expectedCode = 200;
    expect(statusCode).toEqual(expectedCode);

    /**
     * Test 02: The description must contain the authorized state
     * Because the given context provides the correct key (compare --> getContextForTest())
     * */
    const expectedDescription = 'Status: Authorized';
    expect(statusDescription).toMatch(expectedDescription);
  });
});
