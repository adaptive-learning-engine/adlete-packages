import { graphql, GraphQLArgs } from 'graphql';

import { Recommendation } from '../src/entities/Recommendation';

import {
  closeDBConnection,
  createTestLearner,
  deleteTestLearner,
  generateUniqueKey,
  getContext,
  getSchema,
  MongooseConnector,
  openDBConnection,
  startTestSession,
  stopTestSession,
} from './GlobalTestSetup.test';

describe('RecommendationResolver', () => {
  const id = generateUniqueKey('test');

  /**
   * Starts the connection, the session and creates a new learner
   */
  let connector: MongooseConnector;
  beforeAll(async () => {
    connector = await openDBConnection();
    await createTestLearner(id);
    await startTestSession(id);
  });

  /**
   * test mutation fetchNextRecommendation
   */
  it('recommendation', async () => {
    const learnerId = id;
    const args: GraphQLArgs = {
      schema: await getSchema(),
      source: `mutation fetchNextRecommendation($learnerId:String!) {
                fetchNextRecommendation(learnerId:$learnerId) {
                  _id, 
                  activityName,
                  difficulty
                  createdAt,
                  updatedAt
                }
              }`,
      contextValue: getContext(),
      variableValues: { learnerId },
    };
    const result = await graphql(args);

    /**
     * Test 00: The result should not contain any errors
     * */
    expect(result.errors).toEqual(undefined);
    const { difficulty } = result.data.fetchNextRecommendation as Recommendation;

    /**
     * Test 01: The difficulty should be between 0 and 1
     */
    expect(difficulty).toBeGreaterThanOrEqual(0);
    expect(difficulty).toBeLessThan(1);
  });

  /**
   * Stop the Session, the connection and deletes the created Learner
   */
  afterAll(async () => {
    await stopTestSession(id);
    await deleteTestLearner(id);
    await closeDBConnection(connector);
  });
});
