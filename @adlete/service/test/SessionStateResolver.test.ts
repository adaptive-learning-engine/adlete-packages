import { StatusInfo } from '../src/entities/StatusInfo';

import {
  closeDBConnection,
  createTestLearner,
  deleteTestLearner,
  generateUniqueKey,
  MongooseConnector,
  openDBConnection,
  startTestSession,
  stopTestSession,
} from './GlobalTestSetup.test';

describe('SessionResolver', () => {
  const id = generateUniqueKey('test');
  let connector: MongooseConnector;

  /**
   * Starts the connection, the session and creates a new learner
   */
  beforeAll(async () => {
    connector = await openDBConnection();
    await createTestLearner(id);
  });

  /**
   * test mutation start session
   */
  it('session', async () => {
    const result = await startTestSession(id);

    /**
     * Test 00: The result should not contain any errors
     * */
    expect(result.errors).toEqual(undefined);
  });

  /**
   * test mutation stop session
   */
  it('session', async () => {
    const result = await stopTestSession(id);

    /**
     * Test 01: The result should not contain any errors
     * */
    expect(result.errors).toEqual(undefined);

    const { statusDescription } = result.data.stopSession as StatusInfo;

    /**
     * Test 02: The description must contain the authorized state with Authorized as described in API documentation: https://confluence.serrala.com/display/PROW/05.03.+API-Dokumentation+KI-Service
     * Because the given context provides the correct key (compare --> getContextForTest())
     * */
    const expectedDescription = 'Session was stopped!';
    expect(statusDescription).toMatch(expectedDescription);
  });

  /**
   * Stop the Session, the connection and deletes the created Learner
   */
  afterAll(async () => {
    await deleteTestLearner(id);
    await closeDBConnection(connector);
  });
});
