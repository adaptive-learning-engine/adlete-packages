import { graphql, GraphQLArgs } from 'graphql';

import { Learner } from '../src/entities/Learner';

import {
  closeDBConnection,
  createTestLearner,
  deleteTestLearner,
  generateUniqueKey,
  getContext,
  getSchema,
  MongooseConnector,
  openDBConnection,
} from './GlobalTestSetup.test';

describe('LearnerResolver', () => {
  const id = generateUniqueKey('test');

  /**
   * Before everything else, there needs to be a connection to the DB and a new learner needs to be created
   */
  let connector: MongooseConnector;
  beforeAll(async () => {
    connector = await openDBConnection();
    await createTestLearner(id);
    await createTestLearner(null);
  }, 100000);

  /**
   * tests query learner
   */
  it('learner', async () => {
    const args: GraphQLArgs = {
      schema: await getSchema(),
      source: `query learner($learnerId:String!){
                learner(learnerId:$learnerId) {
                  _id,
                  overAllFinishedActivities
                  learnerId, 
                  probabilisticBeliefs, 
                  scalarBeliefs, 
                  tendencies,
                  createdAt,
                  updatedAt
                }
              }`,
      contextValue: getContext(),
      variableValues: { learnerId: id },
    };
    const result = await graphql(args);
    const { overAllFinishedActivities, learnerId } = result.data.learner as Learner;

    /**
     * Test 00: The result should not contain any errors
     * */
    expect(result.errors).toEqual(undefined);

    /**
     * Test 01: output id should match the input id
     * */
    expect(learnerId).toEqual(args.variableValues.learnerId);

    /**
     * Test 02: finishedActivities should be 0
     * */
    expect(overAllFinishedActivities).toEqual(0);
  });

  /**
   *After everything else, the connection needs to be closed and the learner needs to be deleted
   */
  afterAll(async () => {
    await deleteTestLearner(id);
    await deleteTestLearner(null);
    await closeDBConnection(connector);
  }, 100000);
});
