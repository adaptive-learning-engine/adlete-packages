import { graphql, GraphQLArgs } from 'graphql';

import { IProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';

import { Learner } from '../src/entities/Learner';
import { Observation } from '../src/entities/Observation';
import { calculateDifficulty } from '../src/util/SimulationHelpers';

import {
  closeDBConnection,
  createTestLearner,
  deleteTestLearner,
  generateUniqueKey,
  getContext,
  getSchema,
  MongooseConnector,
  openDBConnection,
} from './GlobalTestSetup.test';

describe('SimulationResolver', () => {
  const id = generateUniqueKey('test');
  let connector: MongooseConnector;
  const initialScalarBelief = 0.1;
  /**
   * Starts the connection, the session and creates a new learner
   */
  beforeAll(async () => {
    connector = await openDBConnection();
    await createTestLearner(id);
  });

  afterAll(async () => {
    await deleteTestLearner(id);
    await closeDBConnection(connector);
  });

  it('advanced Simulation correct no storage', async () => {
    const gql = `query advancedSimulation(
            $initialScalarBeliefSetId: String!
            $taskSet: [Task!]
            $maxDifficulty: Float!
            $minDifficulty: Float!
            $stepsDifficulty: Float!
            $agentBehavior: String!
            $storeResults: Boolean!
            $learnerId: String
          ) {
            simulate(
                initialScalarBeliefSetId: $initialScalarBeliefSetId
                taskSet: $taskSet
                maxDifficulty: $maxDifficulty
                minDifficulty: $minDifficulty
                stepsDifficulty: $stepsDifficulty
                agentBehavior: $agentBehavior
                storeResults: $storeResults
                learnerId: $learnerId
            ) {
              serviceConfiguration {
                competenceModel
                globalWeights
                activityObservationWeights
                activityNames
                initialScalarBeliefs
                simulatedLearnerBehaviorTypes
              }
              learner {
                learnerId
                probabilisticBeliefs
                scalarBeliefs
                tendencies
                overAllFinishedActivities
              }
              observations {
                activityName
                activityDifficulty
                activityCorrectness
              }
            }
          }`;
    const args: GraphQLArgs = {
      schema: await getSchema(),
      source: gql,
      contextValue: getContext(),
      variableValues: {
        initialScalarBeliefSetId: 'beginner',
        taskSet: [
          { name: 'Test1', loId: 'activityTw1_1', difficulty: 1 },
          { name: 'Test3', loId: 'activityTw1_1', difficulty: 2 },
        ],
        maxDifficulty: 6,
        minDifficulty: 1,
        stepsDifficulty: 1,
        agentBehavior: 'correct',
        storeResults: false,
        learnerId: id,
      },
    };
    const result = await graphql(args);
    if (result.data != null) {
      const observations = result.data.observations as Observation[];
      const learner = result.data.learner as Learner;
      // Check if all tasks have been recommended 3 times.
      expect(observations.length).toBe(6);
      const beliefs = learner.scalarBeliefs;
      const scalarBeliefs: IProbabilityBeliefBundle = JSON.parse(beliefs[beliefs.length - 1]);
      // Check if TW1_1 belief score has been improved (initial 0.1)
      expect(scalarBeliefs.tw1_1).toBeGreaterThan(0.1);

      const expectedOrder = [1, 2, 1, 2, 1, 2];
      expectedOrder.forEach((value, index) => {
        expectedOrder[index] = calculateDifficulty(1, 6, value, 1);
      });
      const order = observations.map((observation) => observation.activityDifficulty);
      // Test if the correct order was executed
      expect(order).toEqual(expectedOrder);
    } else {
      throw new Error('Data is null');
    }
  });
});
