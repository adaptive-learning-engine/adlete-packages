const jose = require('jose')

const createSecret = async () => {
    const secret = await jose.generateSecret('HS256')
    console.log("Generated Symmetric Key using HS256 with SHA-256. Please store the key")
    const secretKey = Buffer.from(await secret.export()).toString('hex');
      // Path to the text file
    console.log(secretKey)
}

createSecret();