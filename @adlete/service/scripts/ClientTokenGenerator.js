import { readFileSync, unlinkSync, writeFileSync, appendFileSync } from 'fs';

function S4() {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

function guid() {
  return `${S4() + S4()}-${S4() + S4()}-${S4() + S4()}-${S4() + S4()}`.toLocaleUpperCase();
}

try {
  const clientToken = guid();
  const data = readFileSync('.env', 'utf8');
  let lines = data.split('\n');
  lines = lines.filter((line) => line.trim().length > 0);
  unlinkSync('.env');

  let newFile = [];
  for (let line in lines) {
    const curr = lines[line].toString();
    if (!curr.includes('clientToken=')) {
      newFile.push(`${curr}\n`);
    }
    else{
      newFile.push(`clientToken=${clientToken}\n`);
    }
  }
  newFile = newFile.join('');
  writeFileSync('.env', newFile);
  console.log('Successfully updated .env-File!');
} catch (err) {
  console.error('Could not generate token and update .env-File, error message: \n' + err);
}
