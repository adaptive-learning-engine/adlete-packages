# Welcome to the ADLETE Service, [@adlete/service!](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/tree/master/@adlete/service '@adlete/service')

The ADLETE Service wraps the functionality of the [ADLETE - Framework](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Framework 'ADLETE - Framework') into a simple cloud service with predefined endpoints.

Continue your reading in the [ADLETE Service Documentation](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/wikis/ADLETE-Adaptive-Learning-Technology/ADLETE-Service 'ADLETE Service Documentation')

## Tests

This package uses [Apache JMeter]() for Integration and Load Tests. You can find a JMeter project under ./test/jmeter/Adlete.jmx. The first Thread group (JMeter Users) contains a load test for the learner rotes. The second Thread Group (Integration Tests) contains Integrations tests for every Router class.

You will need to add a test user to your Database and write the login data to the Adlete Test Plan variables (clientid, password for clientinstance role and ecucatorid, educatorpassword for educator role).
