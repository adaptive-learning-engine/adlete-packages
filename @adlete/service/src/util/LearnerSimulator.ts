import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { Adjuster } from '@adlete/framework/Adjuster';

export function calculateLearnerCorrectness(
  adjuster: Adjuster,
  adleteConfig: IAdleteConfiguration,
  loId: string,
  difficulty: number,
  stepSize: number
) {
  const names = Object.keys(adleteConfig.activityObservationWeights[loId]);
  const scalarVal = adjuster.scalarBeliefSystem.getBelief(names[0]);
  const probability = calculateProbability(scalarVal.value, difficulty, scalarVal.certainty, stepSize);
  const correctness = Math.random() <= probability ? 1 : 0;
  return correctness;
}

export function calculateProbability(prediction: number, difficulty: number, certainty: number, stepSize: number): number {
  return Math.round((prediction - difficulty) / stepSize) * 0.32 + (0.96 - 0.1 * (1 - certainty));
}

export function calculateCorrectness(maxSteps: number, behavior: string): (step: number) => number {
  switch (behavior) {
    case 'linearDec':
      return (steps: number) => 1 - steps / maxSteps;
    case 'linearAsc':
      return (steps: number) => steps / maxSteps;
    case 'quadraticAsc':
      return (steps: number) => (steps / maxSteps) ** 2;
    case 'quadraticDec':
      return (steps: number) => 1 - (steps / maxSteps) ** 2;
    default:
      return null;
  }
}

export function constantCorrectness(behavior: string): () => number {
  switch (behavior) {
    case 'correct':
      return () => 1;
    case 'incorrect':
      return () => 0;
    case 'random':
      return () => Math.floor(Math.random() * 10) % 2;
    default:
      return null;
  }
}
