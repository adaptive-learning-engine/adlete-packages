
import {createCipheriv, createDecipheriv, createSecretKey, KeyObject, randomBytes} from 'crypto'

import * as bycrypt from 'bcrypt'
import * as jose from 'jose'

let accessSecret: KeyObject 
let refreshSecret: KeyObject
const saltRounds = 10;

export const alg = 'HS256'

export const getAccessSecret = (): KeyObject => {
    if(!accessSecret) {
        accessSecret = createSecretKey(process.env.accessTokenKey, 'hex'); 
    }
    return accessSecret;
}

export const getRefreshSecret = (): KeyObject => {
    if(!refreshSecret) {
        refreshSecret = createSecretKey(process.env.refreshTokenKey, 'hex'); 
    }
    return refreshSecret;
}

// Uses HS256 with SHA-256
export const createSecret = async () => {
    const key = await jose.generateSecret('HS256') as jose.KeyLike
    console.log("Generated Symmetric Key using HS256 with SHA-256. Please store the key")
    console.log(`${key}`)
}

export const hashPassword = async (password: string): Promise<string> => {
    return bycrypt.hash(password, saltRounds)
}

export const comparehashedPassword = async (plainPassword: string, hashedPassword: string): Promise<boolean> => {
    return bycrypt.compare(plainPassword, hashedPassword)
}

export const encryptData = (text: string): {encrypted: string, iv: string} => {
    
    const {encrypted, iv} =  encryptDataWithKey(text, getAccessSecret())
    return {encrypted, iv: iv.toString('hex')}
}

export const decryptData = (encryptedData: string, iv: string): string => {
    return decryptDataWithKey(encryptedData, getAccessSecret(), Buffer.from(iv, 'hex'))
}
const encryptDataWithKey = (text: string, key: KeyObject): {encrypted: string, iv: Buffer} => {
    const iv = randomBytes(16)
    const cipher = createCipheriv('aes-256-cbc', key, iv)
    let encrypted = cipher.update(text, 'utf-8', 'hex')
    encrypted += cipher.final('hex')
    return {encrypted, iv}
}

const decryptDataWithKey = (encryptedData: string, key: KeyObject, iv: Buffer): string => {
    const cipher = createDecipheriv('aes-256-cbc', key, iv)
    let decrypted = cipher.update(encryptedData, 'hex', 'utf-8')
    decrypted += cipher.final('utf-8')
    return decrypted;
}