import { GraphQLError } from 'graphql';

import { roundToPrecision } from '@adlete/engine-blocks/utils/Utils';


export function calculateDifficulty(minDifficulty: number, maxDifficulty: number, difficulty: number, stepSize: number): number {
  if (maxDifficulty <= minDifficulty || stepSize >= (maxDifficulty - minDifficulty) || stepSize <= 0) {
    throw new GraphQLError(`max Difficulty is <= minDifficulty or step Size is greater then ${maxDifficulty - minDifficulty} or stepSize is <= 0`, {
      extensions: {
        code: "BAD_USER_INPUT"
      }
    }) ;
  }
  const precision = Math.round(-Math.log10(stepSize));
  const result = difficulty * (maxDifficulty - minDifficulty) + minDifficulty;
  return roundToPrecision(result, precision);
}

export function calculateNormDifficulty(minDifficulty: number, maxDifficulty: number, difficulty: number) {
  if (maxDifficulty <= minDifficulty) {
    throw new GraphQLError('max Difficulty is <= minDifficulty', {
      extensions: {
        code: "BAD_USER_INPUT"
      }
    }) ;
  }
  return (difficulty - minDifficulty) / (maxDifficulty - minDifficulty);
}
