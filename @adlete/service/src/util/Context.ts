import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

import { IMoodleConfig } from '../entities/MoodleConfiguration';
import { TokenData } from '../entities/User';

import { NetworkScheduler } from './NetworkScheduler';


export interface Context {
  clientToken: string;
  sessionScheduler: NetworkScheduler;
  user?: TokenData
  version?: string;
  adleteConfig: IAdleteConfiguration;
  moodleConfig?: IMoodleConfig;
  serverURL?: string;
}
