import * as fs from 'fs';

export interface IPackageJSON {
  version: string;
}

export function readJSONConfiguration<T>(filePath: string): T {
  try {
    const data = fs.readFileSync(filePath, { encoding: 'utf8' });
    return JSON.parse(data);
  } catch (e) {
    throw new Error(e);
  }
}
