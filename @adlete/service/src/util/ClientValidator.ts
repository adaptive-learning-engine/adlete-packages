
import { type IncomingMessage } from 'http';

import { GraphQLError } from 'graphql';
import * as jose from 'jose'
import { AuthChecker } from 'type-graphql';

import { TokenData } from '../entities/User';

import { Context } from './Context';
import { decryptData } from './SecretGenerator';
import { verifyAccessToken } from './TokenGenerator';

export enum Roles {
  ADMIN,
  CLIENTINSTANCE,
  EDUCATOR
}

export const clientValidator: AuthChecker<Context> = async ({ context }, roles) => {
  if(context.clientToken == null) return false;

  //Check if Admin
  
  try {
    const userData = await verifyAccessToken(context.clientToken);
    context.user = userData;
    if(roles.includes(Roles[Roles.ADMIN])) {
      if(userData.roles.includes(Roles[Roles.ADMIN])) return true;    
    }
    //Check if clientInstance
    if(roles.includes(Roles[Roles.CLIENTINSTANCE])) {
      if(userData.roles.includes(Roles[Roles.CLIENTINSTANCE])) return true;
    }

    //Check if educator
    if(roles.includes(Roles[Roles.EDUCATOR])) {
      if(userData.roles.includes(Roles[Roles.EDUCATOR])) return true;
    }
  } catch (error) {
    // Custom error when expired.
    if( error instanceof jose.errors.JWTExpired) {
      throw new GraphQLError("JWT is expired", {
        extensions: {
          code: 'SESSION_EXPIRED'
        }
      })
    }
    return false;
  }

  return false
};

export const devClientValidator: AuthChecker<Context> = async (resolverData, roles) => {
  if(resolverData.context.clientToken === process.env.devToken) {
    resolverData.context.user = {username: "develop", roles: ["ADMIN"]}
    return true
  }
  return clientValidator(resolverData, roles)
}

export const extractAccessToken = (req: IncomingMessage): string => {
  const authHeader = req.headers.authorization
  if(!authHeader?.startsWith("Bearer ")) {
    return null;
  }

  const accessTokens = authHeader.split(" ")
  return accessTokens[1]
}

export const validateEducatorRights = (idCipher: string, iv: string): string[]  => {
  if(!idCipher) {
    throw new GraphQLError("Educator Role must provide a idCipher.", {
      extensions: {
        code: 'BAD_USER_INPUT'
      }
    })
  }
  let availableLearnerIds: string[] = []
  try {
    availableLearnerIds = JSON.parse(decryptData(idCipher, iv))
  } catch (err) {
    throw new GraphQLError("Malformed idCipher. Cannot parse.", {
      extensions: {
        code: 'BAD_USER_INPUT'
      }
    })
  }

  if(!Array.isArray(availableLearnerIds)) {
    throw new GraphQLError("Malformed idCipher. Cannot parse.", {
      extensions: {
        code: 'BAD_USER_INPUT'
      }
    })
  }

  return availableLearnerIds;
}

export function validateEducatorAccess(user: TokenData, learnerIds: string | string[], idCipher: string) {
  if(user.roles.length == 1 && user.roles[0] === Roles[Roles.EDUCATOR]) {
    if(idCipher === null) {
      throw new GraphQLError(`Educator need to provide an idCipher to access learner Data`, {
        extensions: {
          code: 'FORBIDDEN'
        }
      })
    }

    const availableLearnerIds = validateEducatorRights(idCipher, user.iv)

    if(!Array.isArray(availableLearnerIds)) {
      throw new GraphQLError("Cipher has the wrong format", {
        extensions: {
          code: 'BAD_USER_INPUT'
        }
      })
    }

    let forbidden = true;
    let message: string
    if(Array.isArray(learnerIds)) {
      for (let i = 0; i < learnerIds.length; i++) {
        if(!availableLearnerIds.includes(learnerIds[i])) {
          message = `Not authorized to access learnerId: ${learnerIds[i]}`
          break;
        }
      }
    } else {
      forbidden = !availableLearnerIds.includes(learnerIds)
      message = `Not authorized to access LearnerID: ${learnerIds}`
    }
    if(forbidden) {
      throw new GraphQLError(message, {
        extensions: {
          code: 'FORBIDDEN'
        }
      })
    }
  }
}
