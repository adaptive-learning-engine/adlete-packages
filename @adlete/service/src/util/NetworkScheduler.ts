import { IProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { SessionCache, SessionCacheModel, SessionDocType } from '../entities/SessionCache';
import { DocumentType, isDocument } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { Learner } from '../entities/Learner';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { Session, SessionModel } from '../entities/Session';
import { Mutex, MutexInterface } from 'async-mutex';

export interface BayesNetSession {
  network: BayesBeliefModel;
  currentLearner?: string;
  lastUpdated?: Date;
}

export class NetworkScheduler {
  config: IAdleteConfiguration;
  private networks: Map<string, BayesNetSession> = new Map();
  private mutexes: Map<string, Mutex> = new Map();
  readonly size: number;
  private unassignedNetworks: string[];

  constructor(ids: string[], config: IAdleteConfiguration) {
    this.size = ids.length;
    this.config = config;
    this.unassignedNetworks = ['testid'];
    this.networks.set('testid', { network: new BayesBeliefModel() });
  }

  async initializeSessions() {
    await this.cleanCachedSessions();
    const currentSessions = await SessionCacheModel.find();
    const unassignedLearner: SessionDocType[] = [];
    currentSessions.forEach((value) => {
      const networkObj = this.networks.get(value.network);
      if (networkObj) {
        this.unassignedNetworks.filter((id) => id !== value.network);
        if (!networkObj.lastUpdated || networkObj.lastUpdated > value.timestamp) {
          const beliefs: IProbabilityBeliefBundle = JSON.parse(value.probabilityBelief);
          networkObj.network.setBeliefs(beliefs, true);
          networkObj.currentLearner = value._id;
        }
      } else {
        unassignedLearner.push(value);
      }
    });
    unassignedLearner.forEach(async (session) => {
      const networkId = await this.assignNetwork(JSON.parse(session.probabilityBelief), session._id);
      session.network = networkId;
      await session.save();
    });
  }

  async getNetwork(id: string): Promise<{ session: SessionDocType; release: MutexInterface.Releaser }> {
    const sessionInfo: DocumentType<SessionCache, BeAnObject> = await SessionCacheModel.findById(id);
    if (!sessionInfo) {
      return null;
    }
    let mutex = this.mutexes.get(sessionInfo.network);
    if (!mutex) {
      const networkId = await this.assignNetwork(JSON.parse(sessionInfo.probabilityBelief), sessionInfo._id);
      sessionInfo.network = networkId;
      mutex = this.mutexes.get(sessionInfo.network);
      await sessionInfo.save();
    }
    const release = await mutex.acquire();
    const bayesNet = this.networks.get(sessionInfo.network);
    sessionInfo.bayesNetwork = bayesNet;
    if (id === bayesNet.currentLearner && bayesNet.lastUpdated?.getTime() === sessionInfo.timestamp?.getTime()) {
      return { session: sessionInfo, release };
    }
    const beliefs: IProbabilityBeliefBundle = JSON.parse(sessionInfo.probabilityBelief);
    bayesNet.network.setBeliefs(beliefs, true);
    bayesNet.currentLearner = id;

    return { session: sessionInfo, release };
  }

  async createSession(learner: Learner, session: Session) {
    const beliefs = JSON.parse(learner.probabilisticBeliefs[learner.probabilisticBeliefs.length - 1]);
    const networkId = await this.assignNetwork(beliefs, learner.learnerId);
    const currentSession = new SessionCacheModel({
      _id: learner.learnerId,
      session: session,
      learnerObjectId: learner._id,
      activityStatistics: [],
      network: networkId,
      probabilityBelief: learner.probabilisticBeliefs[learner.probabilisticBeliefs.length - 1],
      scalarBelief: learner.scalarBeliefs[learner.scalarBeliefs.length - 1],
      tendency: learner.tendencies[learner.tendencies.length - 1],
      overallFinishedActivities: learner.overAllFinishedActivities ?? 0,
    });
    await currentSession.save();
  }

  async deleteSession(learnerId: string): Promise<SessionCache> {
    const cache = await SessionCacheModel.findByIdAndDelete(learnerId);
    if (cache) {
      const hasTree = await SessionModel.findOne({ network: cache.network });
      if (!hasTree) {
        const network = this.networks.get(cache.network);
        if (network.currentLearner === cache.network) {
          network.currentLearner = null;
          network.lastUpdated = null;
        }
      }
      return cache;
    }
    return null;
  }

  async cleanCachedSessions() {
    const currentDate = new Date();
    const outdatedSessions = await SessionCacheModel.find({ updatedAt: { $lt: new Date(currentDate.getTime() - 12 * 60 * 60 * 1000) } });
    let toDelete: string[] = [];
    outdatedSessions.forEach(async (cachedSession) => {
      toDelete.push(cachedSession._id);
      await cachedSession.populate('session');
      if (isDocument(cachedSession.session)) {
        cachedSession.session.activitiesStatistics = cachedSession.activityStatistics;
        await cachedSession.session.save();
      }
    });
    const deleteResults = await SessionCacheModel.deleteMany({ _id: { $in: toDelete } });
    console.log(`${currentDate.toLocaleTimeString('de-DE')}: Deleted ${deleteResults.deletedCount} outdates Sessions`);
    const mapKeys = this.networks.keys();
    for (const key of mapKeys) {
      const result = await SessionCacheModel.findOne({ network: key });
      if (!result) {
        const network = this.networks.get(key);
        network.currentLearner = null;
        network.lastUpdated = null;
      }
    }
  }

  async assignNetwork(beliefs: IProbabilityBeliefBundle, learnerId: string): Promise<string> {
    if (this.unassignedNetworks.length > 0) {
      return this.unassignedNetworks.pop();
    }
    let countDocs = await SessionCacheModel.aggregate<{ _id: string; count: number }>([
      {
        $match: { network: { $in: Array.from(this.networks.keys()) } },
      },
      {
        $group: {
          _id: '$network',
          count: { $sum: 1 },
        },
      },
      { $sort: { count: 1 } },
    ]);
    if (countDocs.length == 0) {
      this.networks.clear();
      return this.assignNetwork(beliefs, learnerId);
    }
    return countDocs[0]._id;
  }

  /**
   * Will reset the network to the last probability Beliefs of the session
   * @param session The current session
   */
  resetNetwork(session: SessionCache) {
    const networkContainer = this.networks.get(session.network);
    networkContainer.network.setBeliefs(JSON.parse(session.probabilityBelief), true);
  }
}
