import * as jose from 'jose'
import { AuthenticationError } from 'type-graphql';

import { TokenData } from '../entities/User';

import { alg, getAccessSecret, getRefreshSecret } from './SecretGenerator';


const audiance = process.env.audiance ?? 'adlete:clientinstance';
const issuer = process.env.issuer ?? 'htw:creativemedia:adlete';

export async function generateAccessToken(data: TokenData, timeout:string|number = "30min"): Promise<string> {
    return await new jose.SignJWT(data)
    .setProtectedHeader({alg})
    .setExpirationTime(timeout)
    .setIssuer(issuer)
    .setAudience(audiance)
    .sign(getAccessSecret());
}

export async function generateRefreshToken(data: TokenData): Promise<string> {
    return await new jose.SignJWT(data)
    .setProtectedHeader({alg})
    .setExpirationTime('24h')
    .setIssuer(issuer)
    .setAudience(audiance)
    .sign(getRefreshSecret());
}
    
export async function verifyAccessToken(jsonWebToken: string): Promise<TokenData> {
    const {payload} = await jose.jwtVerify<TokenData>(jsonWebToken, getAccessSecret(), {
        audience: audiance,
        issuer: issuer 
    }) ;
    return payload;
}

export async function refreshExpiredAccessToken(accessToken: string, refreshToken: string): Promise<string> {
    let parsedPayload: TokenData;
    // Check if Access Token is still valid
    try {
        parsedPayload = await verifyAccessToken(accessToken)
    } catch (error) {
        if(!(error instanceof jose.errors.JWTExpired)) {
            throw error;
        }
        parsedPayload = error.payload as TokenData
    }

    const {payload} = await jose.jwtVerify<TokenData>(refreshToken, getRefreshSecret(), {
        audience: audiance,
        issuer: issuer
    });

    if(payload.username !== parsedPayload.username) {
        throw new AuthenticationError("Access and refresh token mismatch");
    }
    return await generateAccessToken(payload)
}