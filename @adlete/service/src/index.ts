/* eslint-disable @typescript-eslint/no-shadow */
/* eslint no-console:off */
import 'reflect-metadata'; // must be the first import!
import * as fs from 'fs';
import * as cluster from 'node:cluster';
import { cpus } from 'node:os';
import * as process from 'node:process';

import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';
import dotenv from 'dotenv';
import { ObjectId } from 'mongodb';
import mongoose from 'mongoose';
import { buildSchema } from 'type-graphql';

import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

import { IMoodleConfig } from './entities/MoodleConfiguration';
import { createAdminUser } from './entities/User';
import { AuthentificationResolver } from './resolvers/AuthentificationResolver';
import { LearnerAnalyticsResolver } from './resolvers/LearnerAnalyticsResolver';
import { LearnerResolver } from './resolvers/LearnerResolver';
import { MoodleResolver } from './resolvers/MoodleResolver';
import { ObservationResolver } from './resolvers/ObservationResolver';
import { RecommendationResolver } from './resolvers/RecommendationResolver';
import { SessionResolver } from './resolvers/SessionResolver';
import { SessionStatisticsResolver } from './resolvers/SessionStatisticsResolver';
import { SimulationResolver } from './resolvers/SimulationResolver';
import { StatusInfoResolver } from './resolvers/StatusInfoResolver';
import { VisualizerResolver } from './resolvers/VisualizerResolver';
import { clientValidator, devClientValidator, extractAccessToken } from './util/ClientValidator';
import { Context } from './util/Context';
import { IPackageJSON, readJSONConfiguration } from './util/JsonConfigurationImporter';
import { ObjectIdScalar } from './util/ObjectIdScalar';
import { TypegooseMiddleware } from './util/TypegooseMiddleware';

// const require = createRequire(import.meta.url);

// load .env-File into
dotenv.config();

// load package.json for version number
const packageJson = readJSONConfiguration<IPackageJSON>('./package.json');
const coreNumber = parseInt(process.env.cores) ?? 4;
const cpuNum = coreNumber <= cpus().length ? coreNumber : cpus().length;
async function bootstrap() {
  try {
    let db: string;
    if (process.env.mongoURI_FILE) {
      db = fs.readFileSync(process.env.mongoURI_FILE, 'utf8') as string;
    } else {
      db = process.env.mongoURI;
    }

    const { adleteConfigPath, port, moodleConfigPath /* , development */ } = process.env;

    if (db == null || adleteConfigPath == null) {
      throw new Error('Missing .env-File, check out ReadMe.md for further instructions.\n');
    }

    // load adleteconfig
    const adleteConfig = readJSONConfiguration<IAdleteConfiguration>(adleteConfigPath);

    // load moodle configuration if .env-file defines a path
    let moodleConfig: IMoodleConfig;
    if (moodleConfigPath !== null && moodleConfigPath !== undefined) {
      try {
        moodleConfig = readJSONConfiguration<IMoodleConfig>(moodleConfigPath);
      } catch (error) {
        console.error('No Moodle Configuration found or an error occurred while reading');
      }
    }

    // sort activityNames for faster
    adleteConfig.activityNames = adleteConfig.activityNames.sort();
    // create mongoose connection
    await mongoose.connect(db, { family: 4 });

    BayesBeliefModel.createBayesNet(adleteConfig.competenceModel);
    let authentificationFunction = clientValidator;
    if(process.env.NODE_ENV !== "production" && process.env.devToken) {
      authentificationFunction = devClientValidator;
    }
    // build TypeGraphQL executable schema
    const schema = await buildSchema({
      resolvers: [
        StatusInfoResolver,
        LearnerResolver,
        SessionResolver,
        SessionStatisticsResolver,
        ObservationResolver,
        RecommendationResolver,
        LearnerAnalyticsResolver,
        SimulationResolver,
        MoodleResolver,
        AuthentificationResolver,
        VisualizerResolver
      ],
      emitSchemaFile: './build/schema.gql',
      // use document converting middleware
      globalMiddlewares: [TypegooseMiddleware],
      // use ObjectId scalar mapping
      scalarsMap: [{ type: ObjectId, scalar: ObjectIdScalar }],
      authChecker: authentificationFunction,
    });
    //const plugins: PluginDefinition[] = [];

    // TODO: Find new Logger Plugin
    // const IsDevelopmentModeEnabled = development !== undefined ? development.toLocaleLowerCase() === 'true' : false;
    // if (IsDevelopmentModeEnabled === true) {
    // }

    // Create GraphQL server
    const server = new ApolloServer<Context>({
      schema,

    });

    // Start the server
    const { url } = await startStandaloneServer(server, {
      listen: {
        port: parseInt(port) || 4000,
      },
      context: async ({ req }) => {
        const ctx: Context = {
          clientToken: extractAccessToken(req),
          version: packageJson.version,
          adleteConfig,
          moodleConfig,
          sessionScheduler: null,
          serverURL: new URL(process.env.adleteurl ?? 'http://localhost:5001/').toString()
        };
        return ctx;
      },
    });

    console.log(`Server is running, GraphQL Playground available at ${url}`);
    if(process.env.NODE_ENV !== 'production') {
      console.log('The server is not run in production mode. The service is NOT secure! To enable production mode set the NODE_ENV to production and restart the service.')
    }
  } catch (err) {
    console.error(err);
  }
}

if (cluster.default.isPrimary) {
  console.log(`Number of running processes is ${cpuNum}`);
  console.log(`Master ${process.pid} is running`);

  if (cpuNum == 1) {
    bootstrap();
    createAdminUser();
  } else {
    // Create an Admin User from env
    let db: string;
    if (process.env.mongoURI_FILE) {
      db = fs.readFileSync(process.env.mongoURI_FILE, 'utf8') as string;
    } else {
      db = process.env.mongoURI;
    }
  
    if (db == null) {
      throw new Error('Missing .env-File, check out ReadMe.md for further instructions.\n');
    }
    mongoose.connect(db, { family: 4 })
    .then(() => createAdminUser())
    .then(() => {
      return mongoose.disconnect()
    })
    .then(() => console.log("Disconnected Primary from db"))
    for (let i = 0; i < cpuNum; i++) {
      const worker = cluster.default.fork();
      console.log(`Spawned worker with PID: ${worker.process.pid} `);
    }

    cluster.default.on('exit', (worker) => {
      console.log(`worker ${worker.process.pid} died`);
      console.log("Let's fork another worker!");
    });
  }
} else {
  bootstrap();
}
