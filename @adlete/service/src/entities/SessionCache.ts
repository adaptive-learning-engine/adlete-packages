import { DocumentType, ReturnModelType, getModelForClass, prop } from '@typegoose/typegoose';
import { BeAnObject, Ref } from '@typegoose/typegoose/lib/types';
import { ActivityStatistics } from './ActivityStatistics';
import { ObjectId } from 'mongodb';
import { Session } from './Session';
import { BayesNetSession } from '../util/NetworkScheduler';

export class SessionCache {
  @prop({ required: true })
  public _id!: string;

  @prop({ required: true })
  public learnerObjectId: ObjectId;

  @prop({ required: true, ref: () => ActivityStatistics, default: [] })
  public activityStatistics: Ref<ActivityStatistics>[];

  @prop({ required: true, ref: () => Session })
  public session: Ref<Session>;

  @prop({ required: true, index: true })
  public network!: string;

  @prop({ require: true })
  public tendency: string;

  @prop({ required: true })
  public probabilityBelief: string;

  @prop({ required: true })
  public scalarBelief: string;

  @prop({ required: true })
  overallFinishedActivities: number;

  @prop()
  public timestamp?: Date;

  bayesNetwork?: BayesNetSession;
}

export const SessionCacheModel: ReturnModelType<typeof SessionCache> = getModelForClass(SessionCache, {
  schemaOptions: { timestamps: true },
});

export type SessionDocType = DocumentType<SessionCache, BeAnObject>;
