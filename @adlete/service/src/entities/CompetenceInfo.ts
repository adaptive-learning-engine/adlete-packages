import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export abstract class CompetenceInfo {
  @Field({ nullable: false, description: 'Holds the timeline of scalar beliefs of this learner.' })
  scalarBeliefs: string;
}
