import { Field, ObjectType } from 'type-graphql';

import { Learner } from './Learner';
import { Observation } from './Observation';
import { ServiceConfiguration } from './ServiceConfiguration';

@ObjectType()
export class LearnerAnalytics {
  @Field(() => ServiceConfiguration, { nullable: false, description: 'Holds the adaptive difficulty adjustment configuration.' })
  serviceConfiguration: ServiceConfiguration;

  @Field(() => Learner, { nullable: false, description: 'Holds a reference to a learner or simulated agent.' })
  learner: Learner;

  @Field(() => [Observation], { nullable: false, description: 'Holds recommended activity and result observations.' })
  observations: Observation[];
}
