import { Field, Int, ObjectType } from 'type-graphql';

import { IServerStatusInfo, IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import { verifyAccessToken } from '../util/TokenGenerator';

@ObjectType()
export class StatusInfo implements IStatusInfo {
  @Field(() => Int, { description: 'Holds the Error code number.' })
  statusCode: number;

  @Field(() => String, { nullable: true, description: 'Contains an error description.' })
  statusDescription?: string;

  @Field(() => Date, { nullable: true, description: 'Contains a timestamp when the error occurs.' })
  timestamp?: Date;

  constructor(statusCode: number, statusDescription?: string) {
    if (!Number.isInteger(statusCode)) {
      // eslint-disable-next-line no-console
      console.log('Warning: You are using an floating point code number, you must use integer. Your given value will be rounded.');
    }

    this.statusCode = Math.trunc(statusCode);
    this.statusDescription = statusDescription;
    this.timestamp = new Date();
  }
}

@ObjectType()
export class ServerStatusInfo extends StatusInfo implements IServerStatusInfo {
  @Field(() => Boolean, {description: "is user authorized"})
  authenticated: boolean

  constructor(statusCode: number, statusDescription: string) {
    super(statusCode, statusDescription)
    this.authenticated = false;
  }

  async verifyUser(token: string): Promise<boolean> {
    try {
      this.authenticated = await verifyAccessToken(token) != null;
      return this.authenticated;
    } catch(e) {
      this.authenticated = false;
      return false
    }
  }
}
