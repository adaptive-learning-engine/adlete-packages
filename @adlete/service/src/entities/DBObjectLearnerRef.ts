import typegoose from '@typegoose/typegoose';
import type { Ref } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';
import { Field, ObjectType } from 'type-graphql';

import { DBObject } from './DBObject';
import { Learner } from './Learner';

const Property = typegoose.prop;

@ObjectType()
export abstract class DBObjectLearnerRef extends DBObject {
  @Field(() => Learner, { nullable: false, description: 'Holds a reference to the performer of the activity.' })
  @Property({ ref: 'Learner', required: true })
  learner: Ref<Learner> | ObjectId;
}
