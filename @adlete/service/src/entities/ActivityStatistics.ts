import typegoose from '@typegoose/typegoose';
import type { DocumentType } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { Field, ObjectType } from 'type-graphql';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';

import { DBObjectLearnerRef } from './DBObjectLearnerRef';

const Property = typegoose.prop;
const { getModelForClass } = typegoose;

@ObjectType()
export class ActivityStatistics extends DBObjectLearnerRef implements IAccumulatedActivityStatistics {
  @Field(() => String, { nullable: false, description: 'Identifies the given accumulated activity statistics of the activity type.' })
  @Property({ required: true })
  activityName: ActivityName;

  @Field(() => Number, { nullable: false, description: 'Holds the information how often the given activity type was played.' })
  @Property({ required: true, default: 0 })
  activitiesPlayed: number;

  @Field(() => Number, { nullable: false, description: 'Holds the cumulative sum of all observation results.' })
  @Property({ required: true, default: 0 })
  sumAnswersCorrectness: number;

  @Field(() => Number, { nullable: false, description: 'Holds the ongoing average: sum of activityCorrectness / activitiesPlayed ' })
  @Property({ required: true, default: 0 })
  avgAnswersCorrectness: number;

  constructor() {
    super();
    this.activitiesPlayed = 0;
    this.sumAnswersCorrectness = 0;
    this.avgAnswersCorrectness = 0;
  }
}
export const ActivityStatisticsModel = getModelForClass(ActivityStatistics, {
  schemaOptions: { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } },
});

export type ActivityStatisticsDocType = DocumentType<ActivityStatistics, BeAnObject>;
