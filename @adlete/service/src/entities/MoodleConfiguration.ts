import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class MoodleConfig {
  @Field({ description: 'Version of Config file' })
  version: string;
  @Field({ description: 'A json string of the moodle config' })
  config: string;
}

export interface IMoodleDBConfig {
  select: Record<string, Object[]>;
  delete: Record<string, Object[]>;
  insert: Record<string, Object[]>;
}

export interface IMoodleConfig {
  version: string;
  config: IMoodleDBConfig;
}
