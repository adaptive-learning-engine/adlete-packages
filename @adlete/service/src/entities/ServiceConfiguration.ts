import { Field, ObjectType } from 'type-graphql';

import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

@ObjectType()
export class ServiceConfiguration {
  @Field(() => String, { nullable: false, description: 'Contains the general competence model structure as JSON-string.' })
  competenceModel: string;

  @Field(() => String, { nullable: false, description: 'Global weights to update competence model by a given observation JSON-string.' })
  globalWeights: string;

  @Field(() => String, { nullable: false, description: 'Holds a list of activity names as JSON-string.' })
  activityNames: string;

  @Field(() => String, { nullable: false, description: 'Activity observations weights as JSON-string.' })
  activityObservationWeights: string;

  @Field(() => String, { nullable: false, description: 'Holds all weights for the different activity observations' })
  initialScalarBeliefs: string;

  @Field(() => String, { nullable: false, description: 'Holds all weights for the different activity observations' })
  simulatedLearnerBehaviorTypes: string;

  constructor(adleteconfig: IAdleteConfiguration) {
    this.competenceModel = JSON.stringify(adleteconfig.competenceModel);
    this.globalWeights = JSON.stringify(adleteconfig.globalWeights);
    this.activityNames = JSON.stringify(adleteconfig.activityNames);
    this.activityObservationWeights = JSON.stringify(adleteconfig.activityObservationWeights);
    this.initialScalarBeliefs = JSON.stringify(adleteconfig.initialScalarBeliefs);
    this.simulatedLearnerBehaviorTypes = JSON.stringify(adleteconfig.simulatedLearnerBehaviorTypes);
  }
}
