import typegoose from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';
import { Field, ObjectType } from 'type-graphql';

const Property = typegoose.prop;

@ObjectType()
export abstract class DBObject {
  @Field({ nullable: false, description: 'Holds the database entry id.' })
  readonly _id: ObjectId;

  @Field(() => Date, { description: 'Holds the actual timestamp when the object was created within the database.' })
  @Property()
  createdAt?: Date;

  @Field(() => Date, { description: 'Holds the actual timestamp when the object was the last time updated within the database.' })
  @Property()
  updatedAt?: Date;
}
