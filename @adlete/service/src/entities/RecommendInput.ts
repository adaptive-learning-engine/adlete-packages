import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class RecommendInput {
  @Field()
  learnerId: string;

  @Field(() => [String], {
    nullable: true,
    description: 'information about available activities within your current learning environment.',
  })
  activitySubset?: string[] | null;
}
