import { Field, Int, ObjectType } from "type-graphql";

@ObjectType()
export class VisualizerPayload {
    @Field(() => String!, {description: "URL to the visualizer. Includes path variables"})
    url: string
    @Field(() => Int, {description: "Time until token expires in seconds"})
    expireTime: number

    constructor(url: string, expireTime: number) {
        this.url = url
        this.expireTime = expireTime
    }
} 