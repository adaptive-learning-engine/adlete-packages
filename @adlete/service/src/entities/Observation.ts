import typegoose, { ReturnModelType } from '@typegoose/typegoose';
import { Field, Float, InputType, ObjectType } from 'type-graphql';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';

import { DBObjectLearnerRef } from './DBObjectLearnerRef';
import { IObservation } from '@adlete/framework/IObservation';

const Property = typegoose.prop;
const { Index } = typegoose;
const { getModelForClass } = typegoose;

@ObjectType()
@Index({ createdAt: 1 })
export class Observation extends DBObjectLearnerRef implements IObservation {
  @Field(() => String, { description: 'Defines the activity type which was performed.' })
  @Property({ required: true })
  activityName: ActivityName;

  @Field(() => Float, { description: 'Holds the activities result normalized between 0-1.' })
  @Property()
  activityCorrectness: number;

  @Field(() => Float, { description: 'Holds the actual difficulty value (compared to difficulty given) of the activities between 0-1.' })
  @Property()
  activityDifficulty: number;

  @Field(() => Date, {
    nullable: true,
    description: 'Holds the timestamp when the activities was played. This is not sync. with server time!',
  })
  @Property()
  timestamp?: Date;

  @Field(() => String, { nullable: true, description: 'Use this field to store additional information for your specific needed analysis.' })
  @Property()
  additionalInfos?: string;
}

export const ObservationModel: ReturnModelType<typeof Observation> = getModelForClass(Observation, {
  schemaOptions: { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } },
});

@InputType()
export class ObservationInput {
  @Field(() => String, { nullable: false, description: 'Must contain the learner ID of the performer of this activity.' })
  learnerId: string;

  @Field(() => String, { nullable: false, description: 'Must contain the name/type of the activity.' })
  activityName: ActivityName;

  @Field(() => Float, { nullable: false, description: 'Holds the activities result normalized between 0-1.' })
  activityCorrectness: number;

  @Field(() => Float, { nullable: false, description: 'Holds the actual difficulty level of the given activities (0-1).' })
  activityDifficulty: number;

  @Field(() => Date, { description: 'Holds the timestamp when the activities was played. Note: This is not sync. with server time!' })
  timestamp?: Date;

  @Field(() => String, { nullable: true, description: 'Use this field to store additional information for your specific needed analysis.' })
  additionalInfos?: string;
}
