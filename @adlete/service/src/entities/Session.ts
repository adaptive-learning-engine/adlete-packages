import typegoose from '@typegoose/typegoose';
import type { Ref } from '@typegoose/typegoose';
import { BeAnObject, DocumentType } from '@typegoose/typegoose/lib/types';
import { Field, ObjectType } from 'type-graphql';

import { ActivityStatistics } from './ActivityStatistics';
import { DBObjectLearnerRef } from './DBObjectLearnerRef';

const Property = typegoose.Prop;
const { getModelForClass } = typegoose;

@ObjectType()
export class Session extends DBObjectLearnerRef {
  @Field(() => [ActivityStatistics], { nullable: false, description: 'Holds the references to each activity type stat.' })
  @Property({ ref: 'ActivityStatistics', required: true, default: [] })
  activitiesStatistics: Ref<ActivityStatistics>[];

  @Field(() => Boolean, { description: 'Holds the information which of the existing sessions is active.' })
  @Property({ required: true })
  active: boolean;

  @Field(() => Date, { nullable: false, description: 'Holds the timestamp when the session was started.' })
  @Property({ required: true })
  startTimestamp: Date;

  @Field(() => Date, { nullable: true, description: 'Holds the timestamp when the session was stopped.' })
  @Property({ required: false })
  stopTimestamp?: Date;
}

export const SessionModel = getModelForClass(Session, {
  schemaOptions: { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } },
});

export type SessionDocType = DocumentType<Session, BeAnObject>;
