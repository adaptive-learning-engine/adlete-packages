import { DocumentType, getModelForClass, prop } from "@typegoose/typegoose";
import { BeAnObject } from "@typegoose/typegoose/lib/types";
import { Field, InputType } from "type-graphql";

import { hashPassword } from "../util/SecretGenerator";

import { DBObject } from "./DBObject";

export type TokenData = {
    username: string,
    roles: string[],
    iv?: string
}

@InputType()
export class User extends DBObject {
    @Field(() => String, {nullable: false})
    @prop({required: true, index: {unique: true}})
    username: string

    @Field(() => String, {nullable: false})
    @prop({required: true})
    password: string

    @Field(() => String, {nullable: false})
    @prop({required: true})
    email: string

    @Field(() => String, {nullable: true})
    @prop({required: false})
    domain?: string

    @Field(() => [String], {nullable: false})
    @prop({type: () => [String], required: true})
    roles: string[]
} 

export const UserModel = getModelForClass(User, {
    schemaOptions: { timestamps: true}
})

export type UserDocType = DocumentType<User, BeAnObject>

export const createAdminUser = async (): Promise<boolean> => {
    return UserModel.findOne({username: 'admin'})
    .then((userModel) => {
      if(!userModel) {
        return hashPassword(process.env.initialAdminPassword)
        .then((hashedPassword) => {
          const adminUser = new UserModel({
            username: 'admin',
            password: hashedPassword,
            email: process.env.initialAdminEmail ?? "example@example.com",
            roles: ['ADMIN']
          })
          return adminUser.save().then((doc) => {
            console.log(`Created new Admin with id ${doc._id}`)
            return true;
          });
        })
      } else {
        console.log("Admin is initilized")
        return false;
      }
  })
}