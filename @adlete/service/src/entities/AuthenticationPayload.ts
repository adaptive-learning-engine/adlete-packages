import { Field, Int, ObjectType } from 'type-graphql';

@ObjectType()
export class AuthenticationPayload {
  @Field(() => Int!, { description: 'Holds the Error code number.' })
  statusCode: number;

  @Field(() => String, { nullable: true, description: 'Contains an jwt token.' })
  accessToken?: string;

  @Field(() => String, {nullable: true, description: 'Token to refresh the access Token'})
  refreshToken?: string

  constructor(statusCode: number, accessToken?: string, refreshToken?: string) {
    if (!Number.isInteger(statusCode)) {
      // eslint-disable-next-line no-console
      console.log('Warning: You are using an floating point code number, you must use integer. Your given value will be rounded.');
    }

    this.statusCode = Math.trunc(statusCode);
    this.accessToken = accessToken;
    this.refreshToken = refreshToken
  }
  
}
