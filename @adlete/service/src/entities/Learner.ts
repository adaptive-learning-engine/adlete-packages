import typegoose from '@typegoose/typegoose';
import type { DocumentType } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { Field, ObjectType } from 'type-graphql';

import { AdjusterFactory } from '@adlete/framework/AdjusterFactory';
import { ISerializableAdjuster } from '@adlete/framework/ISerializableAdjuster';

import { DBObject } from './DBObject';

const Property = typegoose.Prop;
const { Index } = typegoose;
const { getModelForClass } = typegoose;

@ObjectType()
// creates an Index for learnerId. 1 = orders this index in an ascending way. unique = true checks on insert if learnerId is unique
@Index({ learnerId: 1 }, { unique: true })
// pre will be called before the entity is stored to database. adjuster values are serialized to reinitialize adjuster correctly
/*@pre<Learner>('save', function serialize() {
  this.serializeAdjuster();
})*/
export class Learner extends DBObject {
  @Field(() => String, { nullable: false, description: 'Holds a unique learner ID. This id is exclusively used by all API calls.' })
  @Property({ required: true })
  public learnerId: string;

  @Field(() => [String], { nullable: false, description: 'Holds the timeline of probabilistic beliefs of this learner.' })
  @Property({ type: [String], required: true })
  probabilisticBeliefs: string[];

  @Field(() => [String], { nullable: false, description: 'Holds the timeline of scalar beliefs of this learner.' })
  @Property({ type: [String], required: true })
  scalarBeliefs: string[];

  @Field(() => [String], { nullable: false, description: 'Holds the timeline predicated performance tendencies of this learner.' })
  @Property({ type: [String], required: true })
  tendencies: string[];

  /** ***** Global States ****** */
  @Field(() => Number, { description: 'Holds the amount of all finished activities' })
  @Property({ type: Number, default: 0 })
  overAllFinishedActivities?: number;
  /** *****              ***** */
  _factory: AdjusterFactory = null;
  // Getter for the virtual property
  public get factory(): AdjusterFactory {
    if (!this._factory) {
      this._factory = new AdjusterFactory();
    }
    return this._factory;
  }

  // Setter for the virtual property
  public set factory(value: AdjusterFactory) {
    this._factory = value;
  }

  public serializeAdjuster(adjuster: ISerializableAdjuster): void {
    const serialized = adjuster.serializeBeliefs();
    this.probabilisticBeliefs.push(serialized.probabilisticBeliefs);
    this.scalarBeliefs.push(serialized.scalarBeliefs);
    this.tendencies.push(serialized.tendencies);
  }
}
export const LearnerModel = getModelForClass(Learner, {
  schemaOptions: { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } },
});

export type LearnerDocType = DocumentType<Learner, BeAnObject>;
