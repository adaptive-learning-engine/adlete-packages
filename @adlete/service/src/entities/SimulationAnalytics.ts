import { ArgsType, Field, Float, InputType, Int, ObjectType } from 'type-graphql';

import { LearnerAnalytics } from './LearnerAnalytics';

@ObjectType()
export class SimulationAnalytics extends LearnerAnalytics {
  @Field(() => [String], { nullable: false, description: 'Holds the timeline of the agent behavior of this virtual learner.' })
  agent: string[];
}

@InputType()
export class Task {
  @Field(() => String)
  name: string;

  @Field(() => String)
  loId: string;

  @Field(() => Float)
  difficulty: number;
}

@ArgsType()
export class BaseSimulationArgs {
  
  @Field({ nullable: true, defaultValue: 'random' })
  behavior: string;
  
  @Field({ nullable: true, defaultValue: false, description: 'If simulation results should be saved in DB' })
  storeResults: boolean;
  
  @Field({nullable: true})
  initialScalarBeliefSetId?: string;
  
  @Field({ nullable: true, description: 'learnerId created for simulation' })
  learnerId?: string;
  
  @Field({nullable: true, description: ''})
  idCipher?: string
}

@ArgsType()
export class SimpleSimulationArgs extends BaseSimulationArgs{
  
  @Field(() => Int)
  simulationSteps: number

  @Field(() => Float, { nullable: true, description: 'The static difficulty of every task.' })
  difficulty?: number

  @Field(() => [String], { nullable: true, description: 'Activities that should be trained.' })
  activities?: string[]

}

@ArgsType()
export class AdvSimulationArgs extends BaseSimulationArgs{
  @Field(() => [Task], { description: 'The task that are simulated.' })
  taskSet: Task[];

  @Field(() => Float, { description: 'The minimum difficulty of a task' })
  maxDifficulty: number;

  @Field(() => Float, { description: 'The maximum difficulty of a task' })
  minDifficulty: number;

  @Field(() => Float, { description: 'Steps between difficulties' })
  stepsDifficulty: number;

  @Field(() => Int, { nullable: true, defaultValue: 3, description: 'How often should the task be answered correctly before removal' })
  maxAnsweredCorrect?: number;
}
