import typegoose from '@typegoose/typegoose';
import { Field, Float, ObjectType } from 'type-graphql';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';

import { DBObjectLearnerRef } from './DBObjectLearnerRef';

const Property = typegoose.prop;
const { getModelForClass } = typegoose;

@ObjectType()
export class Recommendation extends DBObjectLearnerRef implements IActivityRecommendation<number> {
  @Field(() => String, { nullable: false, description: 'Contains name of recommended next activity' })
  @Property({ required: true })
  activityName: ActivityName;

  @Field(() => Float, { nullable: false, description: 'Holds a scalar difficulty value for the next recommended activity' })
  @Property({ required: true })
  difficulty: number;

  @Field(() => Float, { nullable: false, description: 'Holds a trend value that is negative or positive based on the past performance.' })
  @Property({ required: true })
  trend: number;
}

export const RecommendationModel = getModelForClass(Recommendation, {
  schemaOptions: { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } },
});
