import { GraphQLError } from 'graphql';
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';

import { AuthenticationPayload } from '../entities/AuthenticationPayload';
import { StatusInfo } from '../entities/StatusInfo';
import { User, UserModel } from '../entities/User';
import { Context } from '../util/Context';
import { comparehashedPassword, hashPassword } from '../util/SecretGenerator';
import { generateAccessToken, generateRefreshToken, refreshExpiredAccessToken } from '../util/TokenGenerator';

@Resolver()
export class AuthentificationResolver {
    @Query(() => AuthenticationPayload, {nullable: false})
    async login(
        @Arg('username', {nullable: false}) username: string,
        @Arg('password', {nullable: false}) password: string
    ) {
        const clientInstace = await UserModel.findOne({username: username})
        if(clientInstace) {
            if(await comparehashedPassword(password, clientInstace.password)) {
                const accessToken = await generateAccessToken({username: clientInstace.username, roles: clientInstace.roles})
                const refreshToken = await generateRefreshToken({username: clientInstace.username, roles: clientInstace.roles})
                return new AuthenticationPayload(200, accessToken, refreshToken)
            }
        }
        throw new GraphQLError("Username or password are incorrect", {
            extensions: {
                code: "UNAUTHORIZED"
            }
        });
        
    }

    @Query(() => AuthenticationPayload, {nullable: false})
    async refreshAccessToken(
        @Ctx() {clientToken}: Context,
        @Arg('refreshToken', {nullable: false}) refreshToken: string
    ) {
        if(!clientToken) {
            throw new GraphQLError("No Bearer Token provided in Authorization Header.", {
                extensions: {
                    code: "BAD_USER_INPUT"
                }
            })
        }
        try {
            const accessToken = await refreshExpiredAccessToken(clientToken, refreshToken);
            return new AuthenticationPayload(200, accessToken, refreshToken)
        } catch(e) {
            throw new GraphQLError(e.message, {
                extensions: {
                    code: "UNAUTHORIZED"
                }
            })
        }
    }
    
    @Authorized('ADMIN')
    @Mutation(() => StatusInfo)
    async createUser(
        @Arg('user') clientInstance: User
    ) {
        const hasEntry = UserModel.findOne({username: clientInstance.username})
        if(!hasEntry) {
            throw new GraphQLError(`Username ${clientInstance.username} already exists`, {
                extensions: {
                    code: 'BAD_USER_INPUT'
                }
            });
            
        }
        clientInstance.password = await hashPassword(clientInstance.password)
        const clientInstanceModel = new UserModel(clientInstance)
        const idDoc = await clientInstanceModel.save();
        return new StatusInfo(201, `Created new User ${clientInstance.username} with id ${idDoc._id}`)
    }

    @Authorized('ADMIN')
    @Mutation(() => StatusInfo)
    async deleteUser(
        @Arg('username', {nullable: false}) username: string
    ) {
        const result = await UserModel.deleteOne({username: username})
        return new StatusInfo(200, `Deleted ${result.deletedCount} ClientInstance with username ${username}`)
    }

    @Authorized('ADMIN')
    @Mutation(() => StatusInfo)
    async updateUser(
        @Arg('username') username: string,
        @Arg('user') updatedUser: User
    ) {
        updatedUser.password = await hashPassword(updatedUser.password)
        const response = await UserModel.findOneAndUpdate({username: username}, updatedUser)
        if(response === null) {
            throw new GraphQLError(`User with username ${username} not found.`,{
                extensions: {
                    code: "NOT_FOUND"
                }
            })
        }
        return new StatusInfo(200, `Updated user with learerId ${username}`)
    }

    
    @Authorized('ADMIN', 'CLIENTINSTANCE')
    @Mutation(() => StatusInfo)
    async updatePassword(
        @Ctx() {user}: Context,
        @Arg('password') password: string
    ) {
        if(!user) {
            throw new GraphQLError("Not logged in as a user.", {
                extensions: {
                    code: "FORBIDDEN"
                }
            });
            
        }
        const hashedPassword = await hashPassword(password)
        const response = await UserModel.findOneAndUpdate({username: user.username}, {password: hashedPassword}, {new: true})
        if(response === null) {
            throw new GraphQLError(`User with username ${user.username} not found.`,{
                extensions: {
                    code: "NOT_FOUND"
                }
            })
        }
        return new StatusInfo(200, `Updated user with learerId ${user.username}`)
    }
}
