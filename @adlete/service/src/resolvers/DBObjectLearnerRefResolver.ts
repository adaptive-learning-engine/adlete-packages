import { Authorized, ClassType, FieldResolver, Resolver, Root } from 'type-graphql';

import { DBObjectLearnerRef } from '../entities/DBObjectLearnerRef';
import { Learner, LearnerModel } from '../entities/Learner';

/** *
 * A special kind of inheritance in TypeGraphQL is resolver class inheritance.
 * This pattern allows us e.g. to create a base CRUD resolver class for our
 * resource/entity, so we don't have to repeat common boilerplate code.
 * Since we need to generate unique query/mutation names, we have to create a
 * factory function for our base.
 * Source: https://typegraphql.com/docs/inheritance.html
 *
 * Using any as return type to return a class type
 ** */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createDBObjectLearnerRefResolver<T extends DBObjectLearnerRef>(objectTypeCls: ClassType): any {
  @Resolver(() => objectTypeCls)
  abstract class DBObjectLearnerRefResolver {
    protected items: T[] = [];

    @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
    @FieldResolver(() => Learner)
    async learner(@Root() object: T): Promise<Learner> {
      return LearnerModel.findById(object.learner);
    }
  }

  return DBObjectLearnerRefResolver;
}
