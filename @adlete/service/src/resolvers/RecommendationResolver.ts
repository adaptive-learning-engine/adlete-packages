import { Args, Authorized, Ctx, Mutation, Resolver } from 'type-graphql';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';

import { ActivityStatistics } from '../entities/ActivityStatistics';
import { Recommendation, RecommendationModel } from '../entities/Recommendation';
import { Context } from '../util/Context';

import { createDBObjectLearnerRefResolver } from './DBObjectLearnerRefResolver';
import { RecommendInput } from '../entities/RecommendInput';
import { GraphQLError } from 'graphql';
import { isDocument, isDocumentArray } from '@typegoose/typegoose';
import { LearnerModel } from '../entities/Learner';
import { SessionCacheModel } from '../entities/SessionCache';

const DBObjectLearnerRefResolver = createDBObjectLearnerRefResolver<Recommendation>(Recommendation);

@Resolver(() => Recommendation)
export class RecommendationResolver extends DBObjectLearnerRefResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => Recommendation)
  async fetchNextRecommendation(
    @Args(() => RecommendInput) input: RecommendInput,
    @Ctx() { adleteConfig }: Context
  ): Promise<Recommendation> {
    if (input.activitySubset) {
      input.activitySubset.forEach((entry) => {
        if (adleteConfig.activityNames.indexOf(entry) === -1) {
          throw new GraphQLError(`Your given activity subset list contains at least one unknown activity with the name "${entry}"`, {
            extensions: {
              code: 'BAD_USER_INPUT',
            },
          });
        }
      });
    }

    const session = await SessionCacheModel.findById(input.learnerId);
    if (!session) {
      throw new GraphQLError(`No active Session found for user with learner Id ${input.learnerId}`, {
        extensions: {
          code: 'NOT_FOUND',
        },
      });
    }
    await session.populate('activityStatistics');

    const learner = new LearnerModel({
      _id: session.learnerObjectId,
      learnerId: session._id,
      probabilisticBeliefs: [session.probabilityBelief],
      scalarBeliefs: [session.scalarBelief],
      tendencies: [session.tendency],
    });
    const adjuster = learner.factory.createRecommendationAdjuster({
      config: adleteConfig,
      scalarBeliefs: JSON.parse(session.scalarBelief),
      tendencies: JSON.parse(session.tendency),
    });

    if (!isDocumentArray(session.activityStatistics)) {
      session.activityStatistics = session.activityStatistics.filter((statistic) => isDocument(statistic));
    }

    const currentStates: Record<ActivityName, IAccumulatedActivityStatistics> = {};
    session.activityStatistics.forEach((sessionState) => {
      const statistic = sessionState as ActivityStatistics;
      currentStates[statistic.activityName] = statistic;
    });

    const recommendation = new RecommendationModel({
      ...adjuster.recommend({
        statistics: currentStates,
        totalActivitiesCompleted: learner.overAllFinishedActivities,
        adleteConfig,
        activitySubset: input.activitySubset,
      }),
      learner,
    });

    await session.save();
    await recommendation.save();
    return recommendation;
  }
}
