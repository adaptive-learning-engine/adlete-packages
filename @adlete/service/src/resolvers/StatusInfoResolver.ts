import { Authorized, Ctx, Query, Resolver } from 'type-graphql';

import { ServiceConfiguration } from '../entities/ServiceConfiguration';
import { ServerStatusInfo } from '../entities/StatusInfo';
import { Context } from '../util/Context';


@Resolver()
export class StatusInfoResolver {
  @Query(() => ServerStatusInfo, { nullable: false })
  async status(@Ctx() { version, clientToken}: Context): Promise<ServerStatusInfo> {
    const statusCode = new ServerStatusInfo(
      200,
      `Service up and running in version: ${version}}`
    );
    if(clientToken != null) {
      await statusCode.verifyUser(clientToken)
    }
    return statusCode;
  }

@Authorized('ADMIN', 'CLIENTINSTANCE')
@Query(() => ServiceConfiguration, { nullable: true })
  serviceConfiguration(@Ctx() { adleteConfig }: Context): ServiceConfiguration {
    const serverConfig = new ServiceConfiguration(adleteConfig);

    return serverConfig;
  }
}
