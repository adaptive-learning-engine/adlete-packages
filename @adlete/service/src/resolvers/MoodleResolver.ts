import { Arg, Authorized, Ctx, Query, Resolver } from 'type-graphql';
import { MoodleConfig } from '../entities/MoodleConfiguration';
import { GraphQLError } from 'graphql';
import { Context } from '../util/Context';

@Resolver(() => MoodleConfig)
export class MoodleResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Query(() => MoodleConfig)
  moodleConfiguration(@Ctx() { moodleConfig }: Context, @Arg('version', { nullable: true }) version: string): MoodleConfig {
    if (moodleConfig) {
      if (version) {
        //search for correct version
        return { version: moodleConfig.version, config: JSON.stringify(moodleConfig.config) };
      }
      return { version: moodleConfig.version, config: JSON.stringify(moodleConfig.config) };
    }

    throw new GraphQLError('No Moodle Configuration found', {
      extensions: {
        code: 'NOT FOUND',
      },
    });
  }
}
