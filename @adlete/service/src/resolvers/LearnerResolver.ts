import { GraphQLError } from 'graphql';
import { MongoServerError } from 'mongodb';
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';

import { scalarToProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/convert';
import { Adjuster } from '@adlete/framework/Adjuster';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

import { ActivityStatisticsModel } from '../entities/ActivityStatistics';
import { Learner, LearnerDocType, LearnerModel } from '../entities/Learner';
import { ObservationModel } from '../entities/Observation';
import { RecommendationModel } from '../entities/Recommendation';
import { SessionModel } from '../entities/Session';
import {SessionCacheModel } from '../entities/SessionCache';
import { StatusInfo } from '../entities/StatusInfo';
import { Context } from '../util/Context';
import { decryptData } from '../util/SecretGenerator';


@Resolver(() => Learner)
export class LearnerResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => Learner)
  async createLearner(
    @Ctx() { adleteConfig }: Context,
    @Arg('learnerId') learnerId: string,
    @Arg('initialScalarBeliefSetId') initialScalarBeliefSetId: string
  ): Promise<Learner> {
    // instantiate learner
    const {learner, adjuster} = LearnerResolver.instantiateLearnerModel(adleteConfig, learnerId, initialScalarBeliefSetId);

    learner.serializeAdjuster(adjuster)
    // stores also initialized adjuster data
    try {
      await learner.save()
      return learner;
    } catch(e) {
      if(e.name === "MongoServerError" && e.code == 11000) {
        throw new GraphQLError("A user with these credentials already exists.", {
          extensions: {
            code: "CONFLICT"
          }
        })
      }
      throw e
    }
  }

  public static instantiateLearnerModel(
    adleteConfig: IAdleteConfiguration,
    learnerId: string,
    initialScalarBeliefSetId: string
  ): {learner: LearnerDocType, adjuster: Adjuster} {
    // check whatever initial-scalar-belief-set-id is given
    if (initialScalarBeliefSetId == null && initialScalarBeliefSetId == undefined) {
      throw new GraphQLError(`Please provided an initialScalarBeliefSetId, which is required to initialize a learner correctly.`, {
        extensions: {
          code: 'BAD_USER_INPUT',
        },
      });
    }

    const initialScalarBeliefs = adleteConfig.initialScalarBeliefs[initialScalarBeliefSetId];

    //check if the given initial-scalar-belief-set-id is defined by the adlete-config
    if (initialScalarBeliefs === undefined) {
      throw new GraphQLError(
        `Could not find initialScalarBeliefSetId with name "${initialScalarBeliefSetId}". Use one of the supported initial scalar belief set ids: ${JSON.stringify(
          Object.keys(adleteConfig.initialScalarBeliefs)
        )}`,
        {
          extensions: {
            code: 'BAD_USER_INPUT',
          },
        }
      );
    }
    const numberOfProbabilities = adleteConfig.competenceModel[Object.keys(adleteConfig.competenceModel)[0]].states.length;

    const initialProbabilityBeliefs = scalarToProbabilityBeliefBundle(initialScalarBeliefs, numberOfProbabilities);

    const learner = new LearnerModel({
      learnerId,
    } as Learner);

    // initialize adjuster
    const adjuster = learner.factory.createAdjuster({ config: adleteConfig, beliefs: initialProbabilityBeliefs });
    return {learner, adjuster};
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => StatusInfo)
  async deleteLearner(
    @Arg('learnerId') learnerId: string,
    @Arg('removeComplete', { defaultValue: false }) removeComplete: boolean
  ): Promise<StatusInfo> {
    const learner = await LearnerModel.findOneAndDelete({ learnerId });

    if (learner === null) {
      throw new GraphQLError(`Could not find learner with learnerId ${learnerId}`, {
        extensions: {
          code: 'NOT_FOUND',
        },
      });
    }
    const session = await SessionCacheModel.findByIdAndDelete(learnerId);
    if (removeComplete == true) {
      const observationNumber = await ObservationModel.deleteMany({ learner: learner._id });
      const recommendationNumber = await RecommendationModel.deleteMany({ learner: learner._id });
      const sessionNumber = await SessionModel.deleteMany({ learner: learner._id });
      const statisticsNumbers = await ActivityStatisticsModel.deleteMany({ learner: learner._id });
      return new StatusInfo(
        200,
        `successfully deleted learner with learnerId ${learnerId}. Removed ${observationNumber.deletedCount} observations, ${recommendationNumber.deletedCount} recommendations, ${sessionNumber.deletedCount} sessions and ${statisticsNumbers.deletedCount} learner statistics.`
      );
    } else {
      if (session) {
        SessionModel.findByIdAndUpdate({ active: false, stopTimeestamp: new Date() });
      }
    }
    return new StatusInfo(200, `successfully deleted learner with learnerId ${learnerId}`);
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Query(() => Learner)
  async learner(@Arg('learnerId') learnerId: string): Promise<Learner> {
    return LearnerResolver.getLearner(learnerId);
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
  @Query(() => [Learner])
  async multipleLearner(
    @Ctx() {user}: Context,
    @Arg('learnerIds', () => [String]) learnerIds: string[],
    @Arg('idCipher', () => String, {nullable: true})idCipher?: string 
  ): Promise<Array<Learner>> {
    if(user.roles.length === 1 && user.roles.includes('EDUCATOR')) {
      if(idCipher === null) throw new GraphQLError("No idCipher for veryfication provided", {
        extensions: {
          code: "BAD_USER_INPUT"
        }
      })
      let parsedCipher: string[] = null;
      try {
        parsedCipher = JSON.parse(decryptData(idCipher, user.iv))
      } catch(e) {
        throw new GraphQLError("Could not decrypt idCipher. Access denied", {
          extensions: {
            code: 'BAD_USER_INPUT'
          }
        })
      }
      if(Array.isArray(parsedCipher)) {
        for (let i = 0; i < learnerIds.length; i++) {
          if(!parsedCipher.includes(learnerIds[i])) {
            throw new GraphQLError(`Not authorized to access learnerId: ${learnerIds[i]}`, {
              extensions: {
                code: 'FORBIDDEN'
              }
            })
          }
        }
      } else {
        throw new GraphQLError("Cipher has the wrong format", {
          extensions: {
            code: 'BAD_USER_INPUT'
          }
        })
      }
    }
    const learners = await LearnerResolver.getMultipleLearners(learnerIds)
    learners.forEach((value) => {
      value.scalarBeliefs = value.scalarBeliefs.slice(-1)
      value.probabilisticBeliefs = value.probabilisticBeliefs.slice(-1)
      value.tendencies = value.tendencies.slice(-1)
    })
    return learners
  }

  static async getLearner(learnerId: string): Promise<LearnerDocType> {
    const learner = await LearnerModel.findOne({ learnerId });

    if (learner == null) {
      throw new GraphQLError(`Could not find learner with learnerId ${learnerId}`, {
        extensions: {
          code: 'NOT_FOUND',
        },
      });
    }

    return learner;
  }

  static async getMultipleLearners(learnerIds: string[]): Promise<Array<LearnerDocType>> {
    const learners = await LearnerModel.find({learnerId: {$in: learnerIds}})
    if(learners.length == 0) {
      throw new GraphQLError(`Could not find learner with learnerId ${learnerIds.join(', ')}`, {
        extensions: {
          code: 'NOT_FOUND',
        },
      });
    }

    return learners
  }

}
