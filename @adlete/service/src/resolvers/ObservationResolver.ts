import { Arg, Authorized, Ctx, Mutation, Resolver } from 'type-graphql';

import { ActivityStatistics, ActivityStatisticsModel } from '../entities/ActivityStatistics';
import { Learner, LearnerDocType, LearnerModel } from '../entities/Learner';
import { Observation, ObservationInput, ObservationModel } from '../entities/Observation';
import { StatusInfo } from '../entities/StatusInfo';
import { Context } from '../util/Context';

import { createDBObjectLearnerRefResolver } from './DBObjectLearnerRefResolver';
import { LearnerResolver } from './LearnerResolver';
import { SessionStatisticsResolver } from './SessionStatisticsResolver';
import { GraphQLError } from 'graphql';
import { SessionCache, SessionCacheModel } from '../entities/SessionCache';
import { performance, PerformanceObserver } from 'perf_hooks';
import { ObservationAdjuster } from '@adlete/framework/ObservationAdjuster';

const DBObjectLearnerRefResolver = createDBObjectLearnerRefResolver<Observation>(Observation);

@Resolver(() => Observation)
export class ObservationResolver extends DBObjectLearnerRefResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => StatusInfo)
  async submitActivityResult(
    @Arg('observation') observationInput: ObservationInput,
    @Ctx() { adleteConfig }: Context
  ): Promise<StatusInfo> {
    if (observationInput.activityCorrectness < 0 || observationInput.activityCorrectness > 1) {
      throw new GraphQLError('Could not process observation input activityCorrectness must be between 0 and 1 ', {
        extensions: {
          code: 'BAD_USER_INPUT',
        },
      });
    }
    if (observationInput.activityDifficulty < 0 || observationInput.activityDifficulty > 1) {
      throw new GraphQLError('Could not process observation input activityDifficulty must be between 0 and 1 ', {
        extensions: {
          code: 'BAD_USER_INPUT',
        },
      });
    }
    const perfObserver = new PerformanceObserver((items) => {
      items.getEntries().forEach((entry) => {
        console.log(entry);
      });
    });

    perfObserver.observe({ entryTypes: ['measure'], buffered: true });
    const session = await SessionCacheModel.findById(observationInput.learnerId);
    if (!session) {
      throw new GraphQLError(`No active Session found for user with learner Id ${observationInput.learnerId}`, {
        extensions: {
          code: 'NOT_FOUND',
        },
      });
    }

    const sessionLearner = new LearnerModel({
      _id: session.learnerObjectId,
      learnerId: session._id,
      probabilisticBeliefs: [session.probabilityBelief],
      scalarBeliefs: [session.scalarBelief],
      tendencies: [session.tendency],
    });
    const adjuster = sessionLearner.factory.createObservationAdjuster({
      config: adleteConfig,
      probabilisticBeliefs: JSON.parse(session.probabilityBelief),
      tendencies: JSON.parse(session.tendency),
    });

    const observation = new ObservationModel({
      ...observationInput,
      learner: sessionLearner,
    } as unknown as Observation);

    try {
      ObservationResolver.interpretEvidence(adjuster, observation);
    } catch (error) {
      throw new GraphQLError(error, {
        extensions: {
          code: 'BAD_USER_INPUT',
        },
      });
    }

    sessionLearner.serializeAdjuster(adjuster);
    session.probabilityBelief = JSON.stringify(adjuster.bayesBeliefSystem.beliefModel.getLeafBeliefs());

    session.scalarBelief = sessionLearner.scalarBeliefs[1];
    session.tendency = sessionLearner.tendencies[1];
    session.overallFinishedActivities++;

    await this.updateActivityStatistics(sessionLearner, observation, session);

    await session.save();

    observation.save();
    LearnerResolver.getLearner(observationInput.learnerId)
      .then(async (learner) => {
        learner.probabilisticBeliefs.push(sessionLearner.probabilisticBeliefs[1]);
        learner.scalarBeliefs.push(session.scalarBelief);
        learner.tendencies.push(session.tendency);
        await this.updateGlobalStates(learner);
      })
      .catch((err) => {
        if (err.extensions?.code === 'NOT_FOUND') {
          SessionCacheModel.deleteOne({ _id: observationInput.learnerId });
        }
      });

    return new StatusInfo(200, 'Interpreted evidence successfully!');
  }

  /**
   * interprets given evidence and stores result within the given learner's adjuster
   * @param learner
   * @param observation
   */
  public static interpretEvidence(adjuster: ObservationAdjuster, observation: Observation): void {
    const activityEvidence = adjuster.convertObservationToActivityEvidence(observation);
    const activityScalarEvidence = adjuster.convertActivityEvidenceToScalarEvidence(activityEvidence);

    adjuster.interpretEvidence(activityScalarEvidence, observation);
  }

  /**
   * updates internal activity states due done activities
   * @param learner
   * @param observation
   * @returns
   */
  protected async updateActivityStatistics(
    learner: LearnerDocType,
    observation: Observation,
    session: SessionCache
  ): Promise<ActivityStatistics> {
    let activityStatistics = await ActivityStatisticsModel.findOne({
      _id: { $in: session.activityStatistics },
      activityName: observation.activityName,
    });

    if (!activityStatistics) {
      activityStatistics = await SessionStatisticsResolver.createNewSessionStatistics(observation.activityName, learner, session);
    }

    activityStatistics.activitiesPlayed++;
    activityStatistics.sumAnswersCorrectness += observation.activityCorrectness;
    activityStatistics.avgAnswersCorrectness = activityStatistics.sumAnswersCorrectness / activityStatistics.activitiesPlayed;

    return activityStatistics.save();
  }

  protected async updateGlobalStates(learner: LearnerDocType): Promise<Learner> {
    learner.overAllFinishedActivities += 1;

    return learner.save();
  }

  static async getObservations(learnerId: string): Promise<Observation[]> {
    // check if learner with learnerId exists
    const learner = await LearnerResolver.getLearner(learnerId);

    // load all observations of a specific learner
    // eslint-disable-next-line no-underscore-dangle
    const observations = await ObservationModel.find({ learner: learner._id });

    return observations;
  }
}
