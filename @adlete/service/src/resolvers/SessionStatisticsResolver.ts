import { Resolver } from 'type-graphql';

import { ActivityStatistics, ActivityStatisticsDocType, ActivityStatisticsModel } from '../entities/ActivityStatistics';
import { Learner } from '../entities/Learner';

import { createDBObjectLearnerRefResolver } from './DBObjectLearnerRefResolver';
import { SessionCache } from '../entities/SessionCache';

const DBObjectLearnerRefResolver = createDBObjectLearnerRefResolver<ActivityStatistics>(ActivityStatistics);

@Resolver(() => ActivityStatistics)
export class SessionStatisticsResolver extends DBObjectLearnerRefResolver {
  static async createNewSessionStatistics(
    activityName: string,
    learner: Learner,
    session: SessionCache
  ): Promise<ActivityStatisticsDocType> {
    let activityStatistics = new ActivityStatisticsModel({
      activityName,
      learner,
    });
    activityStatistics = await activityStatistics.save();
    session.activityStatistics.push(activityStatistics);

    return activityStatistics;
  }
}
