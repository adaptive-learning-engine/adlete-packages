/* eslint-disable no-await-in-loop */
import { DocumentType } from '@typegoose/typegoose';
import { GraphQLError } from 'graphql';
import { Args, Authorized, Ctx, Query, Resolver } from 'type-graphql';

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';
import { binarySearch } from '@adlete/engine-blocks/utils/Utils';
import { Adjuster } from '@adlete/framework/Adjuster';
import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { ActivityInputError } from '@adlete/framework/errors/ActivityInputError';

import { ActivityStatisticsModel } from '../entities/ActivityStatistics';
import { Learner, LearnerDocType, LearnerModel } from '../entities/Learner';
import { Observation, ObservationModel } from '../entities/Observation';
import { Recommendation, RecommendationModel } from '../entities/Recommendation';
import { ServiceConfiguration } from '../entities/ServiceConfiguration';
import { Session, SessionModel } from '../entities/Session';
import { AdvSimulationArgs, SimpleSimulationArgs, SimulationAnalytics, Task } from '../entities/SimulationAnalytics';
import { Roles, validateEducatorAccess } from '../util/ClientValidator';
import { Context } from '../util/Context';
import { calculateCorrectness, calculateLearnerCorrectness, constantCorrectness } from '../util/LearnerSimulator';
import { calculateDifficulty, calculateNormDifficulty } from '../util/SimulationHelpers';

import { LearnerResolver } from './LearnerResolver';


type GetCorrectness = (learner: Learner, difficulty: number, loId?: string) => number;

interface TrainedTask {
  task: Task;
  correct: number;
  trained: number;
}

interface ISimulationData {
  activitiesStatistics: Record<ActivityName, IAccumulatedActivityStatistics>;
  adleteConfig: IAdleteConfiguration;
  agentBehavior: string;
  simulationSteps: number;
  minDifficulty: number;
  maxDifficulty: number;
  stepsDifficulty: number;
  learner: LearnerDocType;
  adjuster: Adjuster;
  activities?: string[];
  difficulty?: number;
}

interface IAdvancedSimulationData extends ISimulationData {
  maxCorrect?: number;
}

@Resolver(() => SimulationAnalytics)
export class SimulationResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
  @Query(() => SimulationAnalytics)
  async simulate(
    @Ctx() { adleteConfig, user }: Context,
    @Args(() => SimpleSimulationArgs) input: SimpleSimulationArgs
  ): Promise<SimulationAnalytics> {
    if(user.roles.length === 1 && user.roles.includes(Roles[Roles.CLIENTINSTANCE]) && input.learnerId != null) {
      validateEducatorAccess(user, input.learnerId, input.idCipher)
    }

    if(input.simulationSteps <= 0) {
      throw new GraphQLError("Simulation Steps have to be greater than 0.", {
        extensions: {
          code: "BAD_USER_INPUT"
        }
      })
    }

    if(input.activities && input.activities.length === 0) {
      throw new GraphQLError("No activities provided in array.", {
        extensions: {
          code: "BAD_USER_INPUT"
        }
      })
    }
    // convert adleteConfig
    let observations: DocumentType<Observation>[];

    // setup activityStatistics
    const activitiesStatistics: Record<ActivityName, IAccumulatedActivityStatistics> = {};
    adleteConfig.activityNames.forEach((activityName) => {
      activitiesStatistics[activityName] = {
        activitiesPlayed: 0,
        sumAnswersCorrectness: 0,
        avgAnswersCorrectness: 0,
      } as IAccumulatedActivityStatistics;
    });

    let learnerInfos: {learner: LearnerDocType, adjuster: Adjuster}
    if(input.initialScalarBeliefSetId == null) {
      learnerInfos = await this.fetchLearner(adleteConfig, input.learnerId)
    } else {
      learnerInfos = this.createLearner(adleteConfig, input.initialScalarBeliefSetId, input.learnerId)
      if(input.storeResults) {
        learnerInfos.learner.serializeAdjuster(learnerInfos.adjuster)
        await this.saveLearner(learnerInfos.learner)
      }
    }
    const simulationData: ISimulationData = {
      activitiesStatistics,
      adleteConfig: adleteConfig,
      agentBehavior: input.behavior,
      learner: learnerInfos.learner,
      adjuster: learnerInfos.adjuster,
      maxDifficulty: 1,
      minDifficulty: 0,
      stepsDifficulty: 0.1,
      simulationSteps: input.simulationSteps,
      activities: input.activities,
      difficulty: input.difficulty,
    };

    const getCorrectness: GetCorrectness = this.createCorrectnessFunction(simulationData);

    observations = [];

    if (input.storeResults) {
      // stores also initialized adjuster data
      const session = new SessionModel({
        learner: simulationData.learner,
        startTimestamp: Date.now(),
        active: true,
      } as unknown as Session);

      await session.save();

      // Save to DB in 100 Steps so bulk doesn't get to big
      const bulkSize = Math.floor(simulationData.simulationSteps / 100) + (simulationData.simulationSteps % 100 === 0 ? 0 : 1);
      for (let i = 0; i < bulkSize; i++) {
        const partStepSize = simulationData.simulationSteps - 100 * i >= 100 ? 100 : simulationData.simulationSteps % 100;
        const recommendations: DocumentType<Recommendation>[] = [];

        // Do observations
        try {
          const partObsv = this.makeObservations(simulationData, partStepSize, getCorrectness, recommendations);
          // save data in DB (We don't need to wait for it)
          RecommendationModel.bulkSave(recommendations).catch((err) => {
            console.log(`Error while saving recommendations: \n${err}`);
          });
          ObservationModel.bulkSave(partObsv).catch((err) => {
            console.log(`Error while saving Observations: \n${err}`);
          });
          observations = observations.concat(partObsv);
        } catch (e) {
          if (e instanceof ActivityInputError) {
            // Is thrown when Users Provides Wrong subset.
            throw new GraphQLError(e.message, {
              extensions: {
                code: 'BAD_USER_INPUT',
              },
            });
          }
          throw e;
        }
      }
      
      const activitiesStatisticsModels = Object.entries(activitiesStatistics).map(
        ([key, value]) => new ActivityStatisticsModel({ ...value, activityName: key, learner: simulationData.learner })
      );
      ActivityStatisticsModel.bulkSave(activitiesStatisticsModels)
        .then(() => simulationData.learner.save())
        .then(() =>
          SessionModel.findOneAndUpdate({ learner: simulationData.learner._id, active: true }, { active: false, stopTimestamp: new Date() })
        );
    } else {
      try {
        observations = this.makeObservations(simulationData, simulationData.simulationSteps, getCorrectness);
      } catch (e) {
        if (e instanceof ActivityInputError) {
          // Is thrown when Users Provides Wrong subset.
          throw new GraphQLError(e.message, {
            extensions: {
              code: 'BAD_USER_INPUT',
            },
          });
        }
        throw e;
      }
    }
    // start simulation
    return {
      serviceConfiguration: new ServiceConfiguration(adleteConfig),
      observations: observations as Observation[],
      learner: simulationData.learner,
      agent: [''],
    };
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
  @Query(() => SimulationAnalytics)
  async advancedSimulation(
    @Ctx() { adleteConfig, user }: Context,
    @Args(() => AdvSimulationArgs) input: AdvSimulationArgs
  ): Promise<SimulationAnalytics> {
    if(user.roles.length === 1 && user.roles.includes(Roles[Roles.CLIENTINSTANCE])) {
      validateEducatorAccess(user, input.learnerId, input.idCipher)
    }

    if(input.taskSet.length === 0) {
      throw new GraphQLError("No tasks provided in array.", {
        extensions: {
          code: "BAD_USER_INPUT"
        }
      })
    }

    if(input.maxAnsweredCorrect <= 0) {
      throw new GraphQLError("Max Correctness has to be greater than 0.", {
        extensions: {
          code: "BAD_USER_INPUT"
        }
      })
    }

    input.taskSet.sort((a, b) => a.difficulty - b.difficulty);
    const activities: Map<string, Task[]> = new Map();
    input.taskSet.forEach((task) => {
      const entry = activities.get(task.loId);
      if (task.difficulty > input.maxDifficulty || task.difficulty < input.minDifficulty)
      {
        throw new GraphQLError(`Malformed Task Ids. Check difficulty of task: ${task}`, {
          extensions: {
            code: "BAD_USER_INPUT"
          }
        })
      }
      if (entry) {
        entry.push(task);
      } else {
        activities.set(task.loId, [task]);
      }
    });

    const observations: Observation[] = [];

    // setup activityStatistics
    const activitiesStatistics: Record<ActivityName, IAccumulatedActivityStatistics> = {};
    adleteConfig.activityNames.forEach((activityName) => {
      activitiesStatistics[activityName] = {
        activitiesPlayed: 0,
        sumAnswersCorrectness: 0,
        avgAnswersCorrectness: 0,
      } as IAccumulatedActivityStatistics;
    });

    let learnerInfos: {learner: LearnerDocType, adjuster: Adjuster}
    if(input.initialScalarBeliefSetId == null) {
      learnerInfos = await this.fetchLearner(adleteConfig, input.learnerId)
    } else {
      learnerInfos = this.createLearner(adleteConfig, input.initialScalarBeliefSetId, input.learnerId)
      if(input.storeResults) {
        learnerInfos.learner.serializeAdjuster(learnerInfos.adjuster)
        await this.saveLearner(learnerInfos.learner)
      }
    }
    // setup Simulation Data
    const simulationData: IAdvancedSimulationData = {
      agentBehavior: input.behavior,
      activitiesStatistics,
      learner: learnerInfos.learner,
      adjuster: learnerInfos.adjuster,
      adleteConfig: adleteConfig,
      maxDifficulty: input.maxDifficulty,
      minDifficulty: input.minDifficulty,
      stepsDifficulty: input.stepsDifficulty,
      simulationSteps: input.taskSet.length,
      maxCorrect: Math.min(Math.max(input.maxAnsweredCorrect, 1), 5),
    };
    const getCorrectness = this.createCorrectnessFunction(simulationData, input.stepsDifficulty);


    let previousActivities = 0;
    // create or get Learner
    if (input.storeResults) {
      previousActivities = simulationData.learner.overAllFinishedActivities;
      simulationData.learner.overAllFinishedActivities = 0;

      const session = new SessionModel({
        learner: simulationData.learner,
        startTimestamp: Date.now(),
        active: true,
      } as unknown as Session);

      await session.save();
    }
    const trainedTaskSet: Map<string, TrainedTask[]> = new Map();
    let task: TrainedTask;
    let index;
    const selectWindow = [0, input.stepsDifficulty, 2 * input.stepsDifficulty, -1 * input.stepsDifficulty, 3 * input.stepsDifficulty];
    const activitieNames = Array.from(activities.keys());
    do {
      const recommendations: DocumentType<Recommendation>[] = [];
      task = null;
      index = -1;
      let activitySubset = [...activitieNames];
      // Look up tasks
      while (activitySubset.length > 0) {
        let recommendation;
        try {
          recommendation = new RecommendationModel({
            ...simulationData.adjuster.recommend({
              statistics: activitiesStatistics,
              totalActivitiesCompleted: simulationData.learner.overAllFinishedActivities,
              adleteConfig: simulationData.adleteConfig,
              activitySubset: activitySubset,
            }),
            learner: simulationData.learner,
            createdAt: Date.now(),
          });
        } catch(e) {
          if(e instanceof ActivityInputError) {
            throw new GraphQLError(e.message, {
              extensions: {
                code: "BAD_USER_INPUT"
              }
            })
          }
          throw e
        }

        recommendations.push(recommendation);
        const trend = recommendation.trend < 0 ? -1 : 1;
        const tasks = activities.get(recommendation.activityName);
        const trainedTasks = trainedTaskSet.get(recommendation.activityName) ?? [];
        const recommendedDifficulty = calculateDifficulty(
          input.minDifficulty,
          input.maxDifficulty,
          recommendation.difficulty,
          input.stepsDifficulty
        );

        for (let j = 0; j < selectWindow.length; j++) {
          const difficultyWindow = recommendedDifficulty + selectWindow[j] * trend;
          // untrained tasks
          index = binarySearch<Task, number>(tasks, difficultyWindow, (element, difficulty) => element.difficulty - difficulty);

          if (index !== -1) {
            const untrainedTask = this.removeEntryFromTaskSet(tasks, index);
            if (tasks.length === 0) {
              activities.delete(recommendation.activityName);
            }
            task = { task: untrainedTask, correct: 0, trained: 0 };
            if (trainedTasks.length > 0) {
              trainedTasks.push(task);
              trainedTasks.sort((a, b) => {
                if (a.task.difficulty === b.task.difficulty) return b.correct - a.correct;
                return a.task.difficulty - b.task.difficulty;
              });
            } else {
              trainedTaskSet.set(recommendation.activityName, [task]);
            }
            break;
          }
          //trained tasks
          index = binarySearch<TrainedTask, number>(
            trainedTasks,
            difficultyWindow,
            (element, difficulty) => element.task.difficulty - difficulty
          );
          if (index !== -1) {
            task = trainedTasks[index];
            break; // break from sub-loop
          }
        }

        if (index !== -1) {
          break; // break from main loop
        }
        activitySubset = activitySubset.filter((activityName) => activityName != recommendation.activityName);
      }

      if (index === -1) {
        break;
      }

      const additionalInfos: Record<string, number | string> = {
        taskName: task.task.name,
        recommendedDifficulty: recommendations[recommendations.length - 1].difficulty,
      };

      // Make Observation
      const observation = this.observe(
        simulationData,
        getCorrectness,
        task.task.loId,
        calculateNormDifficulty(input.minDifficulty, input.maxDifficulty, task.task.difficulty),
        JSON.stringify(additionalInfos)
      );

      if (input.storeResults) {
        RecommendationModel.bulkSave(recommendations).catch((err) => {
          this.errorMessage('Recommendation', err);
        });
        await observation.save();
      }
      observations.push(observation);
      task.trained++;
      if (observation.activityCorrectness === 1) task.correct++;
      if (task.correct >= simulationData.maxCorrect || task.trained > 10) {
        const tasks = trainedTaskSet.get(task.task.loId);
        if (tasks.length === 1) {trainedTaskSet.delete(task.task.loId);}
        else {trainedTaskSet.set(task.task.loId, tasks.filter((value) => value != task))}
      }
    } while ((activities.size > 0 || trainedTaskSet.size > 0));

    // Store Results and postprocess data
    if (input.storeResults) {
      //ObservationModel.bulkSave(observations).catch((err) => this.errorMessage('Observations', err));
      simulationData.learner.overAllFinishedActivities += previousActivities;
      simulationData.learner.save().catch((err) => this.errorMessage('Learner', err));
      SessionModel.findOneAndUpdate(
        { learner: simulationData.learner._id, active: true },
        { active: false, stopTimestamp: new Date() }
      ).catch((err) => this.errorMessage('Recommendation', err));
    }
    return {
      serviceConfiguration: new ServiceConfiguration(simulationData.adleteConfig),
      observations: observations as Observation[],
      learner: simulationData.learner,
      agent: [''],
    };
  }

  /**
   *
   * @param simulationData
   * @param steps simulationSteps How many observations should be simulated.
   * @param getCorrectness function get correctness value
   * @param recommendations  Decides If null recommendations wont be saved else recommendations will be pushed
   * @param additionalInfos Some additional information which should be stored next to the observation itself
   * @returns  Observation Model of all observations
   */
  protected makeObservations(
    simulationData: ISimulationData,
    steps: number,
    getCorrectness: GetCorrectness,
    recommendations?: DocumentType<Recommendation>[],
    additionalInfos?: string
  ): DocumentType<Observation>[] {
    const observations: DocumentType<Observation>[] = [];
    for (let i = 1; i <= steps; i++) {
      // simulate recommendation generation
      const recommendation = new RecommendationModel({
        ...simulationData.adjuster.recommend({
          statistics: simulationData.activitiesStatistics,
          totalActivitiesCompleted: simulationData.learner.overAllFinishedActivities,
          adleteConfig: simulationData.adleteConfig,
          activitySubset: simulationData.activities,
        }),
        learner: simulationData.learner,
        createdAt: Date.now(),
      });
      const difficulty = simulationData.difficulty ?? recommendation.difficulty;
      // simulate the activity playing
      observations.push(this.observe(simulationData, getCorrectness, recommendation.activityName, difficulty, additionalInfos));
      if (recommendations) {
        recommendations.push(recommendation);
      }
    }
    return observations;
  }

  private observe(
    simulationData: ISimulationData,
    getCorrectness: GetCorrectness,
    activityName: string,
    difficulty: number,
    additionalInfos?: string
  ): DocumentType<Observation> {
    const correctness = getCorrectness(simulationData.learner, difficulty, activityName);
    // simulate the activity playing
    const observation = new ObservationModel({
      activityName,
      activityCorrectness: correctness,
      activityDifficulty: difficulty,
      timestamp: Date.now(),
      additionalInfos,
      learner: simulationData.learner,
      createdAt: Date.now(),
    });

    // simulate model update
    try {
      const activityEvidence = simulationData.adjuster.convertObservationToActivityEvidence(observation);
      const activityScalarEvidence = simulationData.adjuster.convertActivityEvidenceToScalarEvidence(activityEvidence);

      simulationData.adjuster.interpretEvidence(activityScalarEvidence, observation);
    } catch (error) {
      throw new GraphQLError(error, {
        extensions: {
          code: 'BAD_USER_INPUT',
        },
      });
    }

    // simulate statistics update
    const activityStatistic = simulationData.activitiesStatistics[activityName];
    activityStatistic.activitiesPlayed += 1;
    activityStatistic.sumAnswersCorrectness += observation.activityCorrectness;
    activityStatistic.avgAnswersCorrectness = activityStatistic.sumAnswersCorrectness / activityStatistic.activitiesPlayed;
    simulationData.learner.overAllFinishedActivities++;

    simulationData.learner.serializeAdjuster(simulationData.adjuster);

    return observation;
  }

  private createLearner(adleteConfig: IAdleteConfiguration, initialScalarBeliefSetId: string, learnerId?: string) {
    if (!learnerId) {
      learnerId = `agent-learner-id-${Math.floor(Math.random() * (99999 - 10000) + 10000)}`;
    }
    return LearnerResolver.instantiateLearnerModel(adleteConfig, learnerId, initialScalarBeliefSetId);
  }

  private async saveLearner(learner: LearnerDocType): Promise<LearnerDocType> {
      if(!await LearnerModel.exists({learnerId: learner.learnerId})) {
        return learner.save()
      }
      throw new GraphQLError("Could not generate a new User please provide a new learner Id", {
        extensions: {
          code: "INTERNAL_SERVER_ERROR"
        }
      })
  }

  private async fetchLearner(
    adleteConfig: IAdleteConfiguration,
    learnerId: string,
  ): Promise<{learner: LearnerDocType, adjuster: Adjuster}> {
    // const maxBulkSize = 1000;

    try {
      const learner = await LearnerResolver.getLearner(learnerId);
      const adjuster = learner.factory.createAdjuster({
        beliefs: JSON.parse(learner.probabilisticBeliefs[learner.probabilisticBeliefs.length - 1]),
        config: adleteConfig,
        tendencies: JSON.parse(learner.tendencies[learner.tendencies.length - 1]),
      });
      return {learner, adjuster}
    } catch (e) {
      throw new GraphQLError(`Learner with id ${learnerId} does not exists`,
        {
          extensions: {
            code: 'NOT_FOUND'
          }
        }
      )
    }
  }

  private createCorrectnessFunction(simulationData: ISimulationData, stepSize = 0.1): GetCorrectness {
    if (simulationData.agentBehavior === 'real') {
      const normStepSize = Math.round((stepSize / (simulationData.maxDifficulty - simulationData.minDifficulty)) * 10 ** 2) / 10 ** 2;
      return (learner: Learner, difficulty: number, loId?: string) => {
        if (!loId) {
          throw new Error('Missing values stepSize or loId');
        }
        return calculateLearnerCorrectness(simulationData.adjuster, simulationData.adleteConfig, loId, difficulty, normStepSize);
      };
    }

    const correctFunc = constantCorrectness(simulationData.agentBehavior);
    if (correctFunc != null) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return (learner: Learner, difficulty: number, loId?: string) => correctFunc();
    }

    const expression = calculateCorrectness(simulationData.simulationSteps, simulationData.agentBehavior);
    if (expression != null) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return (learner: Learner, difficulty: number, loId?: string) => {
        return expression(learner.overAllFinishedActivities);
      };
    }
    throw new GraphQLError('Unknown Behaviour', {
      extensions: {
        code: "BAD_USER_INPUT"
      }
    });
  }

  private removeEntryFromTaskSet<T>(array: Array<T>, index: number): T {
    if (index < 0 || index > array.length - 1) return null;
    const element = array.splice(index, 1);
    if (array.length < 0) {
      return null;
    }
    return element[0];
  }

  private errorMessage(set: string, err: Error) {
    console.error(`Error saving recommendations:\n${err}`);
  }
}
