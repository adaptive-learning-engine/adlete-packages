import { GraphQLError } from "graphql";
import { Arg, Authorized, Ctx, Int, Query, Resolver } from "type-graphql";

import { LearnerModel } from "../entities/Learner";
import { VisualizerPayload } from "../entities/VisualizerPayload";
import { Roles } from "../util/ClientValidator";
import { Context } from "../util/Context";
import { decryptData, encryptData } from "../util/SecretGenerator";
import { generateAccessToken } from "../util/TokenGenerator";

@Resolver()
export class VisualizerResolver {
    @Authorized("ADMIN", "CLIENTINSTANCE")
    @Query(() => VisualizerPayload)
    async getVisualizer(
        @Ctx() {serverURL}:Context,
        @Arg("learnerIds", () => [String]) learnerIds: string[],
        @Arg("timeout", () => Int, {defaultValue: 60 * 30, description: "Time until the token expires in seconds"}) timeout: number
    ) {
        if(learnerIds.length === 0) {
            throw new GraphQLError("LearnerID array is empty.", {
                extensions: {
                    code: "BAD_USER_INPUT"
                }
            })
        }

        if(timeout <= 0) {
            throw new GraphQLError("Timeout has to be greater than 0.", {
                extensions: {
                    code: "BAD_USER_INPUT"
                }
            })
        }
        const url = new URL(process.env.visualizerurl)
        const existingLearner = await LearnerModel.find({learnerId: {$in: learnerIds}})
        if(existingLearner.length != learnerIds.length) {
            const unknownIds = learnerIds.filter((id) => (!existingLearner.some(doc => doc.learnerId === id)))
            throw new GraphQLError(`Couldn"t find the following learnerIds: ${unknownIds}`, {
                extensions: {
                    code: "NOT_FOUND"
                }
            })
        }
        try {
            const {encrypted, iv} = encryptData(JSON.stringify(learnerIds))
            const token = await generateAccessToken({username: "educator", roles: [Roles[Roles.EDUCATOR]], iv}, `${timeout}sec`)
            url.searchParams.append("token", token)
            url.searchParams.append("idcipher", encrypted)
            url.searchParams.append("host", serverURL ?? "")
            return new VisualizerPayload(url.toString(), timeout)
        } catch(err) {
            throw new GraphQLError("An error ocured while generating access url", {
                extensions: {
                    code: "INTERNAL_ERROR"
                }
            })
        }
    }

    @Authorized("ADMIN", "CLIENTINSTANCE", "EDUCATOR")
    @Query(() => [String])
    async getLearnerIds(
        @Ctx() {user}: Context,
        @Arg("idCipher") idCipher: string
    ) {
        try {
            const data: string[] = JSON.parse(decryptData(idCipher, user?.iv))
            if(Array.isArray(data)) {
                return data;
            }
            throw new GraphQLError("idCipher has wrong format", {
                extensions: {
                    code: "BAD_USER_INPUT"
                }
            })
        } catch {
            throw new GraphQLError("idCipher not correct", {
                extensions: {
                    code: "BAD_USER_INPUT"
                }
            })
        }
    }
}