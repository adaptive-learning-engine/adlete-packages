import { GraphQLError } from 'graphql';
import { Arg, Authorized, Ctx, Query, Resolver } from 'type-graphql';

import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';

import { CompetenceInfo } from '../entities/CompetenceInfo';
import { LearnerAnalytics } from '../entities/LearnerAnalytics';
import { ServiceConfiguration } from '../entities/ServiceConfiguration';
import { validateEducatorRights } from '../util/ClientValidator';
import { Context } from '../util/Context';

import { LearnerResolver } from './LearnerResolver';
import { ObservationResolver } from './ObservationResolver';


@Resolver(() => LearnerAnalytics)
export class LearnerAnalyticsResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
  @Query(() => LearnerAnalytics)
  async learnerAnalytics(@Ctx() { adleteConfig, user }: Context, 
  @Arg('learnerId') learnerId: string,
  @Arg('idCipher', {nullable: true, description: "A encrypted value of learnerIds that the user can query"}) idCipher: string): Promise<LearnerAnalytics> {
    if(user.roles.length == 1 && user.roles[0] === 'EDUCATOR') {
      if(idCipher === null) {
        throw new GraphQLError(`Not allowed to access LearnerID: ${learnerId}`, {
          extensions: {
            code: 'FORBIDDEN'
          }
        })
      }
      const availableLearnerIds = validateEducatorRights(idCipher, user.iv)

      if(!availableLearnerIds.includes(learnerId)) {
        throw new GraphQLError(`Not allowed to access LearnerID: ${learnerId}`, {
          extensions: {
            code: 'FORBIDDEN'
          }
        })
      }
    }

    return {
      serviceConfiguration: new ServiceConfiguration(adleteConfig),
      learner: await LearnerResolver.getLearner(learnerId),
      observations: await ObservationResolver.getObservations(learnerId),
    };
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE', 'EDUCATOR')
  @Query(() => CompetenceInfo)
  async getLearnerCompetence(
    @Ctx() { user }: Context,
    @Arg('learnerId') learnerId: string,
    @Arg('competenceNames', () => [String], { nullable: true }) competenceNames: string[],
    @Arg('idCipher', {nullable: true, deprecationReason: "A encrypted value of learnerIds that the user can query"}) idCipher: string
  ): Promise<CompetenceInfo> {
    if(user.roles.length == 1 && user.roles[0] === 'EDUCATOR') {
      const availableLearnerIds = validateEducatorRights(idCipher, user.iv)

      if(!availableLearnerIds.includes(learnerId)) {
        throw new GraphQLError(`Not allowed to access LearnerID: ${learnerId}`, {
          extensions: {
            code: 'FORBIDDEN'
          }
        })
      }
    }

    const learner = await LearnerResolver.getLearner(learnerId);
    const beliefs: IScalarBeliefBundle = JSON.parse(learner.scalarBeliefs[learner.scalarBeliefs.length - 1]);
    const names: string[] = [];
    competenceNames.forEach((name) => {
      try {
        const belief = beliefs[name];
        if (belief) {
          beliefs[name] = belief;
          names.push(name);
        }
      } catch (err) {
        // Ignore none existing entries
      }
    });
    return { scalarBeliefs: JSON.stringify(beliefs) };
  }
}
