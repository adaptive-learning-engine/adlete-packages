import { Ref } from '@typegoose/typegoose';
import { GraphQLError } from 'graphql';
import { ObjectId } from 'mongodb';
import { Arg, Authorized, FieldResolver, Mutation, Query, Resolver, Root } from 'type-graphql';

import { BayesBeliefError } from '@adlete/engine-blocks/utils/Errors';

import { ActivityStatistics, ActivityStatisticsModel } from '../entities/ActivityStatistics';
import { Session, SessionDocType, SessionModel } from '../entities/Session';
import { SessionCacheModel } from '../entities/SessionCache';
import { StatusInfo } from '../entities/StatusInfo';

import { createDBObjectLearnerRefResolver } from './DBObjectLearnerRefResolver';
import { LearnerResolver } from './LearnerResolver';

const DBObjectLearnerRefResolver = createDBObjectLearnerRefResolver<Session>(Session);

@Resolver(() => Session)
export class SessionResolver extends DBObjectLearnerRefResolver {
  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => Session)
  async startSession(@Arg('learnerId') learnerId: string): Promise<Session> {
    const learner = await LearnerResolver.getLearner(learnerId);

    const cachedSession = await SessionCacheModel.findByIdAndDelete({ _id: learnerId });
    if (cachedSession) {
      await cachedSession.populate('activityStatistics');
    }
    await this.stopActiveSession(learner._id, cachedSession?.activityStatistics);
    let session = new SessionModel({
      learner,
      startTimestamp: Date.now(),
      active: true,
    } as unknown as Session);

    session = await session.save();
    try {
      const currentSession = new SessionCacheModel({
        _id: learner.learnerId,
        session: session,
        learnerObjectId: learner._id,
        activityStatistics: [],
        network: 'test',
        probabilityBelief: learner.probabilisticBeliefs[learner.probabilisticBeliefs.length - 1],
        scalarBelief: learner.scalarBeliefs[learner.scalarBeliefs.length - 1],
        tendency: learner.tendencies[learner.tendencies.length - 1],
        overallFinishedActivities: learner.overAllFinishedActivities ?? 0,
      });
      await currentSession.save();
    } catch (err) {
      if (err instanceof BayesBeliefError) {
        throw new GraphQLError(err.message, {
          extensions: {
            code: 'BAD_USER_INPUT',
          },
        });
      }
    }

    return session;
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Mutation(() => StatusInfo)
  async stopSession(@Arg('learnerId') learnerId: string): Promise<StatusInfo> {
    const learner = await LearnerResolver.getLearner(learnerId);

    const cachedSession = await SessionCacheModel.findByIdAndDelete(learnerId);
    if (cachedSession) {
      await cachedSession.populate('activityStatistics');
    }
    await this.stopActiveSession(learner._id, cachedSession?.activityStatistics);
    return {
      statusCode: 200,
      statusDescription: 'Session was stopped!',
      timestamp: new Date(),
    };
  }

  protected async stopActiveSession(learnerObjectId: ObjectId, activityStatistics?: Ref<ActivityStatistics>[]): Promise<Session> {
    return SessionModel.findOneAndUpdate(
      { learner: learnerObjectId, active: true },
      { active: false, stopTimestamp: new Date(), activitiesStatistics: activityStatistics }
    );
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @FieldResolver(() => [ActivityStatistics])
  async activitiesStatistics(@Root() session: Session): Promise<ActivityStatistics[]> {
    return ActivityStatisticsModel.find({ _id: { $in: session.activitiesStatistics } });
  }

  @Authorized('ADMIN', 'CLIENTINSTANCE')
  @Query(() => Session)
  async activeSession(@Arg('learnerId') learnerId: string): Promise<Session> {
    return SessionResolver.getActiveSession(learnerId);
  }

  static async getActiveSession(learnerId: string): Promise<SessionDocType> {
    const learner = await LearnerResolver.getLearner(learnerId);

    const session = await SessionModel.findOne({ learner: learner._id, active: true });

    if (session == null) {
      throw new GraphQLError(
        `The given user: ${learner.learnerId} does not have an active session. Use ´startSession´ to create a new session for the user.`,
        {extensions: {
          code: "NOT_FOUND"
        }}
      );
    }

    return session;
  }
}
