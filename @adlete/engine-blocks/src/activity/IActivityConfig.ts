/* use ActivityName to give an Activity a specific name */
export type ActivityName = string;

/* use IActivityConfig to define Activities */
export interface IActivityConfig {
  name: ActivityName;
}
