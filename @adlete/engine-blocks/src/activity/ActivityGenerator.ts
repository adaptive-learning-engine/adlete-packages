import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';

export interface IActivityGenerator<TRecommendation, TActivityConfig> {
  generate(recommendation: TRecommendation): TActivityConfig;
}

export class ActivityGenerator<TDifficulty, TRecommendation extends IActivityRecommendation<TDifficulty>, TActivityConfig>
  implements IActivityGenerator<TRecommendation, TActivityConfig>
{
  generators: { [key: string]: IActivityGenerator<TRecommendation, TActivityConfig> };

  public addGenerator(activity: string, generator: IActivityGenerator<TRecommendation, TActivityConfig>): void {
    this.generators[activity] = generator;
  }

  public generate(recommendation: TRecommendation): TActivityConfig {
    return this.generators[recommendation.activityName].generate(recommendation);
  }
}
