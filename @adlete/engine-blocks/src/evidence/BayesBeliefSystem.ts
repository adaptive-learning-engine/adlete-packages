import { Edge } from 'graphlib';

import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IProbabilityBelief, IProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';

import { IProbabilityBeliefSystem } from '@adlete/engine-blocks/evidence/IBeliefSystem';
import { IProbabilityEvidence, IProbabilityEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { calcUpdatedProbabilityBeliefStandard } from '@adlete/engine-blocks/evidence/belief-update';

/**
 * The BayesBeliefSystem contains a BayesBeliefModel, which holds the current
 * beliefs about the player. Since a Bayesian Network holds probability
 * variables it implements the interface IProbabilityBeliefSystem. The class provides
 * the functionality to create and infer a Bayesian Network and further update it
 * regarding a given evidence.
 */
export class BayesBeliefSystem implements IProbabilityBeliefSystem {
  public beliefModel: BayesBeliefModel;

  private evidenceEffectSize: number;

  constructor(beliefModel: BayesBeliefModel, evidenceEffectSize: number) {
    this.beliefModel = beliefModel;
    this.evidenceEffectSize = evidenceEffectSize;
  }

  /**
   * Check whatever the node with the given name is a leaf node
   *
   * @param name the name of the node
   */
  isLeafNode(name: string): boolean {
    return this.beliefModel.isLeafNode(name);
  }

  /**
   * Get the current value of a belief using its name.
   * Please consider that depending on the node the Bayesian Network
   * must be inferred, which might take some time in large belief models.
   *
   * @param   name    Name of the belief
   *
   * @returns Probability values according to the predefined states.
   */
  public getBelief(name: string): IProbabilityBelief {
    return this.beliefModel.getBelief(name);
  }

  public getBeliefs(): IProbabilityBeliefBundle {
    return this.beliefModel.getBeliefs()
  }

  public getLeafBeliefs(): IProbabilityBeliefBundle {
    return this.beliefModel.getLeafBeliefs();
  }

  /**
   * Update a belief within the Bayesian Network.
   *
   * @param   name    Name of the belief, which should be updated
   * @param   belief  New value of the belief
   */
  public setBelief(name: string, belief: IProbabilityBelief): void {
    this.beliefModel.setBelief(name, belief);
  }

  /**
   * Update a beliefs within the Bayesian Network.
   *
   * @param   beliefs  New beliefs
   */
  public setBeliefs(beliefs: IProbabilityBeliefBundle): void {
    this.beliefModel.setBeliefs(beliefs);
  }

  /**
   * Update the current belief model according to given evidence
   *
   * @param   evidence   Probability evidence, which was inferred from an observation
   */
  public addEvidence(evidence: IProbabilityEvidenceBundle): void {
    const beliefsToUpdate: IProbabilityBeliefBundle = {};
    Object.keys(evidence).forEach((beliefName) => {
      beliefsToUpdate[beliefName] = this.calcSingleEvidence(beliefName, evidence[beliefName], beliefsToUpdate);
    });

    this.setBeliefs(beliefsToUpdate);
  }

  private calcSingleEvidence(beliefName: string, evidence: IProbabilityEvidence, cache: IProbabilityBeliefBundle): IProbabilityBelief {
    if (evidence.probs.length !== this.beliefModel.getStates().length) {
      throw new Error('Evidence must have the same dimension as states!');
    }

    if (1 - evidence.probs.reduce((accumulator: number, currentValue: number) => accumulator + currentValue) > 0.0001) {
      throw new Error('The sum of the probabilities must be one!');
    }

    const structure = this.beliefModel.getStructure();
    const inEdges = structure.inEdges(beliefName) as Edge[];

    if (inEdges === undefined) {
      throw new Error('The given beliefName could not be found.');
    }

    if (inEdges.length > 0) {
      throw new Error('Evidence must be applied to leaf a node!');
    }

    const belief = Object.prototype.hasOwnProperty.call(cache, beliefName) ? cache[beliefName] : this.getBelief(beliefName);

    return calcUpdatedProbabilityBeliefStandard(evidence, belief, this.evidenceEffectSize);
  }
}
