import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';

import { IActivityEvidenceBundle, IScalarEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';

/**
 * Ignore success on easier activities to avoid downgrading.
 *
 * @param   {IScalarBeliefBundle}     beliefs           [beliefs description]
 * @param   {IScalarEvidenceBundle}   scalarEvidences   [scalarEvidences description]
 * @param   {IActivityEvidenceBundle} activityEvidences [activityEvidences description]
 * @param   {number}                  threshold         [threshold description]
 *
 * @return  {[type]}                                    [return description]
 */
// eslint-disable-next-line import/prefer-default-export
export function ignoreDowngrades(
  beliefs: IScalarBeliefBundle,
  scalarEvidences: IScalarEvidenceBundle,
  activityEvidences: IActivityEvidenceBundle,
  threshold: number
): IScalarEvidenceBundle {
  const results: IScalarEvidenceBundle = {};

  Object.entries(activityEvidences).forEach(([name, evd]) => {
    /**
     * To avoid local minima values get rounded to 4 floating points.
     * Only if the different is significant the activity evidence will be dismissed.
     */
    const scalarEvidenceValue = Math.round(scalarEvidences[name].value * 10000) / 10000;
    const beliefValue = Math.round(beliefs[name].value * 10000) / 10000;

    if (!(scalarEvidenceValue < beliefValue && evd.correctness > threshold)) {
      results[name] = scalarEvidences[name];
    }
  });
  return results;
}
