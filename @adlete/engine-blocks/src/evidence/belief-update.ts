import { IProbabilityBelief } from '@adlete/engine-blocks/belief/IBelief';

import { IProbabilityEvidence } from '@adlete/engine-blocks/evidence/IEvidence';

function addVec(a: number[], b: number[]): number[] {
  const c = [];
  for (let i = 0; i < Math.max(a.length, b.length); i += 1) {
    c.push((a[i] || 0) + (b[i] || 0));
  }
  return c;
}

function subVec(a: number[], b: number[]): number[] {
  const c = [];
  for (let i = 0; i < Math.max(a.length, b.length); i += 1) {
    c.push((a[i] || 0) - (b[i] || 0));
  }
  return c;
}

function multiplyVec(a: number[], b: number[]): number[] {
  const c = [];
  for (let i = 0; i < Math.max(a.length, b.length); i += 1) {
    c.push((a[i] || 0) * (b[i] || 0));
  }
  return c;
}

/**
 * Calculates a new belief from a current belief and new evidence.
 *
 * Uses the weight of the evidence and a given effectSize for applying the evidence.
 *
 * @param   evidence    Evidence to apply
 * @param   currBelief  Current belief
 * @param   effectSize  Effect size for the application
 *
 * @return  New belief
 */
export function calcUpdatedProbabilityBeliefStandard(
  evidence: IProbabilityEvidence,
  currBelief: IProbabilityBelief,
  effectSize: number
): IProbabilityBelief {
  const effSizeAndWeight = currBelief.map(() => effectSize * evidence.weight);

  /* MathJS Expression:
   * const expression = `${matrix( currBelief )} + ${matrix( effSize )} .* (${matrix( evidence.probs )} - ${matrix( currBelief )} )`;
   * const result = evaluate( expression );
   */
  const sub = subVec(evidence.probs, currBelief);
  const multi = multiplyVec(effSizeAndWeight, sub);
  const result = addVec(currBelief, multi);
  return result;
}
