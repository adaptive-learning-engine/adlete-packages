import { IProbabilityVector } from '@adlete/engine-blocks/belief/IBelief';

/**
 * Evidence for a belief
 */
export interface IEvidence {
  /**
   * Weight used to apply evidence to belief model
   */
  weight: number;
}

/**
 * Evidence for multiple named beliefs
 */
export interface IEvidenceBundle<TEvidence extends IEvidence> {
  [key: string]: TEvidence;
}

/**
 * Scalar evidence for a belief
 */
export interface IScalarEvidence extends IEvidence {
  /**
   * Scalar value for evidence
   */
  value: number;
}

/**
 * Scalar evidence for multiple named beliefs
 */
export type IScalarEvidenceBundle = IEvidenceBundle<IScalarEvidence>;

/**
 * Activity evidence for a belief
 */
export interface IActivityEvidence extends IEvidence {
  /**
   * Difficulty of the activity the user solved to produce this evidence
   */
  difficulty: number;

  /**
   * Correctness of the how the user solved a given activity to produce this evidence
   */
  correctness: number;
}

/**
 * Activity evidence for multiple named beliefs
 */
export type IActivityEvidenceBundle = IEvidenceBundle<IActivityEvidence>;

/**
 * Probability evidence for a belief. Contains probabilities for the
 * different states.
 */
export interface IProbabilityEvidence extends IEvidence {
  probs: IProbabilityVector;
}

/**
 * Probability evidence for multiple named beliefs
 */
export type IProbabilityEvidenceBundle = IEvidenceBundle<IProbabilityEvidence>;
