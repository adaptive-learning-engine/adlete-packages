/**
 * Describes an object that can interpret evidence
 */
export interface IEvidenceInterpreter<TEvidence> {
  /**
   * Interprets evidence
   *
   * @param   evidence   Evidence, which probably was inferred from observations
   */
  interpret(evidence: TEvidence): void;
}
