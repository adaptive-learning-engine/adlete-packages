import { scalarToProbabilityVector } from '@adlete/engine-blocks/utils/Probability';

import {
  IActivityEvidence,
  IActivityEvidenceBundle,
  IProbabilityEvidence,
  IProbabilityEvidenceBundle,
  IScalarEvidence,
  IScalarEvidenceBundle,
} from './IEvidence.js';

/**
 * Converts a single scalar evidence to a probability evidence by interpreting
 * the given scalar value as the center of mass of a probability distribution.
 *
 * @param   scalarEvidence    Evidence to convert
 * @param   numProbabilities  Amount of probabilities that the resulting probability evidence has
 *
 * @returns Converted probability evidence
 */
export function scalarToProbabilityEvidence(scalarEvidence: IScalarEvidence, numProbabilities: number): IProbabilityEvidence {
  return { probs: scalarToProbabilityVector(scalarEvidence.value, numProbabilities), weight: scalarEvidence.weight };
}

export type ScalarToProbabilityEvidenceConverter = (scalarEvidence: IScalarEvidence, numProbabilities: number) => IProbabilityEvidence;

/**
 * Converts a scalar evidence to a probability evidence by interpreting
 * each given scalar value as the center of mass of a probability distribution.
 *
 * @param   scalarEvidence    Evidence to convert
 * @param   numProbabilities  Amount of probabilities that the resulting probability evidence has
 *
 * @returns Converted probability evidence
 */
export function scalarToProbabilityEvidenceBundle(
  converter: ScalarToProbabilityEvidenceConverter,
  scalarEvidence: IScalarEvidenceBundle,
  numProbabilities: number
): IProbabilityEvidenceBundle {
  const result: IProbabilityEvidenceBundle = {};

  Object.entries(scalarEvidence).forEach(([evdName, evd]) => {
    result[evdName] = converter(evd, numProbabilities);
  });

  return result;
}

export function activityToScalarEvidence(activityEvidence: IActivityEvidence): IScalarEvidence {
  return {
    weight: activityEvidence.weight,
    value: activityEvidence.correctness * activityEvidence.difficulty,
  };
}

export function activityToScalarEvidenceBundle(activityEvidence: IActivityEvidenceBundle): IScalarEvidenceBundle {
  const result: IScalarEvidenceBundle = {};

  Object.entries(activityEvidence).forEach(([evdName, evd]) => {
    result[evdName] = activityToScalarEvidence(evd);
  });

  return result;
}
