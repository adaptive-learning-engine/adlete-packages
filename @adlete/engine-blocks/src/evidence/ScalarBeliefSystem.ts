import { IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { ScalarBeliefModel } from '@adlete/engine-blocks/belief/ScalarBeliefModel';

import { IScalarBeliefSystem } from '@adlete/engine-blocks/evidence/IBeliefSystem';

/**
 * A belief system that holds scalar beliefs. This one is static, meaning it cannot
 * interpret evidence.
 */
export class ScalarBeliefSystem implements IScalarBeliefSystem {
  public beliefModel: ScalarBeliefModel = null;

  constructor(beliefModel: ScalarBeliefModel) {
    this.beliefModel = beliefModel;
  }

  isLeafNode(name: string): boolean {
    return this.beliefModel.isLeafNode(name);
  }

  public getBelief(name: string): IScalarBelief {
    return this.beliefModel.getBelief(name);
  }

  public setBelief(name: string, belief: IScalarBelief): void {
    this.beliefModel.setBelief(name, belief);
  }

  // eslint-disable-next-line class-methods-use-this
  public addEvidence(/* evidence: IScalarEvidence */): void {
    throw new Error('Method not implemented.');
  }
}
