import { IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';

import { IProbabilityBeliefSystem, IScalarBeliefSystem } from '@adlete/engine-blocks/evidence/IBeliefSystem';
import {
  IProbabilityEvidence,
  IProbabilityEvidenceBundle,
  IScalarEvidence,
  IScalarEvidenceBundle,
} from '@adlete/engine-blocks/evidence/IEvidence';
import { IEvidenceInterpreter } from '@adlete/engine-blocks/evidence/IEvidenceInterpreter';
import { scalarToProbabilityEvidenceBundle } from '@adlete/engine-blocks/evidence/convert';

export type ScalarToProbabilityEvidence = (scalarEvidence: IScalarEvidence, numProbabilities: number) => IProbabilityEvidence;
export type ProbabilityToScalarBelief = (probabilityBelief: IProbabilityBelief) => IScalarBelief;

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IScalarEvidenceInterpreterParams {}

export interface IScalarEvidenceInterpreterOptions {
  /**
   * Conversion function for converting scalar to probability evidence
   */
  scalarToProbabilityEvidence: ScalarToProbabilityEvidence;

  /**
   * Conversion function for converting probabilistic to scalar evidence
   */
  probabilityToScalarBelief: ProbabilityToScalarBelief;
}

/**
 * The ScalarEvidenceInterpreter is an evidence interpreter that interprets scalar evidence,
 * but uses a probabilistic belief system (like a BayesBeliefSystem) internally.
 *
 * Thus it converts scalar evidence to probability evidence to update the
 * probabilistic belief system.
 *
 * It is useful, because scalar evidence and belief can be handled easier by users
 * compared to probabilistic evidence and beliefs.
 */
export class ScalarEvidenceInterpreter implements IEvidenceInterpreter<IScalarEvidenceBundle> {
  public opts: IScalarEvidenceInterpreterOptions;

  public params: IScalarEvidenceInterpreterParams;

  public probabilityBeliefSystem: IProbabilityBeliefSystem;

  public scalarBeliefSystem: IScalarBeliefSystem;

  protected numBeliefProbabilityStates: number;

  public scalarToProbabilityEvidence: ScalarToProbabilityEvidence;

  public probabilityToScalarBelief: ProbabilityToScalarBelief;

  /**
   * To create a new scalar evidence interpreter a probability belief system
   * and a scalar belief system is required.
   * Both belief systems will be updated during the interpret process.
   *
   * @param probBeliefSystem
   * @param scalarBeliefSystem
   * @param opts
   */
  constructor(
    probBeliefSystem: IProbabilityBeliefSystem,
    scalarBeliefSystem: IScalarBeliefSystem,
    opts: IScalarEvidenceInterpreterOptions,
    updateBeliefSystem = true
  ) {
    this.probabilityBeliefSystem = probBeliefSystem;
    this.numBeliefProbabilityStates = probBeliefSystem.beliefModel.getStates().length;

    this.scalarBeliefSystem = scalarBeliefSystem;
    this.opts = opts;
    this.scalarToProbabilityEvidence = opts.scalarToProbabilityEvidence;
    this.probabilityToScalarBelief = opts.probabilityToScalarBelief;

    if(updateBeliefSystem) {
      // sync both belief systems
      this.updateScalarBeliefSystem();
    }
  }

  /**
   * Interprets scalar evidence and updates its belief systems accordingly .
   *
   * This method converts a scalar evidence to a probabilistic evidence
   * and updates.
   *
   * @param scalarEvidence
   */
  public interpret(scalarEvidence: IScalarEvidenceBundle): void {
    const probEvidence = scalarToProbabilityEvidenceBundle(
      this.scalarToProbabilityEvidence,
      scalarEvidence,
      this.numBeliefProbabilityStates
    );
    this.interpretProbabilityEvidence(probEvidence);
  }

  protected interpretProbabilityEvidence(probEvidence: IProbabilityEvidenceBundle): void {
    this.probabilityBeliefSystem.addEvidence(probEvidence);
    this.updateScalarBeliefSystem();
  }

  protected updateScalarBeliefSystem(): void {
    const beliefNames = this.probabilityBeliefSystem.beliefModel.getNames();
    beliefNames.forEach((name: string) => {
      const probBelief = this.probabilityBeliefSystem.getBelief(name);
      const scalarBelief = this.probabilityToScalarBelief(probBelief);
      this.scalarBeliefSystem.setBelief(name, scalarBelief);
    });
  }

  public updateParameters(params: IScalarEvidenceInterpreterParams): void {
    this.params = params;
  }
}
