/* eslint-disable @typescript-eslint/no-explicit-any */
import { IBelief, IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IBeliefModel, IProbabilityBeliefModel } from '@adlete/engine-blocks/belief/IBeliefModel';

import { IEvidence, IEvidenceBundle, IProbabilityEvidence, IScalarEvidence } from '@adlete/engine-blocks/evidence/IEvidence';

/**
 * A type to describe a general belief system
 */
export interface IBeliefSystem<TEvidence extends IEvidence, TBelief extends IBelief> {
  // TODO: really expose graph? how to make read-only for outsiders?
  beliefModel: IBeliefModel<TBelief>;

  // TODO: make async?
  /**
   * Get the current value of a belief using its name.
   *
   * @param   name    Name of the belief
   *
   * @returns Belief
   */
  getBelief(name: string): TBelief;

  // TODO: make async?
  /**
   * Update a belief by its name
   *
   * @param   name    Name of the belief, which should be updated
   * @param   belief  New value of the belief
   */
  setBelief(name: string, belief: TBelief): void;

  // TODO: make async?
  /**
   * Update the current belief model according to given evidence
   *
   * @param   evidence   Probability evidence, which was inferred from an observation
   */
  addEvidence(evidence: IEvidenceBundle<TEvidence>): void;

  /**
   * Check whatever the node with the given name is a leaf node
   *
   * @param name the name of the node
   */
  isLeafNode(name: string): boolean;
}

/**
 * A belief system for probability beliefs
 */
export interface IProbabilityBeliefSystem extends IBeliefSystem<IProbabilityEvidence, IProbabilityBelief> {
  beliefModel: IProbabilityBeliefModel;
}

/**
 * A belief system for scalar beliefs
 */
export type IScalarBeliefSystem = IBeliefSystem<IScalarEvidence, IScalarBelief>;
