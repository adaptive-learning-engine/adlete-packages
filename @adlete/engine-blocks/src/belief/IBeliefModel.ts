import { Graph } from 'graphlib';

import { IBeliefBundle, IProbabilityBelief } from '@adlete/engine-blocks/belief/IBelief';

export interface IBeliefModel<TBelief> {
  getNames(): string[];
  getStates(): string[];
  getBelief(name: string): TBelief;
  setBelief(name: string, value: TBelief): void;

  getStructure(): Graph; // contains current CPTs
  getBeliefs(): IBeliefBundle<TBelief>; // get current values
  setBeliefs(beliefs: IBeliefBundle<TBelief>): void; // set current values

  isLeafNode(name: string): boolean;
}

export type IProbabilityBeliefModel = IBeliefModel<IProbabilityBelief>;
