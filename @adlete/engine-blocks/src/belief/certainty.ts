import * as integral from '@nexys/math-ts/dist/integral.js';

import { IProbabilityBelief } from '@adlete/engine-blocks/belief/IBelief';

/**
 * calculates the percentages of the Standard Distribution for different amounts of intervals, scalar Values and standard Deviations
 * @param x
 * @param mu
 * @param sigma
 * @returns
 */
function standardDistribution(x: number, mu: number, sigma: number): number {
  return (1 / (sigma * Math.sqrt(2 * Math.PI))) * Math.E ** ((-1 / 2) * ((x - mu) / sigma) ** 2);
}

/**
 * generates the standard intervals by integrating over the standard Distribution centered on the scalar Value
 * @param belief
 * @param scalar
 * @returns
 */
function standardIntervals(belief: IProbabilityBelief, scalar: number): number[] {
  const standardArray: number[] = [];
  let borderBottom = -10;
  let borderTop = 1 / belief.length;
  for (let i = 0; i < belief.length; i++) {
    /**
     * last belief gets the value 10 to include every
     * possible transformation of the standard distribution
     * The bottom and the top border need to be adletepted after each step
     */
    if (i === belief.length - 1) {
      borderTop = 10;
      borderBottom += 1 / belief.length;
    } else if (i !== 0) {
      borderBottom += 1 / belief.length;
      borderTop += 1 / belief.length;
    }
    // calculates the standard distribution inside the interval for 100 steps with the simpson method
    standardArray[i] = integral.generic(
      (x) => standardDistribution(x, scalar, 1 / (2 * belief.length)),
      [borderBottom, borderTop],
      100,
      'simpson'
    );
    // in order to go on normally from the first step
    // but also include the start value of the bottom Border
    if (i === 0) {
      borderBottom = 0;
    }
  }
  return standardArray;
}

/**
 * This function is for comparison of 2 Intervals (The "original interval" --> Beliefs and the Created one)
 * @param beliefs
 * @param referenceValues
 * @returns
 */
function compareIntervals(beliefs: number[], referenceValues: number[]): number {
  let meanError = 0;
  // calculate the mean absolute error between beliefs[i] and the referenceArray[i] and sum them up
  for (let i = 0; i < beliefs.length; i++) {
    meanError += (beliefs[i] - referenceValues[i]) ** 2;
  }
  meanError = 1 - Math.sqrt(meanError);
  if (meanError < 0) {
    /* eslint-disable no-console */
    console.warn('\x1b[41m%s\x1b[0m', 'calculation error, the certainty would have been below 0');
    meanError = 0;
  }
  if (meanError > 1) {
    /* eslint-disable no-console */
    console.warn('\x1b[41m%s\x1b[0m', 'calculation error, the certainty would have been above 1');
    meanError = 1;
  }
  return meanError;
}

/**
 * main function that calculates the Intervals with the distribution, compares it to the original Beliefs and returns a certainty Value
 * @param belief
 * @param scalarValue
 * @returns
 */
export function calculateCertainty(belief: IProbabilityBelief, scalarValue: number): number {
  const standardDistributed = standardIntervals(belief, scalarValue);
  return compareIntervals(belief, standardDistributed);
}
