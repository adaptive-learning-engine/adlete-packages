import { IBayesNode } from '@adlete/engine-blocks/belief/IBayesNode';

export interface IBayesNet {
  [key: string]: IBayesNode;
}
