export interface IBayesNode {
  id: string;
  parents: string[];
  states: string[];
  cpt: number[];
}
