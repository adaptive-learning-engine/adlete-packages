import { Graph } from 'graphlib';

import { calcMean, scalarToProbabilityVector } from '@adlete/engine-blocks/utils/Probability';

import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import {
  IBeliefBundle,
  IProbabilityBelief,
  IProbabilityBeliefBundle,
  IScalarBelief,
  IScalarBeliefBundle,
} from '@adlete/engine-blocks/belief/IBelief';
import { ScalarBeliefModel } from '@adlete/engine-blocks/belief/ScalarBeliefModel';
import { calculateCertainty } from '@adlete/engine-blocks/belief/certainty';

/**
 * Converts a probability belief to a scalar belief by calculating the center
 * of mass of the probability distribution.
 *
 * @param   probabilityBelief   Belief to convert
 *
 * @returns Converted belief
 */
export function probabilityToScalarBelief(probabilityBelief: IProbabilityBelief): IScalarBelief {
  const belief = calcMean(probabilityBelief);

  return { value: belief, certainty: calculateCertainty(probabilityBelief, belief) };
}

export function probabilityToScalarBeliefBundle(probabilityBelief: IProbabilityBeliefBundle): IScalarBeliefBundle {
  const scalarBeliefs: Record<string, IScalarBelief> = {};
  Object.entries(probabilityBelief).forEach(([name, belief]) => {
    scalarBeliefs[name] = probabilityToScalarBelief(belief);
  });
  return scalarBeliefs;
}

export function scalarToProbabilityBelief(scalarBelief: IScalarBelief, numProbabilities: number): IProbabilityBelief {
  // TODO: use scalarBelief.certainty
  return scalarToProbabilityVector(scalarBelief.value, numProbabilities);
}

export function scalarToProbabilityBeliefBundle(scalarBeliefs: IScalarBeliefBundle, numProbabilities: number): IProbabilityBeliefBundle {
  const probabilityBeliefs: Record<string, IProbabilityBelief> = {};
  Object.entries(scalarBeliefs).forEach(([name, belief]) => {
    probabilityBeliefs[name] = scalarToProbabilityBelief(belief, numProbabilities);
  });
  return probabilityBeliefs;
}

export function structureAndBeliefsToGraph<TBelief>(structure: Graph, beliefs: IBeliefBundle<TBelief>): Graph {
  const graph = new Graph();
  const nodes = structure.nodes();
  const edges = structure.edges();

  for (let i = 0; i < nodes.length; i += 1) {
    graph.setNode(nodes[i], beliefs[nodes[i]]);
  }

  for (let i = 0; i < edges.length; i += 1) {
    graph.setEdge(edges[i].v, edges[i].w);
  }

  return graph;
}

export function structureToGraph(structure: Graph, defaultValue?: number): Graph {
  const graph = new Graph();
  const nodes = structure.nodes();
  const edges = structure.edges();

  for (let i = 0; i < nodes.length; i += 1) {
    graph.setNode(nodes[i], defaultValue);
  }

  for (let i = 0; i < edges.length; i += 1) {
    graph.setEdge(edges[i].v, edges[i].w);
  }

  return graph;
}

export function bayesNetToGraph(bayesNet: IBayesNet): Graph {
  const graph = new Graph();

  Object.keys(bayesNet).forEach((nodeName) => {
    graph.setNode(nodeName, bayesNet[nodeName].cpt);
  });

  Object.keys(bayesNet).forEach((nodeName) => {
    bayesNet[nodeName].parents.forEach((parentName) => {
      graph.setEdge(parentName, nodeName);
    });
  });

  return graph;
}

export function bayesToScalarBeliefModel(bayesModel: BayesBeliefModel): ScalarBeliefModel {
  const beliefs = probabilityToScalarBeliefBundle(bayesModel.getBeliefs());
  const scalarInput = structureAndBeliefsToGraph(bayesModel.getStructure(), beliefs);
  return new ScalarBeliefModel(scalarInput);
}
