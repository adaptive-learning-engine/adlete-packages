/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
import * as graphlib from 'graphlib';
import cloneDeep from 'lodash.clonedeep';

import { IScalarBelief, IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IBeliefModel } from '@adlete/engine-blocks/belief/IBeliefModel';

/**
 * A belief model that simply saves scalar beliefs.
 * Node values are thus of type {@link IScalarBelief}.
 */
export class ScalarBeliefModel implements IBeliefModel<IScalarBelief> {
  protected representationGraph: graphlib.Graph;

  constructor(input: graphlib.Graph) {
    this.representationGraph = input;
  }

  getStates(): string[] {
    throw new Error('Method not implemented.');
  }

  isLeafNode(name: string): boolean {
    const inEdges = this.representationGraph.inEdges(name);

    if (inEdges && inEdges.length > 0) {
      return false;
    }

    return true;
  }

  getNames(): string[] {
    return this.representationGraph.nodes();
  }

  getBelief(name: string): IScalarBelief {
    return this.representationGraph.node(name);
  }

  setBelief(name: string, value: IScalarBelief): void {
    this.representationGraph.setNode(name, value);
  }

  setBeliefs(beliefs: IScalarBeliefBundle): void {
    Object.keys(beliefs).forEach((name) => {
      // TODO: check if node exists
      this.representationGraph.setNode(name, beliefs[name]);
    });
    Object.entries(beliefs);
  }

  getStructure(): graphlib.Graph {
    return this.representationGraph;
  }

  getBeliefs(): IScalarBeliefBundle {
    const beliefs: Record<string, IScalarBelief> = {};
    const nodeNames = this.representationGraph.nodes();
    for (let i = 0; i < nodeNames.length; i += 1) {
      beliefs[nodeNames[i]] = this.representationGraph.node(nodeNames[i]);
    }
    return beliefs;
  }

  public static serializeStructure(model: ScalarBeliefModel): Object {
    return graphlib.json.write(model.representationGraph);
  }

  public static deserializeStructure(serialized: Object): graphlib.Graph {
    return graphlib.json.read(serialized);
  }

  public static serializeBeliefs(model: ScalarBeliefModel): Object {
    return cloneDeep(model.getBeliefs()) as Object;
  }

  public static deserializeBeliefs(serialized: Object): IScalarBeliefBundle {
    return serialized as IScalarBeliefBundle;
  }

  public static serialize(model: ScalarBeliefModel): Object {
    // everything is contained within the structure
    return ScalarBeliefModel.serializeStructure(model);
  }

  public static deserialize(serialized: Object): ScalarBeliefModel {
    const graph = graphlib.json.read(serialized);
    return new ScalarBeliefModel(graph);
  }
}
