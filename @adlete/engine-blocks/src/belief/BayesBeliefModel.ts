import { Graph } from 'graphlib';


import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IBayesNode } from '@adlete/engine-blocks/belief/IBayesNode';
import { IProbabilityBelief, IProbabilityBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IProbabilityBeliefModel } from '@adlete/engine-blocks/belief/IBeliefModel';
import { BayesBeliefError } from '@adlete/engine-blocks/utils/Errors';
import {
  BayesNet,
  BayesNetFactory,
  convertVectorString,
  getCPT,
  getDistributionFromPotential,
  Instantiation,
  ShaferShenoyInference,
  Vector,
  XDSLBNWriter,
} from '@adlete/node-agrum';

/**
 * A belief model that uses a bayes network internally to update related beliefs.
 * Node values are thus of type {@link IProbabilityBelief}.
 */
export class BayesBeliefModel implements IProbabilityBeliefModel {
  private static bayesNet: BayesNet;
  private static leafNodeNames: string[] = []

  protected inferenceEngine: ShaferShenoyInference;

  /**
   * A flag to store the information if the bayesian network was updated
   * ToDo: isInferenceOutdatedPotentials would be the internal used function to get this information
   * however it is not part of the lib...
   * http://www-desir.lip6.fr/~phw/aGrUM/docs/current/doxygen/d3/d16/classgum_1_1EvidenceInference.html#details
   */
  private isBayesNetDirty = true;

  constructor(beliefs?: IProbabilityBeliefBundle) {
    if(!BayesBeliefModel.bayesNet) {
      throw new Error('No bayes network has been initialized.')
    }
    this.isBayesNetDirty = true;
    this.inferenceEngine = new ShaferShenoyInference(BayesBeliefModel.bayesNet);

    if (beliefs !== undefined) {
      this.setBeliefs(beliefs, true);
    }
  }

  isLeafNode(name: string): boolean {
    return BayesBeliefModel.leafNodeNames.includes(name)
  }

  /**
   *
   * @returns
   */
  getStructure(): Graph {
    const graph = new Graph();
    const bayesNet = this.serialize();
    const nodes = Object.values(bayesNet);

    // nodes
    for (let i = 0; i < nodes.length; i += 1) {
      graph.setNode(nodes[i].id, nodes[i]);
    }

    // connections
    for (let i = 0; i < nodes.length; i += 1) {
      for (let j = 0; j < nodes[i].parents.length; j += 1) {
        graph.setEdge(nodes[i].parents[j], nodes[i].id);
      }
    }

    return graph;
  }

  /**
   * Returns the states of the Bayesian network
   * Since we support only uniform bayesian network all nodes have the same amount of states (e.g.: low, medium, high).
   * Therefore this function returns the states of the first node in the bayesian network.
   *
   * @returns states as string[]
   */
  public getStates(): string[] {
    return convertVectorString(BayesBeliefModel.bayesNet.variable(BayesBeliefModel.bayesNet.nodes()[0]).labels());
  }

  /**
   * Returns all names of all nodes within the bayesian network
   * @returns
   */
  public getNames(): string[] {
    const nodes = BayesBeliefModel.bayesNet.nodes();
    return nodes.map((nodeId: number) => BayesBeliefModel.bayesNet.variable(nodeId).name());
  }

  /**
   * Use this method to set the probability beliefs of a set of given nodes
   * Internally this method is using the method @setBelief
   * @param beliefs
   */
  public setBeliefs(beliefs: IProbabilityBeliefBundle, ignoreChildNodes = false): void {
    Object.keys(beliefs).forEach((beliefName) => {
      this.setBelief(beliefName, beliefs[beliefName], ignoreChildNodes);
    });
  }

  public setBelief(name: string, value: IProbabilityBelief, ignoreChildNodes = false): void {
    const nodeId: number = BayesBeliefModel.getIdFromName(name);

    if (!this.isLeafNode(name)) {
      if(ignoreChildNodes) {
        return
      }
      throw new BayesBeliefError('You can only set probabilities to a leaf node.');
    }

    if (BayesBeliefModel.bayesNet.variable(nodeId).domainSize() !== value.length) {
      throw new BayesBeliefError('Given belief has diff. length than internal beliefs!');
    }

    const vec = new Vector();
    value.forEach((val) => vec.add(val));

    if (this.inferenceEngine.hasEvidence(nodeId)) {
      this.inferenceEngine.chgEvidence(nodeId, vec);
    } else {
      this.inferenceEngine.addEvidence(nodeId, vec);
    }

    this.isBayesNetDirty = true;
  }

  getBeliefs(): IProbabilityBeliefBundle {
    const beliefs: IProbabilityBeliefBundle = {};

    const nodes = BayesBeliefModel.bayesNet.nodes();
    for (let i = 0; i < nodes.length; i += 1) {
      const nodeName = BayesBeliefModel.bayesNet.variable(nodes[i]).name();
      beliefs[nodeName] = this.getBelief(nodeName);
    }

    return beliefs;
  }

  getLeafBeliefs(): IProbabilityBeliefBundle {
    const beliefs: IProbabilityBeliefBundle = {};

    for (let i = 0; i < BayesBeliefModel.leafNodeNames.length; i++) {
      const nodeName = BayesBeliefModel.leafNodeNames[i]
      if(this.isLeafNode(nodeName)) {
        beliefs[nodeName] = this.getBelief(nodeName);
      }
    }

    return beliefs;
  }

  public getBelief(name: string): IProbabilityBelief {
    if (this.isBayesNetDirty) {
      this.inferBeliefs();
    }

    const potential = this.inferenceEngine.posterior(name);
    return getDistributionFromPotential(potential);
  }

  printBelief(name: string): string {
    if (this.isBayesNetDirty) {
      this.inferBeliefs();
    }
    return this.inferenceEngine.posterior(name).toString();
  }

  protected inferBeliefs(): void {
    this.inferenceEngine.makeInference();
    this.isBayesNetDirty = false;
  }

  /**
   * Static helper function for loading a model.
   * @param filepath Absolute or relative path to XDSL-file
   * @returns An instance of BayesBeliefModel
   */
  public static deserializeFromXDSLFile(filepath: string): BayesBeliefModel {
    const bayesNet = new BayesNet();
    bayesNet.loadXDSL(filepath);
    BayesBeliefModel.bayesNet = bayesNet;
    return new BayesBeliefModel()
  }

  /**
   * xdslWriter writes the bayesNet into the given file path.
   * @param filepath
   */
  public serializeToXDSLFile(filepath: string): void {
    const xdslWriter = new XDSLBNWriter();
    xdslWriter.write(filepath, BayesBeliefModel.bayesNet);
  }

  /**
   * Static helper function for loading a model.
   * @param bayesNet a deserialized JSON object of the type IBayesNet
   * @returns An instance of BayesBeliefModel
   */
  public static deserializeFromBayesNet(bayesNet: IBayesNet): BayesBeliefModel {
    BayesBeliefModel.createBayesNet(bayesNet)
    return new BayesBeliefModel();
  }

  public serialize(): IBayesNet {
    const bayesNet: IBayesNet = {};
    const nodes = BayesBeliefModel.bayesNet.nodes();

    for (let i = 0; i < nodes.length; i += 1) {
      const node: IBayesNode = {
        id: BayesBeliefModel.bayesNet.variable(nodes[i]).name(),
        states: convertVectorString(BayesBeliefModel.bayesNet.variable(nodes[i]).labels()),
        parents: convertVectorString(BayesBeliefModel.bayesNet.parentNames(nodes[i])),
        cpt: getCPT(BayesBeliefModel.bayesNet.cpt(nodes[i])),
      };

      bayesNet[node.id] = node;
    }

    return bayesNet;
  }

  public serializeBeliefs(): IProbabilityBeliefBundle {
    if (this.isBayesNetDirty) {
      this.inferBeliefs();
    }
    const result: IProbabilityBeliefBundle = {};
    const nodes = BayesBeliefModel.bayesNet.nodes();

    for (let i = 0; i < nodes.length; i += 1) {
      const nodeName: string = BayesBeliefModel.bayesNet.variable(nodes[i]).name();
      const potential = this.inferenceEngine.posterior(nodeName);
      result[nodeName] = getDistributionFromPotential(potential);
    }

    return result;
  }

  public static createBayesNet(bayesNet: IBayesNet | BayesNet) {
    if (bayesNet instanceof BayesNet) {
      BayesBeliefModel.bayesNet = bayesNet;
    } else {
      BayesBeliefModel.bayesNet = new BayesNet();
      const bayesNetFactory = new BayesNetFactory(BayesBeliefModel.bayesNet);
      // generate nodes
      Object.values(bayesNet as IBayesNet).forEach((node) => {
        bayesNetFactory.startVariableDeclaration();
        bayesNetFactory.variableName(node.id);
        node.states.forEach((state) => {
          bayesNetFactory.addModality(state);
        });
        bayesNetFactory.endVariableDeclaration();
      });

      // generate parents
      Object.values(bayesNet as IBayesNet).forEach((node) => {
        bayesNetFactory.startParentsDeclaration(node.id);
        node.parents.forEach((parent) => {
          bayesNetFactory.addParent(parent);
        });
        bayesNetFactory.endParentsDeclaration();
      });

      // generate cpt
      Object.values(bayesNet as IBayesNet).forEach((node) => {
        const vecCPT = new Vector();
        node.cpt.forEach((val) => vecCPT.add(val));
        const nodeID = this.getIdFromName(node.id);
        BayesBeliefModel.bayesNet.cpt(nodeID).fillWith(vecCPT);
        // Could not use bayesNetFactory due to VectorFloat Vector miss match
      });

      const nodes = BayesBeliefModel.bayesNet.nodes();
      for (let i = 0; i < nodes.length; i += 1) {
        const nodeName = BayesBeliefModel.bayesNet.variable(nodes[i]).name();
        if(BayesBeliefModel.isLeafNode(nodeName)) {
          this.leafNodeNames.push(nodeName);
        }
      }
    }
  }

    private static getIdFromName(name: string): number {
      try {
        const id = BayesBeliefModel.bayesNet.idFromName(name);
        return id;
      } catch (err) {
        throw new BayesBeliefError(`Could not find node with name: ${name}`);
      }
    }

    /**
   * Returns if the node with the given id is a leaf node within this hierarchy or not
   * @param nodeId
   * @returns
   */
    public static isLeafNode(nodeName: string): boolean {
      const nodeId = BayesBeliefModel.getIdFromName(nodeName);
      const inst: Instantiation = new Instantiation(BayesBeliefModel.bayesNet.cpt(nodeId));
  
      /* if the cpt contains more numbers than states we are dealing with a parent node */
      const states = convertVectorString(BayesBeliefModel.bayesNet.variable(BayesBeliefModel.bayesNet.nodes()[0]).labels());
      return inst.domainSize() - states.length === 0;
    }
}
