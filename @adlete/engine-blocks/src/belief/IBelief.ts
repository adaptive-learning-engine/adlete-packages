/* eslint-disable @typescript-eslint/no-empty-interface */
/**
 * A belief
 */
export interface IBelief {}

/**
 * A bundle (dictionary) of beliefs
 */
export type IBeliefBundle<TBelief extends IBelief> = Record<string, TBelief>;

/**
 * A belief that holds a value and a certainty
 */
export interface IScalarBelief extends IBelief {
  value: number;
  certainty: number;
}

/**
 * A bundle (dictionary) of scalar beliefs
 */
export type IScalarBeliefBundle = IBeliefBundle<IScalarBelief>;

/**
 * A set of probabilities
 */
export type IProbabilityVector = Array<number>;

/**
 * A belief that consists of a set of probabilities
 */
export interface IProbabilityBelief extends IProbabilityVector, IBelief {}

/**
 * A bundle (dictionary) of probability beliefs
 */
export type IProbabilityBeliefBundle = IBeliefBundle<IProbabilityBelief>;
