import { IEvidence, IEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';

/**
 * Converts observations to evidence
 */
export interface ISpecificObservationsConverter<TObservations, TEvidence extends IEvidence> {
  convert(observations: TObservations): IEvidenceBundle<TEvidence>;
}

/**
 * Converts observations to evidence by internally forwarding to the specific
 * observations converter that is linked to the given context.
 */
export class ObservationsConverter<TObservations, TEvidence extends IEvidence> {
  converters: { [key: string]: ISpecificObservationsConverter<TObservations, TEvidence> };

  constructor() {
    this.converters = {};
  }

  public addConverter(context: string, interpreter: ISpecificObservationsConverter<TObservations, TEvidence>): void {
    this.converters[context] = interpreter;
  }

  public convert(context: string, observations: TObservations): IEvidenceBundle<TEvidence> {
    return this.converters[context].convert(observations);
  }
}
