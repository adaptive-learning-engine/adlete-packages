import { IEvidence, IEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';

import { IActivityObservation } from '@adlete/engine-blocks/observation/IObservation';

/**
 * Describes an object that can interpret activity observations.
 *
 * Compared to {@link IObservationsInterpreter} there is no context, as the
 * context is given by the `activityName`.
 *
 * Internally converts observations to evidence.
 *
 * @param TObservations Type for observations
 * @param TEvidence Type for the resulting evidence, which will be interpreted
 *
 * TODO AS+FG: Needs a where clause for observation, should be of the type IObservation and for TEvidence as well
 */
export interface IActivityObservationInterpreter<TObservations extends IActivityObservation, TEvidence extends IEvidence> {
  /**
   * Interprets observations
   *
   * @param   observations  Observations
   *
   * @return  Converted evidence
   */
  interpret(observations: TObservations): IEvidenceBundle<TEvidence>;
}
