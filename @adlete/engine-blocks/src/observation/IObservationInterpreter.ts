import { IEvidence, IEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';

import { IActivityObservation } from '@adlete/engine-blocks/observation/IObservation';

/**
 * Describes an object that can interpret observations.
 *
 * Internally converts observations to evidence.
 *
 * @param TObservations Type for observations
 * @param TEvidence Type for the resulting evidence, which will be interpreted
 *
 * TODO AS+FG: Needs a where clause for observation, should be of the type IObservation and for TEvidence as well
 */
export interface IObservationsInterpreter<TObservations extends IActivityObservation, TEvidence extends IEvidence> {
  /**
   * Interprets observations
   *
   * @param   context       Context, in which the observations were observed
   * @param   observations  Observations
   *
   * @return  Converted evidence
   */
  interpret(context: string, observations: TObservations): IEvidenceBundle<TEvidence>;
}
