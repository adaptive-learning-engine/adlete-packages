import { IEvidence, IEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { IActivityObservation } from '@adlete/engine-blocks/observation/IObservation';

/**
 * Converts activity observations to evidence
 */
export interface IActivityObservationConverter<TObservations extends IActivityObservation, TEvidence extends IEvidence> {
  convert(observations: TObservations): IEvidenceBundle<TEvidence>;
}

/**
 * Converts observations to evidence by internally forwarding to the specific
 * observations converter that is linked to the given activity type.
 */
export class ActivityObservationConverter<TObservations extends IActivityObservation, TEvidence extends IEvidence> {
  converters: { [key: string]: IActivityObservationConverter<TObservations, TEvidence> };

  constructor() {
    this.converters = {};
  }

  public addConverter(context: string, interpreter: IActivityObservationConverter<TObservations, TEvidence>): void {
    this.converters[context] = interpreter;
  }

  public convert(observations: TObservations): IEvidenceBundle<TEvidence> {
    return this.converters[observations.activityName].convert(observations);
  }
}
