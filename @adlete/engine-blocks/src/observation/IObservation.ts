/**
 * Describe general observations
 */

import { ActivityName } from '@adlete/engine-blocks/activity/IActivityConfig';

export interface IActivityObservation {
  activityName: ActivityName;
}

export interface IBeliefObservation {
  beliefWeights: Record<string, number>;
  difficulty: number;
  correctness: number;
}
