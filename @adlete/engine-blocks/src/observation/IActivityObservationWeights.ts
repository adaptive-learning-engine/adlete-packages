export interface IActivityObservationWeights {
  [key: string]: number;
}
