import { Utility } from '@adlete/engine-blocks/utilityAI/Utility';

/**
 * Function interface for functions that calculate a utility for the given context.
 */
export type Score<TContext = unknown> = (context?: TContext) => Utility;

/**
 * Objects that contain a scoring function.
 */
export interface IScorer<TContext = unknown> {
  score: Score<TContext>;
}

/**
 * Union type for objects and functions that calculate utilities.
 */
export type Scorer<TContext = unknown> = IScorer<TContext> | Score<TContext>;

/**
 * Calls a @see Scorer and returns its utility.
 * This is a helper function to abstract away the differences between scoring functions ( @see Score)
 * and scoring objects ( @see IScorer ).
 *
 * @param scorer The scorer to call
 * @param context The context passed to the scorer
 * @returns The utility calculated
 */
export function callScorer<TContext>(scorer: Scorer<TContext>, context?: TContext): Utility {
  if (typeof scorer === 'function') {
    return scorer(context);
  }
  return scorer.score(context);
}
