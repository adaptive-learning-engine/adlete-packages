/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-param-reassign */

import { ILens } from '@adlete/engine-blocks/utilityAI/Lenses';

/**
 * A utility with a value and a weight.
 */
export interface IWeightedUtility {
  value: number;
  weight: number;
}

/**
 * A union type for utilities to support single numbers as well as weighted utility objects.
 */
export type Utility = IWeightedUtility | number;

/**
 * A lens for retrieving and setting a utility's value.
 * Just a helper to handle the differences between utility objects and numbers.
 */
export const utilityValueLens: ILens<Utility, number> = {
  get(utility: Utility) {
    return typeof utility === 'number' ? utility : utility.value;
  },

  set(utility: Utility, value: number) {
    if (typeof utility === 'number') {
      return value;
    }

    // TODO: return new utility or mutate?
    utility.value = value;
    return utility;
  },
};

/**
 * Retrieves the weighted utility value (a multiplication of the value and the weight).
 *
 * @param utility
 * @param defaultWeight Weight for utilities that are just numbers
 * @returns The weighted value as a single number.
 */
export function getWeightedValue(utility: Utility, defaultWeight = 1) {
  return typeof utility === 'number' ? utility * defaultWeight : utility.value * utility.weight;
}

/**
 * Applies a utility's weight to its value. (a multiplication of the value and the weight)
 * @param utility
 * @param defaultWeight Weight for utilities that are just numbers
 * @param keepObject If utility is an object, keep it this way. If false a single number will be returned
 * @returns A utility that got its weight applied. If @see keepObject is false this will be a number, otherwise
 *   if the utility is an object, it will be mutated and returned.
 */
export function applyWeight(utility: Utility, defaultWeight = 1, keepObject = false): Utility {
  if (typeof utility === 'number') {
    return utility * defaultWeight;
  }

  if (keepObject) {
    utility.value *= utility.weight;
    utility.weight = 1;
    return utility;
  }

  return utility.value * utility.weight;
}
