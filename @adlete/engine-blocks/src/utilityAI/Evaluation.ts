/* eslint-disable no-param-reassign */
import { callScorer, Scorer } from '@adlete/engine-blocks/utilityAI/Scoring';
import { getWeightedValue, Utility, utilityValueLens } from '@adlete/engine-blocks/utilityAI/Utility';

/**
 * The key an action can have in a Record or index signature
 */
export type ActionKey = string | number | symbol;

/**
 * An object that holds the key of an action and its utility
 */
export interface IActionUtilityPair {
  action: ActionKey;
  utility: Utility;
}

/**
 * Returns the utilities for all given actions.
 *
 * @param options A map of actions and their according scorers
 * @param context The context passed to the scorers
 * @returns A map of actions and their calculated utilities
 */
export function getActionUtilities<TContext>(options: Record<ActionKey, Scorer<TContext>>, context?: TContext): Record<ActionKey, Utility> {
  return Object.entries(options).reduce((utilities, [action, scorer]) => {
    const utility = callScorer(scorer, context);
    utilities[action] = utility;
    return utilities;
  }, {} as Record<ActionKey, Utility>);
}

/**
 * Function interface for sorting functions like @see sortActionsByValue or @see sortActionsByWeightedValue
 */
export interface SortActionsFunc {
  <TContext>(options: Record<ActionKey, Scorer<TContext>>, context?: TContext): IActionUtilityPair[];
}

/**
 * Function interface for a getter function used by @see sortActionsBy
 */
type UtilityCompareGetter = (utility: Utility, action?: ActionKey) => number;

/**
 * Sorts the given actions by using a given getter-function for retrieving the value to sort by.
 *
 * @param options A map of actions and their according scorers to evaluate and then sort
 * @param getter Function to retrieve a value from a utility, which is then used for comparison
 * @param context The context passed to the scorers
 * @returns A list of actions and their calculated utilities, sorted
 */
export function sortActionsBy<TContext>(
  options: Record<ActionKey, Scorer<TContext>>,
  getter: UtilityCompareGetter,
  context?: TContext
): IActionUtilityPair[] {
  const pairs = Object.entries(options).map(([action, scorer]) => ({ action, utility: callScorer(scorer, context) }));
  return pairs.sort((a, b) => getter(b.utility, b.action) - getter(a.utility, a.action));
}

/**
 * Sorts the given actions by their utility's value.
 * @param options A map of actions and their according scorers to evaluate and then sort
 * @param context The context passed to the scorers
 * @returns A list of actions and their calculated utilities, sorted
 */
export const sortActionsByValue: SortActionsFunc = <TContext>(options: Record<ActionKey, Scorer<TContext>>, context?: TContext) =>
  sortActionsBy(options, utilityValueLens.get, context);

/**
 * Sorts the given actions by their utility's weighted value.
 * @param options A map of actions and their according scorers to evaluate and then sort
 * @param context The context passed to the scorers
 * @returns A list of actions and their calculated utilities, sorted
 */
export const sortActionsByWeightedValue: SortActionsFunc = <TContext>(options: Record<ActionKey, Scorer<TContext>>, context?: TContext) =>
  sortActionsBy(options, getWeightedValue, context);
