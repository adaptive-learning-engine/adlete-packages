/* eslint-disable no-param-reassign */
import { callScorer, Scorer } from '@adlete/engine-blocks/utilityAI/Scoring';
import { getWeightedValue, IWeightedUtility, utilityValueLens } from '@adlete/engine-blocks/utilityAI/Utility';

// similar to Qualifiers in Apex

/**
 * Creates a scorer that sums up all the utility values of the given scorers, but ignores their weights.
 *
 * @param scorers Scorers that will be summed up.
 * @param weight Weight of the created Scorer
 * @returns See Description
 */
export function createSumScorer<TContext>(scorers: Scorer[], weight = 1): Scorer<TContext> {
  const sumScorer: Scorer = (context?: TContext) =>
    scorers.reduce(
      (sum, scorer) => {
        const utility = callScorer(scorer, context);
        sum.value += utilityValueLens.get(utility);
        return sum;
      },
      { weight, value: 0 }
    );
  return sumScorer;
}

/**
 * Creates a scorer that sums up the weighted utility values of all given scorers.
 *
 * @param scorers Scorers that will be summed up.
 * @param weight Weight of the created Scorer
 * @param defaultScorerWeight Weight for Utilities that are simple numbers (and thus don't have weights)
 * @returns See Description
 */
export function createWeightedSumScorer<TContext>(scorers: Scorer[], weight = 1, defaultScorerWeight = 1): Scorer<TContext> {
  const weightedSumScorer: Scorer = (context?: TContext) =>
    scorers.reduce(
      (sum, scorer) => {
        const utility = callScorer(scorer, context);
        sum.value += getWeightedValue(utility, defaultScorerWeight);
        return sum;
      },
      { weight, value: 0 }
    );
  return weightedSumScorer;
}

/**
 * Creates a scorer that clamps a given scorer between min and max values
 *
 * @param scorers Scorers that will be summed up.
 * @param weight Weight of the created Scorer
 * @returns See Description
 */
export function createClampScorer<TContext>(scorer: Scorer, min: number, max: number, weight = 1): Scorer<TContext> {
  const sumScorer: Scorer = (context?: TContext) => {
    const utility = callScorer(scorer, context);
    const sum: IWeightedUtility = {
      value: Math.min(Math.max(utilityValueLens.get(utility), min), max),
      weight,
    };
    return sum;
  };
  return sumScorer;
}
