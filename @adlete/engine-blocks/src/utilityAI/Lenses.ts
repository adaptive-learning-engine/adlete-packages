/**
 * A lens can retrieve subparts of a an object ( @see get ) and set them ( @see set ).
 * A construct borrowed from functional programming.
 */
export interface ILens<TObject, TFocus> {
  get: (obj: TObject) => TFocus;
  set: (obj: TObject, focus: TFocus) => TObject;
}
