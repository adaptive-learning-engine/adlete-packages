/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { callScorer, Scorer } from '@adlete/engine-blocks/utilityAI/Scoring';
import { utilityValueLens } from '@adlete/engine-blocks/utilityAI/Utility';

// TODO: WeightModifier

/**
 * Function interface for modifiers used by @see applyValueModifier .
 */
export type ValueModifier<TContext> = (value: number, context?: TContext) => number;

/**
 * Creates a scorer that wraps another scorer, but mutates the utility's value
 * according to the given modifyValue function. Weight of the utility is kept.
 *
 * @param scorer Scorer to wrap
 * @param modifyValue Function to modify the resulting value
 * @returns See description
 */
export function applyValueModifier<TContext>(scorer: Scorer<TContext>, modifyValue: ValueModifier<TContext>) {
  return function valueModifier(context?: TContext) {
    const utility = callScorer(scorer, context);
    const newValue = modifyValue(utilityValueLens.get(utility), context);
    return utilityValueLens.set(utility, newValue);
  };
}

/**
 * Applies logistic smoothing to the given number.
 */
function logisticSmoothing(x: number): number {
  return (1 / (1 + 2.71828 ** x) - 0.5) * -0.4;
}

/**
 * Creates a scorer that wraps another scorer, but logistically smooths its utility value.
 *
 * @param scorer Scorer to wrap
 * @returns See description
 */
export function applyLogisticSmoothValue<TContext>(scorer: Scorer<TContext>): Scorer<TContext> {
  return applyValueModifier(scorer, logisticSmoothing);
}

/**
 * Function interface for modifiers used by @see applyContextModifier .
 */
export type ContextModifier<TContext> = (context?: TContext) => void;

/**
 * Creates a scorer that wraps another scorer, but mutates the context according to
 * the given modify function.
 *
 * @param scorer Scorer to wrap
 * @param modifyContext Function to modify the context
 * @returns See description
 */
export function applyContextModifier<TContext>(scorer: Scorer<TContext>, modifyContext: ContextModifier<TContext>) {
  return function contextModifier(context?: TContext) {
    modifyContext(context);
    return callScorer(scorer, context);
  };
}

/**
 * Function interface for replacers used by @see applyContextReplacer .
 */
export type ContextReplacer<TOldContext, TNewContext> = (context?: TOldContext) => TNewContext;

/**
 * Creates a scorer that wraps another scorer, but replaces the context according
 * to the given replace function.
 *
 * @param scorer Scorer to wrap
 * @param replaceContext Function to replace the context
 * @returns See description
 */
export function applyContextReplacer<TOldContext, TNewContext>(
  scorer: Scorer<TNewContext>,
  replaceContext: ContextReplacer<TOldContext, TNewContext>
) {
  return function contextReplacer(oldContext?: TOldContext) {
    const newContext = replaceContext(oldContext);
    return callScorer(scorer, newContext);
  };
}
