import { IProbabilityVector } from '@adlete/engine-blocks//belief/IBelief';

import { clamp01 } from '@adlete/engine-blocks/utils/Utils';

/**
 * Calculates the center of mass of a the given probability Vector
 *
 * @param probabilityVector a probability Vector
 */
export function calcMean(probabilityVector: IProbabilityVector): number {
  const bucketWidth = 1;
  const shiftingArea = { offset: bucketWidth / 2, width: probabilityVector.length * bucketWidth - bucketWidth };

  let mean = 0;
  for (let i = 0; i < probabilityVector.length; i++) {
    // mean += ( probabilityBelief.value[i] / 2 ) * probabilityBelief.value[i]; // Balance Point Ys
    mean += (shiftingArea.offset + i * bucketWidth) * probabilityVector[i]; // Balance Point Xs
  }

  return (mean - shiftingArea.offset) / shiftingArea.width;
}

/**
 * Calculates the variance of the given probability values
 *
 * @param probabilityVector a set of probability values
 * @param mean the mean of the set of probability values. You can use {@link calcMean} to calculate it
 */
export function calcVariance(probabilityVector: IProbabilityVector, mean: number): number {
  const level = 1 / probabilityVector.length;
  let variance = 0;

  for (let i = 0; i < probabilityVector.length; i++) {
    const bucket = level / 2 + i * level;
    variance += (bucket - mean) ** 2 * probabilityVector[i];
  }

  return variance;
}

/**
 * Calculates the standard deviation of a given set of probabilities by using a given mean value.
 *
 * @param probabilityVector a st of probabilities
 * @param mean the mean of the set of probability values. You can use {@link calcMean} to calculate it
 */
export function calcStandardDeviation(probabilityVector: IProbabilityVector, mean: number): number {
  const variance = calcVariance(probabilityVector, mean);
  return Math.sqrt(variance);
}

export function scalarToProbabilityVector(scalar: number, numProbabilities: number): IProbabilityVector {
  if (numProbabilities <= 0) {
    throw new Error('numProbabilities must be greater than 0');
  }

  if (scalar < 0 || scalar > 1) {
    throw new Error('numProbabilities must be a value between 0 and 1 (including)');
  }

  // Shifting a rectangle between 0+level/2 and 0-level/2
  const bucketWidth = 1;
  const bucketWidthNorm = 1 / numProbabilities;
  const width = numProbabilities * bucketWidth;
  const shiftingArea = { offset: bucketWidth / 2, width: numProbabilities * bucketWidth - bucketWidth };
  const rectangle = { center: 0, leftEnd: 0, rightEnd: 0 };
  rectangle.center = scalar * shiftingArea.width + shiftingArea.offset;
  rectangle.leftEnd = rectangle.center - bucketWidth / 2;
  rectangle.rightEnd = rectangle.center + bucketWidth / 2;

  const vec = [];
  const centerBucketNorm = rectangle.center / width;
  const centerBucket = Math.floor(centerBucketNorm * numProbabilities);
  const diff = centerBucketNorm - centerBucket * bucketWidthNorm - bucketWidthNorm / 2;

  for (let i = 0; i < numProbabilities; i++) {
    vec[i] = 0;
  }

  if (diff < 0) {
    vec[centerBucket - 1] = Math.abs(diff / bucketWidthNorm);
    vec[centerBucket] = 1 - vec[centerBucket - 1];
  } else {
    vec[centerBucket + 1] = diff / bucketWidthNorm; // diff is positive in this case
    vec[centerBucket] = 1 - vec[centerBucket + 1];
  }

  // Do slice because of weird rounding bug using a scalar value of 1
  const result = vec.slice(0, numProbabilities);
  vec.forEach((currentValue: number, index: number) => {
    if (index >= numProbabilities) {
      result[numProbabilities - 1] = clamp01(result[numProbabilities - 1] + currentValue);
    }
  });
  return result;
}
