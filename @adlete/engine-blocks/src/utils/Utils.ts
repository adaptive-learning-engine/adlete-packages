import { Graph } from 'graphlib';

export type ConvertNode<TFrom, TTo> = (name: string, from: TFrom) => TTo;

/**
 * Clamps a value between the minimal value and the maximum value. Min and Max are included
 *
 * @param min minimal value (inclusive)
 * @param value  the value to clamp
 * @param max maximum value (inclusive)
 */
export function clamp(value: number, min: number, max: number): number {
  return Math.min(Math.max(value, min), max);
}

/**
 * Clamps a value between 0 and 1. 0 and 1 are included
 *
 * @param value  the value to clamp
 */
export function clamp01(value: number): number {
  return clamp(value, 0, 1);
}

/**
 * Rounds a number to the given precision.
 *
 * @param number
 * @param precision
 * @returns
 */
export function roundToPrecision(number: number, precision: number) {
  const factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

/**
 * Converts an Graph
 * @param inGraph
 * @param convertNode
 * @returns
 */
export function convertGraph<TNodeFrom, TNodeTo>(inGraph: Graph, convertNode: ConvertNode<TNodeFrom, TNodeTo>): Graph {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const outGraph = new Graph();
  outGraph.setGraph({});
  const nodes = inGraph.nodes();
  const edges = inGraph.edges();

  for (let i = 0; i < nodes.length; ++i) {
    outGraph.setNode(nodes[i], convertNode(nodes[i], inGraph.node(nodes[i])));
  }

  for (let i = 0; i < edges.length; ++i) {
    outGraph.setEdge(edges[i].v, edges[i].w, {});
  }

  return outGraph;
}

type CompareFunction<T, V> = (a: T, b: V) => number;

/**
 * A generic binary search function.
 * @param array A sorted array of objects
 * @param target The searched for value
 * @param compare A compare function between the two types
 * @returns The index of the array if the element was foun else -1
 */
export function binarySearch<T, V>(array: T[], target: V, compare: CompareFunction<T, V>): number {
  if(!array || array.length == 0) return -1
  let left = 0;
  let right = array.length - 1;

  while (left <= right) {
    const mid = Math.floor((left + right) / 2);
    const comparisonResult = compare(array[mid], target);

    if (comparisonResult === 0) {
      return mid; // Target found
    }
    if (comparisonResult < 0) {
      left = mid + 1; // Target may be in the right half
    } else {
      right = mid - 1; // Target may be in the left half
    }
  }

  return -1; // Target not found
}
