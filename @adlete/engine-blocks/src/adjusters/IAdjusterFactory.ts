import { IAdjuster } from "./IAdjuster";
import { IObservationAdjuster } from "./IObservationAdjuster";
import { IRecommendationAdjuster } from "./IRecommendationAdjuster";

export interface IBaseAdjusterParam {}

export interface IAdjusterFactory<TEvidence, TRecommendation> {
    createAdjuster(param: IBaseAdjusterParam): IAdjuster<TEvidence, TRecommendation>
    createRecommendationAdjuster(param: IBaseAdjusterParam): IRecommendationAdjuster<TRecommendation>
    createObservationAdjuster(param: IBaseAdjusterParam): IObservationAdjuster<TEvidence>
}