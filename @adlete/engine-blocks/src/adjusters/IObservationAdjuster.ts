import { IActivityObservation } from '@adlete/engine-blocks/observation/IObservation';
/**
 * An interface to describe an adaptive difficulty adjustment engine.
 * It holds the functionality to interpret evidence.
 *
 * @param   TEvidence    The type of evidence, which should be used
 *
 */
export interface IObservationAdjuster<TEvidence> {
  /**
   * Interprets evidence and updates internal beliefs accordingly.
   *
   * @param   evidence   An evidence which was generated from an observation
   */
  interpretEvidence(evidence: TEvidence, observation: IActivityObservation): void;
}
