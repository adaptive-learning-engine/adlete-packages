import { IRecommendationFilter } from '@adlete/engine-blocks/recommendation/IRecommendationFilter';

/**
 * An interface to describe an adaptive difficulty adjustment engine.
 * It holds the functionality to interpret evidence and infer recommendations.
 * It further learns, to give better recommendations according to the current
 * belief.
 *
 * @param   TEvidence    The type of evidence, which should be used
 * @param   TDifficulty  The type of difficulty, which should be used
 *
 */
export interface IRecommendationAdjuster<TRecommendation> {

  /**
   * Creates a recommendation according to the current beliefs.
   *
   * @param   filter   A filter to restrict the recommendations
   *
   * @returns   A recommendation according to your definition.
   */
  recommend(filter?: IRecommendationFilter): TRecommendation;
}
