export interface ITendency {}

export interface IScalarTendency {
  tendency: number;
  equationParams: number[];
  dataPoints: number[];
}

export type ITendencyBundle<TBelief extends ITendency> = Record<string, TBelief>;

export type IScalarTendencyBundle = ITendencyBundle<IScalarTendency>;
