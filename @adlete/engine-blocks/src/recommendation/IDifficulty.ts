/**
 * Describes difficulty in general
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IDifficulty {}

/**
 * Describes difficulty as a scalar value between 0 and 1
 */
export interface IScalarDifficulty extends IDifficulty {
  value: number;
}
