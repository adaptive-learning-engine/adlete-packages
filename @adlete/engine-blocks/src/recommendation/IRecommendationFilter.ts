/**
 * Describes a filter to be used in a {@link IRecommender}.
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IRecommendationFilter {}
