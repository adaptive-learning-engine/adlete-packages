/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarBeliefSystem } from '@adlete/engine-blocks/evidence/IBeliefSystem';

import { IScalarActivityRecommendation, IScalarActivityRecommender } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { IRecommendationFilter } from '@adlete/engine-blocks/recommendation/IRecommendationFilter';

/**
 * A description of a activity
 */
export interface IActivityType {
  name: string;
  competencesToLearn: string[];
  evidenceFor: string[];
}

export interface IActivityTypes {
  [key: string]: IActivityType;
}

/**
 * Adjustable parameters for the recommender
 */
export interface IScalarRecommenderParams {
  difficultyStepSize: number;
}

/**
 * Provides activity recommendations based on the information in the internal
 * BeliefSystem.
 *
 * The recommender chooses a belief and returns a recommendation that has
 * a slightly higher difficulty.
 */
export class ScalarActivityRecommender implements IScalarActivityRecommender {
  beliefSystem: IScalarBeliefSystem;

  activityTypes: IActivityTypes;

  params: IScalarRecommenderParams = { difficultyStepSize: 0.1 };

  constructor(beliefSystem: IScalarBeliefSystem, activityTypes: IActivityTypes) {
    this.beliefSystem = beliefSystem;
    this.activityTypes = activityTypes;
  }

  protected getWeakestChildCompetence(): { name: string; belief: IScalarBelief } {
    const beliefNames = this.beliefSystem.beliefModel.getNames();

    const weakestCompetence: { name: string; belief: IScalarBelief } = {
      name: '',
      belief: { value: Number.POSITIVE_INFINITY, certainty: 1 },
    };
    beliefNames.forEach((name) => {
      const belief = this.beliefSystem.getBelief(name);
      const isLeaf = this.beliefSystem.beliefModel.isLeafNode(name);

      if (isLeaf && belief.value < weakestCompetence.belief.value) {
        weakestCompetence.name = name;
        weakestCompetence.belief = belief;
      }
    });

    return weakestCompetence;
  }

  protected getHighestChildCompetence(): { name: string; belief: IScalarBelief } {
    const beliefNames = this.beliefSystem.beliefModel.getNames();

    const highestCompetence: { name: string; belief: IScalarBelief } = {
      name: '',
      belief: { value: Number.NEGATIVE_INFINITY, certainty: 1 },
    };
    beliefNames.forEach((name) => {
      const belief = this.beliefSystem.getBelief(name);
      const isLeaf = this.beliefSystem.beliefModel.isLeafNode(name);

      if (isLeaf && belief.value > highestCompetence.belief.value) {
        highestCompetence.name = name;
        highestCompetence.belief = belief;
      }
    });

    return highestCompetence;
  }

  getActivityTypesForCompetenceToLearn(name: string) {
    return Object.values(this.activityTypes).filter((activityType) => activityType.competencesToLearn.find((comp) => comp === name));
  }

  recommend(_filter: IRecommendationFilter): IScalarActivityRecommendation {
    const selectedCompetence = Math.random() <= 0.5 ? this.getWeakestChildCompetence() : this.getHighestChildCompetence();
    const activityTypes = this.getActivityTypesForCompetenceToLearn(selectedCompetence.name);
    // TODO: throw Error if length 0
    console.log('selectedCompetence', selectedCompetence);
    const activity = activityTypes[0];
    let difficulty = selectedCompetence.belief.value + this.params.difficultyStepSize;
    if (difficulty > 1) {
      difficulty = 1;
    }

    return {
      activityName: activity.name,
      difficulty: { value: difficulty },
      trend: 0, // TODO:
    };
  }

  updateParameters(params: IScalarRecommenderParams): void {
    this.params = params;
  }
}
