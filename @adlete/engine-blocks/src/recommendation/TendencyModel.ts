import regression, { DataPoint } from 'regression';

import { IScalarBelief, IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarBeliefSystem } from '@adlete/engine-blocks/evidence/IBeliefSystem';
import { IBeliefObservation } from '@adlete/engine-blocks/observation/IObservation';
import { roundToPrecision } from '@adlete/engine-blocks/utils/Utils';
import { IScalarTendency, IScalarTendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';

export function calcLinearRegression(dataPoints: number[]): number[] {
  if (dataPoints.length === 0) {
    return [0, 0];
  }

  const dataSet = dataPoints.map((value: number, index: number) => [index, value] as DataPoint);
  const result = regression.linear(dataSet, { order: 2, precision: 15 });

  return result.equation;
}

/**
 * Performance a line prediction according the the following equation: y = m * x + c.
 * X is given as parameter, m and c are provided as equationParams which is defined as [m,c].
 * For an easy usage it is recommended to use IScalarTendency objects.
 *
 * @param x
 * @param equationParams
 */
export function doLinearPrediction(x: number, equationParams: number[]): number {
  if (equationParams.length !== 2) {
    throw new Error('equationParams must have a length of 2, array must contain [m,c] to perform a linear prediction with y = mx+c.');
  }
  return equationParams[0] * x + equationParams[1];
}

/**
 * Calculating a new tendency value from a given tendency (oldTendency) and new data point (newBelief).
 * The function returns a tendency and containing all given data points plus the new one. If a maximum count
 * of data points (maximumDataPoints) is provided the the data points array will be sliced to this amount
 * of entries by removing the oldest ones.
 *
 * @param newBelief
 * @param oldTendency
 * @param maximumDataPoints
 */
export function calcSimpleTendency(newBelief: IScalarBelief, oldTendency: IScalarTendency, maximumDataPoints?: number): IScalarTendency {
  let dataPoints = [...oldTendency.dataPoints, newBelief.value];

  if (maximumDataPoints !== undefined && oldTendency.dataPoints.length >= maximumDataPoints) {
    dataPoints = dataPoints.slice(1, maximumDataPoints + 1);
  }

  const equationParams = calcLinearRegression(dataPoints);
  const tendency = doLinearPrediction(dataPoints.length + 1, equationParams) - dataPoints[dataPoints.length - 1];

  return { dataPoints, equationParams, tendency };
}

/** *
 * updateTendencies updates the given tendency model. only tendencies for competences will be updated which are part of the evidence bundle
 * Every tendency will be calculated by creating an approximated linear function on the given data points.
 * The given new belief will be added to the data points array within the tendency object.
 * The value of a tendency is ranging from -1 to 1 (data point set [0,1] and [1,0])
 * For activity beliefs this will track how the trend for how many questions have been answered correct or incorrectly.
 * This influences the recommender.
 *
 * @param tendencyModel: will be updated by this function
 * @param evidenceBundle: contains all competence which must be updated
 * @param newBeliefBundle: contains all new competence beliefs
 * @param maximumDataPoints (optional): Use this parameter if you want to keep the data points
 */
export function updateTendencies(
  tendencyModel: IScalarTendencyBundle,
  scalarBeliefSystem: IScalarBeliefSystem,
  beliefObservation: IBeliefObservation,
  tendencyPositiveWeight: number,
  tendencyNegativeWeight: number,
  maximumDataPoints?: number
): void {
  Object.entries(tendencyModel).forEach((evidenceKeyValuePair) => {
    const beliefName = evidenceKeyValuePair[0];
    let newBelief = scalarBeliefSystem.getBelief(beliefName);
    let oldTendency: IScalarTendency;

    if (tendencyModel[beliefName] !== undefined) {
      oldTendency = tendencyModel[beliefName];
    } else {
      oldTendency = { tendency: 0, equationParams: [0, 0], dataPoints: [newBelief.value] };
    }

    /** Check whatever the current beliefName is a node which was addressed by the fulfilled activity */
    if (Object.keys(beliefObservation.beliefWeights).includes(beliefName)) {
      let evidenceImpact = 0;

      const observationBelief = beliefObservation.beliefWeights[beliefName];
      // Reset after belief has been updated.
      if (roundToPrecision(observationBelief, 5) != roundToPrecision(newBelief.value, 5)) {
        oldTendency = { tendency: 0, equationParams: [0, newBelief.value], dataPoints: [] };
      } else if (beliefObservation.correctness == 1) {
        evidenceImpact = tendencyPositiveWeight;
      } else {
        evidenceImpact = -1 * tendencyNegativeWeight;
      }

      const oldDataPoint = oldTendency.dataPoints[oldTendency.dataPoints.length - 1] ?? newBelief.value;

      newBelief = {
        value: oldDataPoint + evidenceImpact,
        certainty: newBelief.certainty,
      };

      tendencyModel[beliefName] = calcSimpleTendency(newBelief, oldTendency, maximumDataPoints);

      /** Check whatever the current beliefName is not a node which was addressed by the fulfilled activity, but is a child node.  */
    } else if (!scalarBeliefSystem.isLeafNode(beliefName)) {
      tendencyModel[beliefName] = calcSimpleTendency(newBelief, oldTendency, maximumDataPoints);

      /** Check whatever the current beliefName is not a node which was addressed by the fulfilled activity and is also a parent node.*/
    } else {
      tendencyModel[beliefName] = oldTendency;
    }
  });
}
