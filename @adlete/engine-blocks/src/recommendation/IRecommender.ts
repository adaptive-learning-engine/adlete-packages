import { IRecommendationFilter } from '@adlete/engine-blocks/recommendation/IRecommendationFilter';

/**
 * Describes an object that can give a recommendation
 */
export interface IRecommender<TRecommendation> {
  /**
   * Creates a recommendation
   *
   * @param   filter   A filter to restrict the types of recommendations
   * @return  A recommendation
   */
  recommend(filter?: IRecommendationFilter): TRecommendation;
}
