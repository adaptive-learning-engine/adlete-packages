import { IScalarDifficulty } from '@adlete/engine-blocks/recommendation/IDifficulty';
import { IRecommendation } from '@adlete/engine-blocks/recommendation/IRecommendation';
import { IRecommender } from '@adlete/engine-blocks/recommendation/IRecommender';

/**
 * Recommendation for a activity that a user is supposed to absolve
 */
export interface IActivityRecommendation<TDifficulty> extends IRecommendation {
  activityName: string;
  difficulty: TDifficulty;
  trend: number;
}

/**
 * Recommendation for a activity that as a scalar difficulty
 */
export type IScalarActivityRecommendation = IActivityRecommendation<IScalarDifficulty>;

/**
 * A recommender that can give a activity recommendation
 */
export type IActivityRecommender<TDifficulty> = IRecommender<IActivityRecommendation<TDifficulty>>;

/**
 * A recommender that can give a activity recommendation with a scalar difficulty
 */
export type IScalarActivityRecommender = IActivityRecommender<IScalarDifficulty>;
