export type TaskInfo = {
  difficulty: number;
  quantity: number;
};

export interface ITaskSubset {
  name: string;
  taskinfo: TaskInfo[];
}
