import { IScalarBelief, IScalarBeliefBundle } from '../../src/belief/IBelief';
import { IScalarEvidenceBundle } from '../../src/evidence/IEvidence';
import {
  calcLinearRegression,
  calcSimpleTendency,
  IScalarTendency,
  IScalarTendencyBundle,
  updateTendencies,
} from '../../src/recommendation/TendencyModel';

describe('recommendation', () => {
  describe('TendencyModel', () => {
    it('calcLinearRegression', () => {
      /* https://www.wolframalpha.com/input/?i=linear+fit+%7B0%2C+0.1%7D%2C%7B1%2C+0.2%7D%2C%7B2%2C+0.3%7D */
      let result = calcLinearRegression([0.1, 0.2, 0.3]);

      expect(result[0]).toBeCloseTo(0.1);
      expect(result[1]).toBeCloseTo(0.1);

      /* Feeding a data set containing only one point, the result contains only an ordinate which has the same amount as the input value. */
      result = calcLinearRegression([0.1]);

      expect(result[0]).toBeCloseTo(0);
      expect(result[1]).toBeCloseTo(0.1);

      /* Feeding an empty set of data point the calculation returns [0,0] */
      result = calcLinearRegression([]);

      expect(result[0]).toBeCloseTo(0);
      expect(result[1]).toBeCloseTo(0);
    });
    it('calcSimpleTendency', () => {
      /* https://www.wolframalpha.com/input/?i=linear+fit+%7B0%2C+0.41%7D%2C%7B1%2C+0.4%7D */
      const oldTendency: IScalarTendency = { tendency: 0, equationParams: [], dataPoints: [0.41] };

      const newBelief1: IScalarBelief = { value: 0.4, certainty: 1 };

      const newTendency1: IScalarTendency = calcSimpleTendency(newBelief1, oldTendency);

      expect(newTendency1.tendency).toBeCloseTo(-0.01);
      expect(newTendency1.dataPoints.length).toEqual(2);

      const newBelief2: IScalarBelief = { value: 0.3, certainty: 1 };
      const newTendency2: IScalarTendency = calcSimpleTendency(newBelief2, newTendency1, 2);

      expect(newTendency2.dataPoints.length).toEqual(2);
    });
    it('updateTendencies simple examples', () => {
      /* https://www.wolframalpha.com/input/?i=linear+fit+%7B0%2C+0.41%7D%2C%7B1%2C+0.4%7D */

      const evidence: IScalarEvidenceBundle = { a: { value: 1, weight: 1 } };
      const newBelief: IScalarBeliefBundle = {
        a: { value: 0.4, certainty: 1 },
        b: { value: 1, certainty: 1 },
        c: { value: 0.4, certainty: 1 },
      };
      const tendencyModel: IScalarTendencyBundle = { a: { tendency: 0, equationParams: [0, 0], dataPoints: [0.41] } };

      updateTendencies(tendencyModel, evidence, newBelief);

      expect(tendencyModel.a.tendency).toBeCloseTo(-0.01, 0.5);

      expect(tendencyModel.b).toBeUndefined();

      expect(tendencyModel.c).toBeUndefined();
    });
    it('updateTendencies repeat 10 times same input', () => {
      const evidence: IScalarEvidenceBundle = { a: { value: 0.8, weight: 1 } };
      const newBelief: IScalarBeliefBundle = { a: { value: 0.85, certainty: 1 } };
      const tendencyModel: IScalarTendencyBundle = { a: { tendency: 0, equationParams: [0, 0.8], dataPoints: [0.8] } };

      for (let i = 0; i < 10; i += 1) {
        updateTendencies(tendencyModel, evidence, newBelief);
      }

      expect(tendencyModel.a.equationParams[1]).toBeCloseTo(0.85);
    });
  });
});
