import { BayesBeliefModel } from '../../src/belief/BayesBeliefModel';
import { IBayesNet } from '../../src/belief/IBayesNet';

describe('BayesBeliefModel', () => {
  test('the creation of a BayesBeliefModel from a deserialized string', () => {
    const beliefModelInput = generateDefaultBayesBeliefModelInputAgrum();

    const beliefModel = new BayesBeliefModel(beliefModelInput);

    // result is based on the calculation with netica (v. 609)
    // compare with ./networks/test.neta, ./networks/test.png, ./networks/test.json
    // The result should be: {"L": 0.29, "M": 0.53, "H": 0.18 }
    const resultBelief = beliefModel.getBelief('A');
    const expectedResult = [0.29, 0.53, 0.18];
    for (let i = 0; i < expectedResult.length; i += 1) {
      expect(resultBelief[i]).toBeCloseTo(expectedResult[i], 4);
    }
  });
  test('the serialization of a BayesBeliefModel', () => {
    const beliefModelInput = generateDefaultBayesBeliefModelInputAgrum();

    const beliefModel = new BayesBeliefModel(beliefModelInput);
    const originSerializedString = JSON.stringify(beliefModelInput);
    const resultSerializedString = JSON.stringify(beliefModel.serialize());
    expect(resultSerializedString).toEqual(originSerializedString);
  });
  test('the serialization of all leaf nodes of the BayesBeliefModel', () => {
    /* SerializeLeafBeliefs was removed
    const beliefModelInput = generateDefaultBayesBeliefModelInputAgrum();

    const beliefModel = new BayesBeliefModel(beliefModelInput);
    const result = beliefModel.serializeLeafBeliefs();
    expect(result.C).toEqual([0.5, 0.3, 0.2]);
    expect(result.B).toEqual([0.2, 0.4, 0.4]);
    */
  });
  test('the method is LeafNode', () => {
    const beliefModelInput = generateDefaultBayesBeliefModelInputAgrum();

    const beliefModel = new BayesBeliefModel(beliefModelInput);
    const result = beliefModel.isLeafNode('A');
    expect(result).toEqual(false);
  });
});

/**
 * Example for a new BayesBeliefModelInput in string format. Probably we can
 * create something more convenient....
 * @returns
 */
function generateDefaultBayesBeliefModelInputAgrum(): IBayesNet {
  return {
    C: {
      id: 'C',
      states: ['low', 'medium', 'high'],
      parents: [],
      cpt: [0.5, 0.3, 0.2],
    },
    B: {
      id: 'B',
      states: ['low', 'medium', 'high'],
      parents: [],
      cpt: [0.2, 0.4, 0.4],
    },
    A: {
      id: 'A',
      states: ['low', 'medium', 'high'],
      parents: ['C', 'B'],
      cpt: [
        1.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.25, 0.75, 0.0, 0.5, 0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.5, 0.5, 0.25, 0.75, 0.0, 0.0, 0.5, 0.5, 0.0,
        0.0, 1.0,
      ],
    },
  };
}
/**
       C B    L       M        H
       L L   1.00,   0.00,    0.00,
       L M   0.50,   0.50,    0.00,
       L H   0.25,   0.75,    0.00,
       M L   0.50,   0.50,    0.00,
       M M   0.00,   1.00,    0.00,
       M H   0.00,   0.50,    0.50,
       H L   0.25,   0.75,    0.00,
       H M   0.00,   0.50,    0.50,
       H H   0.00,   0.00,    1.00,
       * */
