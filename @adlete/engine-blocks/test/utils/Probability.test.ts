import { IProbabilityVector } from '../../src/belief/IBelief';
import { calcMean, scalarToProbabilityVector } from '../../src/utils/Probability';

describe('utils', () => {
  describe('probability', () => {
    it('scalarToProbabilityVector', () => {
      expect(scalarToProbabilityVector(0, 3)).toEqual([1, 0, 0] as IProbabilityVector);

      expect(scalarToProbabilityVector(1, 3)).toEqual([0, 0, 1] as IProbabilityVector);
    });
    it('calcMean', () => {
      expect(calcMean([1, 0, 0])).toEqual(0);

      expect(calcMean([0, 0, 1])).toEqual(1);
    });
  });
});
