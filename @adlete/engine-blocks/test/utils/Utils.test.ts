import { clamp, clamp01 } from '../../src/utils/Utils';

describe('utils', () => {
  describe('utils', () => {
    it('clamp', () => {
      expect(clamp(10, -100, 3)).toEqual(3);

      expect(clamp(-10, -100, 0)).toEqual(-10);

      expect(clamp(-1000, -100, 0)).toEqual(-100);

      expect(clamp(0, -100, 0)).toEqual(0);

      expect(clamp(-100, -100, 0)).toEqual(-100);
    });
    it('clamp01', () => {
      expect(clamp01(-0.00000000001)).toEqual(0);

      expect(clamp01(1.00000000001)).toEqual(1);
    });
  });
});
