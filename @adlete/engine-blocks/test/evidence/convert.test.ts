import { IProbabilityBelief, IProbabilityBeliefBundle, IScalarBelief, IScalarBeliefBundle } from '../../src/belief/IBelief';
import { probabilityToScalarBelief, scalarToProbabilityBelief } from '../../src/belief/convert';

describe('belief', () => {
  describe('convert', () => {
    it('probabilityToScalarBelief', () => {
      const competence0: IProbabilityBelief = [0.7, 0.2, 0.1];
      const competenceA: IProbabilityBelief = [0.23, 0.53, 0.24];
      const competenceB: IProbabilityBelief = [0.5, 0.3, 0.2];
      const competenceC: IProbabilityBelief = [0.2, 0.4, 0.4];
      const competenceD: IProbabilityBelief = [0.4, 0, 0.3, 0, 0.3];
      const scalarNet: IScalarBeliefBundle = {};
      scalarNet['0'] = probabilityToScalarBelief(competence0);
      scalarNet.A = probabilityToScalarBelief(competenceA);
      scalarNet.B = probabilityToScalarBelief(competenceB);
      scalarNet.C = probabilityToScalarBelief(competenceC);
      scalarNet.D = probabilityToScalarBelief(competenceD);

      expect(scalarNet['0'].value).toBeCloseTo(0.2);
      expect(scalarNet.A.value).toBeCloseTo(0.505);
      expect(scalarNet.B.value).toBeCloseTo(0.35);
      expect(scalarNet.C.value).toBeCloseTo(0.6);
      expect(scalarNet.D.value).toBeCloseTo(0.45);

      // console.log(JSON.stringify(scalarNet));
    });
    it('probabilityToScalarBelief', () => {
      const scalarValue08: IScalarBelief = { value: 0.8, certainty: 1 };
      const scalarValue06: IScalarBelief = { value: 0.6, certainty: 1 };
      const scalarValue04: IScalarBelief = { value: 0.4, certainty: 1 };
      const scalarValue02: IScalarBelief = { value: 0.2, certainty: 1 };
      const probabilityNet: IProbabilityBeliefBundle = {};
      probabilityNet['0.8'] = scalarToProbabilityBelief(scalarValue08, 3);
      probabilityNet['0.6'] = scalarToProbabilityBelief(scalarValue06, 3);
      probabilityNet['0.4'] = scalarToProbabilityBelief(scalarValue04, 3);
      probabilityNet['0.2'] = scalarToProbabilityBelief(scalarValue02, 3);

      // console.log(JSON.stringify(probabilityNet));
    });
    it('simulate belief module', () => {
      // SubCompetence B = [0.5, 0.3, 0.2];
      // SubCompetence C = [0.2, 0.4, 0.4];
      /*
      const beliefModelInput:IBayesBeliefModelInput = {"Competence A":{"id":"Competence A","states":["L","M","H"],"parents":["Subcompetence B","Subcompetence C"],"cpt":[{"when":{"Subcompetence B":"L","Subcompetence C":"L"},"then":{"L":1,"M":0,"H":0}},{"when":{"Subcompetence B":"M","Subcompetence C":"L"},"then":{"L":0.5,"M":0.5,"H":0}},{"when":{"Subcompetence B":"H","Subcompetence C":"L"},"then":{"L":0,"M":0.75,"H":0.25}},{"when":{"Subcompetence B":"L","Subcompetence C":"M"},"then":{"L":0.5,"M":0.5,"H":0}},{"when":{"Subcompetence B":"M","Subcompetence C":"M"},"then":{"L":0,"M":1,"H":0}},{"when":{"Subcompetence B":"H","Subcompetence C":"M"},"then":{"L":0,"M":0.5,"H":0.5}},{"when":{"Subcompetence B":"L","Subcompetence C":"H"},"then":{"L":0,"M":0.75,"H":0.25}},{"when":{"Subcompetence B":"M","Subcompetence C":"H"},"then":{"L":0,"M":0.5,"H":0.5}},{"when":{"Subcompetence B":"H","Subcompetence C":"H"},"then":{"L":0,"M":0,"H":1}}]},"Subcompetence B":{"id":"Subcompetence B","states":["L","M","H"],"parents":[],"cpt":{"L":0.5,"M":0.3,"H":0.2}},"Subcompetence C":{"id":"Subcompetence C","states":["L","M","H"],"parents":[],"cpt":{"L":0.2,"M":0.4,"H":0.4}}}
    
      const beliefModel = new BayesBeliefModel(beliefModelInput);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });

      console.log("----------------STEP 1-------------------");

      beliefModel.setBelief("Subcompetence B",[0.45, 0.35, 0.2]);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });

      console.log("----------------STEP 2-------------------");

      beliefModel.setBelief("Subcompetence B",[0.50, 0.30, 0.2]);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });

      console.log("----------------STEP 3-------------------");

      beliefModel.setBelief("Subcompetence B",[0.55, 0.25, 0.2]);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });

      
      console.log("----------------STEP 4-------------------");

      beliefModel.setBelief("Subcompetence C",[0.15, 0.45, 0.4]);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });

      console.log("----------------STEP 5-------------------");

      beliefModel.setBelief("Subcompetence C",[0.10, 0.5, 0.4]);

      Object.keys(beliefModelInput).forEach(key => {
        console.log(key + ": " + (beliefModel.getBelief(key) + "," + probabilityToScalarBelief(beliefModel.getBelief(key)).value) );
      });
      */
    });
  });
});
