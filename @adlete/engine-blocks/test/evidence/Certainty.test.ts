import { IProbabilityBelief } from '../../src/belief/IBelief';
import { calculateCertainty } from '../../src/belief/certainty';

describe('Certainty', () => {
  describe('belief-certainty', () => {
    it('calculateLowCertainty', () => {
      const certainty = calculateCertainty([1, 0, 0] as IProbabilityBelief, 0);
      expect(certainty).toBeCloseTo(0.967);
    });
    it('calculateMediumCertainty', () => {
      const certainty = calculateCertainty([0.33333333333333326, 0.33333333333333326, 0.33333333333333337], 0.5);
      expect(certainty).toBeCloseTo(0.568);
    });
    it('calculateHighCertainty', () => {
      const certainty = calculateCertainty([0.5, 0, 0.5], 0.5);
      expect(certainty).toBeCloseTo(0.159);
    });
    it('calculateHighCertainty', () => {
      const certainty = calculateCertainty([0.2999999999999999, 0, 0.39999999999999986, 0, 0.2999999999999998], 0.5);
      expect(certainty).toBeCloseTo(0.445);
    });
    it('calculateHighCertainty', () => {
      const certainty = calculateCertainty([1, 0, 0, 0, 0, 0], 0);
      expect(certainty).toBeCloseTo(0.977);
    });
  });
});
