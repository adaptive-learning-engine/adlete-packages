import { IProbabilityBelief } from '../../src/belief/IBelief';
import { IProbabilityEvidence } from '../../src/evidence/IEvidence';
import { calcUpdatedProbabilityBeliefStandard } from '../../src/evidence/belief-update';

describe('evidence', () => {
  describe('belief-update', () => {
    it('compare updatedProbabilityBeliefStandard MathJS vs. JS ', () => {
      const oldBeliefs: IProbabilityBelief[] = [
        [0, 0, 0],
        [0, 0.7999999999999998, 0.2],
        [0.0033441232, 0.1, 0.9921],
        [0.0033441232, 0.1, 0.9921],
      ];
      const evidences: IProbabilityEvidence[] = [
        { probs: [0, 0, 0], weight: 0 },
        { probs: [0, 0.8000000000000003, 0.1999999999999997], weight: 1 },
        { probs: [0.8892892, 0.898212, 0.12312], weight: 0.8 },
        { probs: [-0.3, 0.5, -0.0001], weight: 0.00334234 },
      ];
      const effectSizes: number[] = [0.1, 0.1, 0.9, 0.0];

      const results: IProbabilityBelief[] = [];

      for (let i = 0; i < oldBeliefs.length; i++) {
        const resultJS = calcUpdatedProbabilityBeliefStandard(evidences[i], oldBeliefs[i], effectSizes[i]);
        results.push(resultJS);
      }

      // values have been calculated using MathJS
      expect(results[0]).toEqual([0, 0, 0]);
      expect(results[1]).toEqual([0, 0.7999999999999998, 0.19999999999999998]);
      expect(results[2]).toEqual([0.6412245784960001, 0.6747126400000001, 0.36643439999999994]);
      expect(results[3]).toEqual([0.0033441232, 0.1, 0.9921]);
    });
  });
});
