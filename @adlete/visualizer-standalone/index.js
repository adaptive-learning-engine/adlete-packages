const electron = require('electron');
const Store = require('electron-store');

const dotenv = require('dotenv');

// load .env-File into environment variables
dotenv.config();

const { VISUALIZER_PORT } = process.env;
const port = VISUALIZER_PORT || 3000;

const { app } = electron; // Module to control application life.
const { BrowserWindow } = electron; // Module to create native browser window.

const store = new Store();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow = null;

function windowStateKeeper(windowName) {
  let window, windowState;
  function setBounds() {
    // Restore from store
    if (store.has(`windowState.${windowName}`)) {
      windowState = store.get(`windowState.${windowName}`);
      return;
    }
    // Default
    windowState = {
      x: undefined,
      y: undefined,
      width: 1000,
      height: 800,
    };
  }

  function saveState() {
    if (!windowState.isMaximized) {
      windowState = window.getBounds();
    }
    windowState.isMaximized = window.isMaximized();
    store.set(`windowState.${windowName}`, windowState);
  }

  function track(win) {
    window = win;
    ['resize', 'move', 'close'].forEach((event) => {
      win.on(event, saveState);
    });
  }

  setBounds();

  return {
    x: windowState.x,
    y: windowState.y,
    width: windowState.width,
    height: windowState.height,
    isMaximized: windowState.isMaximized,
    track,
  };
}

function createMainWindow() {
  // Get window state
  const mainWindowStateKeeper = windowStateKeeper('main');
  // Creating the window
  const windowOptions = {
    x: mainWindowStateKeeper.x,
    y: mainWindowStateKeeper.y,
    width: mainWindowStateKeeper.width,
    height: mainWindowStateKeeper.height,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      contextIsolation: false,
    },
  };

  // Create the browser window.
  mainWindow = new BrowserWindow(windowOptions);

  // Track window state
  mainWindowStateKeeper.track(mainWindow);

  // and load the index.html of the app.
  // mainWindow.loadURL(`file://${__dirname}/../renderer/index.html`);
  const url = `http://localhost:${port}/`;
  console.log(`Loading URL ${url}`);
  mainWindow.loadURL(url);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', () => {
  createMainWindow();
});
