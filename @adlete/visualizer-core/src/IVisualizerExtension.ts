import { IBeliefBundle, IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IActivityEvidenceBundle, IScalarEvidenceBundle } from '@adlete/engine-blocks/evidence/IEvidence';
import { IScalarActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { IScalarTendency, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { IObservation } from '@adlete/framework/IObservation';

// model results
export interface IActivityModelResult {
  agentCaseModel?: any;
  activityEvidences?: IActivityEvidenceBundle[];
  scalarEvidences: IScalarEvidenceBundle[];

  probabilityBeliefs: IBeliefBundle<IProbabilityBelief>[];
  scalarBeliefs: IBeliefBundle<IScalarBelief>[];

  tendencies?: ITendencyBundle<IScalarTendency>[];
  states: string[]; // TODO: move to IResults
}

export interface ISimulatedActivityModelResult extends IActivityModelResult {
  agentCaseModel: IBeliefBundle<IScalarBelief>;
}

// activity results
export interface IActivityResult<TActivityModelResult> {
  recommendation: IScalarActivityRecommendation;
  activityConfig?: any;
  observations: IObservation[];
  models: Record<string, TActivityModelResult>;
}

export type IUserActivityResult = IActivityResult<IActivityModelResult>;

export type ISimulatedActivityResult = IActivityResult<ISimulatedActivityModelResult>;

// complete results
export interface IResults<TActivityResult> {
  modelStructures: Record<string, any>;
  // probabilityStates: Record<string, string[]> // per Model
  activities: TActivityResult[];
}

export type ISimulationResults = IResults<ISimulatedActivityResult>;

export type IUserResults = IResults<IUserActivityResult>;
