import math from 'mathjs';
import * as R from 'ramda';

import { IBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';

import { IActivityModelResult, IActivityResult } from '../IVisualizerExtension';

export function bundleArrayToBeliefArrays<TBelief>(bundles: IBeliefBundle<TBelief>[]): Record<string, TBelief[]> {
  // TODO: use ramda
  const beliefArrays: Record<string, TBelief[]> = {};
  Object.keys(bundles[0]).forEach((beliefName) => {
    beliefArrays[beliefName] = [];
    for (let i = 0; i < bundles.length; ++i) {
      beliefArrays[beliefName].push(bundles[i][beliefName]);
    }
  });
  return beliefArrays;
}

export function getProbabilityBeliefBundleAt(activities: IActivityResult<IActivityModelResult>[], modelName: string, pos: number) {
  return R.last(activities[pos].models[modelName].probabilityBeliefs);
}

export function getProbabilityBeliefBundles(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  return R.map((activity) => R.last(activity.models[modelName].probabilityBeliefs), activities);
}

export function getProbabilityBeliefArrays(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  const bundles = getProbabilityBeliefBundles(activities, modelName);
  return bundleArrayToBeliefArrays(bundles);
}

export function getScalarBeliefBundleAt(activities: IActivityResult<IActivityModelResult>[], modelName: string, pos: number) {
  return R.last(activities[pos].models[modelName].scalarBeliefs);
}

export function getScalarTendencyBundleAt(activities: IActivityResult<IActivityModelResult>[], modelName: string, pos: number) {
  return R.last(activities[pos].models[modelName].tendencies);
}

export function getScalarBeliefBundles(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  return R.map((activity) => R.last(activity.models[modelName].scalarBeliefs), activities);
}

export function getScalarBeliefArrays(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  const bundles = getScalarBeliefBundles(activities, modelName);
  return bundleArrayToBeliefArrays(bundles);
}

export function getRandomLearnerId(): string {
  return `agent-learner-id-${Math.floor(Math.random() * (99999 - 10000) + 10000)}`;
}
/*
export function getActivityEvidenceBundleAt(_activities: IActivityResult<IActivityModelResult>[], _modelName: string, _pos: number) {
  return null as any; //R.last(activities[pos].models[modelName].activityEvidences);
}

export function getActivityEvidenceBundles(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  return R.map((activity) => R.last(activity.models[modelName].activityEvidences), activities);
}

export function getActivityEvidenceArrays(_activities: IActivityResult<IActivityModelResult>[], _modelName: string) {
  //const bundles = getActivityEvidenceBundles(activities, modelName);
  return null as any; //bundleArrayToBeliefArrays(bundles);
}

export function getScalarEvidenceBundleAt(activities: IActivityResult<IActivityModelResult>[], modelName: string, pos: number) {
  return R.last(activities[pos].models[modelName].scalarEvidences);
}

export function getScalarEvidenceBundles(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  return R.map((activity) => R.last(activity.models[modelName].scalarEvidences), activities);
}

export function getScalarEvidenceArrays(activities: IActivityResult<IActivityModelResult>[], modelName: string) {
  const bundles = getScalarEvidenceBundles(activities, modelName);
  return bundleArrayToBeliefArrays(bundles);
}

export function getAgentCaseModelBundleAt(_activities: ISimulatedActivityResult[], _modelName: string, _pos: number) {
  return null as any; //activities[pos].models[modelName].agentCaseModel;
}

export function getAgentCaseModelBundles(_activities: ISimulatedActivityResult[], _modelName: string) {
  return null as any; // R.map((activity) => activity.models[modelName].agentCaseModel, activities);
}

export function getAgentCaseModelArrays(_activities: ISimulatedActivityResult[], _modelName: string) {
  //const bundles = getAgentCaseModelBundles(activities, modelName);
  return null as any; //bundleArrayToBeliefArrays(bundles);
} */
