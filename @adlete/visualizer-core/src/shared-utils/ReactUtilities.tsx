/**
 * Gets random int
 * @param min
 * @param max
 * @returns random int - min & max inclusive
 */
export function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * generates a unique key for each list item
 * @param pre a prefix for a generated key, which could be useful for debugging
 * @returns a unique key for an list item
 */
export function generateUniqueListKey(pre = ''): string {
  if (pre === '') {
    for (let i = 0; i < 4; i++) {
      pre += getRandomInt(0, 9);
    }
  }
  const newKey = `${pre}_${new Date().getTime()}`;
  return newKey;
}
