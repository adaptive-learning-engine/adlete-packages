import { IActivityModelResult, IActivityResult, IResults } from '../IVisualizerExtension';

export function getModelStructure(results: IResults<IActivityResult<IActivityModelResult>>, modelName: string) {
  return results.modelStructures[modelName];
}
