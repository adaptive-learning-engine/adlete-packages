import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

// ***************************** LOAD CONFIGURATION ************************************
export function parseModelFile(fileString: string): any {
  const parsedFile = JSON.parse(fileString);

  if (parsedFile === undefined) {
    throw new Error('No files specified');
  }

  if (parsedFile === undefined || parsedFile === null) {
    throw new Error('File does not contain a valid Model.');
  }

  // competenceModel
  const keysInParsedFile = Object.keys(parsedFile);
  if (!keysInParsedFile.includes('competenceModel')) {
    throw new Error('File does not contain a valid Model.');
  }

  const { competenceModel } = parsedFile;
  const graphNodeIds = Object.keys(competenceModel);

  if (graphNodeIds.length > 0) {
    for (let i = 0; i < graphNodeIds.length; i++) {
      const checkKeys = Object.keys(competenceModel[graphNodeIds[i]]);
      if (!checkKeys.includes('id') || !checkKeys.includes('states') || !checkKeys.includes('parents') || !checkKeys.includes('cpt')) {
        throw new Error(`wrong key names in Node ${i}`);
      }
    }
  } else {
    throw new Error('Graph does not contain any node.');
  }
  return competenceModel;
}

// ***************************** SAVE CONFIGURATION ************************************

export function generateConfigurationData(competenceModel: IBayesNet) {
  const allNodeNames = Object.keys(competenceModel);
  const allLeafNodeNames: string[] = [];

  //generate default global weights
  const globalWeights = {
    generalEvidenceWeight: 0.5,
    recommendationCompetenceWeaknessScoreWeight: 0.05,
    recommendationActivityRepetitionScoreWeight: 0.9,
    recommendationActivityCorrectIncorrectRatioScore: 0.05,
    recommendationDifficultyFlowModifier: 0,
    recommendationDifficultyConstantAddition: 0.1,
  };

  //generate activityName and its corresponding activityObservationWeight
  const activityNames: string[] = [];
  const activityObservationWeights: { [key: string]: { [key: string]: number } } = {};
  allNodeNames.forEach((nodeName: string) => {
    console.log(nodeName);
    if (competenceModel[nodeName].parents.length === 0) {
      allLeafNodeNames.push(nodeName);
      const activityName = `activity${nodeName[0].toUpperCase()}${nodeName.substring(1)}`;

      activityNames.push(activityName);

      activityObservationWeights[activityName] = {};
      activityObservationWeights[activityName][nodeName] = 1;
    }
  });

  // initialScalarBeliefs level_beginner, level_intermediate, level_advanced --> for each leaf node value & certainty
  const initialScalarBeliefs: { [key: string]: { [key: string]: { value: number; certainty: number } } } = {};
  const initialDefaultLevels: { [key: string]: number } = { level_beginner: 0.1, level_medium: 0.5, level_advanced: 0.8 };

  Object.keys(initialDefaultLevels).forEach((defaultLevelName: string) => {
    const initialScalarBeliefLevels: { [key: string]: { value: number; certainty: number } } = {};

    allLeafNodeNames.forEach((leafNodeName: string) => {
      initialScalarBeliefLevels[leafNodeName] = { value: initialDefaultLevels[defaultLevelName], certainty: 1 };
    });

    initialScalarBeliefs[defaultLevelName] = initialScalarBeliefLevels;
  });

  // activityRecommenderTargets
  const activityRecommenderTargets: { [key: string]: string[] } = {};
  activityNames.forEach((activityName) => {
    activityRecommenderTargets[activityName] = Object.keys(activityObservationWeights[activityName]);
  });

  // competenceModel von model
  return {
    globalWeights,
    activityNames,
    activityObservationWeights,
    initialScalarBeliefs,
    activityRecommenderTargets,
    competenceModel,
  };
}
