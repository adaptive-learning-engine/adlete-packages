export function parseSimFile(fileString: string): any {
  const parsedFile = JSON.parse(fileString);

  if (parsedFile === undefined) {
    throw new Error('File does not contain a valid Simulation.');
  }

  if (parsedFile === undefined || parsedFile === null) {
    throw new Error('File does not contain a valid Simulation.');
  }

  const analyticsComponents = Object.keys(parsedFile);
  if (!analyticsComponents.includes('learner') || !analyticsComponents.includes('observations')) {
    throw new Error('File does not contain a valid simulation.');
  }
  return parsedFile;
}
