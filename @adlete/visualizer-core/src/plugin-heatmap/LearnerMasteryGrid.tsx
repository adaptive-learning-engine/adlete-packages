import { Collapse } from "@mui/material"
import { Box } from "@mui/system"
import * as R from "ramda"
import React, { useCallback, useState } from "react"

import { IBayesNode } from "@adlete/engine-blocks/belief/IBayesNode"
import { IScalarBeliefBundle } from "@adlete/engine-blocks/belief/IBelief"
import { IScalarTendency, ITendencyBundle } from "@adlete/engine-blocks/recommendation/ITendency"
import { IAdleteConfiguration } from "@adlete/framework/IAdleteConfiguration"
import { IHeatmapData, ILearnerMasteryGridData } from "@adlete/framework/IAnalysisData"
import { ILearner } from "@adlete/framework/ILearner"

import { MasteryHeatmap } from "../shared-components/charts/MasteryHeatmap"

import {LearnerDetails} from "./LearnerDetails"


export interface ILearnerGridProps {
    width: number
    height: number
    mainLearner: ILearner
    learner: {scalar: IScalarBeliefBundle, tendencies: ITendencyBundle<IScalarTendency>}[]
    config: IAdleteConfiguration
    competences: IBayesNode[]
}


export const LearnerMasteryGrid: React.FunctionComponent<ILearnerGridProps> = (props: ILearnerGridProps) => {
    const [hoveredCell, setHoveredCell] = useState<ILearnerMasteryGridData>(null)

    const setHoveredCellCheck = ((value: ILearnerMasteryGridData) => {
        if(hoveredCell?.xLabel === value.xLabel && hoveredCell?.yLabel === value.yLabel) {
            setHoveredCell(null)
        } else {
            setHoveredCell(value)
        }
    })
    
    const prepareData = useCallback((): IHeatmapData[] => {
        let data = R.map<IBayesNode, IHeatmapData>((competence) => {
            const beliefBundle = props.mainLearner.scalarBeliefs[props.mainLearner.scalarBeliefs.length - 1]
            const value = beliefBundle[competence.id]?.value ?? 0
            return {x: competence.id, y: "me", value, tooltip: `Value: ${Math.round(value * 100)}%`}
        }, props.competences)

        if(props.learner.length > 0) {
            const otherData = R.map<IBayesNode, IHeatmapData>((competence) => {
                    let sum = 0;
                    props.learner.forEach(bundels => {
                        sum += bundels.scalar[competence.id]?.value ?? 0
                    })
                    return {x: competence.id, y: "group", value: sum / props.learner.length, tooltip: `Average: ${Math.round(sum / props.learner.length * 100)}%`}
                }, props.competences)

            const mapIndexCompetences = R.addIndex<IBayesNode, IHeatmapData>(R.map)
            const collectioveData = mapIndexCompetences((competence, index) => {
                const value = (data[index].value - otherData[index].value + 1) / 2
                return {x: competence.id,
                    y: "me vs group", value,
                    tooltip: `Me: ${Math.round(data[index].value * 100)}%, Group: ${Math.round(otherData[index].value * 100)}%, Diff: ${Math.round((data[index].value - otherData[index].value) * 100)}%`}
            }, props.competences)
            data = R.concat(data, R.concat(otherData, collectioveData))
        }
        return data
    }, [props])

    return (
        <>
            <h3>Progress and Comparison Grid</h3>
            <Box sx={{display: 'inline-flex'}}>
                <MasteryHeatmap width={props.width} height={props.height} data={prepareData()} setHoveredCell={setHoveredCellCheck}/>
                <Collapse in={hoveredCell !== null}>
                    <LearnerDetails
                    height={props.height}
                    mainLearners={[props.mainLearner]}
                    learnerData={props.learner}
                    labels={hoveredCell === null ? [] : props.config.competenceModel[hoveredCell.xLabel].parents}/>
                </Collapse>
            </Box>
        </>
    )
}