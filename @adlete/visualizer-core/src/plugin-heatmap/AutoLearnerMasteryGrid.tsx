import { Autocomplete, Box, TextField } from "@mui/material";
import React, { FunctionComponent, useMemo, useState } from "react";

import { IBayesNode } from "@adlete/engine-blocks/belief/IBayesNode";
import { IScalarBeliefBundle } from "@adlete/engine-blocks/belief/IBelief";
import { IScalarTendency, ITendencyBundle } from "@adlete/engine-blocks/recommendation/ITendency";
import { IAdleteConfiguration } from "@adlete/framework/IAdleteConfiguration";
import { ILearner } from "@adlete/framework/ILearner";

import {LearnerMasteryGrid } from "./LearnerMasteryGrid";

interface IAutoLearnerMasteryGridProps{
    defaultValue: IBayesNode
    width: number
    height: number
    mainLearner: ILearner
    learner: {scalar: IScalarBeliefBundle, tendencies: ITendencyBundle<IScalarTendency>}[]
    config: IAdleteConfiguration
}
 
export const AutoLearnerMasteryGrid: FunctionComponent<IAutoLearnerMasteryGridProps> = (props: IAutoLearnerMasteryGridProps) => {
    const selectableNodes = useMemo(() => {
        return Object.keys(props.config.competenceModel).filter((key) => props.config.competenceModel[key].parents.length !== 0)
    }, [props.config])

    const [hMSelection, setHMSelection] = useState<IBayesNode>(props.defaultValue)

    return ( 
        <Box>
            <Autocomplete
            options={selectableNodes}
            value={hMSelection.id}
            renderInput={(params) => <TextField {...params} label="Select Competence"/>}
            onInputChange={(event, newInputValue) => {setHMSelection(props.config.competenceModel[newInputValue])}}
            sx={{maxWidth: "250px"}}
            >
            </Autocomplete>
            <LearnerMasteryGrid
              config={props.config}
              height={props.height}
              width={props.width}
              learner={props.learner}
              mainLearner={props.mainLearner}
              competences={hMSelection.parents.map(parent => props.config.competenceModel[parent])}
            />
        </Box>
    );
}