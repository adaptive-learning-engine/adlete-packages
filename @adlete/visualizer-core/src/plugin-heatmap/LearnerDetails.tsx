import * as R from 'ramda'
import * as React from 'react';

import { IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendency, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { ILearner } from '@adlete/framework/ILearner';

import { LearnerBarPlot } from '../shared-components/charts/LearnerBarPlot';

interface ILearnerDetailsProp {
    mainLearners: ILearner[],
    learnerData: {scalar: IScalarBeliefBundle, tendencies: ITendencyBundle<IScalarTendency>}[]
    labels: string[],
    height?: number,
    width?:number
}

export const LearnerDetails: React.FunctionComponent<ILearnerDetailsProp> = ({mainLearners, learnerData, labels, height, width}: ILearnerDetailsProp) => {

    const  prepareSubData = React.useCallback((): {data: number[], label: string}[] => {
        const data = R.map<ILearner,{data: number[], label: string}>((learner) => {
            const scalarBeliefs = learner.scalarBeliefs[learner.scalarBeliefs.length - 1]
            const dataMap: number[] = labels.map((value) => {
                return scalarBeliefs[value].value * 100
            })
            return {data: dataMap, label: `${learner.learnerId}`}
    }, mainLearners)

    if(learnerData.length > 0) {
        const averageData = R.map<string, number>(label => {
            const sumValues = R.reduce<{scalar: IScalarBeliefBundle, tendencies: ITendencyBundle<IScalarTendency>}, number>((sum, ele) => {
                return sum += ele.scalar[label].value
            }, 0, learnerData)
            return sumValues / learnerData.length * 100
        }, labels)
        data.push({data: averageData, label: 'average Data'})
    }
    return data;
    }, [labels, learnerData, mainLearners])

    if(labels.length == 0) {
        return null
    }

    return (
        <LearnerBarPlot height={height ?? 500} width={width ?? 500} data={prepareSubData()} yLabel={labels}/>
    )
}