import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';
import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '../plugin-layout/LayoutManagerPlugin';

import { LearnerAnalysisModule } from './LearnerAnalysisModule';

export interface IWithLearnerAnalysis extends IWithLayoutManager {
  learnerAnalysis: LearnerAnalysisPlugin;
}

export const LearnerAnalysisMeta: IPluginMeta<IWithLearnerAnalysis, 'learnerAnalysis'> = {
  id: 'learnerAnalysis',
  dependencies: ['layoutManager'],
};

export class LearnerAnalysisPlugin implements IPlugin<IWithLearnerAnalysis> {
  public meta: IPluginMeta<IWithLearnerAnalysis, 'learnerAnalysis'> = LearnerAnalysisMeta;

  public initialize(pluginMan: IPluginManager<IWithLearnerAnalysis>): Promise<void> {
    const layoutMan = pluginMan.get('layoutManager');
    layoutMan.panelTypes.add('LearnerAnalysis', {
      createPanel: () => <LearnerAnalysisModule />,
      title: 'Learner Analysis',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
