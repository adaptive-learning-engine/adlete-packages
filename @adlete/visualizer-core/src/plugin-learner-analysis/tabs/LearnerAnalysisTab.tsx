import { Box } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { AnalysisContainer, LearnerAnalysisMode } from '../../shared-containers/LearnerAnalysisContainer';

interface ILearnerAnalysisTabProps {
  ModeOfDisplay: LearnerAnalysisMode;
  analytics: IAnalyticsResponse;
}

// eslint-disable-next-line react/prefer-stateless-function
export class LearnerAnalysisTab extends React.Component<ILearnerAnalysisTabProps> {
  public render(): React.ReactNode {
    return (
      <Box>
        <AnalysisContainer modeOfDisplay={this.props.ModeOfDisplay} learnerAnalytics={this.props.analytics} />
      </Box>
    );
  }
}
