import ExpandMore from '@mui/icons-material/ExpandMore';
import { Backdrop, Box, Button, CircularProgress, Collapse, Container, Grid, IconButton, IconButtonProps, styled, TextField } from '@mui/material';
import Stack from '@mui/material/Stack';
import * as React from 'react';
import { useSearchParams } from 'react-router-dom';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';
import * as AnalysisRoutines from '@adlete/client-ts/routines/AnalysisRoutines';
import { fetchMultipleLearner } from '@adlete/client-ts/routines/LearnerRoutines';

import { useCurrentUser, useSetCurrentUser } from '../../context/CurrentUserContext';
import { useGraphQLClient } from '../../custom-hooks/useGraphQLClient';
import { useIsMounted } from '../../custom-hooks/useIsMounted';
import { useLearnerIds } from '../../custom-hooks/useLearnerIds';
import { useServerStatus } from '../../custom-hooks/useServerStatus';
import { LearnerIdTextbox } from '../../shared-components/LearnerIdTextbox';


interface ILoadLearnerAnalysisTabProps {
  onLearnerAnalyticsLoaded: (learnerAnalytics: IAnalyticsResponse) => void;
}

interface ILoadLearnerAnalysisTabState {
  learnerId: string; // holds the current learnerId input string
  errorMessage: string; // holds an error message
  statusCode: number;
  backDropVisible: boolean;
  learnerIds?: string | string[];
}

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMoreButton = styled((props: ExpandMoreProps) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export const LoadLearnerAnalysisTab: React.FunctionComponent<ILoadLearnerAnalysisTabProps> = ({onLearnerAnalyticsLoaded}: ILoadLearnerAnalysisTabProps) => {


  const setCurrentUser = useSetCurrentUser();
  const currentUser = useCurrentUser();
  const graphQLClient = useGraphQLClient();
  const [isOnline, isAuthenticated] = useServerStatus()
  const [searchParams] = useSearchParams()
  const compIsMounted = useIsMounted()
  const availableLearnerIds = useLearnerIds();
  const [analysisTabState, setAnalysisTabState] = React.useState<ILoadLearnerAnalysisTabState>({
      learnerId: '',
      statusCode: 200,
      errorMessage: '',
      backDropVisible: false,})
  const [expanded, setExpanded] = React.useState(false)

    React.useEffect(() => {
      if(isOnline) {
        if(isAuthenticated) {
          setAnalysisTabState(previousState => ({...previousState, statusCode: 200, errorMessage: "Connected"}))
        } else {
          setAnalysisTabState(previousState => ({...previousState, statusCode: 401, errorMessage: "Not authenticated"}))
        }
      } else {
        setAnalysisTabState(previousState => ({...previousState, statusCode: 404, errorMessage: "Server not Online"}))
      }
    }, [isOnline, isAuthenticated])

  const fetchLearner = React.useCallback(async () => {
    const state = analysisTabState;
    if(analysisTabState.learnerId === null || analysisTabState.learnerId === "") {
      setAnalysisTabState({...state, errorMessage: "Please enter a correct learnerId", statusCode: 402 });
      return;
    }

    setAnalysisTabState({ ...state, backDropVisible: true });
    const responseFetchLearnerAnalytics = await AnalysisRoutines.fetchLearnerAnalytics(graphQLClient, state.learnerId, currentUser.clientData?.idCipher);
    let errorMessage = '';
    
    if (responseFetchLearnerAnalytics.statusInfo.statusCode === 200) {
      if(expanded && state.learnerIds) {
        let learnerIds: string[]
        if(!Array.isArray(state.learnerIds)) {
          learnerIds = state.learnerIds.split(',')
          learnerIds = learnerIds.map((id) => id.trim())
        } else {
          learnerIds = state.learnerIds
        }
        const learnerData = await fetchMultipleLearner(graphQLClient, learnerIds, currentUser.clientData?.idCipher)
        if(learnerData.statusInfo.statusCode === 200) {
          responseFetchLearnerAnalytics.analyticsData = {...responseFetchLearnerAnalytics.analyticsData, learners: learnerData.learners}
        }
      }
      onLearnerAnalyticsLoaded(responseFetchLearnerAnalytics.analyticsData);
    } else {
      errorMessage = responseFetchLearnerAnalytics.statusInfo.statusDescription;
      setAnalysisTabState({...state, backDropVisible: false });
    }

    if (compIsMounted) {
      setAnalysisTabState({...state, errorMessage, statusCode: responseFetchLearnerAnalytics.statusInfo.statusCode });
    }
  }, [analysisTabState, compIsMounted, currentUser.clientData, expanded, graphQLClient, onLearnerAnalyticsLoaded])

  function backDrop(value: boolean): React.ReactNode {
    const backDropElement = (
      <Grid>
        <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }} open={value}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    );
    return backDropElement;
  }

  function serverStatusTextHelper(): React.ReactNode {
    let TextHelper;
    if (analysisTabState.statusCode == 200) {
      TextHelper = (
        <Box component="div" display="inline">
          <span>server status: </span>
          <span style={{ color: 'green' }}> Connected </span>
        </Box>
      );
    } else {
      TextHelper = (
        <Box component="div" display="inline">
          <span>server status: </span>
          <span style={{ color: 'red' }}> {analysisTabState.errorMessage} </span>
        </Box>
      );
    }
    return TextHelper;
  }

  return (
      <Box sx={{ mt: 3 }}>
        <Container maxWidth="md">
          <Box sx={{ mt: 3 }}>
            <Stack spacing={2}>
              <LearnerIdTextbox
                label='Learner Id'
                multiple={false}
                error={analysisTabState.statusCode !== 200}
                handleLearnerId={(learnerId) => setAnalysisTabState({...analysisTabState, learnerId: learnerId as string})}
                id='LearnerId'
                options={availableLearnerIds}
              />
              <TextField
                fullWidth
                id="host"
                error={!isOnline}
                value={currentUser.host}
                disabled={isOnline && searchParams.get("host") !== null}
                label="host"
                margin="none"
                onChange={(e) => {
                  setCurrentUser({ ...currentUser, host: e.target.value});
                }}
              />
              <Box sx={{display: "flex", justifyContent: "flex-start", maxWidth: '250px'}}>
                <p >Compare Settings</p>
                <ExpandMoreButton expand={expanded} onClick={() => setExpanded(!expanded)}>
                  <ExpandMore/>
                </ExpandMoreButton>
              </Box>
              <Collapse in={expanded}>
                <LearnerIdTextbox
                  id='LearnerIds'
                  label='Choose Learner to compare'
                  error={analysisTabState.statusCode !== 200}
                  handleLearnerId={(selectedLearnerIds) => setAnalysisTabState({...analysisTabState, learnerIds: selectedLearnerIds})}
                  multiple={true}
                  options={availableLearnerIds}
                />
              </Collapse>
              <Box component="div" display="inline">
                {serverStatusTextHelper()}
              </Box>
              <Button variant="contained" color="primary" style={{ margin: '10px' }} onClick={() => fetchLearner()}>
                Submit
              </Button>
              {backDrop(analysisTabState.backDropVisible)}
            </Stack>
          </Box>
        </Container>
      </Box>
    );
  }
