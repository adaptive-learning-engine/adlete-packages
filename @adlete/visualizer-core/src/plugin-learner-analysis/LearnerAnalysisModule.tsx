import AddIcon from '@mui/icons-material/Add';
import { Box, IconButton, Switch, Toolbar, Tooltip, Typography } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { LearnerAnalysisMode } from '../shared-containers/LearnerAnalysisContainer';

import { LoadLearnerAnalysisDialog } from './dialogs/LoadLearnerAnalysisDialog';
import { LearnerAnalysisTab } from './tabs/LearnerAnalysisTab';

export enum TabID {
  USER_LOAD = 0,
  USER_ANALYSIS_GRID = 1,
  USER_ANALYSIS_GRAPH = 2,
}

export enum LearnerDialog {
  None,
  LoadLearner,
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ILearnerAnalysisModuleProps {}

interface ILearnerAnalysisModuleState {
  learnerAnalytics: IAnalyticsResponse;
  displayMode: LearnerAnalysisMode;
  currentDialog: LearnerDialog;
  // tabIndex: TabID;
}

export class LearnerAnalysisModule extends React.Component<ILearnerAnalysisModuleProps, ILearnerAnalysisModuleState> {
  constructor(props: ILearnerAnalysisModuleProps) {
    super(props);
    this.state = {
      learnerAnalytics: null,
      // tabIndex: TabID.USER_LOAD,
      currentDialog: LearnerDialog.None,
      displayMode: LearnerAnalysisMode.GridMode,
    };
    //this.onTabChanged = this.onTabChanged.bind(this);
    this.onLearnerAnalyticsLoaded = this.onLearnerAnalyticsLoaded.bind(this);
    this.onSwitchChange = this.onSwitchChange.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  //private onTabChanged = (_event: React.SyntheticEvent, newValue: TabID) => {
    // this.setState({ tabIndex: newValue });
  //};

  private onLearnerAnalyticsLoaded(learnerAnalytics: IAnalyticsResponse) {
    this.setState({
      learnerAnalytics,
      currentDialog: LearnerDialog.None,
    });
  }

  // toolbar
  private onSwitchChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      this.setState({ displayMode: LearnerAnalysisMode.GridMode });
    } else {
      this.setState({ displayMode: LearnerAnalysisMode.GraphMode });
    }
  }

  private openDialog(dialog: LearnerDialog) {
    this.setState({ currentDialog: dialog });
  }

  private closeDialog() {
    this.setState({ currentDialog: LearnerDialog.None });
  }

  protected renderAnalysis(): React.ReactNode {
    if (!this.state.learnerAnalytics) {
      return (
        <Box marginLeft="10px">
          <Typography>Please load learner analytics data to start...</Typography>
        </Box>
      );
    }

    return <LearnerAnalysisTab analytics={this.state.learnerAnalytics} ModeOfDisplay={this.state.displayMode} />;
  }

  protected renderDialog(): React.ReactElement {
    switch (this.state.currentDialog) {
      case LearnerDialog.LoadLearner:
        return <LoadLearnerAnalysisDialog onLearnerAnalyticsLoaded={this.onLearnerAnalyticsLoaded} onClose={this.closeDialog} />;
      default:
        return null;
    }
  }

  public render(): React.ReactNode {
    return (
      <Box sx={{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
        <Toolbar>
          <Tooltip title="Load learner">
            <IconButton sx={{ mr: 5 }} color="primary" onClick={() => this.openDialog(LearnerDialog.LoadLearner)}>
              <AddIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="switch display mode">
            <span>
              <Switch defaultChecked onChange={this.onSwitchChange} disabled={this.state.learnerAnalytics === null} />
            </span>
          </Tooltip>
        </Toolbar>
        {this.renderDialog()}
        {this.renderAnalysis()}
      </Box>
    );
  }
}
