import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { LoadLearnerAnalysisTab } from '../tabs/LoadLearnerAnalysisTab';

export interface LoadLearnerAnalysisDialogProps {
  onLearnerAnalyticsLoaded: (learnerAnalytics: IAnalyticsResponse) => void;
  onClose: () => void;
}

export function LoadLearnerAnalysisDialog(props: LoadLearnerAnalysisDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Load learner</DialogTitle>
        <DialogContent>
          <LoadLearnerAnalysisTab onLearnerAnalyticsLoaded={props.onLearnerAnalyticsLoaded} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
