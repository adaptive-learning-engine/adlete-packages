import React, { useRef } from "react"

export interface ICurrentUserContext {
    host: string
    token: string
    refreshToken?: string
    clientData?: {idCipher: string}
}

type Props = {
    children?: React.ReactNode
}

const CurrentUserContext = React.createContext<ICurrentUserContext>(null)
const SetCurrentUserContext = React.createContext<React.Dispatch<React.SetStateAction<ICurrentUserContext>>>(null)


export const CurrentUserProvider: React.FunctionComponent<Props> = ({children}: Props) => {
    let initialState: ICurrentUserContext;
    const isCrossOrigin = useRef(false)
    try {
        if(window.self === window.top) {
            const storedValues = window.sessionStorage.getItem("currentUser")
            if(storedValues !=  null) {
                initialState = JSON.parse(storedValues)
            } else {
                initialState = readFromURL();
                sessionStorage.setItem("currentUser", JSON.stringify(initialState))
            }
        } else {
            initialState = readFromURL()
        }
        isCrossOrigin.current = false;
    } catch (e) {
        initialState = readFromURL()
        isCrossOrigin.current = true;
    }
    const [currentUser, setCurrentUser] = React.useState<ICurrentUserContext>(initialState)
    const setUserContextWithStorage = (newContext: React.SetStateAction<ICurrentUserContext>) => {
        if(!isCrossOrigin.current) {
            sessionStorage.setItem("currentUser", JSON.stringify(newContext))
        }
        setCurrentUser(newContext); // Call the original setter
    };
    
    return (
        <CurrentUserContext.Provider value={currentUser}>
            <SetCurrentUserContext.Provider value={setUserContextWithStorage}>
                {children}  
            </SetCurrentUserContext.Provider>
        </CurrentUserContext.Provider>
    )
}

function readFromURL(): ICurrentUserContext {
    const searchParams = new URLSearchParams(window.location.search)
    const token = searchParams.get("token")
    const idCipher = searchParams.get("idcipher")
    const host = searchParams.get("host")
    const refreshToken = searchParams.get("refreshtoken")
    return {host, token, refreshToken, clientData: {idCipher}}
}
export function useCurrentUser() {
    return React.useContext(CurrentUserContext)
}

export function useSetCurrentUser() {
    return React.useContext(SetCurrentUserContext)
}