import { ThemeManager } from '@fki/editor-core/theme/ThemeManager';
import type { IPlugin, IPluginMeta } from '@fki/plugin-system/specs';

export interface IWithThemeManager {
  themeManager: ThemeManagerPlugin;
}

export const ThemeManagerMeta: IPluginMeta<IWithThemeManager, 'themeManager'> = {
  id: 'themeManager',
};

export class ThemeManagerPlugin extends ThemeManager implements IPlugin<IWithThemeManager> {
  meta: IPluginMeta<IWithThemeManager, 'themeManager'> = ThemeManagerMeta;

  public initialize(): Promise<void> {
    return super.initialize();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
