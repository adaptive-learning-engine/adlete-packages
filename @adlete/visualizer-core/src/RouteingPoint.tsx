import * as React from "react";
import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";

import { CurrentUserProvider } from "./context/CurrentUserContext";
import { Homepage } from "./routes/default/Homepage";
import { Login } from "./routes/login/Login";



interface RoutingPointProps {
    mainPage: string | JSX.Element | JSX.Element[]
}
 
export const RoutingPoint: React.FunctionComponent<RoutingPointProps> = ({mainPage}: RoutingPointProps) => {
const base = window.location.pathname
const router = createBrowserRouter([
    {path: "login", element: <Login/>},
    {path: "*", element: <Homepage>{mainPage}</Homepage>}
], {basename: base})
    
    return ( 
        <CurrentUserProvider>
            <RouterProvider router={router}></RouterProvider>
        </CurrentUserProvider>
    );
}