import { useEffect, useState } from "react"

import { getLearnerIds } from "@adlete/client-ts/routines/LearnerRoutines"

import { useCurrentUser } from "../context/CurrentUserContext"

import { useGraphQLClient } from "./useGraphQLClient"

export function useLearnerIds() {
    const currentUser = useCurrentUser()
    const graphQL = useGraphQLClient()
    const [learnerIds, setLearnerIds] = useState(currentUser.clientData?.idCipher ? [] : null)

    useEffect(() => {
        if(currentUser.clientData?.idCipher) {
            getLearnerIds(graphQL, currentUser.clientData.idCipher)
            .then((fetchedLearnerIds) => {
                setLearnerIds(fetchedLearnerIds)
            })
        }
    }, [currentUser, graphQL])

    return learnerIds
}