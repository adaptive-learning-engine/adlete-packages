import { useEffect, useRef } from "react";
/**
 * A custom hook that returns if the current component is mounted.
 * @returns {boolean} true if component is mounted
 */
export const useIsMounted = () => {
  const isMountedRef = useRef(true);

  useEffect(() => {
    isMountedRef.current = true
    return () => {isMountedRef.current = false}
  }, []);

  return isMountedRef;
}