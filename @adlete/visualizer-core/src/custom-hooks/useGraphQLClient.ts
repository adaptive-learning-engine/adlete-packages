import {useMemo} from 'react'

import {createClient} from '@adlete/client-ts/GraphQLClientExtension';
import { GraphQLClient } from "@adlete/client-ts/Types";

import { useCurrentUser } from "../context/CurrentUserContext";

/**
 * A Custom hook that returns a valid GraphQLClient with the current user context
 * @returns {GraphQLClient} client with correct configuration
 */
export function useGraphQLClient(): GraphQLClient {
    const user = useCurrentUser();
    if (!user) {
        throw new Error('GraphQLClient cannot be created without user context');
    }
    const graphQLClient = useMemo(() => createClient(user.host, user.token), [user])

    return graphQLClient
}