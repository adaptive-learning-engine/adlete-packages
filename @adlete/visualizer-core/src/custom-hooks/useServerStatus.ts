import { useCallback, useEffect, useState } from "react"

import { refreshLogin } from "@adlete/client-ts/routines/AuthentificationRoutines"
import {checkStatus} from "@adlete/client-ts/routines/ServiceRoutines"

import { useCurrentUser, useSetCurrentUser } from "../context/CurrentUserContext"

import { useGraphQLClient } from "./useGraphQLClient"

/**
 * Hook to check server status with the currentUser.host.
 * @param interval The intervalll after which a new status check is send
 * @returns {boolean[]} 0: is server up, 1: is user authorized
 */
export const useServerStatus = (interval=10000) => {
    const [isOnline, setIsOnline] = useState(false)
    const [isAuthorized, setIsAuthorized] = useState(false)
    const graphQLClient = useGraphQLClient()
    const currentUser = useCurrentUser()
    const setCurentUser = useSetCurrentUser()

    const checkServerStatus = useCallback(async () => {
        const responseCheckStatus = await checkStatus(graphQLClient)
        const serverOnline = responseCheckStatus.statusCode === 200
        setIsOnline(serverOnline)
        if(serverOnline && !responseCheckStatus.authenticated && currentUser.token && currentUser.refreshToken) {
          const refreshResponse = await refreshLogin(graphQLClient, currentUser.refreshToken)
          if(refreshResponse.statusInfo.statusCode === 200) {
            setCurentUser({...currentUser, token: refreshResponse.tokens.accessToken})
          }
          return
        }
        setIsAuthorized(serverOnline ? responseCheckStatus.authenticated : false)
      }, [currentUser, graphQLClient, setCurentUser])
      
    useEffect(() => {
        checkServerStatus()
        const intervalID = setInterval(() => checkServerStatus(), interval);
        return () => {
          clearInterval(intervalID);
        }
      }, [checkServerStatus, interval]
    )
    return [isOnline, isAuthorized]
}