import AddBox from '@mui/icons-material/AddBox';
import HighlightOff from '@mui/icons-material/HighlightOff';
import { Box, Button, Card, Divider, List, ListItemText, Stack } from '@mui/material';
import dagre from 'dagre';
import { Graph } from 'graphlib';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { bayesNetToGraph } from '@adlete/engine-blocks/belief/convert';
import { convertGraph } from '@adlete/engine-blocks/utils/Utils';

import { DEFAULT_STYLE, wrapAllAsForeignObject } from '../charts/Plots';

import { IContentNode, INodeProps, IVXGraph } from './VXGraphInterfaces';

export function getNodeContent(props: INodeProps): React.ReactElement {
  return props.node.content;
}

export function toVXGraph(graph: Graph): IVXGraph<IContentNode> {
  return {
    nodes: graph.nodes().map((nodeName: string) => graph.node(nodeName)),
    links: graph.edges().map((edge: { v: string; w: string }) => ({
      source: graph.node(edge.v),
      target: graph.node(edge.w),
    })),
  };
}

// Layout https://github.com/dagrejs/dagre/wiki#configuring-the-layout
export function toContentGraph(
  structure: Graph,
  contents: Record<string, React.ReactElement>,
  nodeLayout: dagre.NodeConfig,
  graphLayout: dagre.GraphLabel = {
    nodesep: 100,
    ranksep: 150,
    ranker: 'tight-tree',
    rankdir: 'BT',
  }
): IVXGraph<IContentNode> {
  // convert to something dagre will understand
  const convertNode = (name: string) => {
    if (!contents[name]) {
      throw new Error(`Content record does not contain a value for graph node ${name}`);
    }
    return {
      x: 0, // will be set by dagre
      y: 0, // will be set by dagre
      width: nodeLayout.width,
      height: nodeLayout.height,
      name,
      content: contents[name],
    };
  };
  const contentGraph = convertGraph(structure, convertNode);

  // layouting
  contentGraph.setGraph(graphLayout);
  dagre.layout(contentGraph as unknown as dagre.graphlib.Graph); // TODO: wait for fixed typo in @types/dagre

  return toVXGraph(contentGraph);
}

export function generateLearnerAnalysisVXGraph(
  competenceModel: IBayesNet,
  plots: Record<string, React.ReactElement>
): IVXGraph<IContentNode> {
  let wrappedNodePlots: Record<string, React.ReactElement> = {};

  Object.keys(plots).forEach((nodeName) => {
    wrappedNodePlots[nodeName] = (
      <Box>
        <Card>{plots[nodeName]}</Card>
      </Box>
    );
  });

  wrappedNodePlots = wrapAllAsForeignObject(wrappedNodePlots, 250, 250, 'graph-node');

  return toContentGraph(bayesNetToGraph(competenceModel), wrappedNodePlots, {
    width: DEFAULT_STYLE.beliefBarCharts.width,
    height: DEFAULT_STYLE.beliefBarCharts.height,
  });
}

export function generateModelBuilderVXGraph(
  selectedGraph: string,
  competenceModel: IBayesNet,
  root: string,
  addGraphNode: (id: string) => void,
  deleteGraphNode: (id: string) => void,
  getNodeInfo: (id: string) => void
): IVXGraph<IContentNode> {
  let plots: Record<string, React.ReactElement> = {};
  Object.keys(competenceModel).forEach((nodeName, index) => {
    const plot = (
      <Box key={index}>
        <Card
          style={{ backgroundColor: nodeName === selectedGraph ? 'yellow' : 'white', padding: '5px 5px 5px 5px' }}
          onClick={() => getNodeInfo(nodeName)}
        >
          <h2>{nodeName}</h2>
          <Divider variant="middle" />
          <List>
            {competenceModel[nodeName].states.map((state, subIndex) => {
              return (
                <ListItemText key={subIndex} sx={{ marginLeft: '2em' }} primaryTypographyProps={{ fontSize: 16 }}>
                  {state}
                </ListItemText>
              );
            })}
          </List>
        </Card>
        <Stack direction="row" spacing={2}>
          <Button variant="contained" color="success" size="small" onClick={() => addGraphNode(nodeName)}>
            <AddBox />
          </Button>
          <Button onClick={() => deleteGraphNode(nodeName)} size="small" variant="contained" color="error" disabled={nodeName === root}>
            <HighlightOff />
          </Button>
        </Stack>
      </Box>
    );

    plots[nodeName] = plot;
  });

  plots = wrapAllAsForeignObject(plots, 380, 330, 'graph-node');

  const myIVXGraph = toContentGraph(bayesNetToGraph(competenceModel), plots, {
    width: DEFAULT_STYLE.beliefBarCharts.width,
    height: DEFAULT_STYLE.beliefBarCharts.height,
  });

  return myIVXGraph;
}
