export interface IContentNode {
  x: number;
  y: number;
  width: number;
  height: number;
  name: string;
  content: React.ReactElement;
}

export interface IVXLink<TNode> {
  source: TNode;
  target: TNode;
}

export interface IVXGraph<TNode> {
  links: IVXLink<TNode>[];
  nodes: TNode[];
}

export interface INodeProps {
  node: IContentNode;
}
