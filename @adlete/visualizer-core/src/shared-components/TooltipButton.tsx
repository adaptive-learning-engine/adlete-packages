import { IconButton, Tooltip } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import React from 'react';

interface ITooltipButtonProps {
  message: React.ReactNode;
}

function TooltipButton({ message }: ITooltipButtonProps) {
  return (
    <Tooltip title={message} placement="right">
      <IconButton>
        <InfoIcon></InfoIcon>
      </IconButton>
    </Tooltip>
  );
}

export default TooltipButton;
