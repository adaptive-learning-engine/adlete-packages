import { Autocomplete, TextField } from "@mui/material";
import React, { FunctionComponent} from "react";

interface LearnerIdTextboxProps {
    label: string
    handleLearnerId: (learner: string | string[] ) => void
    multiple: boolean
    error: boolean
    options?: string[],
    id?: string
}

export const LearnerIdTextbox: FunctionComponent<LearnerIdTextboxProps> = (props: LearnerIdTextboxProps) => {
    if(props.options) {
        return (<Autocomplete
        multiple={props.multiple}
        id={props.id}
        options={props.options}
        onChange={(e, newValue: string | string[]) => props.handleLearnerId(newValue)}
        renderInput={(params) => <TextField {...params} error={props.error} label={props.label}/>}
    />)
    }
    else {
        return (              
        <TextField
            sx={{width: "100%"}}
            id={props.id}
            error={props.error}
            label={props.label}
            onChange={(e) => props.handleLearnerId(e.target.value)}
            margin="none"
          />);
    }
}