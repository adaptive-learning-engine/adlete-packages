/* eslint-disable react/prefer-stateless-function */
import { Autocomplete, AutocompleteChangeDetails, AutocompleteChangeReason, TextField } from '@mui/material';
import * as React from 'react';

export interface ISearchFieldComponentProps {
  sourceData: string[];
  onSearchRequest: (value: string[], reason: AutocompleteChangeReason, details?: AutocompleteChangeDetails<string> ) => void;
}

export const SearchFieldComponent = ({sourceData, onSearchRequest}: ISearchFieldComponentProps) => {
    if (sourceData) {
      return (
        <div style={{ minWidth: 300, maxWidth: 700}}>
          <Autocomplete
            key={sourceData.length}
            limitTags={5}
            size="small"
            options={sourceData}
            multiple
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            onChange={(_event: any, value: string[], reason: AutocompleteChangeReason, details?: AutocompleteChangeDetails<string>) => {
              onSearchRequest(value, reason, details);
            }}
            autoComplete
            // eslint-disable-next-line react/jsx-props-no-spreading
            renderInput={(params) => <TextField {...params} label="Search" variant="outlined" fullWidth />}
          />
        </div>
      );
    }
    return <div>No content</div>;
}
