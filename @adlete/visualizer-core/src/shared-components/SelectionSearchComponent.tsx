import { AutocompleteChangeDetails, AutocompleteChangeReason, Grid, Stack } from '@mui/material';
import * as React from 'react';

import { SearchFieldComponent } from './SearchFieldComponent';

export interface ISelectionSearchComponentProps {
  options: string[];
  stateCallback: (pinnedBeliefNames: string[], reason: AutocompleteChangeReason, details?: AutocompleteChangeDetails<string>) => void;
}

export enum SearchChanges {
  Select,
  Remove,
  Clear
}

export const SelectionSearchComponent = ({options, stateCallback}:ISelectionSearchComponentProps) => {

  return (
    <Grid container>
      <Stack direction="row" spacing={2}>
        <SearchFieldComponent onSearchRequest={stateCallback} sourceData={options} />
      </Stack>
    </Grid>
  );
}
