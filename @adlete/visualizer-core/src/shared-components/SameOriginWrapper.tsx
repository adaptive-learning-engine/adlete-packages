import { FunctionComponent } from "react";

interface ISameOriginWrapperProps {
    children: JSX.Element
}
 /**
  * Component that only renders if its not part of a cross-origin iframe
  * @param children The JSX.Element that should be displayed. 
  * @returns The JSX.Elements taken as props or null if the side is loaded as a cross-origin iframe
  */
export const SameOriginWrapper: FunctionComponent<ISameOriginWrapperProps> = ({children}: ISameOriginWrapperProps) => {
    try {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const isSameOrigin = window.top === window.parent
        return children
    } catch (e) {
        return null;
    }
}