/* eslint-disable react/jsx-props-no-spreading */
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import TrendingFlatIcon from '@mui/icons-material/TrendingFlat';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import { Box, Grid, styled, Typography } from '@mui/material';
import Fade from '@mui/material/Fade';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import { AxisBottom, AxisLeft } from '@visx/axis';
import { TickFormatter } from '@visx/axis/lib/types';
import { Group } from '@visx/group';
import { scaleBand, scaleLinear } from '@visx/scale';
import { Bar } from '@visx/shape';
import { ScaleBand, ScaleLinear } from 'd3-scale';
import PopupState, { bindPopper } from 'material-ui-popup-state';
import { bindHover } from 'material-ui-popup-state/hooks';
import * as React from 'react';

import { IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendency } from '@adlete/engine-blocks/recommendation/ITendency';

import { PROBABILITY_COLORS } from './Colors';

export interface IBeliefBarChartProps {
  width: number;
  height: number;
  title: string;
  probabilityBelief: IProbabilityBelief;
  // eslint-disable-next-line react/no-unused-prop-types
  scalarBelief: IScalarBelief;
  // eslint-disable-next-line react/no-unused-prop-types
  tendency: IScalarTendency;
  states: string[];
}

function createBar(x: number, y: number, color: string, xScale: ScaleLinear<number, number>, yScale: ScaleBand<number>) {
  return <Bar key={`bar-${y}`} x={60} y={yScale(y) - 20} width={xScale(x)} height={yScale.bandwidth()} fill={color} />;
}

function createBars(
  probabilityBelief: IProbabilityBelief,
  barColors: string[],
  xScale: ScaleLinear<number, number>,
  yScale: ScaleBand<number>
) {
  const bars: React.ReactNode[] = [];
  for (let i = 0; i < barColors.length; i++) {
    bars.push(createBar(probabilityBelief[i], i, barColors[i], xScale, yScale));
  }
  return bars;
}
// draw Trend Icon Inspector again
function getTrendIcon(tendency: number): React.ReactElement {
  tendency = Math.round((tendency + Number.EPSILON) * 100000) / 100000;
  if (tendency < 0) {
    return <TrendingDownIcon />;
  }
  if (tendency === 0) {
    return <TrendingFlatIcon />;
  }
  return <TrendingUpIcon />;
}

// draw Trend Scalar Value Inspector again
function drawScalarValue(scalarValue: number, style: React.CSSProperties): React.ReactElement {
  return <div style={style}>- Scalar Value: {scalarValue.toFixed(5)} </div>;
}

// draw Trend Value Inspector again
function drawTrendValue(tendency: IScalarTendency, style: React.CSSProperties): React.ReactElement {
  return (
    <div style={style}>
      {' '}
      - Trend Value: {tendency !== undefined ? tendency.tendency.toFixed(5) : 'No Trend calculation'}{' '}
      {getTrendIcon(tendency !== undefined ? tendency.tendency : 0)}
    </div>
  );
}

// draw Trend Data Point Inspector again
function drawTrendDataPoints(tendency: IScalarTendency, style: React.CSSProperties): React.ReactElement {
  return (
    <div style={style}>
      {' '}
      - DataPoints: {tendency !== undefined ? JSON.stringify(tendency.dataPoints.map((value) => value.toFixed(3))) : 'No Trend calculation'}
    </div>
  );
}
// draw Trend Equation Inspector again
function drawTrendEquation(tendency: IScalarTendency, style: React.CSSProperties): React.ReactElement {
  return (
    <div style={style}>
      {' '}
      - Equation:{' '}
      {tendency !== undefined
        ? `y = ${tendency.equationParams[0].toFixed(3)} * x + ${tendency.equationParams[1].toFixed(3)}`
        : 'No Trend calculation'}
    </div>
  );
}
// TODO draw Trend Inspector again
function drawTrendInspector(scalarValue: number, tendency: IScalarTendency): React.ReactElement {
  const style = { color: '#F5B041', margin: '10px' };
  return (
    <div>
      {drawScalarValue(scalarValue, style)}
      {drawTrendValue(tendency, style)}
      {drawTrendEquation(tendency, style)}
      {drawTrendDataPoints(tendency, style)}
    </div>
  );
}

function drawInfo(props: IBeliefBarChartProps): React.ReactElement {
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  // is null only in model builder where tendency and scalBelief are defined as null
  if (props.tendency != null) {
    return (
      <Grid item xs={6} md={4}>
        <Item>
          <PopupState variant="popover">
            {(popupState: any) => (
              <Box>
                <Typography style={{ color: '#F5B041' }} variant="subtitle2" {...bindHover(popupState)}>
                  Info
                </Typography>

                <Popper {...bindPopper(popupState)} transition>
                  {({ TransitionProps }) => (
                    <Fade {...TransitionProps}>
                      <Paper>
                        <Box style={{ marginLeft: '40px' }}>{drawTrendInspector(props.scalarBelief.value, props.tendency)}</Box>
                      </Paper>
                    </Fade>
                  )}
                </Popper>
              </Box>
            )}
          </PopupState>
        </Item>
      </Grid>
    );
  }
  return <Box />;
}

export function BeliefBarChart(props: IBeliefBarChartProps): React.ReactElement {
  const barColors: string[] = [];
  for (let i = 0; i < props.states.length; i++) {
    barColors.push(PROBABILITY_COLORS[i]);
  }

  const margin: { left: number; top: number; right: number; bottom: number } = { left: 50, top: 20, right: 20, bottom: 70 };

  const diagramHeight: number = props.height - 100;
  const diagramWidth: number = props.width - 15;

  const xMax: number = diagramWidth - margin.left - margin.right;
  const yMax: number = diagramHeight - margin.top - margin.bottom;

  // scaleLinear from visx does not provide a type
  const xScale = scaleLinear({
    range: [0, xMax],
    domain: [0, 1],
  });

  const simpleDomain: number[] = [];
  for (let i = 0; i < props.states.length; i++) {
    simpleDomain.push(i);
  }
  // scaleBand from visx does not provide a type
  const yScale = scaleBand({
    range: [0, yMax],
    domain: simpleDomain,
    padding: 0.2,
  });

  const formatTicks: TickFormatter<number> = (value: number) => props.states[value];

  return (
    <Box style={{ padding: '10px 10px 10px 10px' }}>
      <Grid container spacing={2}>
        <Grid item xs={6} md={8}>
          <Typography variant="subtitle1">{props.title}</Typography>
        </Grid>

        {drawInfo(props)}
      </Grid>

      <svg width={diagramWidth} height={diagramHeight}>
        <Group top={40}>{createBars(props.probabilityBelief, barColors, xScale, yScale)}</Group>
        <Group left={margin.left}>
          <AxisLeft
            top={margin.top}
            left={10}
            scale={yScale}
            numTicks={props.states.length}
            stroke="#3F51B5"
            tickStroke="#3F51B5"
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'end',
              fontSize: 12,
              fontFamily: 'Roboto',
              dx: '-0.25em',
              dy: '0.25em',
            })}
            tickFormat={formatTicks}
          />
          <AxisBottom
            top={diagramHeight - margin.bottom}
            left={10}
            scale={xScale}
            numTicks={5}
            label="Probability"
            stroke="#3F51B5"
            labelProps={{
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Roboto',
            }}
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Roboto',
              dx: '-0.0em',
              dy: '0.25em',
            })}
          />
        </Group>
      </svg>
    </Box>
  );
}
