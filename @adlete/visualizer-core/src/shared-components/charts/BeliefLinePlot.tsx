import { Card, Stack, Typography } from '@mui/material';
import { AxisBottom, AxisLeft } from '@visx/axis';
import { Group } from '@visx/group';
import { LegendItem, LegendLabel, LegendOrdinal } from '@visx/legend';
import { scaleLinear, scaleOrdinal } from '@visx/scale';
import { LinePath } from '@visx/shape';
import * as React from 'react';

import { IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';

import { PROBABILITY_COLORS, SCALAR_COLOR } from './Colors';
import { IMargin } from './ScalarBeliefLinePlot';

interface IBeliefLinePlotProps {
  height: number;
  width: number;
  title: string;
  states: string[];
  domain: number[];
  probabilityBeliefs: IProbabilityBelief[];
  scalarBeliefs: IScalarBelief[];
}

function genLinePath(
  linesData: number[][],
  probsVectorIndex: number,
  color: string,
  xScale: any, // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
  yScale: any // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
): React.ReactElement {
  return (
    <Group key={color}>
      <LinePath data={linesData} x={(_d, i) => xScale(i)} y={(d: number[]) => yScale(d[probsVectorIndex])} stroke={color} strokeWidth={2} />
    </Group>
  );
}

function drawLines(
  linesData: number[][],
  lineColors: string[],
  xScale: any, // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
  yScale: any // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
): React.ReactElement[] {
  const result = [];
  for (let i = 0; i < lineColors.length; i++) {
    result.push(genLinePath(linesData, i, lineColors[i], xScale, yScale));
  }
  return result;
}

export function BeliefLinePlot(props: IBeliefLinePlotProps): React.ReactElement {
  const linesData: number[][] = [];
  const diagramHeight: number = props.height - 60;
  const diagramWidth: number = props.width - 15;

  props.probabilityBeliefs.forEach((belief: IProbabilityBelief | IScalarBelief, index: number) => {
    if (Array.isArray(belief)) {
      // clone belief array to avoid change of input data
      linesData.push(JSON.parse(JSON.stringify(belief)));
    } else if(belief != null) {
      // clone belief value to avoid change of input data     
      linesData.push([(belief as IScalarBelief).value]);
    } else {
      linesData.push(undefined);
    }

    if(linesData !== undefined && linesData[index] !== undefined) {
      linesData[index].push(props.scalarBeliefs[index].value);
    }
  });

  const lineColors: string[] = [];
  for (let i = 0; i < props.states.length; i++) {
    lineColors.push(PROBABILITY_COLORS[i]);
  }
  lineColors.push(SCALAR_COLOR);

  const margin: IMargin = { left: 55, top: 10, right: 0, bottom: 36 };

  const xMax: number = diagramWidth - margin.left - margin.right;
  const yMax: number = diagramHeight - margin.top - margin.bottom;

  // scaleOrdinal from vx does not provide a type
  const ordinalColorScale = scaleOrdinal({
    domain: [...props.states, 'Scalar Value'],
    range: lineColors,
  });

  // scaleLinear from vx does not provide a type
  const xScale = scaleLinear({
    range: [0, xMax],
    domain: [props.domain[0], props.domain[1]],
    round: true,
    clamp: true,
  });
  // scaleLinear from vx does not provide a type
  const yScale = scaleLinear({
    range: [yMax, 0],
    domain: [0, 1],
    nice: true,
  });

  return (
    <Card style={{ padding: '5px 5px 5px 5px' }}>
      <Typography variant="subtitle2"> {props.title}</Typography>
      <svg width={diagramWidth} height={diagramHeight}>
        <Group left={margin.left} top={margin.top}>
          <AxisLeft
            scale={yScale}
            numTicks={5}
            label="Probability/Normalized Scalar"
            labelProps={{
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 10,
              fontFamily: 'Roboto',
            }}
            stroke="#3F51B5"
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'end',
              fontSize: 10,
              fontFamily: 'Roboto',
              dx: '-0.25em',
              dy: '0.25em',
            })}
          />
          {drawLines(linesData, lineColors, xScale, yScale)}
          <AxisBottom
            top={yMax}
            scale={xScale}
            numTicks={5}
            label="Evidence Steps over Time"
            stroke="#3F51B5"
            labelProps={{
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 10,
              fontFamily: 'Roboto',
            }}
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 10,
              fontFamily: 'Roboto',
              dx: '-0.25em',
              dy: '0.25em',
            })}
          />
        </Group>
      </svg>
      <LegendOrdinal scale={ordinalColorScale} labelFormat={(label) => `${label}`}>
        {(labels) => (
          <Stack direction="row" justifyContent="space-evenly" alignItems="center">
            {labels.map((label, i) => {
              const size = 6;
              return (
                <LegendItem
                  style={{
                    marginRight: '5px',
                    color: '#3F51B5',
                    fontFamily: 'Roboto',
                    fontSize: '10px',
                  }}
                  key={`legend-quantile-${i * size}`}
                >
                  <LegendLabel />
                  <svg width={size + 2} height={size}>
                    <rect fill={label.value} width={size - 1} height={size - 1} />
                  </svg>
                  {`${label.text} `}
                </LegendItem>
              );
            })}
          </Stack>
        )}
      </LegendOrdinal>
    </Card>
  );
}
