import * as R from 'ramda';
import * as React from 'react';

import { IBeliefBundle, IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendency, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';

import { ISimulationResults } from '../../IVisualizerExtension';

import { BeliefBarChart } from './BeliefBarChart';
import { BeliefLinePlot } from './BeliefLinePlot';
import { ScalarBeliefLinePlot } from './ScalarBeliefLinePlot';

export interface IPlotStyle {
  width: number;
  height: number;
}

// TODO: add margin?
// eslint-disable-next-line no-shadow
export enum PlotType {
  BELIEF_LINE_PLOTS = 'beliefLinePlots',
  BELIEF_BAR_CHARTS = 'beliefBarCharts',
  AGENT_LINE_PLOTS = 'agentLinePlots',
}

export const DEFAULT_STYLE: Record<PlotType, IPlotStyle> = {
  beliefLinePlots: { width: 350, height: 250 },
  beliefBarCharts: { width: 350, height: 250 },
  agentLinePlots: { width: 350, height: 250 },
};

export interface IPlotsProps {
  simResults: ISimulationResults;
  modelName: string;
  states: string[];
  step: number;
}

export interface IExtraPlotData {
  pos: number;
  domain: number[];
  states: string[];
}

export function createBeliefLinePlot(
  probabilityBeliefs: IProbabilityBelief[],
  scalarBeliefs: IScalarBelief[],
  beliefName: string,
  states: string[],
  domain: number[],
  pWidth: number,
  pHeight: number
): React.ReactElement {
  return (
    <BeliefLinePlot
      key={beliefName}
      height={pHeight}
      width={pWidth}
      probabilityBeliefs={probabilityBeliefs}
      scalarBeliefs={scalarBeliefs}
      states={states}
      domain={domain}
      title={beliefName}
    />
  );
}

export function createBeliefBarPlot(
  probBelief: IProbabilityBelief,
  scalBelief: IScalarBelief,
  tendency: IScalarTendency,
  states: string[],
  beliefName: string,
  pWidth: number,
  pHeight: number
): React.ReactElement {
  return (
    <BeliefBarChart
      key={beliefName}
      probabilityBelief={probBelief}
      scalarBelief={scalBelief}
      states={states}
      width={pWidth}
      height={pHeight}
      title={beliefName}
      tendency={tendency}
    />
  );
}

export function createScalarBeliefLinePlot(
  scalarBeliefs: IScalarBelief[],
  beliefName: string,
  domain: number[],
  pWidth: number,
  pHeight: number
): React.ReactElement {
  return (
    <ScalarBeliefLinePlot
      key={beliefName}
      height={pHeight}
      width={pWidth}
      domain={domain}
      scalarBeliefs={scalarBeliefs}
      title={`Belief ${beliefName}`}
    />
  );
}

export function beliefsToLinePlots(
  probBeliefs: Record<string, IProbabilityBelief[]>,
  scalarBeliefs: Record<string, IScalarBelief[]>,
  states: string[],
  domain: number[],
  pWidth: number,
  pHeight: number
): Record<string, React.ReactElement> {
  return R.mapObjIndexed(
    (_, beliefName) =>
      createBeliefLinePlot(probBeliefs[beliefName], scalarBeliefs[beliefName], beliefName, states, domain, pWidth, pHeight),
    probBeliefs
  );
}

export function beliefsToBarPlots(
  sortedBeliefs: string[],
  probBeliefs: IBeliefBundle<IProbabilityBelief>,

  scalarBeliefs: IBeliefBundle<IScalarBelief>,
  tendencies: ITendencyBundle<IScalarTendency>,

  states: string[],
  pWidth: number,
  pHeight: number
): Record<string,React.ReactElement> {
   const barPlots = R.map(
    (beliefName) =>
      createBeliefBarPlot(probBeliefs[beliefName],
        scalarBeliefs[beliefName],
        tendencies[beliefName],
        states,
        beliefName, 
        pWidth,
        pHeight
    ),
    sortedBeliefs
  );
  return R.zipObj(sortedBeliefs, barPlots)
}

export function agentModelsToLinePlots(
  agentCaseModels: Record<string, IScalarBelief[]>,
  domain: number[],
  pWidth: number,
  pHeight: number
): Record<string, React.ReactElement> {
  return R.mapObjIndexed(
    (_, beliefName) => createScalarBeliefLinePlot(agentCaseModels[beliefName], beliefName, domain, pWidth, pHeight),
    agentCaseModels
  );
}

export function wrapAsForeignObject(node: React.ReactNode, width: number, height: number, className: string): React.ReactElement {
  return (
    <foreignObject width={width} height={height} x={-width / 2} y={-height / 2} className={className}>
      {node}
    </foreignObject>
  );
}

export function wrapAllAsForeignObject(
  nodes: Record<string, React.ReactElement>,
  width: number,
  height: number,
  className: string
): Record<string, React.ReactElement> {
  return R.map((node) => wrapAsForeignObject(node, width, height, className), nodes);
}
