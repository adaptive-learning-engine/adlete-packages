import { Box, Typography } from '@mui/material';
import { AxisBottom, AxisLeft } from '@visx/axis';
import { Group } from '@visx/group';
import { LegendItem, LegendLabel, LegendOrdinal } from '@visx/legend';
import { scaleLinear, scaleOrdinal } from '@visx/scale';
import { LinePath } from '@visx/shape';
import * as React from 'react';

import { IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';

import { SCALAR_COLOR } from './Colors';

export interface IMargin {
  left: number;
  top: number;
  right: number;
  bottom: number;
}

interface IScalarBeliefLinePlotProps {
  height: number;
  width: number;
  title: string;
  domain: number[];
  scalarBeliefs: IScalarBelief[];
}

class ScalarBeliefLinePlotUtility {
  public static genLinePath(
    lineData: IScalarBelief[],
    color: string,
    margin: IMargin,
    counter: number,
    xScale: any, // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
    yScale: any // Type ScaleLinear<number, number> not exposed by visx the usage of ScaleTypeToD3Scale is to complicated
  ) {
    return (
      <Group left={margin.left + 10} key={color} top={margin.top}>
        <LinePath data={lineData} x={() => xScale(counter++)} y={(p) => yScale(p.value)} stroke={color} strokeWidth={2} />
      </Group>
    );
  }
}

export function ScalarBeliefLinePlot(props: IScalarBeliefLinePlotProps): React.ReactElement {
  const diagramHeight: number = props.height - 60;
  const diagramWidth: number = props.width - 15;

  // inner positioning of axis
  const margin: IMargin = { left: 50, top: 20, right: 20, bottom: 45 };
  const xMax: number = diagramWidth - margin.left - margin.right;
  const yMax: number = diagramHeight - margin.top - margin.bottom;

  // scaleLinear from vx does not provide a type
  const xScale = scaleLinear({
    range: [0, xMax],
    domain: [props.domain[0], props.domain[1]],
  });

  // scaleLinear from vx does not provide a type
  const yScale = scaleLinear({
    range: [yMax, 0],
    domain: [0, 1],
    nice: true,
  });

  // scaleOrdinal from vx does not provide a type
  const ordinalColorScale = scaleOrdinal({
    domain: ['Scalar Value'],
    range: [SCALAR_COLOR],
  });

  return (
    <Box /*
      style={{
        marginTop: '5px',
        marginLeft: '5px',
        marginRight: '5px',
        backgroundColor: 'white',
        padding: '5px 5px 5px 5px',
      }}
      */
    >
      <Typography variant="subtitle2">{props.title}</Typography>
      <svg width={diagramWidth} height={diagramHeight}>
        {ScalarBeliefLinePlotUtility.genLinePath(props.scalarBeliefs, SCALAR_COLOR, margin, props.domain[0], xScale, yScale)}
        <Group left={margin.left}>
          <AxisLeft
            top={margin.top}
            left={10}
            scale={yScale}
            numTicks={5}
            label="Normalized Scalar"
            labelProps={{
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Roboto',
            }}
            stroke="#3F51B5"
            tickStroke="#3F51B5"
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'end',
              fontSize: 12,
              fontFamily: 'Roboto',
              dx: '-0.25em',
              dy: '0.25em',
            })}
          />
          <AxisBottom
            top={diagramHeight - margin.bottom}
            left={10}
            scale={xScale}
            numTicks={5}
            label="Evidence Steps over Time"
            stroke="#3F51B5"
            labelProps={{
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Roboto',
            }}
            tickLabelProps={() => ({
              fill: '#3F51B5',
              textAnchor: 'middle',
              fontSize: 12,
              fontFamily: 'Roboto',
              dx: '-0.25em',
              dy: '0.25em',
            })}
          />
        </Group>
      </svg>
      <Box>
        <LegendOrdinal scale={ordinalColorScale} labelFormat={(label) => `${label}`}>
          {(labels) => (
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              {labels.map((label, i) => {
                const size = 10;
                return (
                  <LegendItem
                    style={{
                      color: '#3F51B5',
                      fontFamily: 'Roboto',
                    }}
                    key={`legend-quantile-${i * size}`}
                  >
                    <svg width={size} height={size}>
                      <rect fill={label.value} width={size} height={size} />
                    </svg>
                    <LegendLabel className="legend-title">
                      <Typography variant="caption">{label.text} </Typography>
                    </LegendLabel>
                  </LegendItem>
                );
              })}
            </div>
          )}
        </LegendOrdinal>
      </Box>
    </Box>
  );
}
