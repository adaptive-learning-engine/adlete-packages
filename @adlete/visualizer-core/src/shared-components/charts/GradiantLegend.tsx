import * as d3 from "d3";
import React, { FunctionComponent, useEffect, useRef } from "react";

interface GradiantLegendProps {
    width: number,
    height: number,
    colorFunction: (t: number)=> string
    domain?: [number, number]
}
 
export const GradiantLegend: FunctionComponent<GradiantLegendProps> = ({width, height, colorFunction, domain=[0,1]}: GradiantLegendProps) => {

    const yScale = d3
    .scaleLinear()
    .domain(domain)
    .range([height - 5, 5])

    const colorScale = d3
    .scaleSequential()
    .domain(domain)
    .interpolator(colorFunction)

    const yAxisRef = useRef<SVGSVGElement>(null)
    useEffect(() => {
        if(yAxisRef) {
            const yAxis = d3.axisRight(yScale).ticks(5).tickSize(-width)
            d3.select(yAxisRef.current).call(yAxis)
        }
    }, [width, yScale])

    const range = d3.range(domain[0],domain[1], (domain[1] - domain[0])/ height)
    const legend = range.map((value) => (
        <rect
            key={`legend${value}`}
            x={0}
            y={yScale(value)}
            width={width}
            height={1}
            style={{fill: colorScale(value)}}
        />
    ))

    return ( 
    <svg width={width + 30} height={height}>
        <g>
            {legend}
        </g>
        <g ref={yAxisRef} transform={`translate(${width}, 0)`}/>
    </svg>);
}