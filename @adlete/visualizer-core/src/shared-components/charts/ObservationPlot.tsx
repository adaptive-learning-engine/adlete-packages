import * as React from 'react';
// import MaterialTable from 'material-table';

import { IObservation } from '@adlete/framework/IObservation';

import { ISimulatedActivityResult, IUserActivityResult } from '../../IVisualizerExtension';

export interface IObservationPlotProps {
  activities: ISimulatedActivityResult[] | IUserActivityResult[];
  domain: number[];
}

export class ObservationPlot extends React.Component<IObservationPlotProps> {
  private genColumns(keys: string[]): { title: string; field: string }[] {
    return [{ title: 'step', field: 'step' }, ...keys.map((key) => ({ title: key, field: key }))];
  }

  private genData(): IObservation[] {
    return (this.props.activities as ISimulatedActivityResult[]).map((activity: ISimulatedActivityResult, index: number) => {
      const result: any = { step: index + this.props.domain[0] };
      Object.keys(activity.observations[0]).forEach((key) => {
        result[key] = (activity.observations[0] as any)[key].toString();
        if (result[key].length > 25) {
          result[key] = `${result[key].slice(0, 25)}`;
        }
      });

      return result;
    });
  }

  private genSummery(data: IObservation[]): { activityName: string; count: number }[] {
    const dict: { [key: string]: number } = {};

    data.forEach((value) => {
      if (dict[value.activityName] === undefined) {
        dict[value.activityName] = 0;
      }

      dict[value.activityName]++;
    });

    return Object.keys(dict).map((key) => ({ activityName: key, count: dict[key] }));
  }

  public render(): React.ReactNode {
    /* const keys = Object.keys(this.props.activities[0].observations[0]).map((key) => key);
    const data = this.genData(); */
    return <div style={{ overflow: 'visible', marginTop: 5 }}>To Do add activities list</div>;
  }
}
