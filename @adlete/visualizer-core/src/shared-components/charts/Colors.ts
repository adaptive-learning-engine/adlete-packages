export const SCALAR_COLOR = '#EF6C00';
export const PROBABILITY_COLORS = [
  '#673ab7',
  '#3f51b5',
  '#2196f3' /* Purple */,
  '#607d8b',
  '#00bcd4',
  '#009688' /* Blue */,
  '#4caf50',
  '#8bc34a',
  '#cddc39' /* Green */,
  '#ffeb3b',
  '#ffc107',
  '#ff9800' /* Yellow */,
  '#ff5722',
  '#795548',
  '#9e9e9e',
];
