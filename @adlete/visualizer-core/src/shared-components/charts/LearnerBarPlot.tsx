import { BarChart } from "@mui/x-charts/BarChart";
import { BarSeriesType } from "@mui/x-charts/models";
import React, { useCallback } from "react";

interface ILearnerBarPlot {
    yLabel: string[]
    data: {data: number[], label: string}[]
    width: number
    height: number
}

export const LearnerBarPlot = ({yLabel, data, width, height}: ILearnerBarPlot) => {
    
    const valueFormatter = (value: number | null) => `${value}%`

    const createData = useCallback((): BarSeriesType[] => {
        const statMap: BarSeriesType[] = data.map<BarSeriesType>((value) => ({
            ...value, type: "bar", valueFormatter
        }))
        return statMap
    }, [data])


    return ( <BarChart 
        layout="horizontal" 
        yAxis={[{scaleType: "band", data: yLabel}]} 
        series={createData()} 
        width={width} 
        height={height}
        xAxis={[{label: "belief value (%)", min: 0, max: 100}]}></BarChart> )
}

