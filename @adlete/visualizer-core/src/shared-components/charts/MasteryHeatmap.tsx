import {Tooltip} from "@mui/material"
import * as d3 from "d3"; 
import React from "react";

import {IHeatmapData, ILearnerMasteryGridData} from "@adlete/framework/IAnalysisData";

import { GradiantLegend } from "./GradiantLegend";

  type IMasteryHeatmapProps = {
    width: number;
    height: number;
    data: IHeatmapData[];
    setHoveredCell: (hoveredCell: ILearnerMasteryGridData) => void;
  };

  const MARGIN = { top: 10, right: 10, bottom: 50, left: 70 };
  
  export const MasteryHeatmap: React.FunctionComponent<IMasteryHeatmapProps> = ({ width, height, data, setHoveredCell }: IMasteryHeatmapProps) => {
    const boundsWidth = width -  MARGIN.right - MARGIN.left;
    const boundsHeight = height - MARGIN.top - MARGIN.bottom;

    const allYGroups = React.useMemo(() => [...new Set(data.map((d) => d.y),)], [data])
    const allXGroups = React.useMemo(() => [...new Set(data.map((d) => d.x),)], [data])

    const xScale = React.useMemo(() => {
        return d3
        .scaleBand()
        .range([0, boundsWidth])
        .domain(allXGroups)
        .padding(0.01);
    }, [allXGroups, boundsWidth])

    const yScale = React.useMemo(() => {
        return d3
        .scaleBand()
        .range([boundsHeight, 0])
        .domain(allYGroups)
        .padding(0.01);
    }, [allYGroups, boundsHeight])

    //const [min, max] = d3.extent(data.map((d) => d.value));

    const colorScale = d3
        .scaleSequential()
        .interpolator(d3.interpolateRdYlGn)


    const allRects = data.map((d,i) => {
        if(d.value === null) {
            return;
        }
        const cell = (
            <rect
                r={4}
                x={xScale(d.x)}
                y={yScale(d.y)}
                width={xScale.bandwidth()}
                height={yScale.bandwidth()}
                opacity={1}
                fill={colorScale(d.value)}
                rx={5}
                stroke={"white"}
                onMouseDown={() => {
                  setHoveredCell({
                    xLabel: d.x,
                    yLabel: d.y,
                    value: d.value
                  })
                }}
                />
        )
        if(d.tooltip) {
          return (
            <Tooltip key={i} title={d.tooltip}>
              {cell}
            </Tooltip>
          )
        } else {
          return cell
        }
    })

    const xLabels = allXGroups.map((name, i) => {
        const xPos = xScale(name) ?? 0;
        return (
          <text
            key={i}
            x={xPos + xScale.bandwidth() / 2}
            y={boundsHeight + 10}
            textAnchor="middle"
            dominantBaseline="middle"
            fontSize={10}
          >
            {name}
          </text>
        );
      });

      const yLabels = allYGroups.map((name, i) => {
        const yPos = yScale(name) ?? 0;
        return (
          <text
            key={i}
            x={-5}
            y={yPos + yScale.bandwidth() / 2}
            textAnchor="end"
            dominantBaseline="middle"
            fontSize={10}
          >
            {name}
          </text>
        );
      });
    // read the data
    // do some stuff with d3 like building scales
    // compute all the <rect>
  
    return (
      <div>
      <GradiantLegend colorFunction={d3.interpolateRdYlGn} height={height} width={25} domain={[0, 100]}/>
      <svg width={width} height={height}>
        <g
          width={boundsWidth}
          height={boundsHeight}
          transform={`translate(${[MARGIN.left, MARGIN.top].join(",")})`}
        >
          {allRects}
          {xLabels}
          {yLabels}
        </g>
      </svg>
      </div>
    );
  };