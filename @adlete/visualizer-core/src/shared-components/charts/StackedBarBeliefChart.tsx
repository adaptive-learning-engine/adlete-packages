import { Group } from '@visx/group';
// import cityTemperature, { CityTemperature } from '@visx/mock-data/lib/mocks/cityTemperature';
import { scaleBand, scaleLinear, scaleOrdinal } from '@visx/scale';
import { BarStackHorizontal } from '@visx/shape';
import { BarStack } from '@visx/shape/lib/types';
// import { withTooltip } from '@visx/tooltip';
// import { WithTooltipProvidedProps } from '@visx/tooltip/lib/enhancers/withTooltip';
import React from 'react';

import { IProbabilityBelief } from '@adlete/engine-blocks/belief/IBelief';

import { IBeliefBarChartProps } from './BeliefBarChart';
import { PROBABILITY_COLORS } from './Colors';

const background = '#eaedff';

/* type CityName = 'New York' | 'San Francisco' | 'Austin';

type TooltipData = {
  bar: SeriesPoint<CityTemperature>;
  key: CityName;
  index: number;
  height: number;
  width: number;
  x: number;
  y: number;
  color: string;
};

export type BarStackHorizontalProps = {
  width: number;
  height: number;
  margin?: { top: number; right: number; bottom: number; left: number };
  events?: boolean;
};

const purple1 = '#6c5efb';
const purple2 = '#c998ff';
export const purple3 = '#a44afe';
export const background = '#eaedff';
const defaultMargin = { top: 40, left: 50, right: 40, bottom: 100 };

const data = cityTemperature.slice(0, 12);
const keys = Object.keys(data[0]).filter((d) => d !== 'date') as CityName[];

const temperatureTotals = data.reduce((allTotals, currentDate) => {
  const totalTemperature = keys.reduce((dailyTotal, k) => {
    dailyTotal += Number(currentDate[k]);
    return dailyTotal;
  }, 0);
  allTotals.push(totalTemperature);
  return allTotals;
}, [] as number[]);

// accessors
const getDate = (d: CityTemperature) => d.date;

// scales
const temperatureScale = scaleLinear<number>({
  domain: [0, Math.max(...temperatureTotals)],
  nice: true,
});
const dateScale = scaleBand<string>({
  domain: data.map(getDate),
  padding: 0.2,
}); */

export interface IStackedBarBeliefChartProps extends IBeliefBarChartProps {
  foo?: boolean;
}

export interface IProbabilityBarDatum {
  probabilityBelief: IProbabilityBelief;
  title: string;
}

const getName = (datum: IProbabilityBarDatum) => datum.title;

function createHorizontalBar(barStack: BarStack<IProbabilityBarDatum, string>): React.ReactElement {
  return (
    <rect
      key={`barstack-horizontal-${barStack.index}-${barStack.bars[0].index}`}
      x={barStack.bars[0].x}
      y={barStack.bars[0].y}
      width={barStack.bars[0].width}
      height={barStack.bars[0].height}
      fill={barStack.bars[0].color}
      onClick={() => {
        // if (events) alert(`clicked: ${JSON.stringify(bar)}`);
      }}
      // onMouseLeave={() => {
      //   tooltipTimeout = window.setTimeout(() => {
      //     hideTooltip();
      //   }, 300);
      // }}
      // onMouseMove={() => {
      //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
      //   const top = barStack.bars[0].y + margin.top;
      //   const left = barStack.bars[0].x + barStack.bars[0].width + margin.left;
      //   showTooltip({
      //     tooltipData: barStack.bars[0],
      //     tooltipTop: top,
      //     tooltipLeft: left,
      //   });
      // }}
    />
  );
}

//function x0(point: SeriesPoint<IProbabilityBarDatum>): ScaleInput<PositionScale> {}

export function StackedBarBeliefChart(props: IStackedBarBeliefChartProps): React.ReactElement {
  const { height, width } = props;
  const margin: { left: number; top: number; right: number; bottom: number } = { left: 50, top: 20, right: 20, bottom: 45 };
  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  const data = [
    {
      probabilityBelief: props.probabilityBelief,
      title: props.title,
    },
  ];

  const colorScale = scaleOrdinal<string, string>({
    domain: props.states,
    range: PROBABILITY_COLORS,
  });

  const probabilityScale = scaleLinear<number>({
    domain: [0, 1],
    nice: true,
  });

  const nameScale = scaleBand<string>({
    domain: data.map(getName),
    padding: 0.2,
  });

  return width < 10 ? null : (
    <div>
      <svg width={width} height={height}>
        <rect width={width} height={height} fill={background} rx={14} />
        <Group top={margin.top} left={margin.left}>
          <BarStackHorizontal<IProbabilityBarDatum, string>
            data={data}
            keys={props.states}
            height={yMax}
            y={getName}
            xScale={probabilityScale}
            yScale={nameScale}
            color={colorScale}
            //x1={}
          >
            {(barStacks) => barStacks.map(createHorizontalBar)}
          </BarStackHorizontal>
        </Group>
      </svg>
    </div>
  );
}

/*
export default withTooltip<BarStackHorizontalProps, TooltipData>(
  ({ width, height, events = false, margin = defaultMargin }: BarStackHorizontalProps & WithTooltipProvidedProps<TooltipData>) => {
    // bounds
    const xMax = width - margin.left - margin.right;
    const yMax = height - margin.top - margin.bottom;

    temperatureScale.rangeRound([0, xMax]);
    dateScale.rangeRound([yMax, 0]);

    return width < 10 ? null : (
      <div>
        <svg width={width} height={height}>
          <rect width={width} height={height} fill={background} rx={14} />
          <Group top={margin.top} left={margin.left}>
            <BarStackHorizontal<CityTemperature, CityName>
              data={data}
              keys={keys}
              height={yMax}
              y={getDate}
              xScale={temperatureScale}
              yScale={dateScale}
              color={colorScale}
            >
              {(barStacks) =>
                barStacks.map((barStack) => (
                  <rect
                    key={`barstack-horizontal-${barStack.index}-${barStack.bars[0].index}`}
                    x={barStack.bars[0].x}
                    y={barStack.bars[0].y}
                    width={barStack.bars[0].width}
                    height={barStack.bars[0].height}
                    fill={barStack.bars[0].color}
                    onClick={() => {
                      if (events) alert(`clicked: ${JSON.stringify(bar)}`);
                    }}
                    // onMouseLeave={() => {
                    //   tooltipTimeout = window.setTimeout(() => {
                    //     hideTooltip();
                    //   }, 300);
                    // }}
                    // onMouseMove={() => {
                    //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
                    //   const top = barStack.bars[0].y + margin.top;
                    //   const left = barStack.bars[0].x + barStack.bars[0].width + margin.left;
                    //   showTooltip({
                    //     tooltipData: barStack.bars[0],
                    //     tooltipTop: top,
                    //     tooltipLeft: left,
                    //   });
                    // }}
                  />
                ))
              }
            </BarStackHorizontal>
          </Group>
        </svg>
      </div>
    );
  }
);
*/
