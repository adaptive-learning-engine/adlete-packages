import { Autocomplete, Box, Button, Chip, Stack, TextField, Typography } from '@mui/material';
import cloneDeep from 'lodash.clonedeep';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IBayesNode } from '@adlete/engine-blocks/belief/IBayesNode';

import * as CompetenceGraphUtility from './CompetenceGraphUtility';
import { CptParentsBuilderComponent } from './CptParentsBuilderComponent';

interface INodeInspectorContainerProps {
  model: IBayesNet;
  selected: string;
  setNewParents: (model: IBayesNet, id: string, parents: string[], competenceToSelect: string) => void;
  onModelUpdated: (model: IBayesNet, competenceToSelect: string) => void;
}
interface INodeInspectorContainerStates {
  newName: string;
  isNameAlreadyTaken: boolean;
  isNameError: boolean;
}

export class CompetenceInspectorContainer extends React.Component<INodeInspectorContainerProps, INodeInspectorContainerStates> {
  constructor(props: INodeInspectorContainerProps) {
    super(props);
    this.state = {
      newName: '',
      isNameAlreadyTaken: true,
      isNameError: false,
    };

    // this.onSubmit = this.onSubmit.bind(this);
    this.updateCompetenceNameInModel = this.updateCompetenceNameInModel.bind(this);
  }

  private onCompetenceNameChanged(newName: string) {
    const error = this.props.model[newName] !== undefined && newName !== this.props.selected && newName !== '';
    this.setState({
      newName,
      isNameError: error,
      isNameAlreadyTaken: error,
    });
  }

  /**
   * is supposed to replace all occurrences of the old node id with the new one
   * @param newId
   */
  private updateCompetenceNameInModel(newName: string) {
    const newGraph = cloneDeep(this.props.model) as IBayesNet;
    const newNode: IBayesNode = cloneDeep(this.props.model[this.props.selected]);
    newNode.id = newName;

    Object.keys(newGraph).forEach((nodeName) => {
      const index = newGraph[nodeName].parents.indexOf(this.props.selected);

      if (index === -1) {
        return;
      }

      let newParents: string[] = [];
      const newCpt = cloneDeep(newGraph[nodeName].cpt);

      newParents = cloneDeep(newGraph[nodeName].parents);
      newParents[index] = newNode.id;

      newGraph[nodeName].cpt = newCpt;
      newGraph[nodeName].parents = newParents;
    });

    newGraph[newNode.id] = newNode;
    delete newGraph[this.props.selected];

    this.setState({ newName: '', isNameAlreadyTaken: true, isNameError: false });
    this.props.onModelUpdated(newGraph, newNode.id);
  }

  /**
   * passes the updates to @CompetenceGraphsContainer to update the graph
   * @param cpt
   */
  public render(): React.ReactNode {
    // filter out the already connected child Nodes
    const allNodes = Object.keys(this.props.model);
    const filteredAlreadyConnected = this.props.model[this.props.selected].parents;
    const filteredDirectedAcyclicGraph = CompetenceGraphUtility.findForbiddenOptions(this.props.selected, this.props.model);
    const insecureConnections = filteredAlreadyConnected.concat(filteredDirectedAcyclicGraph);
    const secureConnections = allNodes
      .filter((value) => !insecureConnections.includes(value))
      .concat(this.props.model[this.props.selected].parents);

    return (
      <Box>
        <Stack direction="row" spacing={0.5}>
          <TextField
            sx={{ minWidth: 500 }}
            key={this.props.selected}
            label="Competence Name"
            defaultValue={this.props.selected}
            onChange={(prop) => this.onCompetenceNameChanged(prop.target.value)}
            error={this.state.isNameError}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={() => this.updateCompetenceNameInModel(this.state.newName)}
            disabled={this.state.isNameAlreadyTaken}
          >
            Update Competence Name
          </Button>
        </Stack>

        <Autocomplete
          multiple
          key={this.props.selected}
          size="medium"
          value={this.props.model[this.props.selected].parents}
          options={secureConnections}
          onChange={(_event: React.SyntheticEvent, value: string[]) =>
            this.props.setNewParents(cloneDeep(this.props.model), this.props.selected, value, this.props.selected)
          }
          renderTags={(tagValue, getTagProps) =>
            // eslint-disable-next-line react/jsx-props-no-spreading
            tagValue.map((option, index) => <Chip key={index} label={option} {...getTagProps({ index })} color="primary" />)
          }
          // eslint-disable-next-line react/jsx-props-no-spreading
          renderInput={(params) => <TextField {...params} label="Add a Parent" margin="normal" variant="outlined" fullWidth />}
        />
        <CptParentsBuilderComponent
          parents={this.props.model[this.props.selected].parents.length > 0}
          node={cloneDeep(this.props.model[this.props.selected])}
          network={cloneDeep(this.props.model)}
          onModelUpdated={this.props.onModelUpdated}
        />
        <Box>
          <Typography color="error">{/* this.state.errorText */}</Typography>
        </Box>
      </Box>
    );
  }
}
