import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';
import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '../plugin-layout/LayoutManagerPlugin';

import { ModelBuilderModule } from './ModelBuilderModule';

export interface IWithModelBuilder extends IWithLayoutManager {
  modelBuilder: ModelBuilderPlugin;
}

export const ModelBuilderMeta: IPluginMeta<IWithModelBuilder, 'modelBuilder'> = {
  id: 'modelBuilder',
  dependencies: ['layoutManager'],
};

export class ModelBuilderPlugin implements IPlugin<IWithModelBuilder> {
  public meta: IPluginMeta<IWithModelBuilder, 'modelBuilder'> = ModelBuilderMeta;

  public initialize(pluginMan: IPluginManager<IWithModelBuilder>): Promise<void> {
    const layoutMan = pluginMan.get('layoutManager');
    layoutMan.panelTypes.add('ModelBuilder', {
      createPanel: () => <ModelBuilderModule />,
      title: 'Model Builder',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
