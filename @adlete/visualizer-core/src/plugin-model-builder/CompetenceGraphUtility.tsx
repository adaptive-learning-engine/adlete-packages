import { clone, evaluate } from 'mathjs';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IBayesNode } from '@adlete/engine-blocks/belief/IBayesNode';

/**
 * @returns returns a list of root nodes
 */
export function getRootNodesNames(graph: IBayesNet): string[] {
  if (graph === undefined || graph === null) {
    return null;
  }

  const allNodes = Object.keys(graph);
  let popNodes = [...allNodes];
  for (let i = 0; i < allNodes.length; i++) {
    popNodes = popNodes.filter((value) => {
      if (!graph[allNodes[i]].parents.includes(value)) {
        return true;
      }
      return false;
    });
  }
  return popNodes;
}

/**
 * generates state/parent pairs for later cpt usage --> e.g. competence_1: low, competence_2:low; ... for all possibilities
 * parents:[a,b] states:[state1,state2]
 * [[{a: state1},{a: state2}]
 * [{b: state1},{b: state2}]]
 * @param states
 * @param parents
 * @returns
 */
export function getStateParentPairs(states: string[], parents: string[]): Record<string, string>[][] {
  const result: Record<string, string>[][] = [];
  for (let i = 0; i < parents.length; i++) {
    result[i] = [];
    for (let j = 0; j < states.length; j++) {
      const emptyObj: Record<string, string> = {};
      emptyObj[parents[i]] = states[j];
      result[i][j] = emptyObj;
    }
  }
  return result;
}

export function cartesianProductIterative(stateParentsObj: Record<string, string>[][]): Record<string, string>[] {
  const results: Record<string, string>[] = [];
  const indexes = Array<number>(stateParentsObj.length).fill(0);
  let j = 1;
  while (j >= 0) {
    j = indexes.length - 1;
    let singleResult: Record<string, string>;
    stateParentsObj.forEach((state, i) => {
      singleResult = { ...state[indexes[i]], ...singleResult };
    });
    results.push(singleResult);
    while (j >= 0) {
      indexes[j] += 1;
      if (indexes[j] < stateParentsObj[j].length) break;
      indexes[j] = 0;
      j -= 1;
    }
  }
  return results;
}

/**
 * Creates Linear CPT Table based on State Pairs
 * @param nodeStates All Node States e.g. ["low", "medium", "high"]
 * @param combinations Parent State combinations e.g. {"competence_1":"low","competence_2":"low"}
 * @returns An array containing the cpt
 */
export function createFlatCPTWithParents(nodeStates: string[], combinations: Record<string, string>[]): number[] {
  return createCPTWithParents(nodeStates, combinations, (x: number) => x);
}

/**
 * Creates CPT with quadratic incline in the parent probabilities
 * @param nodeStates All Node States e.g. ["low", "medium", "high"]
 * @param combinations Parent State combinations e.g. {"competence_1":"low","competence_2":"low"}
 * @returns
 */
export function createQuadraticCPTWithParents(nodeStates: string[], combinations: Record<string, string>[]): number[] {
  return createCPTWithParents(nodeStates, combinations, (x: number) => x + Math.pow(x, 2));
}
/**
 * Creates CPT Table based on State Pairs
 * @param nodeStates All Node States e.g. ["low", "medium", "high"]
 * @param combinations Parent State combinations e.g. {"competence_1":"low","competence_2":"low"}
 * @param expression An expression that calculates the probability for each parent for the conditional probability of the child
 * @returns An array containing the cpt
 */
export function createCPTWithParents(
  nodeStates: string[],
  combinations: Record<string, string>[],
  expression: (x: number) => number
): number[] {
  const childSize = Object.keys(combinations[0]).length;
  const stateNum = nodeStates.length;
  const cpt = new Array<number>(nodeStates.length * combinations.length).fill(0);
  const normalProb = 1 / childSize; // bei 2 Parents --> 0,5
  for (let i = 0; i < combinations.length; i++) {
    const singleCombinations = Object.keys(combinations[i]); // {"competence_1":"low","competence_2":"low"} --> [competence_1, competence_2]
    for (let j = 0; j < singleCombinations.length; j++) {
      // competence 1 --> competence 2
      for (let k = 0; k < stateNum; k++) {
        // to find out what distribution is going to be for what combination
        if (nodeStates[k] === combinations[i][singleCombinations[j]]) {
          cpt[i * nodeStates.length + k] += expression(normalProb);
        }
      }
    }
  }
  // rounding and normalize for better results
  let sumNorm = 0;
  let sum = 0;
  for (let i = 0; i < cpt.length / stateNum; i++) {
    for (let j = 0; j < stateNum; j++) {
      //normalize
      const index = stateNum * i + j;
      sumNorm += cpt[index];
    }
    for (let k = 0; k < stateNum; k++) {
      const index = stateNum * i + k;
      cpt[index] = cpt[index] / sumNorm;
      cpt[index] = roundTo(cpt[index], 5);
      sum += cpt[index];
    }
    if (sum !== 1) {
      cpt[stateNum * i + (stateNum - 1)] += 0.00001;
    }
    sumNorm = 0;
    sum = 0;
  }
  return cpt;
}

/**
 * Creates even probabilities for nodes without parents.
 * @param stateNumber Number of states of the node
 * @returns An array representing the CPT
 */
function createFlatCptWithoutParents(stateNumber: number): number[] {
  const val = roundTo(1 / stateNumber, 5);
  const cpt = new Array(stateNumber).fill(val);
  if (val * stateNumber != 1) {
    cpt[stateNumber - 1] += 0.00001;
  }
  return cpt;
}

/**
 * Creates a linear CPT Table or base probabilities for nodes without parents.
 * @param node The node the CPT is generated fo
 * @returns An array representing the CPT
 */
export function getFlatCpt(node: IBayesNode): number[] {
  if (node.parents.length === 0) {
    return createFlatCptWithoutParents(node.states.length);
  }
  const statePairs = getStateParentPairs(node.states, node.parents);
  const cp = cartesianProductIterative(statePairs);
  return createFlatCPTWithParents(node.states, cp);
}

/**
 * Creates a quadratic CPT Table or  base probabilities for nodes without parents.
 * @param node The node the CPT is generated fo
 * @returns An array representing the CPT
 */
export function getQuadraticCpt(node: IBayesNode): number[] {
  if (node.parents.length === 0) {
    return createFlatCptWithoutParents(node.states.length);
  }
  const statePairs = getStateParentPairs(node.states, node.parents);
  const cp = cartesianProductIterative(statePairs);
  return createQuadraticCPTWithParents(node.states, cp);
}

/**
 * generate next unique id which can be used for a new node. In generale new node ids have
 * the from "competence_<Unique Number>".
 * @param graphNodeIds
 * @returns
 */
export function generateUniqueNodeId(graphNodeIds: string[]): number {
  let id = 0;
  for (let i = 0; i < graphNodeIds.length; i++) {
    graphNodeIds[i] = graphNodeIds[i].trim();
  }
  for (let i = 0; i <= graphNodeIds.length; i++) {
    if (!graphNodeIds.includes(`competence_${i}`)) {
      id = i;
      break;
    }
  }
  return id;
}

/**
 * traverse a given graph ( @currentGraph ) in a recursive way from a given id ( @currentId ).
 * The function checks during its walk through all sub-nodes if they refer to a cyclic graph.
 *
 * @param currentId starting point of the function, use the root node to traverse the whole graph
 * @param resultNodes contains nodes which refer to its parents
 * @returns returns resultNodes
 */
export function findForbiddenOptions(currentId: string, currentGraph: IBayesNet, resultNodes: string[] = []): string[] {
  let keys = Object.keys(currentGraph);
  const parents: string[] = [];
  keys = keys.filter((value) => !resultNodes.includes(value));
  for (let i = 0; i < keys.length; i++) {
    if (currentGraph[keys[i]].parents.includes(currentId)) {
      parents.push(keys[i]);
    }
  }
  if (parents.length === 0) {
    resultNodes.push(currentId);
  } else {
    parents.forEach((e) => {
      if (!resultNodes.includes(currentId)) {
        resultNodes.push(currentId);
      }
      this.findForbiddenOptions(e, currentGraph, resultNodes);
    });
  }
  return resultNodes;
}

/**
 * use this function to create a simple graph including one parent and two child-nodes.
 * @param states the set of states all nodes use
 * @returns
 */
export function generateSimpleDefaultGraph(states: string[]): IBayesNet {
  const newBayesNet: IBayesNet = {};

  addNode(newBayesNet, 'competence_A', states);
  addNode(newBayesNet, 'competence_B', states);
  addNode(newBayesNet, 'competence_C', states);

  setParents(newBayesNet, 'competence_A', ['competence_B', 'competence_C']);

  return newBayesNet;
}

export function addNode(bayesNet: IBayesNet, nodeName: string, states: string[]): void {
  bayesNet[nodeName] = {
    id: nodeName,
    states: clone(states),
    cpt: generateCPT(states.length, 0),
    parents: [],
  };
}

export function addParent(bayesNet: IBayesNet, nodeName: string, parentNames: string[]): void {
  parentNames.forEach((parentName) => {
    bayesNet[nodeName].parents.push(parentName);
  });

  bayesNet[nodeName].cpt = generateCPT(bayesNet[nodeName].states.length, bayesNet[nodeName].parents.length);
}

export function setParents(bayesNet: IBayesNet, nodeName: string, parentNames: string[]) {
  bayesNet[nodeName].parents = parentNames
  bayesNet[nodeName].cpt = getFlatCpt(bayesNet[nodeName]);
}

export function generateCPT(amountOfStates: number, amountOfParents: number): number[] {
  const cpt: number[] = [];
  const cptLength = evaluate(`${amountOfStates} * ${amountOfStates} ^ ${amountOfParents}`);
  const evenVal: number = roundTo(evaluate(`1 / ${amountOfStates}`), 5);

  for (let i = 0; i < cptLength; i++) {
    cpt.push(evenVal);
    if ((i + 1) % amountOfStates === 0) {
      cpt[i] += 0.00001;
    }
  }

  return cpt;
}

/**
 * sets values for each member of a cpt to the fraction of 1/ (length of states)
 * @param cpt
 * @returns
 */
export function evenValues(cpt: number[], stateLength: number): number[] {
  const newCpt: number[] = [];
  const evenVal: number = roundTo(evaluate(`1 / ${stateLength}`), 5);
  const rows = cpt.length / stateLength;
  let sum = 0;
  for (let i = 0; i < rows; i++) {
    sum = 0;
    for (let j = 0; j < stateLength; j++) {
      newCpt.push(evenVal);
      sum += evenVal;
    }
    if (sum !== 1) {
      newCpt[newCpt.length - 1] += 0.00001;
    }
  }
  return newCpt;
}

export function roundTo(n: number, digits: number): number {
  const multiplication = 10 ** digits;
  return Math.round(n * multiplication) / multiplication;
}
