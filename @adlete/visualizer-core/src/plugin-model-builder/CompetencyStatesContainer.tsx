import AddBox from '@mui/icons-material/AddBox';
import ArrowDownward from '@mui/icons-material/ArrowDownward';
import ArrowUpward from '@mui/icons-material/ArrowUpward';
import HighlightOff from '@mui/icons-material/HighlightOff';
import { Box, Button, IconButton, List, ListItem, Stack, TextField } from '@mui/material';
import * as React from 'react';

import { generateUniqueListKey } from '../shared-utils/ReactUtilities';

interface IStatesWidgetProps {
  defaultModelStates: string[];
  onStatesChanged: (states: string[]) => void;
}

interface IStatesWidgetState {
  states: { name: string; uniKey: string }[];
}

export class CompetencyStatesContainer extends React.Component<IStatesWidgetProps, IStatesWidgetState> {
  constructor(props: IStatesWidgetProps) {
    super(props);

    const states: { name: string; uniKey: string }[] = [];
    for (let i = 0; i < this.props.defaultModelStates.length; i++) {
      states.push({
        name: this.props.defaultModelStates[i],
        uniKey: generateUniqueListKey(this.props.defaultModelStates[i]),
      });
    }

    this.state = {
      states,
    };
  }

  // adds a state to the current list of states
  private addHandler(name: string) {
    const uniKey = generateUniqueListKey(name);
    const tmpStates = this.state.states.concat({ name, uniKey });
    this.setState((prevState) => ({ states: prevState.states.concat({ name, uniKey }) }));

    this.props.onStatesChanged(tmpStates.map((value) => value.name));
  }

  // removes an Item from the current list of states
  private removeHandler(uniKey: string) {
    const tmpStates = this.state.states.filter((value) => value.uniKey !== uniKey);
    this.setState((prevState) => ({ states: prevState.states.filter((value) => value.uniKey !== uniKey) }));
    this.props.onStatesChanged(tmpStates.map((value) => value.name));
  }

  private updateName(name: string, uniqueKey: string) {
    const tmpStates = this.state.states;

    const state = tmpStates.find((value) => value.uniKey === uniqueKey);
    if (state !== undefined && state !== null) {
      state.name = name;
      this.setState({ states: tmpStates });
    }
    this.props.onStatesChanged(this.state.states.map((value) => value.name));
  }

  // switches the position of two array values
  private arrowHandler(uniKey: string, arrow: 'up' | 'down') {
    let direction: -1 | 1;
    const states = [...this.state.states];
    const position = states.findIndex((i) => i.uniKey === uniKey);
    let possible = true;
    if (arrow === 'up') {
      direction = -1;
    } else if (arrow === 'down') {
      direction = 1;
    }

    if (position < 0) {
      throw new Error('Given Item not found.');
    } else if (direction === -1 && position === 0) {
      possible = false;
    } else if (direction === 1 && position === states.length - 1) {
      possible = false;
    }

    if (possible) {
      const item = states[position];
      const newItems = states.filter((i) => i.uniKey !== uniKey);
      newItems.splice(position + direction, 0, item);

      this.setState({ states: newItems });
      this.props.onStatesChanged(newItems.map((value) => value.name));
    }
  }

  public render(): React.ReactNode {
    return (
      <Box>
        <List>
          {this.generateRatings(this.state.states)}
          <ListItem>
            <Button variant="outlined" color="success" size="large" onClick={() => this.addHandler('new category')}>
              <AddBox />
            </Button>
          </ListItem>
        </List>
      </Box>
    );
  }

  // creates the Ratings from the given Array of Ratings with The AddSingleRating function
  private generateRatings(states: { name: string; uniKey: string }[]) {
    const jsxRatings = states.map((state, index) => {
      let firstArrow = false;
      let lastArrow = false;

      // to check if the current item is the last / first one
      if (states.length - 1 === index) {
        lastArrow = true;
      }
      if (index === 0) {
        firstArrow = true;
      }

      return (
        <ListItem key={state.uniKey}>
          <Stack direction="row" sx={{ width: '100%', maxWidth: '500px' }}>
            <TextField
              fullWidth
              label={`Taxonomy State ${index + 1}`}
              variant="outlined"
              defaultValue={state.name}
              onChange={(e) => this.updateName(e.target.value, state.uniKey)}
            />
            <IconButton disabled={firstArrow} onClick={() => this.arrowHandler(state.uniKey, 'up')}>
              <ArrowUpward />
            </IconButton>
            <IconButton onClick={() => this.arrowHandler(state.uniKey, 'down')} disabled={lastArrow}>
              <ArrowDownward />
            </IconButton>
            <IconButton onClick={() => this.removeHandler(state.uniKey)}>
              <HighlightOff color="error" />
            </IconButton>
          </Stack>
        </ListItem>
      );
    });
    return jsxRatings;
  }
}
