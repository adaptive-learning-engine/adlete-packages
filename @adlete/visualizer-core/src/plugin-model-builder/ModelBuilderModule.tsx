import AddIcon from '@mui/icons-material/Add';
import DownloadIcon from '@mui/icons-material/Download';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { Box, IconButton, Toolbar, Tooltip, Typography } from '@mui/material';
import * as React from 'react';

import * as CompetenceGraphUtility from './CompetenceGraphUtility';
import { ModelDialog } from './EnumModelDialog';
import { CreateModelDialog } from './dialog/CreateModelDialog';
import { EditModelDialog } from './dialog/EditModelDialog';
import { LoadModelDialog } from './dialog/LoadModelDialog';
import { SaveModelDialog } from './dialog/SaveModelDialog';
import { EditModelTab } from './tabs/EditModelTab';
import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IModelBuilderModuleProps {}

interface IModelBuilderModuleState {
  model: IBayesNet;
  selectedCompetence: string;
  currentDialog: ModelDialog;
}

export class ModelBuilderModule extends React.Component<IModelBuilderModuleProps, IModelBuilderModuleState> {
  constructor(props: IModelBuilderModuleProps) {
    super(props);
    this.state = {
      model: null,
      selectedCompetence: '',
      currentDialog: ModelDialog.None,
    };

    this.onModelCreatedOrLoaded = this.onModelCreatedOrLoaded.bind(this);
    this.onModelUpdate = this.onModelUpdate.bind(this);
    this.onModelSaved = this.onModelSaved.bind(this);
    this.onCompetenceSelectionChanged = this.onCompetenceSelectionChanged.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  private onModelCreatedOrLoaded(model: IBayesNet) {
    this.setState({
      model,
      selectedCompetence: CompetenceGraphUtility.getRootNodesNames(model)[0],
    });

    this.closeDialog();
  }

  private onModelUpdate(model: IBayesNet, selectedCompetence: string) {
    this.setState({ model, selectedCompetence });
  }

  private onModelSaved() {
    this.closeDialog();
  }

  private onCompetenceSelectionChanged(selectedCompetence: string) {
    this.setState({ selectedCompetence });
    this.closeDialog();
  }

  private openDialog(dialog: ModelDialog) {
    this.setState({ currentDialog: dialog });
  }

  private closeDialog() {
    this.setState({ currentDialog: ModelDialog.None });
  }

  protected renderDialog(): React.ReactElement {
    switch (this.state.currentDialog) {
      case ModelDialog.CreateModel:
        return (
          <CreateModelDialog
            onClose={this.closeDialog}
            defaultModelStates={['high', 'medium', 'low']}
            onModelCreated={this.onModelCreatedOrLoaded}
          />
        );
      case ModelDialog.EditMOdel:
        return (
          <EditModelDialog
            onClose={this.closeDialog}
            model={this.state.model}
            onModelUpdated={this.onModelUpdate}
            selectedCompetence={this.state.selectedCompetence}
            onCompetenceSelectionChanged={this.onCompetenceSelectionChanged}
          />
        );
      case ModelDialog.LoadModel:
        return <LoadModelDialog onClose={this.closeDialog} onModelLoaded={this.onModelCreatedOrLoaded} />;
      case ModelDialog.SaveModel:
        return <SaveModelDialog onClose={this.closeDialog} model={this.state.model} onModelSaved={this.onModelSaved} />;
      default:
        return null;
    }
  }

  protected renderAnalysis(): React.ReactNode {
    if (!this.state.selectedCompetence) {
      return (
        <Box marginLeft="10px">
          <Typography>Please create a competency model or load one to start...</Typography>
        </Box>
      );
    }
    return (
      <EditModelTab
        model={this.state.model}
        onModelUpdated={this.onModelUpdate}
        selectedCompetence={this.state.selectedCompetence}
        onCompetenceSelectionChanged={this.onCompetenceSelectionChanged}
      />
    );
  }

  public render(): React.ReactNode {
    return (
      <Box>
        <Toolbar>
          <Tooltip title="Create new model">
            <IconButton sx={{ mr: 5 }} color="primary" onClick={() => this.openDialog(ModelDialog.CreateModel)}>
              <AddIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Load model">
            <IconButton sx={{ mr: 5 }} color="primary" onClick={() => this.openDialog(ModelDialog.LoadModel)}>
              <UploadFileIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Save model">
            <span>
              <IconButton
                disabled={this.state.model === null}
                sx={{ mr: 5 }}
                color="primary"
                onClick={() => this.openDialog(ModelDialog.SaveModel)}
              >
                <DownloadIcon />
              </IconButton>{' '}
            </span>
          </Tooltip>
        </Toolbar>

        {this.renderAnalysis()}
        {this.renderDialog()}
      </Box>
    );
  }
}
