import { Card, Typography } from '@mui/material';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { LoadFileContainer } from '../../shared-containers/LoadFileContainer';
import { parseModelFile } from '../../shared-utils/load-save-model';

interface ILoadModelTabProps {
  onModelLoaded: (model: IBayesNet) => void;
}
interface ILoadModelTabState {
  errorMessage: string;
}

// eslint-disable-next-line react/prefer-stateless-function
export class LoadModelTab extends React.Component<ILoadModelTabProps, ILoadModelTabState> {
  constructor(props: ILoadModelTabProps) {
    super(props);
    this.state = {
      errorMessage: '',
    };

    this.onModelLoaded = this.onModelLoaded.bind(this);
  }

  private onModelLoaded(fileString: string): void {
    try {
      const graph = parseModelFile(fileString);
      this.props.onModelLoaded(graph);
    } catch (e) {
      this.setState({ errorMessage: e.message });
    }
  }

  public render(): React.ReactNode {
    return (
      <Card>
        <Typography variant="subtitle1" gutterBottom>
          Load an existing model from a file
        </Typography>
        <LoadFileContainer onFileLoaded={this.onModelLoaded} />
        <Typography style={{ color: 'red' }}>
          <span>{this.state.errorMessage}</span>
        </Typography>
      </Card>
    );
  }
}
