import { Card } from '@mui/material';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { SaveFileContainer } from '../../shared-containers/SaveFileContainer';
import { generateConfigurationData } from '../../shared-utils/load-save-model';

interface ISaveModelTabProps {
  model: IBayesNet;
  onModelSaved: () => void;
}
interface ISaveModelContainerStates {
  fileName: string;
}

export class SaveModelTab extends React.Component<ISaveModelTabProps, ISaveModelContainerStates> {
  constructor(props: ISaveModelTabProps) {
    super(props);
    this.state = {
      fileName: `${`${new Date().toISOString().slice(0, 10).toString()}- NewFile`}.json`,
    };
  }

  public render(): React.ReactNode {
    return (
      <Card>
        <SaveFileContainer
          onGenerateSerializedData={() => JSON.stringify(generateConfigurationData(this.props.model))}
          onFileSavedFinished={this.props.onModelSaved}
        />
      </Card>
    );
  }
}
