import { Button, Card, Typography } from '@mui/material';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import * as CompetenceGraphUtility from '../CompetenceGraphUtility';
import { CompetencyStatesContainer } from '../CompetencyStatesContainer';

interface ICreateModelTabProps {
  defaultModelStates: string[];
  onModelCreated: (model: IBayesNet) => void;
}

interface ICreateModelTabStates {
  errorText: string;
  ratings: string[];
}

export class CreateModelTab extends React.Component<ICreateModelTabProps, ICreateModelTabStates> {
  constructor(props: ICreateModelTabProps) {
    super(props);
    this.state = {
      errorText: '',
      ratings: this.props.defaultModelStates,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onCompetencyStateChanged = this.onCompetencyStateChanged.bind(this);
  }

  private onCompetencyStateChanged(ratings: string[]) {
    this.setState({ ratings, errorText: this.validate(ratings) });
  }

  private validate(ratings: string[]): string {
    // to check if array has duplicates
    const namesArray: string[] = [];
    let error = false; // (new Set(namesArray)).size !== namesArray.length;

    for (let i = 0; i < ratings.length; i++) {
      const name = ratings[i];
      if (name === '' || namesArray.includes(name) || ratings[i].replace(/\s/g, '') === '') {
        error = true;
      }
      namesArray.push(name);
    }
    if (error) {
      return 'Some of your defined states are not valid or occur multiple times.';
    }
    return '';
  }

  private onSubmit() {
    const error = this.validate(this.state.ratings);
    if (error === '') {
      const newGraph = CompetenceGraphUtility.generateSimpleDefaultGraph(this.state.ratings);
      this.props.onModelCreated(newGraph);
    } else {
      this.setState({ errorText: error });
    }
  }

  public render(): React.ReactNode {
    return (
      <Card>
        <Typography variant="subtitle1" gutterBottom>
          Define the competence taxonomy for the new model
        </Typography>
        <CompetencyStatesContainer defaultModelStates={this.props.defaultModelStates} onStatesChanged={this.onCompetencyStateChanged} />
        <Button variant="contained" color="primary" onClick={this.onSubmit} disabled={this.state.errorText !== ''}>
          Create New Model
        </Button>
        <Typography sx={{ color: 'red' }}>
          <span>{this.state.errorText}</span>
        </Typography>
      </Card>
    );
  }
}
