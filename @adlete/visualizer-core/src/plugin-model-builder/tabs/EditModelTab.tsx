import { Card, Typography } from '@mui/material';
import cloneDeep from 'lodash.clonedeep';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { CompetenceGraphContainer } from '../CompetenceGraphContainer';
import * as CompetenceGraphUtility from '../CompetenceGraphUtility';
import { CompetenceInspectorContainer } from '../CompetenceInspectorContainer';

interface IEditModelTabProps {
  model: IBayesNet;
  selectedCompetence: string;
  onModelUpdated: (model: IBayesNet, selectedCompetence: string) => void;
  onCompetenceSelectionChanged: (selectedCompetence: string) => void;
}

export class EditModelTab extends React.Component<IEditModelTabProps> {
  constructor(props: IEditModelTabProps) {
    super(props);
    this.removeCompetenceNode = this.removeCompetenceNode.bind(this);
    this.addCompetence = this.addCompetence.bind(this);
    this.setCompetenceParents = this.setCompetenceParents.bind(this);
  }

  /**
   * takes a node id, adds a parent to it and refreshes the parents/cpts of the given node
   * @param nodeId
   */
  private addCompetence(nodeId: string): void {
    // all nodes must use the same states, so we can use the root node states
    const rootNode = CompetenceGraphUtility.getRootNodesNames(this.props.model)[0];
    const nodeStates = [...this.props.model[rootNode].states];

    // get the current amount of Nodes to later push a new Node
    const nextId = CompetenceGraphUtility.generateUniqueNodeId(Object.keys(this.props.model));
    // create the Node that needs to be added
    CompetenceGraphUtility.addNode(this.props.model, `competence_${nextId}`, nodeStates);

    this.addCompetenceParents(this.props.model, nodeId, [`competence_${nextId}`], `competence_${nextId}`);
  }

  /**
   * Adds a new Parent to the node
   * @param model
   * @param id
   * @param parents
   * @param setSelectedNodeId (optional) set a specific node which should be selected
   */
  private addCompetenceParents(model: IBayesNet, id: string, parents: string[], competenceToSelect: string) {
    CompetenceGraphUtility.addParent(model, id, parents);

    this.props.onModelUpdated(model, competenceToSelect);
  }

  /**
   * takes a model and set the parents set of the node with the given id.
   * @param model
   * @param id
   * @param parents
   * @param setSelectedNodeId (optional) set a specific node which should be selected
   */
  private setCompetenceParents(model: IBayesNet, id: string, parents: string[], competenceToSelect: string) {
    CompetenceGraphUtility.setParents(model, id, parents);

    this.props.onModelUpdated(model, competenceToSelect);
  }

  /**
   * takes an node id, filters out all the sub-nodes, deletes them and refreshes the children parents/cpts
   * @param id
   */
  private removeCompetenceNode(id: string): void {
    const newModel = cloneDeep(this.props.model);
    if (CompetenceGraphUtility.getRootNodesNames(newModel).includes(id)) {
      delete newModel[id];
    } else {
      delete newModel[id];
      const myKeys = Object.keys(newModel);
      for (let i = 0; i < myKeys.length; i++) {
        // if a Node contains the id of the deleted child node, then filter it out of their "Parents"

        if (newModel[myKeys[i]].parents.includes(id)) {
          newModel[myKeys[i]].parents = newModel[myKeys[i]].parents.filter((value) => value !== id);
          // create new Cpts for Nodes With / Without Parents
          newModel[myKeys[i]].cpt = CompetenceGraphUtility.getFlatCpt(newModel[myKeys[i]]); /* CompetenceGraphUtility.generateCPT(
            newModel[myKeys[i]].states.length,
            newModel[myKeys[i]].parents.length
          );*/
        }
      }
    }
    this.props.onModelUpdated(
      newModel,
      this.props.selectedCompetence !== id ? this.props.selectedCompetence : CompetenceGraphUtility.getRootNodesNames(newModel)[0]
    );
  }

  public render(): React.ReactNode {
    return (
      <Card>
        <CompetenceGraphContainer
          competenceModel={this.props.model}
          selectedCompetence={this.props.selectedCompetence}
          addCompetenceNode={this.addCompetence}
          removeCompetenceNode={this.removeCompetenceNode}
          changeCompetenceSelection={this.props.onCompetenceSelectionChanged}
        />
        <Typography variant="subtitle1" gutterBottom>
          Competence Inspector
        </Typography>
        <CompetenceInspectorContainer
          model={this.props.model}
          selected={this.props.selectedCompetence}
          onModelUpdated={this.props.onModelUpdated}
          setNewParents={this.setCompetenceParents}
        />
      </Card>
    );
  }
}
