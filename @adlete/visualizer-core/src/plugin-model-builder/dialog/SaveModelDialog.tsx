import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { SaveModelTab } from '../tabs/SaveModelTab';

interface ISaveModelDialogProps {
  onClose: () => void;
  model: IBayesNet;
  onModelSaved: () => void;
}

export function SaveModelDialog(props: ISaveModelDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Load Model</DialogTitle>
        <DialogContent>
          <SaveModelTab model={props.model} onModelSaved={props.onModelSaved} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
