import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { CreateModelTab } from '../tabs/CreateModelTab';

interface ICreateModelDialogProps {
  onClose: () => void;
  defaultModelStates: string[];
  onModelCreated: (model: IBayesNet) => void;
}

export function CreateModelDialog(props: ICreateModelDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Create new model</DialogTitle>
        <DialogContent>
          <CreateModelTab defaultModelStates={props.defaultModelStates} onModelCreated={props.onModelCreated} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
