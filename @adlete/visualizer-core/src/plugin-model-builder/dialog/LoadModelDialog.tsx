import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { LoadModelTab } from '../tabs/LoadModelTab';

interface ILoadModelDialogProps {
  onClose: () => void;
  onModelLoaded: (model: IBayesNet) => void;
}

export function LoadModelDialog(props: ILoadModelDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Load Model</DialogTitle>
        <DialogContent>
          <LoadModelTab onModelLoaded={props.onModelLoaded} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
