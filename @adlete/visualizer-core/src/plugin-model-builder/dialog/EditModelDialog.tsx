import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { EditModelTab } from '../tabs/EditModelTab';

interface IEditModelDialogProps {
  onClose: () => void;
  model: IBayesNet;
  selectedCompetence: string;
  onModelUpdated: (model: IBayesNet, selectedCompetence: string) => void;
  onCompetenceSelectionChanged: (selectedCompetence: string) => void;
}

export function EditModelDialog(props: IEditModelDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Load Model</DialogTitle>
        <DialogContent>
          <EditModelTab
            model={props.model}
            onModelUpdated={props.onModelUpdated}
            onCompetenceSelectionChanged={props.onCompetenceSelectionChanged}
            selectedCompetence={props.selectedCompetence}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
