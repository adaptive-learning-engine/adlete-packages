import { Alert, AlertProps, Box, Button, FormControl, FormHelperText, Grid, MenuItem, Select, SelectChangeEvent, Snackbar, Typography } from '@mui/material';
import { DataGrid, GridColDef, GridRenderCellParams, GridRowId, GridRowModel} from '@mui/x-data-grid';
import cloneDeep from 'lodash.clonedeep';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IBayesNode } from '@adlete/engine-blocks/belief/IBayesNode';

import * as CompetenceGraphUtility from './CompetenceGraphUtility';
import { cartesianProductIterative, getStateParentPairs } from './CompetenceGraphUtility';

interface INewCptParentsBuilderComponentProps {
  node: IBayesNode;
  parents: boolean;
  network: IBayesNet;
  onModelUpdated: (model: IBayesNet, competenceToSelect: string) => void;
}

enum GenerateCPT {
  EVEN,
  LINEAR,
  EXPONENTIAL,
}

interface INewCptParentsBuilderComponentStates {
  errorText: string;
  errorRows: number[];
  generationMethod: GenerateCPT;
  snackbar: Pick<AlertProps, 'children' | 'severity'> | null
}

interface RowData {
  id: number,
  [key: string]: number | string
}

const TOLERANCE = 0.0000000000000001

export class CptParentsBuilderComponent extends React.Component<INewCptParentsBuilderComponentProps, INewCptParentsBuilderComponentStates> {
  constructor(props: INewCptParentsBuilderComponentProps) {
    super(props);

    this.state = {
      errorRows: [],
      errorText: '',
      generationMethod: GenerateCPT.EVEN,
      snackbar: null
    };
    this.handleGenMethodChange = this.handleGenMethodChange.bind(this);
    this.validateRow = this.validateRow.bind(this)
    this.closeSnackbar = this.closeSnackbar.bind(this)
  }

  componentDidUpdate(prevProps: INewCptParentsBuilderComponentProps): void {
    this.UpdateState(prevProps);
  }

  private UpdateState(prevProps: INewCptParentsBuilderComponentProps): void {
    if (this.props.node !== prevProps.node) {
      // TODO: use getDerivedStateFromProps?
      this.setState({ ...this.state, errorRows: [], errorText: '' });
    }
  }

  private closeSnackbar(): void {
    this.setState({...this.state, snackbar: null})
  }
  /**
   * For the Nodes, this method creates the Data Columns using the cpts
   * @param cpt
   * @returns
   */
  private createTableDataColumns(): GridColDef[] {
    const columns: string[] = this.props.node.states;
    const result: GridColDef[] = [];
    if (this.props.node.parents.length > 0) {
      result.push({
        field: 'parentStates',
        headerName: this.props.node.parents.join(', '),
        sortable: false,
        filterable: false,
        editable: false, // only states should be edit able
        flex: 0.3,
        width: 120,
        minWidth: 10,
      });
    }

    for (let i = 0; i < columns.length; i++) {
      result.push({
        field: columns[i],
        headerName: columns[i],
        sortable: false,
        filterable: false,
        type: 'number',
        editable: true, // only states should be edit able
        flex: 0.3,
        width: 120,
        minWidth: 10,
        // TODO: valueFormatter: https://mui.com/components/data-grid/columns/#value-formatter
        renderCell: (params: GridRenderCellParams<unknown, string>) => (
          <Grid
            container
            spacing={3}
            alignItems="center"
            sx={{ color: this.state.errorRows.includes(params.id as number) ? 'red' : 'black' }}
          >
            <Grid item xs={12}>
              {params.value.toString()}
            </Grid>
          </Grid>
        ),
      });
    }
    return result;
  }

  /**
   * For both Nodes with and without parents, this node creates the Data Rows, which is why
   * some parts of this method need to seperate between ICptWithParents and ICptWithoutParents
   * @returns
   */
  private createTableDataRows(): GridRowModel[] {
    // Nodes with vs Nodes without Parents
    const cpt = cloneDeep(this.props.node.cpt);
    const parents = [...this.props.node.parents];
    const states = this.props.node.states;
    const rows: GridRowModel[] = [];
    if (parents.length === 0) {
      const rowObject: RowData = { id: 0};
      states.forEach((value, index) => (rowObject[value] = cpt[index]));
      rows.push(rowObject);
    } else {
      const pairs = Object.values(cartesianProductIterative(getStateParentPairs(states, parents)));
      for (let i = 0; i < pairs.length; i++) {
        const rowObject: RowData = { id: i, parentStates: Object.values(pairs[i]).join(', ') };
        for (let s = 0; s < states.length; s++) {
          rowObject[states[s]] = cpt[states.length * i + s];
        }
        rows.push(rowObject);
      }
    }
    return rows;
  }

  /**
   * If the number in any changed cell of the grid is NAN, Above 1 or Below 0, the value gets set back to a normal value
   * @param rating
   * @param value
   * @param rowNumber
   */
  private checkValueWithParents(rating: string, value: string, rowNumber: GridRowId): void {
    let numericVal: number = +value;
    const column = this.props.node.states.findIndex((value) => value === rating);
    if (column === -1) {
      return;
    }
    const numericId: number = +rowNumber * this.props.node.states.length + column;
    if (Number.isNaN(numericVal)) {
      numericVal = 0;
    } else if (numericVal < 0) {
      numericVal = 0;
    } else if (numericVal > 1) {
      numericVal = 1;
    }
        this.props.node.cpt[numericId] = numericVal;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private validateRow(newRow: RowData, oldRow: RowData): RowData {
    let rowSum = 0; 
    this.props.node.states.forEach(state => {
        const val: number = newRow[state] as number;
        rowSum += val
    });

    if(Math.abs(rowSum - 1) < TOLERANCE) {
      const numericId: number = newRow.id * this.props.node.states.length;
      let stateIndex = 0;
      this.props.node.states.forEach(state => {
        this.props.node.cpt[numericId + stateIndex] = newRow[state] as number
        stateIndex++
    });
      this.setState({...this.state, snackbar: {children: 'Row updated successfully', severity: 'success'}})
      this.updateModel()
      return newRow;
    }

    throw Error("Sum of Columns must be 1.")
  }

  /**
   * gets called to put the edited Table up in the Graph view to change the Nodes itself with the new values
   */
  private updateModel(): void {
    // Removed validation because it is validated via process row (only changable via editing html)
    //if (this.validateValues()) {
      this.props.network[this.props.node.id] = this.props.node;
      this.props.onModelUpdated(this.props.network, this.props.node.id);
    //}
  }

  /**
   * checks the whole table for incorrect Values,
   * puts out an error message and returns if the validation has been completed
   * @returns
   */
  private validateValues(): boolean {
    const cpt = this.props.node.cpt;
    const stateCount = this.props.node.states.length;
    const rowsCount = cpt.length;
    if (rowsCount % stateCount != 0) {
      this.setState({ ...this.state, errorText: 'Badly formated propability tables' });
      return false;
    }
    const notOne: number[] = [];
    for (let i = 0; i < rowsCount / stateCount; i++) {
      let addValues = 0;
      for (let j = 0; j < stateCount; j++) {
        addValues += cpt[i * stateCount + j];
      }
      if (addValues !== 1) {
        notOne.push(i);
      }
    }

    if (notOne.length > 0) {
      this.setState({ ...this.state, errorText: 'The max. sum per row is 1.', errorRows: notOne });
    } else {
      this.setState({ ...this.state, errorText: '', errorRows: [] });
    }
    return notOne.length === 0;
  }

  private handleGenMethodChange(e: SelectChangeEvent<GenerateCPT>) {
    this.setState({ ...this.state, generationMethod: e.target.value as GenerateCPT });
  }

  /**
   * evenly distributes values over the table for Nodes with Parents
   */
  private evenCptValues(): void {
    switch (this.state.generationMethod) {
      case GenerateCPT.EVEN:
        this.props.node.cpt = CompetenceGraphUtility.evenValues(this.props.node.cpt, this.props.node.states.length);
        break;
      case GenerateCPT.LINEAR:
        this.props.node.cpt = CompetenceGraphUtility.getFlatCpt(this.props.node);
        break;
      case GenerateCPT.EXPONENTIAL:
        this.props.node.cpt = CompetenceGraphUtility.getQuadraticCpt(this.props.node);
        break;
      default:
        this.setState({ ...this.state, errorText: 'Option not supported', errorRows: [] });
        return;
    }

    this.updateModel();
  }
  // TODO: Use sx Prop to display boarder, validate New Cell Values diferently
  public render(): React.ReactNode {
    const rows: GridRowModel[] = this.createTableDataRows();
    const columns = this.createTableDataColumns();

    return (
      <Box>
        <Box sx={{ display: 'flex', flexDirection:'column', maxHeight: '430px', minHeight: '200px', width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            getRowClassName={(params) => {
              if (this.state.errorRows.includes(params.id as number)) {
                return 'error';
              }
              return '';
            }}
            disableColumnMenu
            editMode='row'
            //onCellEditStop={} Maybe for a back button?
            processRowUpdate={this.validateRow}
            onProcessRowUpdateError={(error) => {
              this.setState({...this.state, snackbar: {children: error.message, severity: 'error'}})
            }}
          />

          {!!this.state.snackbar && (
            <Snackbar
              open
              anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
              onClose={this.closeSnackbar}
              autoHideDuration={6000}
            >
              <Alert {...this.state.snackbar} onClose={this.closeSnackbar}/>
            </Snackbar>
          )}
        </Box>
        <Box>
          <Typography sx={{ color: 'red' }}>
            <span>{this.state.errorText}</span>
          </Typography>
          {this.props.node.parents.length > 0 ? (
          <Box sx={{ display: 'flex', mt: '1em', alignItems: 'flex-end', gap: '10px'}}>
                <FormControl sx={{ ml: '1em' }}>
                  <FormHelperText>CPT generation method</FormHelperText>
                  <Select
                    value={this.state.generationMethod}
                    onChange={this.handleGenMethodChange}
                    inputProps={{ 'aria-label': 'CPT generation method' }}
                  >
                    <MenuItem value={GenerateCPT.EVEN}>Even Values</MenuItem>
                    <MenuItem value={GenerateCPT.LINEAR}>Linear Values</MenuItem>
                  </Select>
                </FormControl>
                <Button variant="outlined" color="secondary" onClick={() => this.evenCptValues()}>
                  Apply
                </Button>
          </Box>
          ) : null}
        </Box>
      </Box>
    );
  }
}
