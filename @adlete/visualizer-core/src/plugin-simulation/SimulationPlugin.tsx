import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';
import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '../plugin-layout/LayoutManagerPlugin';

import { SimulationModule } from './SimulationModule';

export interface IWithSimulation extends IWithLayoutManager {
  simulation: SimulationPlugin;
}

export const SimulationMeta: IPluginMeta<IWithSimulation, 'simulation'> = {
  id: 'simulation',
  dependencies: ['layoutManager'],
};

export class SimulationPlugin implements IPlugin<IWithSimulation> {
  public meta: IPluginMeta<IWithSimulation, 'simulation'> = SimulationMeta;

  public initialize(pluginMan: IPluginManager<IWithSimulation>): Promise<void> {
    const layoutMan = pluginMan.get('layoutManager');
    layoutMan.panelTypes.add('Simulation', {
      createPanel: () => <SimulationModule />,
      title: 'Simulation',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
