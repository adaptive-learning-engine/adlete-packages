import AddIcon from '@mui/icons-material/Add';
import DownloadIcon from '@mui/icons-material/Download';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { Box, IconButton, Switch, Toolbar, Tooltip, Typography } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { LearnerAnalysisMode } from '../shared-containers/LearnerAnalysisContainer';

import { SimulationDialog } from './EnumSimulationDialog';
import { LoadSimulationDialog } from './dialogs/LoadSimulationDialog';
import { NewSimulationDialog } from './dialogs/NewSimulationDialog';
import { SaveSimulationDialog } from './dialogs/SaveSimulationDialog';
import { SimulationAnalysisTab } from './tabs/SimulationAnalysisTab';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ISimulationModuleProps {}

interface ISimulationModuleState {
  simulationResults: IAnalyticsResponse;
  displayMode: LearnerAnalysisMode;
  currentDialog: SimulationDialog;
}

export class SimulationModule extends React.Component<ISimulationModuleProps, ISimulationModuleState> {
  constructor(props: ISimulationModuleProps) {
    super(props);
    this.state = {
      currentDialog: SimulationDialog.None,
      displayMode: LearnerAnalysisMode.GridMode,
      simulationResults: null,
    };
    this.simulationsCallback = this.simulationsCallback.bind(this);
    this.onSimulationLoaded = this.onSimulationLoaded.bind(this);
    this.onModelSaved = this.onModelSaved.bind(this);
    this.onSwitchChange = this.onSwitchChange.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  private onSimulationLoaded(simulationResults: IAnalyticsResponse) {
    if (simulationResults) {
      window.setTimeout(() => {
        this.setState({ simulationResults });
      }, 100);
    }
    this.closeDialog();
  }

  private simulationsCallback(simulationResults: IAnalyticsResponse) {
    if (simulationResults) {
      this.setState({ simulationResults });
      this.closeDialog();
    }
  }

  private onModelSaved() {
    this.closeDialog();
  }

  private onSwitchChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      this.setState({ displayMode: LearnerAnalysisMode.GridMode });
    } else {
      this.setState({ displayMode: LearnerAnalysisMode.GraphMode });
    }
  }

  private openDialog(dialog: SimulationDialog) {
    this.setState({ currentDialog: dialog });
  }

  private closeDialog() {
    this.setState({ currentDialog: SimulationDialog.None });
  }

  protected renderDialog(): React.ReactElement {
    switch (this.state.currentDialog) {
      case SimulationDialog.NewSimulation:
        return <NewSimulationDialog onClose={this.closeDialog} onSimulationFinished={this.simulationsCallback} />;
      case SimulationDialog.LoadSimulation:
        return <LoadSimulationDialog onSimulationLoaded={this.onSimulationLoaded} onClose={this.closeDialog} />;
      case SimulationDialog.SaveSimulationResults:
        return (
          <SaveSimulationDialog
            analyticsResponse={this.state.simulationResults}
            onModelSaved={this.onModelSaved}
            onClose={this.closeDialog}
          />
        );
      default:
        return null;
    }
  }

  protected renderAnalysis(): React.ReactNode {
    if (!this.state.simulationResults) {
      return (
        <Box marginLeft="10px">
          <Typography>Please run a simulation or load simulation result data to start...</Typography>
        </Box>
      );
    }
    return <SimulationAnalysisTab analytics={this.state.simulationResults} ModeOfDisplay={this.state.displayMode} />;
  }

  public render(): React.ReactNode {
    return (
      <Box>
        <Toolbar>
          <Tooltip title="Create new simulation">
            <IconButton sx={{ mr: 5 }} color="primary" onClick={() => this.openDialog(SimulationDialog.NewSimulation)}>
              <AddIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Load simulation Results">
            <IconButton sx={{ mr: 5 }} color="primary" onClick={() => this.openDialog(SimulationDialog.LoadSimulation)}>
              <UploadFileIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Save Simulation Results">
            <span>
              <IconButton
                disabled={this.state.simulationResults === null}
                sx={{ mr: 5 }}
                color="primary"
                onClick={() => this.openDialog(SimulationDialog.SaveSimulationResults)}
              >
                <DownloadIcon />
              </IconButton>
            </span>
          </Tooltip>
          <Tooltip title="switch display mode">
            <span>
              <Switch defaultChecked onChange={this.onSwitchChange} disabled={this.state.simulationResults === null} />
            </span>
          </Tooltip>
        </Toolbar>
        {this.renderDialog()}
        {this.renderAnalysis()}
      </Box>
    );
  }
}
