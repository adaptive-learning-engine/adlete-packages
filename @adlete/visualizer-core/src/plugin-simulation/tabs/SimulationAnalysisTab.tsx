import { Box, Grid, Typography } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { AnalysisContainer, LearnerAnalysisMode } from '../../shared-containers/LearnerAnalysisContainer';

interface ISimulationAnalysisTabProps {
  ModeOfDisplay: LearnerAnalysisMode;
  analytics: IAnalyticsResponse;
}

// eslint-disable-next-line react/prefer-stateless-function
export class SimulationAnalysisTab extends React.Component<ISimulationAnalysisTabProps> {
  public render(): React.ReactNode {
    if (this.props.analytics.observations.length === 0) {
      return (
        <Grid
          sx={{ mt: 5 }}
          container
          justifyContent="center" // Align items horizontally in the center
          alignItems="center" // Align items vertically in the center
          style={{ minHeight: '100%' }} // Ensure the container takes up the full viewport height
        >
          <Grid item>
            <Typography variant="h5" align="center">
              No activities were trained. Please try a different configuration
            </Typography>
          </Grid>
        </Grid>
      );
    }
    return (
      <Box>
        <AnalysisContainer modeOfDisplay={this.props.ModeOfDisplay} learnerAnalytics={this.props.analytics} />
      </Box>
    );
  }
}
