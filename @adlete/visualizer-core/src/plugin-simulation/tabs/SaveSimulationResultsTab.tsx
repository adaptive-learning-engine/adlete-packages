import { Card } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { SaveFileContainer } from '../../shared-containers/SaveFileContainer';

interface ISaveSimulationResultsTabProps {
  analyticsResponse: IAnalyticsResponse;
  onModelSaved: () => void;
}

// eslint-disable-next-line react/prefer-stateless-function
export class SaveSimulationResultsTab extends React.Component<ISaveSimulationResultsTabProps> {
  public render(): React.ReactNode {
    return (
      <Card>
        <SaveFileContainer
          onGenerateSerializedData={() => JSON.stringify(this.props.analyticsResponse)}
          onFileSavedFinished={this.props.onModelSaved}
        />
      </Card>
    );
  }
}
