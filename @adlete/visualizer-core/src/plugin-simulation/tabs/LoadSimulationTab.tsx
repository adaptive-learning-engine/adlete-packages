import { Backdrop, Card, CircularProgress, Grid, Typography } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { LoadFileContainer } from '../../shared-containers/LoadFileContainer';
import { parseSimFile } from '../../shared-utils/load-save-simulation';

interface ILoadSimulationTabProps {
  onSimulationLoaded: (Simulation: IAnalyticsResponse) => void;
}
interface ILoadSimulationTabState {
  errorMessage: string;
  backDropVisible: boolean;
}
// eslint-disable-next-line react/prefer-stateless-function
export class LoadSimulationTab extends React.Component<ILoadSimulationTabProps, ILoadSimulationTabState> {
  constructor(props: ILoadSimulationTabProps) {
    super(props);
    this.state = {
      errorMessage: '',
      backDropVisible: false,
    };

    this.onSimulationFileLoaded = this.onSimulationFileLoaded.bind(this);
  }

  private onSimulationFileLoaded(fileString: string): void {
    try {
      const simFile = parseSimFile(fileString);
      this.setState({ backDropVisible: true });
      this.props.onSimulationLoaded(simFile);
    } catch (e) {
      this.setState({ errorMessage: e.message });
    }
  }

  backDrop(value: boolean): React.ReactNode {
    const backDropElement = (
      <Grid>
        <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }} open={value}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    );
    return backDropElement;
  }

  public render(): React.ReactNode {
    return (
      <Card>
        <Typography variant="subtitle1" gutterBottom>
          Load an existing Simulation from a file
        </Typography>
        <LoadFileContainer onFileLoaded={this.onSimulationFileLoaded} />
        <Typography style={{ color: 'red' }}>
          <span>{this.state.errorMessage}</span>
        </Typography>
        {this.backDrop(this.state.backDropVisible)}
      </Card>
    );
  }
}
