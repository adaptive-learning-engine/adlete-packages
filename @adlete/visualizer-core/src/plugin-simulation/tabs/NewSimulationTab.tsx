/* eslint-disable react/no-unescaped-entities */
import {
  Backdrop,
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Collapse,
  Container,
  FormControl,
  FormControlLabel,
  Grid,
  Input,
  InputAdornment,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
  Stack,
  Switch,
  TextField,
  Typography,
} from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Papa from 'papaparse';
import React, { useCallback, useEffect, useState } from 'react';

import { IAnalyticsResponse, ITask } from '@adlete/client-ts/Types';
import * as AnalysisRoutines from '@adlete/client-ts/routines/AnalysisRoutines';

import { useCurrentUser, useSetCurrentUser } from '../../context/CurrentUserContext';
import { useGraphQLClient } from '../../custom-hooks/useGraphQLClient';
import { useIsMounted } from '../../custom-hooks/useIsMounted';
import { useServerStatus } from '../../custom-hooks/useServerStatus';
import TooltipButton from '../../shared-components/TooltipButton';
import { getRandomLearnerId } from '../../shared-utils/activity-transforms';



interface IConfigurationsTabProps {
  onSimulationFinished?: (simulationResults: IAnalyticsResponse) => void;
}

enum ViewState {
  CONFIGURATION,
  LOADRESULTS,
  LOADING,
  RESULTSLOADED,
  NORESULTS,
}

interface IConfigurationsTabState {
  advancedMode: boolean;
  viewState: ViewState;
  backDropVisible: boolean;
}

interface IErrorState {
  statusCode: number;
  errorMessage: string;
}

interface IBaseSimulationForm {
  agentBehavior: string;
  saveToDB: boolean;
  fetchUser: boolean
  agentId?: string;
  initialScalarBeliefSetId: string;
}

interface INormalSimulationForm {
  simulationSteps: number;
  activityString: string;
  staticDiff: boolean;
  difficulty: number;
}

interface IAdvancesSimulationForm {
  fileContent: ITask[];
  minDifficulty: number;
  maxDifficulty: number;
  difficultyStep: number;
  maxCorrect: number;
  csvFile?: File;
}

const columns: GridColDef[] = [
  { field: 'name', headerName: 'Task Name', flex: 1 },
  { field: 'loId', headerName: 'Learning Objective', flex: 1 },
  { field: 'difficulty', headerName: 'Difficulty', flex: 0.5 },
];

export const NewSimulationTab: React.FunctionComponent<IConfigurationsTabProps> = ({ onSimulationFinished }: IConfigurationsTabProps) => {
  const behaviourString = [
    'random learner',
    'always correct',
    'always incorrect',
    'simulate real learner',
    'linear learner ASC',
    'linear learner DEC',
    'quadratic learner ASC',
    'quadratic learner DEC',
  ];
  const initialScalarBeliefSetId_Values = ['level_beginner', 'level_medium', 'level_advanced'];
  const agentBehavior_Values = ['random', 'correct', 'incorrect', 'real', 'linearAsc', 'linearDec', 'quadraticAsc', 'quadraticDec'];

  const currentUser = useCurrentUser()
  const setCurrentUser = useSetCurrentUser();
  const isCompMounted = useIsMounted()
  const [isOnline, isAuthorized] = useServerStatus()
  const graphQLClient = useGraphQLClient()
  const [tabState, setTabState] = useState<IConfigurationsTabState>({
    advancedMode: false,
    viewState: ViewState.CONFIGURATION,
    backDropVisible: false,
  });

  const [baseFormState, setBaseFormState] = useState<IBaseSimulationForm>({
    initialScalarBeliefSetId: 'level_beginner',
    agentBehavior: 'random',
    saveToDB: false,
    fetchUser: false,
    agentId: getRandomLearnerId(),
  });
  
  const [normalFormState, setNormalFormState] = useState<INormalSimulationForm>({
    simulationSteps: 1,
    activityString: '',
    staticDiff: false,
    difficulty: 1,
  });

  const [advancedFormState, setAdvancedFormState] = useState<IAdvancesSimulationForm>({
    fileContent: [],
    minDifficulty: 0,
    maxDifficulty: 1,
    difficultyStep: 0.1,
    maxCorrect: 3,
  });
  const [serverState, setServerState] = useState<IErrorState>({
    statusCode: 200,
    errorMessage: '',
  });

  const [errorState, setErrorState] = useState<string>(null)

  useEffect(() => {
    if(isOnline) {
      if(isAuthorized) {
        setServerState({
          statusCode: 200,
          errorMessage: ""
        })
      } else {
        setServerState({
          statusCode: 401,
          errorMessage: "Not Authorized"})
      }
    } else {
      setServerState({
        statusCode: 404,
        errorMessage: "Server not reachable"
      })
    }

  }, [isAuthorized, isOnline])

  const validateFields = useCallback((): boolean => {
    if (tabState.advancedMode) {
      return (
        advancedFormState.minDifficulty < advancedFormState.maxDifficulty &&
        advancedFormState.difficultyStep < advancedFormState.maxDifficulty - advancedFormState.minDifficulty &&
        advancedFormState.fileContent.length !== 0
      );
    }
    return true;
  }, [advancedFormState, tabState])

  const simulateLearner = useCallback(async (form: IBaseSimulationForm, normalForm: INormalSimulationForm, advancedForm: IAdvancesSimulationForm) => {
    if (!validateFields()) {
      return setErrorState('Invalid Form Input: Please check your inputs');
    }

    // TODO: Look up if it works
    setTabState((previousState) => ({ ...previousState, backDropVisible: true }));
    let responseSimulateLearner;
    if (tabState.advancedMode) {
      responseSimulateLearner = await AnalysisRoutines.advancedSimulation(
        graphQLClient,
        form.fetchUser ? null : form.initialScalarBeliefSetId,
        advancedForm.fileContent,
        advancedForm.maxDifficulty,
        advancedForm.minDifficulty,
        advancedForm.difficultyStep,
        form.agentBehavior,
        form.saveToDB,
        (form.fetchUser || form.saveToDB) ? form.agentId : null,
        advancedForm.maxCorrect,
        currentUser.clientData.idCipher
      );
    } else {
      let activities = null;
      if (normalForm.activityString != '') {
        activities = normalForm.activityString.split(',').map((value) => value.trim());
      }
      responseSimulateLearner = await AnalysisRoutines.simulateLearner(
        graphQLClient,
        form.fetchUser ? null : form.initialScalarBeliefSetId,
        normalFormState.simulationSteps,
        form.agentBehavior,
        form.saveToDB,
        (form.fetchUser || form.saveToDB) ? form.agentId : null,
        activities,
        normalForm.staticDiff ? normalForm.difficulty : null
      );
    }

    if (isCompMounted) {
      let errorMessage = '';

      if (responseSimulateLearner.statusInfo.statusCode === 200) {
        onSimulationFinished(responseSimulateLearner.analyticsData);
      } else {
        errorMessage = responseSimulateLearner.statusInfo.statusDescription;
        setTabState((previousState) => ({ ...previousState, backDropVisible: false }));
      }

      setServerState({ errorMessage, statusCode: responseSimulateLearner.statusInfo.statusCode });
    }
  }, [currentUser, graphQLClient, isCompMounted, normalFormState, onSimulationFinished, tabState, validateFields])

  function createTableFromFile(csvFile: File) {
    Papa.parse<ITask>(csvFile, {
      header: true,
      dynamicTyping: true,
      complete: (results) => {
        let min = 0;
        let max = 0;
        results.data.forEach((element) => {
          if (element.difficulty < min) {
            min = element.difficulty;
          }

          if (element.difficulty > max) {
            max = element.difficulty;
          }
        });
        if (min < 0) {
          throw new RangeError('Task Difficulty is below zero.');
        }
        setAdvancedFormState({
          ...advancedFormState,
          fileContent: results.data,
          minDifficulty: min,
          maxDifficulty: max,
        });
      },
      error: () => {
        setErrorState('Could not parse CSV file: ');
      },
    });
  }

  function backDrop(value: boolean): React.ReactNode {
    const backDropElement = (
      <Grid>
        <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }} open={value}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    );
    return backDropElement;
  }

  function serverStatusTextHelper(): React.ReactNode {
    let TextHelper;
    if (serverState.statusCode === 200) {
      TextHelper = (
        <Box component="div" display="inline">
          <span>server status: </span>
          <span style={{ color: 'green' }}> Connected </span>
        </Box>
      );
    } else {
      TextHelper = (
        <Box component="div" display="inline">
          <span>server status: </span>
          <span style={{ color: 'red' }}> {serverState.errorMessage} </span>
        </Box>
      );
    }
    return TextHelper;
  }

  function createNormalForm(): React.ReactNode {
    return (
      <>
        <TextField
          required={!tabState.advancedMode}
          id="simulationSteps"
          error={errorState !== null}
          label="simulationSteps"
          type="number"
          InputProps={{
            inputProps: { min: 1 },
            endAdornment: (
              <InputAdornment position="end">
                <TooltipButton message="The number of activities that should be simulated" />
              </InputAdornment>
            ),
          }}
          value={normalFormState.simulationSteps}
          onChange={(e) => {
            setNormalFormState({ ...normalFormState, simulationSteps: Number(e.target.value) });
          }}
        />
        <TextField
          id="Activities"
          error={errorState !== null}
          label="Activities"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <TooltipButton
                  message={
                    <>
                      <Typography>Configure the set of trainable activities e.g:</Typography>
                      <Typography>"activityPw8_1, activityTw1_1"</Typography>
                      <Typography>or leave empty to train with all activities</Typography>
                    </>
                  }
                />
              </InputAdornment>
            ),
          }}
          value={normalFormState.activityString}
          onChange={(e) => {
            setNormalFormState({ ...normalFormState, activityString: e.target.value });
          }}
        />
        <Box flex="inline">
          <FormControlLabel
            label={
              <Box flex="inline">
                <span>Set constant difficulty</span>
                <TooltipButton message="All activities will have the same difficulty as given" />
              </Box>
            }
            control={
              <Checkbox
                value={normalFormState.staticDiff}
                onChange={(e, checked) => {
                  setNormalFormState({ ...normalFormState, staticDiff: checked });
                }}
              ></Checkbox>
            }
          ></FormControlLabel>
          <Collapse in={normalFormState.staticDiff}>
            <TextField
              type="number"
              value={normalFormState.difficulty}
              InputProps={{ inputProps: { min: 0, max: 1, step: 0.1 } }}
              label="Difficulty"
              error={errorState !== null}
              onChange={(e) => {
                setNormalFormState({ ...normalFormState, difficulty: parseFloat(e.target.value) });
              }}
            ></TextField>
          </Collapse>
        </Box>
      </>
    );
  }

  function createAdvancedForm(): React.ReactNode {
    return (
      <>
        <Box flex="inline">
          <Input
            error={errorState !== null}
            type="file"
            value={advancedFormState.csvFile}
            onChange={(e) => {
              createTableFromFile((e.currentTarget as HTMLInputElement).files[0]);
            }}
            inputProps={{ accept: '.csv' }}
          ></Input>
          <TooltipButton
            message={
              <>
                <Typography sx={{ mb: 1 }}>Insert a csv file with the following structure:</Typography>
                <Typography>name,loId,difficulty</Typography>
                <Typography>task1,activityTw1_1,1</Typography>
                <Typography sx={{ mt: 1 }}>
                  "name" is the name of the task, "loId" is the activity name, "difficulty" is the difficulty of the task. Add more tasks as
                  you see fit.
                </Typography>
              </>
            }
          />
        </Box>
        {advancedFormState.fileContent.length == 0 ? null : (
          <Box>
            <Box flex="inline">
              <TextField
                sx={{ mr: 3 }}
                error={errorState !== null}
                type="number"
                value={advancedFormState.minDifficulty}
                label="Min Difficulty"
                InputProps={{ inputProps: { min: 0, max: 999 } }}
                onChange={(e) => {
                  setAdvancedFormState({ ...advancedFormState, minDifficulty: parseFloat(e.target.value) });
                }}
              ></TextField>
              <TextField
                sx={{ mr: 3 }}
                error={errorState !== null}
                type="number"
                value={advancedFormState.maxDifficulty}
                label="Max Difficulty"
                InputProps={{ inputProps: { min: 0, max: 999 } }}
                onChange={(e) => {
                  setAdvancedFormState({ ...advancedFormState, maxDifficulty: parseFloat(e.target.value) });
                }}
              ></TextField>
              <TextField
                type="number"
                error={errorState !== null}
                value={advancedFormState.difficultyStep}
                label="Step Size"
                InputProps={{ inputProps: { min: 0, max: 999, step: 0.1 } }}
                onChange={(e) => {
                  setAdvancedFormState({ ...advancedFormState, difficultyStep: parseFloat(e.target.value) });
                }}
              ></TextField>
              <TextField
                type="number"
                error={errorState !== null}
                value={advancedFormState.maxCorrect}
                label="Max. Task answered correctly"
                InputProps={{ inputProps: { min: 1, max: 5, step: 1 } }}
                onChange={(e) => {
                  setAdvancedFormState({ ...advancedFormState, maxCorrect: parseInt(e.target.value) });
                }}
              ></TextField>
              <TooltipButton
                message={
                  <>
                    <Typography>Set the constrainst for the task set:</Typography>
                    <Typography>
                      e.g. Min Difficulty = 0, Max Difficulty = 3, Step Size=1 will result in the possible task difficulties of 1,2 and 3
                    </Typography>
                    <Typography>
                      "Max. Task answered correctly" describes how often a task should be proposed to the user and answered correctly before
                      it is removed from the pool.
                    </Typography>
                  </>
                }
              />
            </Box>
            <Box sx={{ height: 56 + 5 * 52 + 53, width: '100%', mt: 3 }}>
              <DataGrid
                columns={columns}
                rows={advancedFormState.fileContent.map((element, index) => {
                  return { ...element, id: index };
                })}
                initialState={{
                  pagination: {
                    paginationModel: {
                      pageSize: 10
                    },
                  },
                }}
              ></DataGrid>
            </Box>
          </Box>
        )}
      </>
    );
  }

  function createNewSimulationView(): React.ReactNode {
    return (
      <Box sx={{ mt: 3 }}>
        {/* <LinearProgress color={this.state.statusCode !== 200 ? 'error' : 'success'} /> */}
        <Container maxWidth="md">
          <FormControlLabel
            label={
              <Box flex="inline">
                <span>Advanced Mode</span>
                <TooltipButton message="Advanced Mode allows to use a csv-File with Task data" />
              </Box>
            }
            control={
              <Switch value={tabState.advancedMode} onChange={(e, checked) => setTabState({ ...tabState, advancedMode: checked })}></Switch>
            }
          ></FormControlLabel>
          <Box sx={{ mt: 3 }}>
            <Stack spacing={2}>
              <TextField
                fullWidth
                id="host"
                error={serverState.statusCode !== 200}
                value={currentUser.host ?? ""}
                label="host"
                margin="none"
                onChange={(e) => {
                  setCurrentUser({ ...currentUser, host: e.target.value });
                }}
              />
              <Box component="div" display="inline">
                {serverStatusTextHelper()}
              </Box>
              <br />
            <Box flex="inline">
                <FormControl>
                  <InputLabel variant="standard">Learner Behaviour</InputLabel>
                  <Select
                    required
                    id="agentBehavior"
                    error={errorState !== null}
                    label="learner Behavior"
                    value={baseFormState.agentBehavior}
                    onChange={(e) => {
                      setBaseFormState({ ...baseFormState, agentBehavior: e.target.value });
                    }}
                  >
                    {agentBehavior_Values.map((element, index) => (
                      <MenuItem key={element} value={element}>
                        {behaviourString[index]}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <TooltipButton message="Describes the behaviour of the simulator" />
              </Box>
              <Box flex="inline">
                <FormControlLabel
                    id="fetchUser"
                    control={
                      <Switch
                        checked={baseFormState.fetchUser}
                        onChange={(e) => {
                          setBaseFormState({ ...baseFormState, fetchUser: e.target.checked });
                        }}
                      />
                    }
                    label={
                      <Box flex="inline">
                        <span>Use an existing learner</span>
                        <TooltipButton message="Fetches the user with the given learner ID. The learner will be modified if 'Store Results in Database' is active." />
                      </Box>
                    }
                  />
                {currentUser.clientData?.idCipher == null ? 
                    <FormControlLabel
                    id="storeInDb"
                    control={
                      <Switch
                        checked={baseFormState.saveToDB}
                        onChange={(e) => {
                          setBaseFormState({ ...baseFormState, saveToDB: e.target.checked });
                        }}
                      />
                    }
                    label={
                      <Box flex="inline">
                        <span>Store Results in Database</span>
                        <TooltipButton message="Should the results be stored in the Database" />
                      </Box>
                    }
                  />
                : null}
              </Box>
              <Collapse in={!baseFormState.fetchUser} timeout="auto" unmountOnExit>
                <Box flex="inline">
                  <FormControl>
                    <InputLabel sx={{ ml: 2 }} variant="standard">
                      initialScalarBeliefSetId
                    </InputLabel>
                    <Select
                      required
                      id="initialScalarBeliefSetId"
                      error={errorState !== null}
                      label="initial Scalar Belief Set"
                      value={baseFormState.initialScalarBeliefSetId}
                      onChange={(e) => {
                        setBaseFormState({ ...baseFormState, initialScalarBeliefSetId: e.target.value })}}
                    >
                      {initialScalarBeliefSetId_Values.map((element) => (
                        <MenuItem key={element} value={element}>
                          {element}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <TooltipButton message="Describes the learners initial skill level" />
                </Box>
              </Collapse>
              <Collapse in={baseFormState.saveToDB || baseFormState.fetchUser} timeout="auto" unmountOnExit>
                <TextField
                  id="agentid"
                  error={errorState !== null}
                  label="Learner Id"
                  value={baseFormState.agentId}
                  onChange={(e) => {
                    setBaseFormState({ ...baseFormState, agentId: e.target.value })}}
                />
              </Collapse>
              {tabState.advancedMode ? createAdvancedForm() : createNormalForm()}
              {errorState && (
                <Typography color="error" variant="body2">
                  {errorState}
                </Typography>
              )}
              <Button
                variant="contained"
                color="primary"
                style={{ margin: '10px' }}
                onClick={() => {
                  simulateLearner(baseFormState, normalFormState, advancedFormState);
                }}
              >
                Submit
              </Button>
              {backDrop(tabState.backDropVisible)}
            </Stack>
          </Box>
        </Container>
        {/* <Typography color="error">
          <span>{this.state.statusCode != 200 ? this.state.errorMessage : ''}</span>
        </Typography> */}
      </Box>
    );
  }

  function renderViewState(): React.ReactElement {
    switch (tabState.viewState) {
      case ViewState.LOADING:
        return (
          <div style={{ marginTop: 20 }}>
            <LinearProgress />
          </div>
        );
      case ViewState.CONFIGURATION:
        return <Grid>{createNewSimulationView()}</Grid>;
      default:
        throw new Error(`Unknown viewState ${tabState.viewState}`);
    }
  }

  return renderViewState();
};
