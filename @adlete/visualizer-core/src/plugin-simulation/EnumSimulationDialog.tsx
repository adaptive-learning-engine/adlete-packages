export enum SimulationDialog {
  None,
  NewSimulation,
  LoadSimulation,
  SaveSimulationResults,
}
