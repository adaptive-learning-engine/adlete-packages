import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { SaveSimulationResultsTab } from '../tabs/SaveSimulationResultsTab';

export interface NewSimulationDialogProps {
  analyticsResponse: IAnalyticsResponse;
  onModelSaved: () => void;
  onClose: () => void;
}

export function SaveSimulationDialog(props: NewSimulationDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Save Simulation Results</DialogTitle>
        <DialogContent>
          <SaveSimulationResultsTab analyticsResponse={props.analyticsResponse} onModelSaved={props.onModelSaved} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
