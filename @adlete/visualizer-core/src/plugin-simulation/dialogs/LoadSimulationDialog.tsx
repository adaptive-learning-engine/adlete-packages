import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { LoadSimulationTab } from '../tabs/LoadSimulationTab';

interface ILoadSimulationDialogProps {
  onClose: () => void;
  onSimulationLoaded: (Simulation: IAnalyticsResponse) => void;
}

export function LoadSimulationDialog(props: ILoadSimulationDialogProps): React.ReactElement {
  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>Load Simulation</DialogTitle>
        <DialogContent>
          <LoadSimulationTab onSimulationLoaded={props.onSimulationLoaded} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
