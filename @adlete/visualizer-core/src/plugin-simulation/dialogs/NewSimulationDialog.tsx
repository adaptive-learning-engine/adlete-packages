import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';

import { NewSimulationTab } from '../tabs/NewSimulationTab';

export interface NewSimulationDialogProps {
  onSimulationFinished: (simulationResults: IAnalyticsResponse) => void;
  onClose: () => void;
}

export function NewSimulationDialog(props: NewSimulationDialogProps): React.ReactElement {
  const onSimulationFinished = (simResults: IAnalyticsResponse) => {
    props.onSimulationFinished(simResults);
  };

  return (
    <div>
      <Dialog maxWidth="xl" fullWidth open onClose={props.onClose}>
        <DialogTitle>New Simulation</DialogTitle>
        <DialogContent>
          <NewSimulationTab onSimulationFinished={onSimulationFinished} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
