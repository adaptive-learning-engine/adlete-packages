import { ILayout, IPanelType, LayoutManager } from '@fki/editor-core/layout/LayoutManager';
import { Registry } from '@fki/editor-core/utils/Registry';
import type { IPlugin, IPluginMeta } from '@fki/plugin-system/specs';

export interface IWithLayoutManager {
  layoutManager: LayoutManagerPlugin;
}

export const LayoutManagerMeta: IPluginMeta<IWithLayoutManager, 'layoutManager'> = {
  id: 'layoutManager',
};

export class LayoutManagerPlugin extends LayoutManager implements IPlugin<IWithLayoutManager> {
  meta: IPluginMeta<IWithLayoutManager, 'layoutManager'> = LayoutManagerMeta;

  public constructor(builtinLayouts: Record<string, ILayout>) {
    const registry = new Registry<Record<string, IPanelType>>();
    super(registry, builtinLayouts);
  }

  public initialize(): Promise<void> {
    return super.initialize();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
