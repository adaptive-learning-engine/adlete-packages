import { Box, Button, Paper, SxProps, TextField, Theme, Typography } from "@mui/material";
import * as React from "react";
import { useNavigate } from "react-router-dom";

import { createClient } from "@adlete/client-ts/GraphQLClientExtension";
import { loginUser } from "@adlete/client-ts/routines/AuthentificationRoutines";

import { useCurrentUser, useSetCurrentUser } from "../../context/CurrentUserContext";
import { useServerStatus } from "../../custom-hooks/useServerStatus";
 
const boxSx: SxProps<Theme> = {
    width: "320px",
    marginRight: "auto",
    marginLeft: "auto",
}

const formElementsSx: SxProps<Theme> = {
    width: "100%",
    marginTop: "1em",
    marginBottom: "1em"
}

const formContainerSx: SxProps<Theme> = {
    ...boxSx,
    padding: "10px",
    border: "solid"
}

const headerSx: SxProps<Theme> = {
    ...boxSx,
    display: "block",
    marginTop: "5%",
    marginBottom: "20px"
}
export const Login: React.FunctionComponent = () => {
    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [error, setError] = React.useState(false);
    const user = useCurrentUser();
    const setUser = useSetCurrentUser()
    const [isOnline] = useServerStatus()
    const navigate = useNavigate()

    
    const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(username !== "" || password !== "")
        try {
            const {tokens} = await loginUser(createClient(user.host), username, password)
            const currentUser = {host: user.host, token: tokens.accessToken, refreshToken: tokens.refreshToken}
            const redirect = ".."
            setUser(currentUser)
            navigate(redirect, {relative: "path"}) //Todo for default if nothing before
        } catch (error) {
            setError(true)
        }
    }



    return ( 
        <div>
            <Box sx={headerSx}>
                <div id="icon"></div>
                <Typography variant="h4">Login Visualizer</Typography>
            </Box>
            <Paper sx={formContainerSx}>
                <form onSubmit={onSubmit} style={{display: "block"}}>
                    <TextField sx={formElementsSx} type="text" label="Host" defaultValue={user.host ?? ""} error={!isOnline} onBlur={(e) => setUser({...user, host: e.target.value})}/>
                    {isOnline ? null : (<Box component="div" display="inline" fontFamily={"roboto"}>
                                            <span>server status: </span>
                                            <span style={{ color: 'red' }}>Server not reachable</span>
                                        </Box>
                    )}
                    <TextField sx={formElementsSx} type="text" label="Username" value={username} error={error} onChange={e => setUsername(e.target.value)}></TextField>
                    <TextField sx={formElementsSx} type="password" label="Password" value={password} error={error} onChange={e => setPassword(e.target.value)}></TextField>
                    <Button sx={formElementsSx} color="primary" type="submit">Submit</Button>
                </form>
                {error ? <p>Login was invalid</p> : null}
            </Paper>
        </div>
    );
}