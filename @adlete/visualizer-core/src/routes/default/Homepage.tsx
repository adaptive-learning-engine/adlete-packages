import React, { FunctionComponent } from "react";
import { Navigate } from "react-router-dom";

import { useCurrentUser } from "../../context/CurrentUserContext";

type HomepageProps = {
    children: string | JSX.Element | JSX.Element[] | (() => JSX.Element)
  }
 
export const Homepage: FunctionComponent<HomepageProps> = ({children}: HomepageProps) => {
    const user = useCurrentUser()

    const displayHomepage = (): JSX.Element => {
        if(user.token) {
            return <>{children}</>
        }
        return <Navigate to={"login"} replace={false}/>
    }

    return displayHomepage()
}