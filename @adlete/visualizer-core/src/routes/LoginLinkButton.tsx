import { useTheme } from "@mui/material/styles";
import React,{ FunctionComponent } from "react";
import { Link } from "react-router-dom";

import { SameOriginWrapper } from "../shared-components/SameOriginWrapper";
interface ILoginLinkProps {
    style?: React.CSSProperties
} 

export const LoginLinkButton: FunctionComponent<ILoginLinkProps> = ({style}: ILoginLinkProps) => {
    const theme = useTheme()
    return (
    <SameOriginWrapper>
        <Link to={"login"} style={{...style, color: theme.palette.primary.main}}>Login</Link>
    </SameOriginWrapper>);
}
