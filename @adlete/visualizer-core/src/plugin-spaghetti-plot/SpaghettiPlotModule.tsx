import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import {
  Autocomplete,
  Box,
  Button,
  CircularProgress,
  DialogActions,
  DialogContent,
  DialogTitle,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import TextField from '@mui/material/TextField';
import { TransitionProps } from '@mui/material/transitions';
import { DataGrid, GridColDef, GridRowSelectionModel } from '@mui/x-data-grid';
import * as React from 'react';

import { SpaghettiPlotView } from './SpaghettiPlotView';

interface Learner {
  id: string;
}
export function SpaghettiPlotModule(): any {
  const Transition = React.forwardRef(
    (
      props: TransitionProps & {
        children: React.ReactElement;
      },
      ref: React.Ref<unknown>
      // eslint-disable-next-line react/jsx-props-no-spreading
    ) => <Slide direction="up" ref={ref} {...props} />
  );
  Transition.displayName = "Transition"
  const [selectionModel, setSelectionModel] = React.useState<GridRowSelectionModel>([]);

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const LearnersDialog = () => {
    // call the query to get ids here 
    const [learnersIds, setLearnersId] = React.useState<Learner[]>([
      { id: 'anel' },
      { id: 'flo' },
      { id: 'arithmetic_246562' },
      { id: 'arithmetic_35656' },
      { id: 'arithmetic_22373' },
      { id: 'arithmetic_5373' },
    ]);
    // this is the one that the user sees 
    const [learnersIdsToShow, setLearnersIdsToShow] = React.useState<Learner[]>(learnersIds);

    const LearnerSelectSearch = () => {
      const [open, setOpen] = React.useState(false);
      const [options, setOptions] = React.useState<readonly Learner[]>([]);
      const loading = open && options.length === 0;

      React.useEffect(() => {
        let active = true;

        if (!loading) {
          return undefined;
        }

        (async () => {
          if (active) {
            setOptions([...learnersIds]);
          }
        })();

        return () => {
          active = false;
        };
      }, [loading]);

      React.useEffect(() => {
        if (!open) {
          setOptions([]);
        }
      }, [open]);
      return (
        <Autocomplete
          id="asynchronous-demo"
          sx={{ width: 300 }}
          open={open}
          onOpen={() => {
            setOpen(true);
          }}
          onClose={() => {
            setOpen(false);
          }}
          isOptionEqualToValue={(option, value) => option.id === value.id}
          getOptionLabel={(option) => option.id}
          options={options}
          loading={loading}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Learners"
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          )}
          onChange={(event, value) => {
            setLearnersIdsToShow([value as undefined as Learner]);
          }}
        />
      );
    };

    let rows: readonly any[] = [];

    if (learnersIdsToShow) {
      rows = learnersIdsToShow.map((learner) => ({ id: learner.id }));
    } else {
      rows = learnersIds.map((learner) => ({ id: learner.id }));
    }
    const columns: GridColDef[] = [{ headerName: 'Learner ID', field: 'id', width: 160 }];
    const [selectionModelInDialog, setSelectionModelInDialog] = React.useState(selectionModel);

    return (
      <div>
        <Dialog open={open} maxWidth="xl" fullWidth onClose={handleClose} TransitionComponent={Transition}>
          <DialogTitle>Load learner</DialogTitle>
          <DialogContent>
            <Box sx={{ mt: '10px', mb: '10px' }}>
              <LearnerSelectSearch />
            </Box>
            <Box sx={{ height: 600 }}>
              <DataGrid
                onRowSelectionModelChange={(newSelectionModel) => {
                  setSelectionModelInDialog(newSelectionModel);
                }}
                rowSelectionModel={selectionModelInDialog}
                rows={rows}
                columns={columns}
                checkboxSelection
                disableRowSelectionOnClick
              />
            </Box>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                setSelectionModel(selectionModelInDialog);
                handleClose();
              }}
            >
              close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  };

  return (
    <Box sx={{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
      <Toolbar>
        <Tooltip title="Add new learner">
          <Button color="primary" onClick={handleClickOpen}>
            <Typography variant="button">Learners&emsp;</Typography> <PersonAddAltIcon sx={{ mb: '4px' }} />
          </Button>
        </Tooltip>
      </Toolbar>
      <SpaghettiPlotView />
      <LearnersDialog />
    </Box>
  );
}
