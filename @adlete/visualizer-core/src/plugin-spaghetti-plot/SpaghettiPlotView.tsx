import React from 'react'

import { SpaghettiPlotGraph } from './SpaghettiPlotGraph'

export  function SpaghettiPlotView(): any {
  return (
    <SpaghettiPlotGraph width={500} height={500} />
  )
}
