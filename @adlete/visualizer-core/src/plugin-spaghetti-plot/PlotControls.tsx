/* eslint-disable jsx-a11y/accessible-emoji */
/* eslint-disable jsx-a11y/label-has-associated-control */
import { curveCardinal, curveLinear, curveStep } from '@visx/curve';
import { GlyphCross, GlyphDot, GlyphStar } from '@visx/glyph';
import cityTemperature, { CityTemperature } from '@visx/mock-data/lib/mocks/cityTemperature';
import { PatternLines } from '@visx/pattern';
import { AnimationTrajectory } from '@visx/react-spring/lib/types';
import { darkTheme, lightTheme, XYChartTheme } from '@visx/xychart';
import { RenderTooltipGlyphProps } from '@visx/xychart/lib/components/Tooltip';
import { GlyphProps } from '@visx/xychart/lib/types';
import React, { useCallback, useMemo, useState } from 'react';

import { getAnimatedOrUnanimatedComponents } from './getAnimatedOrUnanimatedComponents';
import { userPrefersReducedMotion } from './userPrefersReducedMotion';

const dateScaleConfig = { type: 'band', paddingInner: 0.3 } as const;
const temperatureScaleConfig = { type: 'linear' } as const;
const numTicks = 4;
const data = cityTemperature.slice(225, 275);
const dataMissingValues = data.map((d, i) =>
  i === 10 || i === 11 ? { ...d, 'San Francisco': 'nope', 'New York': 'notanumber', Austin: 'null' } : d
);
const dataSmall = data.slice(0, 15);
const dataSmallMissingValues = dataMissingValues.slice(0, 15);
const getDate = (d: CityTemperature) => d.date;
const getSfTemperature = (d: CityTemperature) => Number(d['San Francisco']);
const getNegativeSfTemperature = (d: CityTemperature) => -getSfTemperature(d);
const getNyTemperature = (d: CityTemperature) => Number(d['New York']);
const getAustinTemperature = (d: CityTemperature) => Number(d.Austin);
const defaultAnnotationDataIndex = 13;
const selectedDatumPatternId = 'xychart-selected-datum';
type Accessor = (d: CityTemperature) => number | string;

interface Accessors {
  'San Francisco': Accessor;
  'New York': Accessor;
  Austin: Accessor;
}

type DataKey = keyof Accessors;

type SimpleScaleConfig = { type: 'band' | 'linear'; paddingInner?: number };

type ProvidedProps = {
  accessors: {
    x: Accessors;
    y: Accessors;
    date: Accessor;
  };
  animationTrajectory?: AnimationTrajectory;
  annotationDataKey: DataKey | null;
  annotationDatum?: CityTemperature;
  annotationLabelPosition: { dx: number; dy: number };
  annotationType?: 'line' | 'circle';
  colorAccessorFactory: (key: DataKey) => (d: CityTemperature) => string | null;
  config: {
    x: SimpleScaleConfig;
    y: SimpleScaleConfig;
  };
  curve: typeof curveLinear | typeof curveCardinal | typeof curveStep;
  data: CityTemperature[];
  editAnnotationLabelPosition: boolean;
  numTicks: number;
  setAnnotationDataIndex: (index: number) => void;
  setAnnotationDataKey: (key: DataKey | null) => void;
  setAnnotationLabelPosition: (position: { dx: number; dy: number }) => void;
  renderGlyph: React.FC<GlyphProps<CityTemperature>>;
  renderTooltipGlyph: React.FC<RenderTooltipGlyphProps<CityTemperature>>;
  showVerticalCrosshair: boolean;
  enableTooltipGlyph: boolean;
  sharedTooltip: boolean;
  stackOffset?: 'wiggle' | 'expand' | 'diverging' | 'silhouette';
  theme: XYChartTheme;
  xAxisOrientation: 'top' | 'bottom';
  yAxisOrientation: 'left' | 'right';
} & ReturnType<typeof getAnimatedOrUnanimatedComponents>;

type ControlsProps = {
  children: (props: ProvidedProps) => React.ReactNode;
};

export function PlotControls({ children }: ControlsProps) {
  const [useAnimatedComponents] = useState(!userPrefersReducedMotion());
  const [theme] = useState<XYChartTheme>(lightTheme);
  const [animationTrajectory] = useState<AnimationTrajectory | undefined>('center');
  const [xAxisOrientation] = useState<'top' | 'bottom'>('bottom');
  const [yAxisOrientation] = useState<'left' | 'right'>('left');
  const [renderHorizontally] = useState(false);
  const [annotationDataKey, setAnnotationDataKey] = useState<ProvidedProps['annotationDataKey']>(null);
  const [annotationType] = useState<ProvidedProps['annotationType']>('line');
  const [showVerticalCrosshair] = useState(true);
  const [stackOffset] = useState<ProvidedProps['stackOffset']>();
  const [editAnnotationLabelPosition] = useState(false);
  const [annotationLabelPosition, setAnnotationLabelPosition] = useState({ dx: -40, dy: -20 });
  const [annotationDataIndex, setAnnotationDataIndex] = useState(defaultAnnotationDataIndex);
  const [negativeValues] = useState(false);
  const [fewerDatum] = useState(false);
  const [missingValues] = useState(false);
  const [glyphComponent] = useState<'star' | 'cross' | 'circle' | '🍍'>('circle');
  const [curveType] = useState<'linear' | 'cardinal' | 'step'>('cardinal');
  const [sharedTooltip] = useState(true);
  const glyphOutline = theme.gridStyles.stroke;
  const renderGlyph = useCallback(
    ({ size, color, onPointerMove, onPointerOut, onPointerUp }: GlyphProps<CityTemperature>) => {
      const handlers = { onPointerMove, onPointerOut, onPointerUp };

      // eslint-disable-next-line react/jsx-props-no-spreading
      return <GlyphDot stroke={glyphOutline} fill={color} r={size / 2} {...handlers} />;
    },
    [glyphComponent, glyphOutline]
  );
  const [enableTooltipGlyph] = useState(true);
  const [tooltipGlyphComponent] = useState('circle');
  const renderTooltipGlyph = useCallback(
    ({ x, y, size, color, onPointerMove, onPointerOut, onPointerUp, isNearestDatum }: RenderTooltipGlyphProps<CityTemperature>) => {
      const handlers = { onPointerMove, onPointerOut, onPointerUp };

      // eslint-disable-next-line react/jsx-props-no-spreading
      return <GlyphDot left={x} top={y} stroke={glyphOutline} fill={color} r={size} {...handlers} />;
    },
    [tooltipGlyphComponent, glyphOutline]
  );
  // for series that support it, return a colorAccessor which returns a custom color if the datum is selected
  const colorAccessorFactory = useCallback(
    (dataKey: DataKey) => (d: CityTemperature) =>
      annotationDataKey === dataKey && d === data[annotationDataIndex] ? `url(#${selectedDatumPatternId})` : null,
    [annotationDataIndex, annotationDataKey]
  );

  const accessors = useMemo(
    () => ({
      x: {
        // eslint-disable-next-line no-nested-ternary
        'San Francisco': renderHorizontally ? (negativeValues ? getNegativeSfTemperature : getSfTemperature) : getDate,
        'New York': renderHorizontally ? getNyTemperature : getDate,
        Austin: renderHorizontally ? getAustinTemperature : getDate,
      },
      y: {
        // eslint-disable-next-line no-nested-ternary
        'San Francisco': renderHorizontally ? getDate : negativeValues ? getNegativeSfTemperature : getSfTemperature,
        'New York': renderHorizontally ? getDate : getNyTemperature,
        Austin: renderHorizontally ? getDate : getAustinTemperature,
      },
      date: getDate,
    }),
    [renderHorizontally, negativeValues]
  );

  const config = useMemo(
    () => ({
      x: renderHorizontally ? temperatureScaleConfig : dateScaleConfig,
      y: renderHorizontally ? dateScaleConfig : temperatureScaleConfig,
    }),
    [renderHorizontally]
  );

  // cannot snap to a stack position

  return (
    <>
      {children({
        accessors,
        animationTrajectory,
        annotationDataKey,
        annotationDatum: data[annotationDataIndex],
        annotationLabelPosition,
        annotationType,
        colorAccessorFactory,
        config,
        curve: (curveType === 'cardinal' && curveCardinal) || (curveType === 'step' && curveStep) || curveLinear,
        // eslint-disable-next-line no-nested-ternary
        data: fewerDatum ? (missingValues ? dataSmallMissingValues : dataSmall) : missingValues ? dataMissingValues : data,
        editAnnotationLabelPosition,
        numTicks,
        enableTooltipGlyph,
        renderGlyph,
        renderTooltipGlyph,
        setAnnotationDataIndex,
        setAnnotationDataKey,
        setAnnotationLabelPosition,
        showVerticalCrosshair,
        stackOffset,
        sharedTooltip,
        theme,
        xAxisOrientation,
        yAxisOrientation,
        ...getAnimatedOrUnanimatedComponents(useAnimatedComponents),
      })}
    </>
  );
}
