import { Box } from '@mui/system';
import { CityTemperature } from '@visx/mock-data/lib/mocks/cityTemperature';
import React from 'react';

import { CustomChartBackground } from './CustomChartBackground';
import { PlotControls } from './PlotControls';

export type XYChartProps = {
  width: number;
  height: number;
};

type City = 'San Francisco' | 'New York' | 'Austin';

export function SpaghettiPlotGraph({ height }: XYChartProps): any {
  return (
    <Box>
      <PlotControls>
        {({
          accessors,
          animationTrajectory,
          annotationDataKey,
          annotationDatum,
          annotationLabelPosition,
          showVerticalCrosshair,
          config,
          curve,
          data,
          editAnnotationLabelPosition,
          numTicks,
          enableTooltipGlyph,
          renderTooltipGlyph,
          setAnnotationDataIndex,
          setAnnotationDataKey,
          setAnnotationLabelPosition,
          stackOffset,
          theme,
          xAxisOrientation,
          yAxisOrientation,

          // components are animated or not depending on selection
          Annotation,
          Axis,

          Grid,
          LineSeries,
          AnnotationCircleSubject,
          AnnotationConnector,
          AnnotationLabel,
          AnnotationLineSubject,
          Tooltip,
          XYChart,
        }) => (
          <XYChart
            theme={theme}
            xScale={config.x}
            yScale={config.y}
            height={Math.min(400, height)}
            captureEvents={!editAnnotationLabelPosition}
            onPointerUp={(d) => {
              setAnnotationDataKey(d.key as 'New York' | 'San Francisco' | 'Austin');
              setAnnotationDataIndex(d.index);
            }}
          >
            <CustomChartBackground />
            <Grid
              key={`grid-${animationTrajectory}`} // force animate on update
              rows={false}
              columns={false}
              animationTrajectory={animationTrajectory}
              numTicks={numTicks}
            />

            <>
              <LineSeries dataKey="Austin" data={data} xAccessor={accessors.x.Austin} yAccessor={accessors.y.Austin} curve={curve} />
              (
              <LineSeries
                dataKey="New York"
                data={data}
                xAccessor={accessors.x['New York']}
                yAccessor={accessors.y['New York']}
                curve={curve}
              />
              )
              <LineSeries
                dataKey="San Francisco"
                data={data}
                xAccessor={accessors.x['San Francisco']}
                yAccessor={accessors.y['San Francisco']}
                curve={curve}
              />
            </>

            <Axis
              key={`time-axis-${animationTrajectory}-${true}`}
              orientation={true ? yAxisOrientation : xAxisOrientation}
              numTicks={numTicks}
              animationTrajectory={animationTrajectory}
            />
            <Axis
              key={`temp-axis-${animationTrajectory}-${true}`}
              label={
                // eslint-disable-next-line no-nested-ternary
                stackOffset == null ? 'Probability/Normalized Scalar' : stackOffset === 'expand' ? 'Fraction of total temperature' : ''
              }
              orientation={true ? xAxisOrientation : yAxisOrientation}
              numTicks={numTicks}
              animationTrajectory={animationTrajectory}
              // values don't make sense in stream graph
              tickFormat={stackOffset === 'wiggle' ? () => '' : undefined}
            />

            <Annotation
              dataKey={annotationDataKey}
              datum={annotationDatum}
              dx={annotationLabelPosition.dx}
              dy={annotationLabelPosition.dy}
              editable={editAnnotationLabelPosition}
              canEditSubject={false}
              onDragEnd={({ dx, dy }) => setAnnotationLabelPosition({ dx, dy })}
            >
              <AnnotationConnector />
              <AnnotationCircleSubject /> : <AnnotationLineSubject />
              <AnnotationLabel
                title={annotationDataKey}
                subtitle={`${annotationDatum.date}, ${annotationDatum[annotationDataKey]}°F`}
                width={135}
                backgroundProps={{
                  stroke: theme.gridStyles.stroke,
                  strokeOpacity: 0.5,
                  fillOpacity: 0.8,
                }}
              />
            </Annotation>

            <Tooltip<CityTemperature>
              renderGlyph={enableTooltipGlyph ? renderTooltipGlyph : undefined}
              showSeriesGlyphs
              showVerticalCrosshair={showVerticalCrosshair}
              renderTooltip={({ tooltipData, colorScale }) => (
                <>
                  {/** date */}
                  {(tooltipData?.nearestDatum?.datum && accessors.date(tooltipData?.nearestDatum?.datum)) || 'No date'}
                  <br />
                  <br />
                  {/** temperatures */}
                  {(
                    (true ? Object.keys(tooltipData?.datumByKey ?? {}) : [tooltipData?.nearestDatum?.key]).filter((city) => city) as City[]
                  ).map((city) => {
                    const temperature =
                      tooltipData?.nearestDatum?.datum && accessors[false ? 'x' : 'y'][city](tooltipData?.nearestDatum?.datum);

                    return (
                      <div key={city}>
                        <em
                          style={{
                            color: colorScale?.(city),
                            textDecoration: tooltipData?.nearestDatum?.key === city ? 'underline' : undefined,
                          }}
                        >
                          {city}
                        </em>{' '}
                        {temperature == null || Number.isNaN(temperature) ? '–' : `${temperature}° F`}
                      </div>
                    );
                  })}
                </>
              )}
            />
          </XYChart>
        )}
      </PlotControls>
    </Box>
  );
}
