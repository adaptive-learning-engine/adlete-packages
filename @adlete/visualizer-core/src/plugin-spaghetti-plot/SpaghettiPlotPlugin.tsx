import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';
import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '../plugin-layout/LayoutManagerPlugin';

import { SpaghettiPlotModule } from './SpaghettiPlotModule';


export interface IWithSpaghettiPlot extends IWithLayoutManager {
  SpaghettiPlot: SpaghettiPlotPlugin;
}

export const SpaghettiPlotMeta: IPluginMeta<IWithSpaghettiPlot, 'SpaghettiPlot'> = {
  id: 'SpaghettiPlot',
  dependencies: ['layoutManager'],
};

export class SpaghettiPlotPlugin implements IPlugin<IWithSpaghettiPlot> {
  public meta: IPluginMeta<IWithSpaghettiPlot, 'SpaghettiPlot'> = SpaghettiPlotMeta;

  public initialize(pluginMan: IPluginManager<IWithSpaghettiPlot>): Promise<void> {
    const layoutMan = pluginMan.get('layoutManager');
    layoutMan.panelTypes.add('SpaghettiPlot', {
      createPanel: () => <SpaghettiPlotModule />,
      title: 'SpaghettiPlot',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
