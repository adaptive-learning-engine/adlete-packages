import { Box, Button, TextField, Typography } from '@mui/material';
import FileSaver from 'file-saver';
import * as React from 'react';

interface ISaveFileContainerProps {
  onGenerateSerializedData: () => string;
  onFileSavedFinished: () => void;
}
interface ISaveFileContainerStates {
  fileName: string;
}

export class SaveFileContainer extends React.Component<ISaveFileContainerProps, ISaveFileContainerStates> {
  constructor(props: ISaveFileContainerProps) {
    super(props);
    this.state = {
      fileName: `${`${new Date().toISOString().slice(0, 10).toString()}- NewFile`}.json`,
    };
  }

  /**
   * user needs to enter the new model name which gets stored in a separate state
   * @param name
   */
  private updateModelName(fileName: string) {
    this.setState({ fileName });
  }

  /**
   * saves file to downloads folder and checks if its already used
   */
  private saveFile() {
    const results = new Blob([this.props.onGenerateSerializedData()], { type: 'text/plain;charset=utf-8' });
    const file = new File([results], this.state.fileName, { type: 'json' });
    FileSaver.saveAs(file);
    this.props.onFileSavedFinished();
  }

  render(): React.ReactElement {
    return (
      <div>
        <Box>
          <Typography variant="button" display="block" gutterBottom />

          <TextField
            id="fileName"
            value={this.state.fileName}
            label="file name"
            style={{ display: 'inline-block' }}
            variant="outlined"
            fullWidth
            onChange={(event) => this.updateModelName(event.target.value)}
          />
          <Button variant="contained" color="primary" onClick={() => this.saveFile()}>
            <span>Save</span>
          </Button>
        </Box>
      </div>
    );
  }
}
