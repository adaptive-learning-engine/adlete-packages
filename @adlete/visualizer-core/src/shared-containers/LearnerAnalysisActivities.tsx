import { ILearner } from '@adlete/framework/ILearner';
import { IObservation } from '@adlete/framework/IObservation';
import { Box, Grid, Paper } from '@mui/material';
import React from 'react';

export const LearnerAnalysisActivities: React.FunctionComponent<IObservation[]> = (observations: IObservation[]) => {
  const activities: React.ReactElement[] = observations.map((observation, index) => (
    <Grid item>
      <Paper>
        <h5>{observation.activityName}</h5>
        <Box>
          <div>Difficulty:</div>
          <div>{observation.activityDifficulty}</div>
        </Box>
        <Box>
          <div>Correctness:</div>
          <div>{observation.activityCorrectness}</div>
        </Box>
        <Box>
          <div>Activitie played:</div>
          <div>{observation.activityCorrectness}</div>
        </Box>
      </Paper>
    </Grid>
  ));
  return <Grid container>{activities}</Grid>;
};
