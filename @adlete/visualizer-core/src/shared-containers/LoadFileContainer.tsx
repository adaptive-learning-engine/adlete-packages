import { Box, Button } from '@mui/material';
import * as React from 'react';

interface ILoadFileContainerProps {
  onFileLoaded: (text: string) => void;
}

// define states for results loading after format checking here
export class LoadFileContainer extends React.Component<ILoadFileContainerProps> {
  private inputRef: React.RefObject<HTMLInputElement>;

  constructor(props: ILoadFileContainerProps) {
    super(props);
    this.inputRef = React.createRef();

    this.openFileDialog = this.openFileDialog.bind(this);
    this.getFileString = this.getFileString.bind(this);
  }

  private openFileDialog() {
    const fileLoader = this.inputRef.current;
    const e = document.createEvent('MouseEvents');
    // Use of deprecated function to satisfy TypeScript.
    e.initMouseEvent('click', true, false, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    fileLoader.dispatchEvent(e);
  }

  private getFileString = (e: any) => {
    e.preventDefault();
    const reader = new FileReader();
    reader.onload = (event) => {
      const text = event.target.result;
      this.props.onFileLoaded(text as string);
    };

    reader.readAsText(e.target.files[0]);
  };

  public render(): React.ReactNode {
    return (
      <Box>
        <input ref={this.inputRef} accept=".json" type="file" style={{ display: 'none' }} onChange={this.getFileString} />
        <Button variant="contained" color="primary" onClick={() => this.openFileDialog()}>
          Load
        </Button>
      </Box>
    );
  }
}
