import ZoomIn from '@mui/icons-material/ZoomIn';
import ZoomOut from '@mui/icons-material/ZoomOut';
import { Autocomplete, Box, Fab, Stack, Tab, Tabs, TextField } from '@mui/material';
import { Graph } from '@visx/network';
import { Zoom } from '@visx/zoom';
import { ProvidedZoom } from '@visx/zoom/lib/types';
import * as React from 'react';

import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';

import { generateLearnerAnalysisVXGraph, getNodeContent } from '../shared-components/graph/VXGraphGenerator';
import { IContentNode, IVXGraph } from '../shared-components/graph/VXGraphInterfaces';

enum LEARNERANALYSISTABID {
  LINEPLOTS = 0,
  BARPLOT = 1,
}

export interface LearnerAnalysisGraphContainerProps {
  barChartPlots: Record<string, React.ReactElement>;
  lineChartPlots: Record<string, React.ReactElement>;
  barChartPlotsSlider: React.ReactElement;
  lineChartPlotsSlider: React.ReactElement;
  competenceModel: IBayesNet;
}

interface ICompetenceGraphContainerStates {
  canvasMeasures: { width: number; height: number };
  tabId: LEARNERANALYSISTABID;
}

export class LearnerAnalysisGraphContainer extends React.Component<LearnerAnalysisGraphContainerProps, ICompetenceGraphContainerStates> {
  private zoom: ProvidedZoom<Element>;

  constructor(props: LearnerAnalysisGraphContainerProps) {
    super(props);

    this.state = {
      canvasMeasures: this.calcGraphViewWidgetSize(window.innerWidth, window.innerHeight),
      tabId: LEARNERANALYSISTABID.LINEPLOTS,
    };

    this.onTabChanged = this.onTabChanged.bind(this);
    this.onSearchRequest = this.onSearchRequest.bind(this);
    this.handlerResize = this.handlerResize.bind(this);
  }

  private onTabChanged(_event: React.SyntheticEvent<Element, Event>, newValue: LEARNERANALYSISTABID): void {
    this.setState({ tabId: newValue });
  }

  private handlerResize(): void {
    this.setState({ canvasMeasures: this.calcGraphViewWidgetSize(window.innerWidth, window.innerHeight) });
  }

  componentDidMount(): void {
    window.addEventListener('resize', this.handlerResize);
  }

  componentWillUnmount(): void {
    window.removeEventListener('resize', this.handlerResize);
  }

  private onSearchRequest(value: string | string[], graph: IVXGraph<IContentNode>) {
    if (value == null) {
      return;
    }

    if (value instanceof Array) {
      value = value[0]; // we select only the first node in the list
    }

    const node = graph.nodes.find((node) => node.name.localeCompare(value as string) === 0);

    if (node != null) {
      this.translateTo(node.x, node.y);
    }
  }

  private translateTo(x: number, y: number) {
    const center = { x: this.state.canvasMeasures.width / 2, y: this.state.canvasMeasures.height / 2 };
    const inverseCentroid = this.zoom.applyInverseToPoint(center);
    this.zoom.translate({
      translateX: inverseCentroid.x - x,
      translateY: inverseCentroid.y - y,
    });
  }

  private calcGraphViewWidgetSize(windowWidth: number, windowHeight: number): { width: number; height: number } {
    return { width: windowWidth - 35, height: windowHeight * 0.7 };
  }

  private getGraphNodeNames(graph: IVXGraph<IContentNode>): string[] {
    if (!graph) {
      return [];
    }

    return graph.nodes.map((node) => node.name);
  }

  private drawSlider() {
    if (this.state.tabId === LEARNERANALYSISTABID.LINEPLOTS) {
      return this.props.lineChartPlotsSlider;
    }
    return this.props.barChartPlotsSlider;
  }

  public render(): React.ReactNode {
    const graph: IVXGraph<IContentNode> = generateLearnerAnalysisVXGraph(
      this.props.competenceModel,
      this.state.tabId === LEARNERANALYSISTABID.BARPLOT ? this.props.barChartPlots : this.props.lineChartPlots
    );

    return (
      <Box sx={{ margin: '1em' }}>
        {this.drawSlider()}
        <Tabs value={this.state.tabId} onChange={this.onTabChanged}>
          <Tab label="Line Plots" />
          <Tab label="Bar Plot" />
        </Tabs>
        <Box sx={this.state.canvasMeasures}>
          <Zoom
            width={this.state.canvasMeasures.width}
            height={this.state.canvasMeasures.height}
            scaleXMin={1 / 15}
            scaleXMax={4}
            scaleYMin={1 / 15}
            scaleYMax={4}
          >
            {(zoom) => {
              this.zoom = zoom;
              return (
                <svg
                  width={this.state.canvasMeasures.width}
                  height={this.state.canvasMeasures.height}
                  style={{ cursor: zoom.isDragging ? 'grabbing' : 'default', display: 'block', position: 'absolute' }}
                  ref={zoom.containerRef as React.LegacyRef<SVGSVGElement>}
                >
                  <marker id="arrow" viewBox="0 -5 10 10" refX="0" refY="0" markerWidth="10" markerHeight="10" orient="auto" fill="#fff">
                    <path d="M0,-5L10,0L0,5" />
                  </marker>
                  <rect
                    width={this.state.canvasMeasures.width}
                    height={this.state.canvasMeasures.height}
                    rx={14}
                    fill="#272b4d"
                    onWheel={(event) => {
                      if (zoom.isDragging) {
                        event.preventDefault();
                      }
                      return zoom.handleWheel;
                    }}
                    onMouseDown={zoom.dragStart}
                    onMouseMove={zoom.dragMove}
                    onMouseUp={zoom.dragEnd}
                    onMouseLeave={() => {
                      if (!zoom.isDragging) {
                        return;
                      }
                      zoom.dragEnd();
                    }}
                  />
                  <g transform={zoom.toString()}>
                    <Graph<{ source: IContentNode; target: IContentNode }>
                      graph={graph}
                      nodeComponent={getNodeContent}
                      linkComponent={({ link: { source, target } }) => (
                        <polyline
                          points={`${source.x},${source.y} ${(source.x + target.x) / 2},${(source.y + target.y) / 2}  ${target.x},${
                            target.y
                          }`}
                          pathLength={100}
                          strokeWidth={2}
                          stroke="#999"
                          strokeOpacity={0.6}
                          markerMid="url(#arrow)"
                        />
                      )}
                    />
                  </g>
                </svg>
              );
            }}
          </Zoom>
          <Stack mt={3} direction="row" justifyContent="space-between" alignItems="flex-start" spacing={0.5}>
            <Box sx={{ zIndex: '1' }}>
              <Autocomplete
                disabled={false}
                size="small"
                sx={{ minWidth: 300, bgcolor: 'white' }}
                options={this.getGraphNodeNames(graph)}
                onChange={(_event: React.SyntheticEvent, value: string) => this.onSearchRequest(value, graph)}
                autoComplete
                // eslint-disable-next-line react/jsx-props-no-spreading
                renderInput={(params) => <TextField {...params} label="Search" />}
              />
            </Box>
            <Stack direction="row" spacing={0.5}>
              <Fab size="small" color="secondary" aria-label="zoom out" onClick={() => this.zoom.scale({ scaleX: 0.8, scaleY: 0.8 })}>
                <ZoomOut />
              </Fab>
              <Fab size="small" color="secondary" aria-label="zoom in" onClick={() => this.zoom.scale({ scaleX: 1.2, scaleY: 1.2 })}>
                <ZoomIn />
              </Fab>
            </Stack>
          </Stack>
        </Box>
      </Box>
    );
  }
}
