import { AutocompleteChangeDetails, AutocompleteChangeReason, Box, Card, Grid } from '@mui/material';
import * as React from 'react';

import { SelectionSearchComponent } from '../shared-components/SelectionSearchComponent';

export interface IGridContainerProps {
  beliefNames: string[];
  barChartPlots: Record<string, JSX.Element>;
  lineChartPlots: Record<string, React.ReactNode>;
  barChartPlotsSlider: React.ReactNode;
  lineChartPlotsSlider: React.ReactNode;
  observationAnalysis: React.ReactNode;
  masteryGrid: React.ReactNode;
  updateBeliefNames: (pinnedBeliefNames: string[], reason: AutocompleteChangeReason, details?: AutocompleteChangeDetails<string>) => void;
  title?: string;
}

export class LearnerAnalysisGridContainer extends React.Component<IGridContainerProps> {
  constructor(props: IGridContainerProps) {
    super(props);
  }

  public render(): React.ReactNode {
    return (
      <Box maxWidth="100%" sx={{ margin: '1em' }}>
        {this.props.title ? <h3>{this.props.title}</h3> : null}
        {this.props.masteryGrid}
        <Grid wrap="nowrap" overflow="auto" container direction="row" alignItems="baseline">
          {this.createSearchContainer()}
        </Grid>
        {this.props.lineChartPlotsSlider}
        <Grid wrap="nowrap" overflow="auto" container direction="row" alignItems="baseline">
          {this.useLineGraph()}
        </Grid>
        {this.props.barChartPlotsSlider}
        <Grid wrap="nowrap" overflow="auto" container direction="row" alignItems="baseline">
          {this.useBarChart()}
        </Grid>
        <Grid wrap="nowrap" overflow="auto" container direction="row" alignItems="baseline">
          {this.props.observationAnalysis}
        </Grid>
      </Box>
    );
  }

  /**
   * changes the bar chart plots related to the grid container
   * @returns
   */
  useBarChart(): React.ReactNode {
    const beliefBarChartPlots = Object.values(this.props.barChartPlots).map((beliefLinePlot: React.ReactElement) => (
      <div key={beliefLinePlot.key}>
        <Grid container direction="column" alignItems="flex-start">
          <Card sx={{ mr: 1 }}>{beliefLinePlot}</Card>
        </Grid>
      </div>
    ));
    return beliefBarChartPlots;
  }

  /**
   * changes the line graph plots related to the grid container
   * @returns
   */
  useLineGraph(): React.ReactNode {
    const beliefLinePlots = Object.values(this.props.lineChartPlots).map((beliefLinePlot: React.ReactElement) => (
      <div key={beliefLinePlot.key}>
        <Grid container direction="column" alignItems="flex-start">
          <div>{beliefLinePlot}</div>
        </Grid>
      </div>
    ));
    return beliefLinePlots;
  }

  /**
   * Creates the Container to sort the BeliefNames above
   * @returns
   */
  createSearchContainer(): React.ReactNode {
    return (
      <Grid container direction="column" alignItems="flex-start">
        <SelectionSearchComponent
          options={this.props.beliefNames}
          stateCallback={this.props.updateBeliefNames}
        />
      </Grid>
    );
  }
}
