import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@mui/material';
import React from 'react';

import { roundToPrecision } from '@adlete/engine-blocks/utils/Utils';
import { IObservation } from '@adlete/framework/IObservation';

interface IObservationProps {
  observations: IObservation[];
}

export const ObservationAnalysis = ({ observations }: IObservationProps) => {
  function displayObservationTimestamp(timestamp: Date): string {
    if (typeof timestamp === 'string') {
      return timestamp;
    }
    return timestamp.toLocaleTimeString();
  }

  const displayAdditionalInfos = ({additionalInfos}: IObservation): React.ReactNode[] => {
    const parsedInfo: Record<string, number | string> = JSON.parse(additionalInfos);
    console.log(parsedInfo)
      return Object.entries(parsedInfo).map(([key, value], index) => {
        console.log(`${key}:${value}`)
        if(typeof value === 'number') {
          value = roundToPrecision(value, 3)
        }
        return (
        <TableRow key={index}>
          <TableCell>{key}</TableCell>
          <TableCell>{value}</TableCell>
        </TableRow>
      )
    } 
  )
  }
 
  function displayObservations(observation: IObservation, index: number): React.ReactNode {
    return (
      <Grid key={index} item overflow="visible" sx={{ mr: 1, minHeight: 280 }}>
        <Card>
          <CardHeader
            avatar={
              <Avatar sx={{ bgcolor: `hsl(${Math.round(observation.activityCorrectness * 110)} 70% 70%)` }}>
                {Math.round(observation.activityCorrectness * 100)}
              </Avatar>
            }
            title={<Typography fontSize={16}>{observation.activityName}</Typography>}
            action={
              <Box sx={{ display: 'flex', alignItems: 'center', margin: 'auto' }}>
                <Typography fontSize={16}>{`${index + 1}.`}</Typography>
              </Box>
            }
          ></CardHeader>
          <Divider></Divider>
          <CardContent>
            <TableContainer>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>Difficulty</TableCell>
                    <TableCell>{Math.round(observation.activityDifficulty * 1000) / 1000}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Time</TableCell>
                    <TableCell>{observation.timestamp ? displayObservationTimestamp(observation.timestamp) : '-'}</TableCell>
                  </TableRow>
                  {observation.additionalInfos ? displayAdditionalInfos(observation) : null}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>
        </Card>
      </Grid>
    );
  }

  return (
    <Grid container wrap="nowrap" overflow="auto" direction="row" alignItems="flex-start">
      {observations.map((observation, index) => displayObservations(observation, index))}
    </Grid>
  );
};
