import { AutocompleteChangeDetails, AutocompleteChangeReason, Box, Grid, Slider } from '@mui/material';
import * as React from 'react';

import { IAnalyticsResponse } from '@adlete/client-ts/Types';
import { BayesBeliefModel } from '@adlete/engine-blocks/belief/BayesBeliefModel';
import { IBayesNet } from '@adlete/engine-blocks/belief/IBayesNet';
import { IProbabilityBelief, IScalarBelief, IScalarBeliefBundle } from '@adlete/engine-blocks/belief/IBelief';
import { ScalarBeliefModel } from '@adlete/engine-blocks/belief/ScalarBeliefModel';
import { IScalarTendency, IScalarTendencyBundle, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';

import { AutoLearnerMasteryGrid } from '../plugin-heatmap/AutoLearnerMasteryGrid';
import { beliefsToBarPlots, beliefsToLinePlots as beliefsToLineChartPlots } from '../shared-components/charts/Plots';

import { LearnerAnalysisGraphContainer } from './LearnerAnalysisGraphContainer';
import { LearnerAnalysisGridContainer } from './LearnerAnalysisGridContainer';
import {ObservationAnalysis} from './ObservationAnalysis';


export enum LearnerAnalysisMode {
  GridMode,
  GraphMode,
}

interface IAnalysisContainersProps {
  modeOfDisplay: LearnerAnalysisMode; // TODO: replace with ModeOfDisplay
  learnerAnalytics: IAnalyticsResponse;
}

interface IAnalysisContainerState {
  pinnedBeliefNames: string[];
  unsortedBeliefNames: string[];
}

interface ISortedBeliefs {
  sortedProbabilityBeliefs: Record<string, IProbabilityBelief[]>;
  sortedScalarBeliefs: Record<string, IScalarBelief[]>;
}

export interface IEmptyModel {
  bayesBeliefModel: BayesBeliefModel;
  scalarBeliefModel: ScalarBeliefModel;
  tendencies: IScalarTendencyBundle;
}

export const AnalysisContainer = (props: IAnalysisContainersProps) => {
  const beliefNames = React.useMemo(() => Object.keys(props.learnerAnalytics.serviceConfiguration.competenceModel), [props.learnerAnalytics])
  const [analysisState, setAnalysisState] = React.useState<IAnalysisContainerState>(
    {
      unsortedBeliefNames: beliefNames,
      pinnedBeliefNames: []
    }
  )
  /**
   * Meos component to avoid rerender on every change of the slider
   */
  const sortedBeliefs = React.useMemo<ISortedBeliefs>(() => {
    const convertedProbabilityBeliefs: Record<string, IProbabilityBelief[]> = {};
    const convertedScalarBeliefs: Record<string, IScalarBelief[]> = {};
    const allBeliefNames = [...analysisState.pinnedBeliefNames, ...analysisState.unsortedBeliefNames];

    // convert data into better format
    for (let k = 0; k < props.learnerAnalytics.learner.overAllFinishedActivities; k++) {
      allBeliefNames.forEach((beliefName) => {
        if (convertedProbabilityBeliefs[beliefName] === undefined) {
          convertedProbabilityBeliefs[beliefName] = [];
        }
        convertedProbabilityBeliefs[beliefName].push(props.learnerAnalytics.learner.probabilisticBeliefs[k][beliefName]);

        if (convertedScalarBeliefs[beliefName] === undefined) {
          convertedScalarBeliefs[beliefName] = [];
        }
        convertedScalarBeliefs[beliefName].push(props.learnerAnalytics.learner.scalarBeliefs[k][beliefName]);
      });
    }
    return {
      sortedProbabilityBeliefs: convertedProbabilityBeliefs,
      sortedScalarBeliefs: convertedScalarBeliefs
    }
  }, [analysisState, props.learnerAnalytics.learner])

  const [barSliderStep, setBarSliderStep] = React.useState<number>(0)
  const [linePlotDomain, setLinePlotDomain] = React.useState<number[]>([0, props.learnerAnalytics.learner.probabilisticBeliefs.length])

  /**
   * We need to update the BeliefNames for the order to change
   * @param unsortedBeliefNames
   * @param pinnedBeliefNames
   */
  function updateBeliefNames(pinnedBeliefNames: string[], reason: AutocompleteChangeReason, details?: AutocompleteChangeDetails<string>): void {
    if (pinnedBeliefNames === null) {
      return;
    }
    if(reason === "selectOption") {
      setAnalysisState({
        pinnedBeliefNames,
        unsortedBeliefNames: analysisState.unsortedBeliefNames.filter((name) => !(name === details.option))
      })
    }

    if(reason === "removeOption") {
      setAnalysisState({
        pinnedBeliefNames: pinnedBeliefNames,
        unsortedBeliefNames: [...analysisState.unsortedBeliefNames, details.option]
      })
    }

    if(reason === "clear") {
      setAnalysisState({
        pinnedBeliefNames: pinnedBeliefNames,
        unsortedBeliefNames: [...analysisState.unsortedBeliefNames, ...analysisState.pinnedBeliefNames]
      })
    }
  }

  /**
   * Generating foreach competence a bar chart plot.
   * Uses memo to avoid rerender when the bar plot isnt changed
   */
  const generateBarChartPlots = React.useMemo((): Record<string, JSX.Element> => {
    const config = props.learnerAnalytics.serviceConfiguration as IAdleteConfiguration;
    const states = config.competenceModel[Object.keys(config.competenceModel)[0]].states;

    return beliefsToBarPlots(
      [...analysisState.pinnedBeliefNames, ...analysisState.unsortedBeliefNames],
      props.learnerAnalytics.learner.probabilisticBeliefs[barSliderStep],
      props.learnerAnalytics.learner.scalarBeliefs[barSliderStep],
      props.learnerAnalytics.learner.tendencies[barSliderStep],
      states,
      250,
      350
    );
  }, [barSliderStep, props.learnerAnalytics, analysisState])

  /**
   * Generate a range Slider to set the selected a step for all BarChartPlots
   * @returns
   */
  function generateBarChartSlider(): JSX.Element {
    return (
      <Grid container direction="column" alignItems="flex-start" maxWidth="100%">
        <Box sx={{ width: '98%', marginLeft: '1%' }}>
          <Slider
            value={barSliderStep}
            onChange={(_e, newValue) => {
              setBarSliderStep(newValue as number)
            }}
            marks
            step={1}
            valueLabelDisplay="auto"
            max={props.learnerAnalytics.learner.overAllFinishedActivities - 1}
          />
        </Box>
      </Grid>
    );
  } 

  /**
   * Generating foreach competence a line chart plot
   * Using memo to avoid rerender
   */
  const generateLineChartPlots = React.useMemo((): Record<string, React.ReactElement> => {
    const config = props.learnerAnalytics.serviceConfiguration as IAdleteConfiguration;
    const states = config.competenceModel[Object.keys(config.competenceModel)[0]].states;
    return beliefsToLineChartPlots(
      sortedBeliefs.sortedProbabilityBeliefs,
      sortedBeliefs.sortedScalarBeliefs,
      states,
      linePlotDomain,
      props.modeOfDisplay === LearnerAnalysisMode.GridMode ? 350 : 250,
      props.modeOfDisplay === LearnerAnalysisMode.GridMode ? 350 : 250
    );
  }, [linePlotDomain, props.learnerAnalytics.serviceConfiguration, props.modeOfDisplay, sortedBeliefs])

  /**
   * Slider Component for Range in the Line Plots
   * @returns
   */
  function generateLineChartPlotsSlider(): React.ReactElement {
    const linePlotSlider = (
      <Grid container direction="column" alignItems="flex-start" maxWidth="100%">
        <Box sx={{ width: '98%', marginLeft: '1%' }}>
          <Slider
            value={linePlotDomain}
            onChange={(_e, newValue) => {
              setLinePlotDomain(newValue as number[]);
            }}
            marks
            step={1}
            valueLabelDisplay="auto"
            max={props.learnerAnalytics.learner.overAllFinishedActivities - 1}
          />
        </Box>
      </Grid>
    );
    return linePlotSlider;
  }

  function generateActivitieOverview(): React.ReactElement {
    return <ObservationAnalysis observations={props.learnerAnalytics.observations}></ObservationAnalysis>;
  }

  function generateMasteryGrid(): React.ReactElement {
    const learners = props.learnerAnalytics.learners
    if(!learners || learners.length === 0) return null

    const learnersData: {scalar: IScalarBeliefBundle, tendencies: ITendencyBundle<IScalarTendency>}[] = []
    learners.forEach((learner) => {
      if(learner.scalarBeliefs.length > 0 && learner.tendencies.length > 0) {
        learnersData.push({scalar: learner.scalarBeliefs[learner.scalarBeliefs.length - 1], tendencies: learner.tendencies[learner.tendencies.length - 1]})
      }
    }) 
    const config = props.learnerAnalytics.serviceConfiguration as IAdleteConfiguration
    return(
      <AutoLearnerMasteryGrid
        width={500}
        height={250}
        config={props.learnerAnalytics.serviceConfiguration as IAdleteConfiguration}
        mainLearner={props.learnerAnalytics.learner}
        learner={learnersData} 
        defaultValue={config.competenceModel["wissen"]}        />
    )
  }

  switch (props.modeOfDisplay) {
    case LearnerAnalysisMode.GridMode:
      return (
        <LearnerAnalysisGridContainer
          title={`${props.learnerAnalytics.observations.length} Observations simulated`}
          masteryGrid={generateMasteryGrid()}
          beliefNames={beliefNames}
          barChartPlots={generateBarChartPlots}
          barChartPlotsSlider={generateBarChartSlider()}
          lineChartPlots={generateLineChartPlots}
          lineChartPlotsSlider={generateLineChartPlotsSlider()}
          updateBeliefNames={updateBeliefNames}
          observationAnalysis={generateActivitieOverview()}
        />
      );
    case LearnerAnalysisMode.GraphMode:
      return (
        <LearnerAnalysisGraphContainer
          barChartPlots={generateBarChartPlots}
          barChartPlotsSlider={generateBarChartSlider()}
          lineChartPlots={generateLineChartPlots}
          lineChartPlotsSlider={generateLineChartPlotsSlider()}
          competenceModel={props.learnerAnalytics.serviceConfiguration.competenceModel as IBayesNet}
        />
      );
    default:
      throw Error(`Unknown mode ${props.modeOfDisplay}`);
  }
}
