# @adlete/visualizer

[Electron](https://electronjs.org/)-based visualizer application for learning applications and games that use `@adaptive-difficulty/framework`. Provides facilities for simulating agents and analysing user data.

## Starting

The visualizer needs to load a `visualizerExtension` class to function.

Currently you need to provide the package name of this extension as a command-line argument, e.g.:

```bash
yarn start
```

## Writing a visualizerExtension

Copy the `IVisualizerExtension` interface to a new package and default-export a class, which implements this interface.

Copying is currently necessary, because we don't want a circular dependency between the `visualizer` and the extension.

## Development

Electron applications are split into `main` and `renderer`. In this package, all the interesing stuff happens in the `renderer`.

The UI of the visualizer uses [React](https://reactjs.org/) and [Material-UI](https://material-ui.com/).

### Dependencies

- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

### Install package dependencies

```bash
yarn install
```

### Building

This package uses [Gulp](https://gulpjs.com/) and [Typescript](http://typescriptlang.org/).

```bash
# clean and build
yarn build

# watch and build
yarn watch:build

# clean only
yarn clean
```

### IDE

#### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/).

#### Linting

This package uses a prettier configuration to set a style format for all source code files, we ship this config in the package @adlete/dev-config.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).

### data flow

the picture below describes the data flow between the different components of the software
![Alt text](url 'adlete-packages/@adlete/visualizer-core/dataFlwo.png')
