# @ada/eslint-config

This module simply forwards the eslint configs from @ada/dev-config, because ESLint currently does not allow to expose shareable configs from packages that do not start with `eslint-config`, see [here](https://github.com/eslint/eslint/issues/9188).
