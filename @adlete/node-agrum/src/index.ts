/* eslint-disable @typescript-eslint/no-redeclare */
//@ts-ignore
import type agrum from './addon/nodeAgrum';

import { createRequire } from 'module';
const require = createRequire(import.meta.url);

//@ts-ignore
const agrum = require('./addon/nodeAgrum.node');

export const BayesNet = agrum.BayesNet;

export type BayesNet = agrum.BayesNet;

export const BayesNetFactory = agrum.BayesNetFactory;
export type BayesNetFactory = agrum.BayesNetFactory;

export const GibbsSampling = agrum.GibbsSampling;
export type GibbsSampling = agrum.GibbsSampling;

export const LazyPropagation = agrum.LazyPropagation;
export type LazyPropagation = agrum.LazyPropagation;

export const LoopyBeliefPropagation = agrum.LoopyBeliefPropagation;
export type LoopyBeliefPropagation = agrum.LoopyBeliefPropagation;

export const ShaferShenoyInference = agrum.ShaferShenoyInference;
export type ShaferShenoyInference = agrum.ShaferShenoyInference;

export const VariableElimination = agrum.VariableElimination;
export type VariableElimination = agrum.VariableElimination;

export const EvidenceInference = agrum.EvidenceInference;
export type EvidenceInference = agrum.EvidenceInference;

export const BNWriter = agrum.BNWriter;
export type BNWriter = agrum.BNWriter;

export const XDSLBNReader = agrum.XDSLBNReader;
export type XDSLBNReader = agrum.XDSLBNReader;

export const XDSLBNWriter = agrum.XDSLBNWriter;
export type XDSLBNWriter = agrum.XDSLBNWriter;

export const Vector = agrum.Vector;
export type Vector = agrum.Vector;

export const VectorFloat = agrum.VectorFloat;
export type VectorFloat = agrum.VectorFloat;

export const VectorString = agrum.VectorString;
export type VectorString = agrum.VectorString;

export const Instantiation = agrum.Instantiation;
export type Instantiation = agrum.Instantiation;

export const Potential = agrum.Potential;
export type Potential = agrum.Potential;

export const DiscreteVariable = agrum.DiscreteVariable;
export type DiscreteVariable = agrum.DiscreteVariable;

export const LabelizedVariable = agrum.LabelizedVariable;
export type LabelizedVariable = agrum.LabelizedVariable;

export function getDistributionFromPotential(potential: agrum.Potential, index?: number): number[] {
  const inst = new agrum.Instantiation(potential);

  const disVar = potential.variable(index || 0);

  inst.setFirstVar(disVar);
  const array = [];
  while (!inst.end()) {
    array.push(potential.get(inst));
    inst.incVar(disVar);
  }

  return array;
}

export function convertVectorString(vectorString: agrum.VectorString): string[] {
  const result: string[] = [];

  for (let i = 0; i < vectorString.size(); i += 1) {
    result.push(vectorString.get(i));
  }

  return result;
}

export function convertVectorFloat(vectorString: agrum.VectorFloat): number[] {
  const result: number[] = [];

  for (let i = 0; i < vectorString.size(); i += 1) {
    result.push(vectorString.get(i));
  }

  return result;
}

export function convertVector(vectorString: agrum.Vector): number[] {
  const result: number[] = [];

  for (let i = 0; i < vectorString.size(); i += 1) {
    result.push(vectorString.get(i));
  }

  return result;
}

export function getCPT(potential: agrum.Potential): number[] {
  const inst = new agrum.Instantiation(potential);

  const result: number[] = [];
  do {
    result.push(potential.get(inst));
    inst.inc();
  } while (!inst.end());

  return result;
}
