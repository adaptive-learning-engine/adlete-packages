import { BayesNet } from '../BayesNet';

/**
 * BayesNet
 */
export abstract class BNWriter {
  constructor();
  write(filePath: string, bn: BayesNet): void;
}
