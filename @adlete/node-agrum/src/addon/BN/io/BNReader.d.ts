import { BayesNet } from '../BayesNet';

/**
 * BayesNet
 */
export abstract class BNReader {
  constructor(bn: BayesNet, filePath: string);
  proceed(): void;
}
