import { BayesNet } from '../../BayesNet';
import { BNReader } from '../BNReader';
/**
 * BayesNet
 */
export class XDSLBNReader extends BNReader {
  constructor(bn: BayesNet, filePath: string);
  proceed(): void;
}
