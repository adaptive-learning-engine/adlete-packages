import { BayesNet } from '../../BayesNet';
import { BNWriter } from '../BNWriter';

/**
 * BayesNet
 */
export class XDSLBNWriter extends BNWriter {
  constructor();
  write(filePath: string, bn: BayesNet): void;
}
