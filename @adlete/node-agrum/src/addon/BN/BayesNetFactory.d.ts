import { Vector_float, Vector_string } from '../tools/core/Vector';

import { BayesNet } from './BayesNet';

/**
 * BayesNetFactory is a helper class to create BayesNet
 */
export class BayesNetFactory {
  constructor(bayesNet: BayesNet);
  // #region Variable Declaration
  /**
   * Tells the factory that we're in a variable declaration.
   */
  startVariableDeclaration(): void;

  /**
   * Tells the factory the current variable's name.
   * @param name
   */
  variableName(name: string): void;

  /**
   * Tells the factory the current variable's description.
   * @param desc
   */
  variableDescription(desc: string): void;

  /**
   * Adds a modality to the current labelized variable.
   * @param name
   */
  addModality(name: string): void;

  /**
   * Adds the min value of the current range variable.
   * @param min
   */
  addMin(min: number): void;

  /**
   * Adds the max value of the current range variable.
   * @param max
   */
  addMax(max: number): void;

  /**
   * Tells the factory that we're out of a variable declaration.
   * @returns the generated nodeId
   */
  endVariableDeclaration(): number;
  // #endregion

  // #region Parent Declaration
  /**
   * Tells the factory that we're declaring parents for some variable.
   * @param nodeName
   */
  startParentsDeclaration(nodeName: string): void;

  /**
   * 	Tells the factory for which variable we're declaring parents.
   * @param nodeName
   */
  addParent(nodeName: string): void;

  /**
   * 	Tells the factory that we've finished declaring parents for some variable.
   */
  endParentsDeclaration(): void;

  // #endregion

  // #region Raw CPT Declaration
  /**
   * Tells the factory that we're declaring a conditional probability table for some variable.
   * @param nodeName
   */
  startRawProbabilityDeclaration(nodeName: string): void;

  /**
   * Fills the variable's table with the values in rawTable.
   */
  rawConditionalTable(variables: Vector_string, rawTable: Vector_float): void;

  /**
   * Fills the variable's table with the values in rawTable.
   * @param rawTable
   */
  rawConditionalTable(rawTable: Vector_float): void;

  /**
   * Tells the factory that we finished declaring a conditional probability table.
   */
  endRawProbabilityDeclaration(): void;

  // #endregion
}
