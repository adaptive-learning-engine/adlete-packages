import { BayesNet } from '../BayesNet';

import { EvidenceInference } from './tools/EvidenceInference';

/**
 * GibbsSampling
 */
export class GibbsSampling extends EvidenceInference {
  constructor(BN: BayesNet);
}
