import { EvidenceInference } from './tools/EvidenceInference';
import { BayesNet } from '../BayesNet';

/**
 * VariableElimination
 */
export class VariableElimination extends EvidenceInference {
  constructor(BN: BayesNet, relevantFinderTypes?: any, FindBarenTypes?: any);
}
