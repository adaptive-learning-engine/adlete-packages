import { BayesNet } from '../BayesNet';

import { EvidenceInference } from './tools/EvidenceInference';

/**
 * LazyPropagation
 */
export class LazyPropagation extends EvidenceInference {
  constructor(BN: BayesNet);
}
