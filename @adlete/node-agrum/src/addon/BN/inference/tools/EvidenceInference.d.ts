import { Vector } from '../../../tools/core/Vector';
import { Potential } from '../../../tools/multidim/Potential';

export abstract class EvidenceInference {
  posterior(node: number | string): Potential;
  addEvidence(id: number, evidence: Vector): void;
  chgEvidence(id: number, evidence: Vector): void;
  hasEvidence(id: number): boolean;
  makeInference(): void;
  setNumberOfThreads(nb: number): void;
  eraseAllEvidence(): void;
  eraseAllTargets(): void;
  eraseTarget(node: number | string): void;
}
