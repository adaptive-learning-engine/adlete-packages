import { BayesNet } from '../BayesNet';

import { EvidenceInference } from './tools/EvidenceInference';

/**
 * LoopyBeliefPropagation
 */
export class LoopyBeliefPropagation extends EvidenceInference {
  constructor(BN: BayesNet);
}
