import { BayesNet } from '../BayesNet';

import { EvidenceInference } from './tools/EvidenceInference';

/**
 * ShaferShenoyInference
 */
export class ShaferShenoyInference extends EvidenceInference {
  constructor(BN: BayesNet);
}
