import { Potential } from '../tools/multidim/Potential';
import { NodeSet } from '../tools/core/Set';
import { DiscreteVariable } from '../tools/variables/DiscreteVariable';
import { Vector_string } from '../tools/core/Vector';

/**
 * BayesNet
 */
export class BayesNet {
  constructor(name: string);
  constructor();

  variable(id: number): DiscreteVariable;
  add(variable: DiscreteVariable): any;
  addArc(tail: string, target: string): void;
  nodes(): number[];
  size(): number;
  idFromName(name: string): number;
  parents(id: number): NodeSet;
  cpt(id: number): Potential;
  clear(): void;

  /* self developed extension for better usage of BayesNet */
  loadXDSL(name: string, l?: any): void;
  saveXDSL(name: string, allowModificationWhenSaving?: boolean): void;
  parentNames(nodeId: number): Vector_string;
}
