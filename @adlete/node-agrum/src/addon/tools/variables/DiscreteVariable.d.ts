import { Vector_string } from '../core/Vector';

/**
 * DiscreteVariable
 */
export class DiscreteVariable {
  constructor(aName: string, aDesc: string);
  name(): string;
  domainSize(): number;
  labels(): Vector_string;
  label(idx: number): string;
}
