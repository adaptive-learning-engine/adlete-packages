import { DiscreteVariable } from './DiscreteVariable';

export class LabelizedVariable extends DiscreteVariable {
  constructor(aName: string, aDesc?: string, nbrLabel?: number);
  addLabel(aLabel: string): void;
}
