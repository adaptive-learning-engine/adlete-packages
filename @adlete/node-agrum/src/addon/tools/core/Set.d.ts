export abstract class Set {}

export class NodeSet extends Set {
  constructor();
  /*  Removes all the elements, if any, from the set. */
  clear(): void;
  /* Returns the number of elements in the set. */
  size(): number;
  /* Indicates whether a given elements belong to the set. */
  contains(nodeId: number): boolean;
  /* Indicates whether the set is the empty set. */
  empty(): boolean;
  /* Prints the content of the set. */
  toString(): void;
}
