/* eslint-disable @typescript-eslint/naming-convention */
/**
 * C++ Double Vector
 */
export class Vector {
  add(val: number): void;
  get(idx: number): number;
  size(): number;
  isEmpty(): boolean;
}

/**
 * C++ Float Vector
 */
export class Vector_float {
  add(val: number): void;
  get(idx: number): number;
  size(): number;
  isEmpty(): boolean;
}

/**
 * C++ String Vector
 */
export class Vector_string {
  add(val: string): void;
  get(idx: number): string;
  size(): number;
  isEmpty(): boolean;
}
