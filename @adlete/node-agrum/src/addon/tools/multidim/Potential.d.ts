import { Instantiation } from './Instantiation';
import { DiscreteVariable } from '../variables/DiscreteVariable';
import { Vector } from '../core/Vector';

export class Potential {
  get(i: Instantiation): number;
  variable(idx: number): DiscreteVariable;
  getCPtr(): any;
  nbrDim(): number;
  toString(): string;
  content(): any;
  fillWith(cpt: Vector): any;
}
