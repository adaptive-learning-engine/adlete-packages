import { DiscreteVariable } from '../variables/DiscreteVariable';

// eslint-disable-next-line import/no-cycle
import { Potential } from './Potential';

export class Instantiation {
  constructor(potential: Potential);
  setFirstVar(discreteVariable: DiscreteVariable): void;
  end(): boolean;
  incVar(discreteVariable: DiscreteVariable): void;
  inc(): void;
  domainSize(): number;
}
