import { Vector, Vector_float as VectorFloat, Vector_string as VectorString } from '../tools/core/Vector';
import { Potential } from '../tools/multidim/Potential';

export declare function getDistributionFromPotential(potential: Potential, index?: number): number[];
export declare function convertVectorString(vectorString: VectorString): string[];
export declare function convertVectorFloat(vectorString: VectorFloat): number[];
export declare function convertVector(vectorString: Vector): number[];
export declare function getCPT(potential: Potential): number[];
