# TestNodeAgrum

This project is a playground to test the via swig convert agrum implementation in a nodejs environment.

## init project

Build the nodeAgrum Project via Visual Studio 2019 or higher. You will find the agrum.lib and nodeAgrum.node files in within ./build/nodeAgrum/release/Release. Copy those two files into the ./addon folder of this project. You can open now this project with VS Code 1.70 or higher.

Afterwards run the following commands in this order to setup and build the project demo. (You can also use npm)

```
 yarn

 yarn build
```

## start project

If you have initialized the project run the following command to start a demo.

```
 yarn start
```

Have a more detailed look into the jest test cases and run them with the following command.

```
 yarn test
```

### Soft and hard evidence:

`Hard` evidence:

- A certain empiric evidence, e.g. a coin lands on head therefore our evidence is: "Head: 1, Tails: 0"

`Soft`evidence:

- Soft evidence uses a probability distribution to show tendencies as evidence, e.g. "Head: 0.5, Tails: 0.5"
