/* eslint-disable no-console */
import { BayesNet, LazyPropagation, XDSLBNReader } from '../src/index';
import { getDistributionFromPotential } from '../src/utils/BayesNetUtils';

describe('BayesBeliefModel', () => {
  test('load bayesian network from xdsl file', () => {
    const bayesNetwork = loadDefaultBayesNetFromXDSLFile();

    const nodes = bayesNetwork.nodes();

    expect(nodes.length).toEqual(3);
  });
  test('load bayesian network and inference the network', () => {
    const bayesNetwork = loadDefaultBayesNetFromXDSLFile();

    // First inference after loading the model
    const ie = new LazyPropagation(bayesNetwork);
    ie.makeInference();
    const potentialOfA = ie.posterior('A');

    // result is based on the calculation with netica (v. 609)
    // compare with ./networks/test.neta, ./networks/test.png, ./networks/test.json
    // The result should be: {"H": 0.18, "L": 0.29, "M": 0.53 }
    const resultBelief = getDistributionFromPotential(potentialOfA);
    const expectedResult = [0.29, 0.53, 0.18];
    for (let i = 0; i < expectedResult.length; i += 1) {
      expect(resultBelief[i]).toBeCloseTo(expectedResult[i], 4);
    }
  });
  test('load bayesian network and add evidence', () => {
    /* TODO
    const bayesNetwork = loadDefaultBayesNetFromXDSLFile();

    const ie = new gum.LazyPropagation(bayesNetwork);

    const arr = [0, 0.4, 0.6];
    const vector = new gum.Vector();
    arr.forEach((num) => vector.add(num));
    ie.addEvidence(bayesNetwork.idFromName('B'), vector);
    console.log(ie.posterior('B').toString());
    ie.makeInference();
    /* 
    nodes.forEach((node) => {
      console.log(ie.posterior(node).toString());
    });
    */
  });
});
/**
 * Loads a default bayesian network from a given xdsl file see ./test/networks/test.xdsl
 * @returns
 */
function loadDefaultBayesNetFromXDSLFile(): BayesNet {
  const bayesNetwork = new BayesNet();

  try {
    const xdslBNReader = new XDSLBNReader(bayesNetwork, './test/networks/test.xdsl');
    xdslBNReader.proceed();
  } catch (e) {
    console.log(e);
  }

  return bayesNetwork;
}
