import { GraphQLClient } from 'graphql-request';

import { AdleteServiceConnection } from '../src/AdleteServiceConnection';
import { LearnerRoutines } from '../src/routines/LearnerRoutines';

import { createNewLearner, generateAdleteServiceConnection, generateGraphQLClientConnection } from './GlobalTestSetup';

describe('learnerRequest', () => {
  let adleteServiceConnection: AdleteServiceConnection;
  let graphQLClient: GraphQLClient;
  const learnerId = 'unitTestDefaultUser';
  beforeEach(async () => {
    adleteServiceConnection = generateAdleteServiceConnection();
    graphQLClient = generateGraphQLClientConnection();
    await createNewLearner(graphQLClient, learnerId);
  }, 100000);

  afterEach(async () => {
    await LearnerRoutines.deleteLearner(graphQLClient, learnerId);
  }, 100000);

  test('if an existing default learner could login correctly. Returned statusCode must be 200', async () => {
    const result = await adleteServiceConnection.login(learnerId);
    expect((await result).statusInfo.statusCode).toEqual(200);
  }, 100000);

  test('to create another learner and delete it correctly, returned statusCode must be 200', async () => {
    const resultCreateLearner = await adleteServiceConnection.createNewLearnerAndLogin();
    expect(resultCreateLearner.statusInfo.statusCode).toEqual(200);

    const resultCreateDelete = await adleteServiceConnection.deleteLearner();
    expect(resultCreateDelete.statusCode).toEqual(200);
  }, 10000);

  test('if learnerId contains the given prefix "unitTestDefaultUser" ', async () => {
    expect(learnerId).toContain('unitTestDefaultUser');
  }, 100000);

  test('to delete a learner correctly', async () => {
    const result = await LearnerRoutines.deleteLearner(graphQLClient, learnerId);
    expect(result.statusCode).toEqual(200);
  }, 100000);
});
