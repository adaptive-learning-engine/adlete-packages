import { GraphQLClient } from 'graphql-request';
import { AdleteServiceConnection } from '../src/AdleteServiceConnection';
import { GraphQLClientExtension } from '../src/GraphQLClientExtension';
import { LearnerRoutines } from '../src/routines/LearnerRoutines';
import { IStatusInfo } from '../src/Types';

export function generateAdleteServiceConnection(): AdleteServiceConnection {
  return new AdleteServiceConnection('6F9A5BB8-C799F76C-5BF19B50-3F0CAA52', 'http://localhost:5001');
}
export function generateGraphQLClientConnection(): GraphQLClient {
  return GraphQLClientExtension.createClient('6F9A5BB8-C799F76C-5BF19B50-3F0CAA52', 'http://localhost:5001');
}

export async function createDefaultUser(adleteServiceConnection: AdleteServiceConnection, prefix = 'unitTestDefaultUser'): Promise<string> {
  const { learnerId } = await adleteServiceConnection.createNewLearnerAndLogin();
  return learnerId;
}

export async function createNewLearner(
  graphQLClient: GraphQLClient,
  learnerId: string
): Promise<{
  statusInfo: IStatusInfo;
  learnerId: string;
}> {
  const learner = LearnerRoutines.createLearner(graphQLClient, learnerId);
  return learner;
}
