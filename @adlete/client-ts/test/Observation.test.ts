import { GraphQLClient } from 'graphql-request';

import { AdleteServiceConnection } from '../src/AdleteServiceConnection';
import { AnalysisRoutines } from '../src/routines/AnalysisRoutines';
import { LearnerRoutines } from '../src/routines/LearnerRoutines';

import { createNewLearner, generateAdleteServiceConnection, generateGraphQLClientConnection } from './GlobalTestSetup';

describe('analyticsRequest', () => {
  let adleteServiceConnection: AdleteServiceConnection;
  let graphQLClient: GraphQLClient;
  const learnerId = 'unitTestDefaultUser';

  beforeEach(async () => {
    adleteServiceConnection = generateAdleteServiceConnection();
    graphQLClient = generateGraphQLClientConnection();
    await createNewLearner(graphQLClient, learnerId);
  }, 100000);

  afterEach(async () => {
    await LearnerRoutines.deleteLearner(graphQLClient, learnerId);
  }, 100000);

  test('if the response has a valid analytics data', async () => {
    const result = await AnalysisRoutines.fetchLearnerAnalytics(graphQLClient, learnerId);
    expect(result.statusInfo.statusCode).toEqual(200);

    expect(result.analyticsData.serviceConfiguration).not.toBe('');
    expect(result.analyticsData.serviceConfiguration).not.toBe(null);
  }, 100000);
});
