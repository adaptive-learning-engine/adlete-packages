import { GraphQLClient } from 'graphql-request';

import { ServiceRoutines } from '../src/routines/ServiceRoutines';

import { generateGraphQLClientConnection } from './GlobalTestSetup';

describe('statusRequest', () => {
  let graphQLClient: GraphQLClient;

  beforeEach(async () => {
    graphQLClient = generateGraphQLClientConnection();
  }, 100000);

  test('if the status request returns 200', async () => {
    const result = await ServiceRoutines.checkStatus(graphQLClient);
    expect(result.statusCode).toEqual(200);
  }, 100000);
});
