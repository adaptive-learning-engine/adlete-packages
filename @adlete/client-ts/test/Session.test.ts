import { GraphQLClient } from 'graphql-request';

import { AdleteServiceConnection } from '../src/AdleteServiceConnection';
import { LearnerRoutines } from '../src/routines/LearnerRoutines';
import { SessionRoutines } from '../src/routines/SessionRoutines';

import { createNewLearner, generateAdleteServiceConnection, generateGraphQLClientConnection } from './GlobalTestSetup';

describe('startSession', () => {
  let adleteServiceConnection: AdleteServiceConnection;
  let graphQLClient: GraphQLClient;
  const learnerId = 'unitTestDefaultUser';

  beforeEach(async () => {
    adleteServiceConnection = generateAdleteServiceConnection();
    graphQLClient = generateGraphQLClientConnection();
    await createNewLearner(graphQLClient, learnerId);
  }, 100000);

  afterEach(async () => {
    await SessionRoutines.stopSession(graphQLClient, learnerId);
    await LearnerRoutines.deleteLearner(graphQLClient, learnerId);
  }, 100000);

  test('to start a new session.', async () => {
    const result = await SessionRoutines.startSession(graphQLClient, learnerId);
    expect(result.statusInfo.statusCode).toEqual(200);
  }, 100000);

  test('if startTimeStamp for a new session is defined.', async () => {
    const result = await SessionRoutines.startSession(graphQLClient, learnerId);
    expect(result.startTimestamp).toBeDefined();
  }, 100000);

  test('to stop a session.', async () => {
    const result = await SessionRoutines.stopSession(graphQLClient, learnerId);

    expect(result.statusCode).toEqual(200);
  }, 100000);
});
