import { GraphQLClient } from 'graphql-request';

import { AnalysisRoutines } from '../src/routines/AnalysisRoutines';

import { generateGraphQLClientConnection } from './GlobalTestSetup';

describe('SimulateRequest', () => {
  let graphQLClient: GraphQLClient;

  beforeEach(async () => {
    graphQLClient = generateGraphQLClientConnection();
  }, 100000);

  test('to simulated a learner', async () => {
    const result = await AnalysisRoutines.simulateLearner(graphQLClient, 'level_beginner', 1, 'easy_learner');
    expect(result.statusInfo).toEqual({ statusCode: 200, statusDescription: '' });
  }, 100000);
});
