import { GraphQLClient } from 'graphql-request';

import { AdleteServiceConnection } from '../src/AdleteServiceConnection';
import { ActivityRoutines } from '../src/routines/ActivityRoutines';
import { LearnerRoutines } from '../src/routines/LearnerRoutines';
import { SessionRoutines } from '../src/routines/SessionRoutines';

import { createNewLearner, generateAdleteServiceConnection, generateGraphQLClientConnection } from './GlobalTestSetup';

describe('fetchNextRecommendation', () => {
  let adleteServiceConnection: AdleteServiceConnection;
  let graphQLClient: GraphQLClient;
  const learnerId = 'unitTestDefaultUser';

  beforeEach(async () => {
    adleteServiceConnection = generateAdleteServiceConnection();
    graphQLClient = generateGraphQLClientConnection();
    await createNewLearner(graphQLClient, learnerId);
  }, 100000);

  afterEach(async () => {
    await SessionRoutines.stopSession(graphQLClient, learnerId);
    await LearnerRoutines.deleteLearner(graphQLClient, learnerId);
  }, 100000);

  test('if the fetchNextRecommendation method is sending back a response.', async () => {
    await SessionRoutines.startSession(graphQLClient, learnerId);
    const result = await ActivityRoutines.fetchNextActivityRecommendation(graphQLClient, learnerId);

    expect(result.statusInfo.statusCode).toEqual(200);
  }, 100000);
});
