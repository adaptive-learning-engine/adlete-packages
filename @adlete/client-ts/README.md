# @adlete/client-ts

A client plugin to connect typescript-based virtual learning environments to the adaption engine @adlete/service.

## Example - Before the main trainings loop of your virtual learning environment starts

```
/* Before the main trainings loop */

/* Create a new instance of the service connection */
const adleteServiceConnection = new AdleteServiceConnection(this.token, this.host, 'arithmetic-game');

/* Check if the service is available. */
const {statusCode, statusDescription} = await adleteService.checkStatus();
if (statusCode !== 200) {
      console.error('Unable to connect to adlete service! Error message: ' + statusDescription);
}

/* If you do not have a learner create one and login. Otherwise just use login. */
const {statusInfo, learnerId} = await adleteService.createNewLearnerAndLogin();
if (statusInfo.statusCode !== 200) {
    console.error('Unable to create learner! Error message: ' + statusInfo.statusDescription);
}

/* Whenever the a new practicing session starts create a new session. */
const startedSession = await adleteService.startSession();
if (startedSession.StatusInfo.statusCode !== 200) {
    console.error('Unable to start session! Error message: ' + startedSession.StatusInfo.statusDescription);
}
```

## Example - Within the main trainings loop of your virtual learning environment

```
/* Before your virtual learning environment creates each new activities you can request *
 * a recommendation for a specific activity type and difficulty level. Activity types  *
 * and difficulty levels are described and can be defined via the @adlete/service.        */
const recommendation = await adleteService.fetchNextActivityRecommendation();
if (recommendation.statusInfo.statusCode !== 200) {
    console.error('Unable to fetch recommendation! Error message: ' + recommendation.StatusInfo.statusDescription);
}

/* Create new activity with the recommended activities type/name and the difficulty level *
 * For example if your virtual learning environment supports the training of the basic   *
 * mathematic operation addition and subtraction this could look like this:              */
if(recommendation.nextActivityName == "activityAddition") {
    console.log("Please calculate:")
    if(recommendation.nextActivityDifficulty < 0.3) {
        console.log("1 + 1 = ?")
        generatedDifficulty
    } else if(recommendation.nextActivityDifficulty < 0.6) {
        console.log("152 + 65 = ?")
    } else {
       console.log("1545206787 + 596 = ?")
    }
}
if(recommendation.nextActivityName == "activitySubtraction") {
    //etc...
}

/* the user has given an answer and the given answer was validated. 1 = correct, 0 = incorrect */
const correctness = 1
/* define the difficulty level of your activity. This is a balancing parameter, since your *
 * activities would most like do not meet the actual difficulty level */
const generatedDifficulty = 0.5

const submitResults = await adleteService.submitResults(recommendation.nextActivityName, recommendation.nextActivityDifficulty, correctness, generatedDifficulty);
if (response.statusCode !== 200) {
    console.error('Unable to submit results! Error message: ' + recommendation.StatusInfo.statusDescription);
}
```

## Example - After the main trainings loop of your virtual learning environment ended

```
const stopSessionResponse = await adleteService.stopSession();
if (stopSessionResponse.statusCode !== 200) {
    console.error('Unable to stop session! Error message: ' + stopSessionResponse.StatusInfo.statusDescription);
}
```

## Development

### Dependencies

- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

### Install package dependencies

```bash
yarn install
```

### Building code

This package uses [Typescript](http://typescriptlang.org/).

```bash
# clean and build
yarn build

# watch and build
yarn watch:build
```

### Building documentation

```bash
yarn build:docs
```

### IDE

#### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/).

#### Linting

This package uses a prettier configuration to set a style format for all source code files, we ship this config in the package @adlete/dev-config.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).
