import { gql, GraphQLClient } from 'graphql-request';

import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import * as GraphQLClientExtension from '../GraphQLClientExtension';


/**
 * submit an activity result which was created by the learner with the given learnerId
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    statusInfo
 */
export async function submitActivityResult(
  graphQLClient: GraphQLClient,
  learnerId: string,
  activityName: string,
  activityCorrectness: number,
  activityDifficulty: number
): Promise<IStatusInfo> {
  const mutation = gql`
    mutation submitActivityResult($observation: ObservationInput!) {
      submitActivityResult(observation: $observation) {
        statusCode
        statusDescription
      }
    }
  `;
  const variables = {
    observation: {
      learnerId,
      activityName,
      activityCorrectness,
      activityDifficulty,
      timestamp: new Date().toISOString(),
    },
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{submitActivityResult: IStatusInfo}>(graphQLClient, mutation, variables);
    return serverResponse.submitActivityResult
  } catch (error) {
    return GraphQLClientExtension.processError(error);
  }
}

/**
 * fetch the next activity recommendation for a specific learner
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    statusInfo
 *    nextActivityDifficulty
 *    nextActivityName
 */
export async function fetchNextActivityRecommendation(
  graphQLClient: GraphQLClient,
  learnerId: string,
  activitySubset?: string[]
): Promise<{ statusInfo: IStatusInfo; recommendation: IActivityRecommendation<number> }> {
  const mutation = gql`
    mutation FetchNextRecommendation($learnerId: String!, $activitySubset: [String!]) {
      fetchNextRecommendation(learnerId: $learnerId, activitySubset: $activitySubset) {
        activityName
        difficulty
        trend
      }
    }
  `;
  const variables = {
    learnerId,
    activitySubset,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{fetchNextRecommendation: IActivityRecommendation<number>}>(graphQLClient, mutation, variables);
    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
      recommendation: serverResponse.fetchNextRecommendation
    };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      recommendation: null
    };
  }
}
