import { gql, GraphQLClient } from 'graphql-request';

import { IAdleteConfiguration, IAdleteJSONConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { IServerStatusInfo, IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import * as GraphQLClientExtension from '../GraphQLClientExtension';

/**
 * Checks the server status, and returns also information about the authorization status.
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns IStatusInfo
 */
export async function checkStatus(graphQLClient: GraphQLClient): Promise<IServerStatusInfo> {
  const query = gql`
    query {
      status {
        statusCode
        timestamp
        statusDescription
        authenticated
      }
    }
  `;
  const variables = {};
  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{status: IServerStatusInfo}>(graphQLClient, query, variables);
    return serverResponse.status
  } catch (error) {
    const compiledError = GraphQLClientExtension.processError(error);
    return {statusCode: compiledError.statusCode, authenticated: false, statusDescription: compiledError.statusDescription, timestamp: compiledError.timestamp}
  }
}
/**
 * checking the server availability
 *
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *    503 Network request failed
 * @returns IStatusInfo
 */
export async function checkConnectionAndStatus(host: string, token?: string): Promise<IStatusInfo> {
  try {
    const client = GraphQLClientExtension.createClient(host, token);
    return await checkStatus(client);
  } catch (error) {
    return {
      statusCode: 503,
      statusDescription: error.message,
    };
  }
}

/**
 * fetching the service configuration
 *
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 *
 * @returns IStatusInfo
 */
export async function fetchServiceConfiguration(
  graphQLClient: GraphQLClient
): Promise<{ statusInfo: IStatusInfo; serviceConfiguration: IAdleteConfiguration }> {
  const query = gql`
    query serviceConfiguration {
      serviceConfiguration {
        globalWeights
        activityObservationWeights
        initialScalarBeliefs
        simulatedLearnerBehaviorTypes
      }
    }
  `;
  const variables = {};

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{serviceConfiguration: IAdleteJSONConfiguration}>(graphQLClient, query, variables);
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        serviceConfiguration: GraphQLClientExtension.parseServiceConfiguration(serverResponse.serviceConfiguration),
      };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      serviceConfiguration: null,
    };
  }
}
