import { gql, GraphQLClient } from 'graphql-request';

import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import * as GraphQLClientExtension from '../GraphQLClientExtension';
import { ISession } from '../Types';


/**
 * starts a new session for the learner with the given learnerId
 *
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 * @returns
 *    statusInfo
 *    startTimeStamp
 */
export async function startSession(
  graphQLClient: GraphQLClient,
  learnerId: string
): Promise<{ statusInfo: IStatusInfo; session: ISession}> {
  const mutation = gql`
    mutation startSession($learnerId: String!) {
      startSession(learnerId: $learnerId) {
        startTimestamp
      }
    }
  `;
  const variables = {
    learnerId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{startSession: ISession}>(graphQLClient, mutation, variables);
    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
      session: serverResponse.startSession,
    };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      session: null,
    };
  }
}

/**
 * get active session of the learner with the given learnerId
 *
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 * @returns
 *    statusInfo
 *    startTimeStamp
 */
export async function activeSession(
  graphQLClient: GraphQLClient,
  learnerId: string
): Promise<{ statusInfo: IStatusInfo; activeSession: ISession }> {
  const query = gql`
    query activeSession($learnerId: String!) {
      activeSession(learnerId: $learnerId) {
        startTimestamp
        active
        activitiesStatistics {
          activityName
          activitiesPlayed
          avgAnswersCorrectness
          sumAnswersCorrectness
        }
      }
    }
  `;
  const variables = {
    learnerId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{activeSession: ISession}>(graphQLClient, query, variables);

      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        activeSession: serverResponse.activeSession,
      };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      activeSession: null,
    };
  }
}

/**
 * stops a session for a learner with the given learnerId
 *
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    statusInfo
 */
export async function stopSession(graphQLClient: GraphQLClient, learnerId: string): Promise<IStatusInfo> {
  const mutation = gql`
    mutation stopSession($learnerId: String!) {
      stopSession(learnerId: $learnerId) {
        statusCode
        statusDescription
      }
    }
  `;
  const variables = {
    learnerId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{stopSession: IStatusInfo}>(graphQLClient, mutation, variables);
    return GraphQLClientExtension.genSTATUSCODE200(serverResponse.stopSession.statusDescription);
  } catch (error) {
    return GraphQLClientExtension.processError(error);
  }
}
