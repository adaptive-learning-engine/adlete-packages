import { gql, GraphQLClient } from 'graphql-request';

import { ILearner } from '@adlete/framework/ILearner';
import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import * as GraphQLClientExtension from '../GraphQLClientExtension';

/**
 * Fetching a single learner from the server, by providing a specific learnerId
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    IStatusInfo
 */
export async function fetchLearner(
  graphQLClient: GraphQLClient,
  learnerId: string
): Promise<{ statusInfo: IStatusInfo; learner: ILearner }> {
  const query = gql`
    query learner($learnerId: String!) {
      learner(learnerId: $learnerId) {
        overAllFinishedActivities
        learnerId
        probabilisticBeliefs
        scalarBeliefs
        tendencies
        createdAt
        updatedAt
      }
    }
  `;
  const variables = {
    learnerId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{learner: ILearner}> (graphQLClient, query, variables);
    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
      learner: serverResponse.learner,
    };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      learner: null,
    };
  }
}

/**
 * Fetching a single learner from the server, by providing a specific learnerId
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    IStatusInfo
 */
export async function fetchMultipleLearner(
  graphQLClient: GraphQLClient,
  learnerIds: string[],
  idCipher?: string
): Promise<{ statusInfo: IStatusInfo; learners: ILearner[] }> {
  const query = gql`
    query Query($learnerIds: [String!]!, $idCipher: String) {
      multipleLearner(learnerIds: $learnerIds, idCipher: $idCipher) {
        scalarBeliefs
        tendencies
        probabilisticBeliefs
      }
    }
  `;
  const variables = {
    learnerIds,
    idCipher
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{multipleLearner: [ILearner]}>(graphQLClient, query, variables);
    
    if (serverResponse.multipleLearner.length > 0) {
      serverResponse.multipleLearner.forEach((learner: ILearner) => {
        const data = GraphQLClientExtension.parseLearnerAnalyticsData(learner)
        learner.probabilisticBeliefs = data.probabilisticBeliefs;
        learner.scalarBeliefs = data.scalarBeliefs;
        learner.tendencies = data.tendencies;
      })
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        learners: serverResponse.multipleLearner
      };
    }

    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE402(),
      learners: [],
    };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      learners: [],
    };
  }
}

/**
 * creating a learner with a given learnerId, the given learnerId must be unique.
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    learnerId
 *    statusInfo
 */
export async function createLearner(
  graphQLClient: GraphQLClient,
  learnerId: string,
  initialScalarBeliefSetId: string
): Promise<{ statusInfo: IStatusInfo; learnerId: string }> {
  const mutation = gql`
    mutation createLearner($learnerId: String!, $initialScalarBeliefSetId: String!) {
      createLearner(learnerId: $learnerId, initialScalarBeliefSetId: $initialScalarBeliefSetId) {
        learnerId
      }
    }
  `;
  const variables = {
    learnerId,
    initialScalarBeliefSetId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{createLearner: ILearner}>(graphQLClient, mutation, variables);
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        learnerId: serverResponse.createLearner.learnerId,
      };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      learnerId: '',
    };
  }
}

/**
 * delete a learner with a given learnerId
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @returns
 *    statusInfo
 */
export async function deleteLearner(graphQLClient: GraphQLClient, learnerId: string): Promise<IStatusInfo> {
  const mutation = gql`
    mutation deleteLearner($learnerId: String!) {
      deleteLearner(learnerId: $learnerId) {
        statusCode
      }
    }
  `;
  const variables = {
    learnerId,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{deleteLearner: IStatusInfo}>(graphQLClient, mutation, variables);
      return serverResponse.deleteLearner
  } catch (error) {
    return GraphQLClientExtension.processError(error);
  }
}

/**
 * Gets learnerIds from idCipher
 * @returns
 *    Array of strings
 */
export async function getLearnerIds(graphQLClient: GraphQLClient, idCipher: string): Promise<
string[]> {
  const mutation = gql`
    query Query($idCipher: String!) {
      getLearnerIds(idCipher: $idCipher)
    }
  `;
  const variables = {
    idCipher,
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{getLearnerIds: string[]}>(graphQLClient, mutation, variables);
      return serverResponse.getLearnerIds
  } catch (error) {
    console.error(error)
    return [];
  }
}
