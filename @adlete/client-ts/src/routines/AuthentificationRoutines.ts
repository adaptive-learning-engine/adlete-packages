import { gql, GraphQLClient } from "graphql-request";

import { IStatusInfo } from "@adlete/framework/utils/StatusInformation";

import * as GraphQLClientExtension from '../GraphQLClientExtension';
import { ILoginResponse, ITokenResponse } from "../Types";

export const loginUser = async (
    graphQLClient: GraphQLClient,
    username: string,
    password: string
  ): Promise<{statusInfo: IStatusInfo; tokens: ITokenResponse}> => {
    const query = gql`
        query login($username: String!, $password: String!) {
            login(username: $username, password: $password) {
                accessToken
                refreshToken
                statusCode
            }
        }
    `
    const variables = {
        username,
        password
    }

    try {
        const serverResponse = await GraphQLClientExtension.sendRequest<{login: ILoginResponse}>(graphQLClient, query, variables)
        if(serverResponse.login.statusCode !== 200) {
            return {
                statusInfo: {statusCode: serverResponse.login.statusCode, statusDescription: "Couldn't authenticate user correctly"},
                tokens: null
            }
        }
        return {
            statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
            tokens: serverResponse.login
        }
    } catch (error) {
        return {
        statusInfo: GraphQLClientExtension.processError(error),
        tokens: null
        };
    }
  }

  export const refreshLogin = async (
    graphQLClient: GraphQLClient,
    refreshToken: string
  ): Promise<{statusInfo: IStatusInfo; tokens: ITokenResponse}> => {
    const query = gql`
        query RefreshToken($refreshToken: String!) {
            refreshAccessToken(refreshToken: $refreshToken) {
                accessToken
                refreshToken
                statusCode
            }
        }
    `

    const variables = {
        refreshToken
    }

    try {
        const serverResponse = await GraphQLClientExtension.sendRequest<{refreshAccessToken: ILoginResponse}>(graphQLClient, query, variables)
            if(serverResponse.refreshAccessToken.statusCode !== 200) {
                return {
                    statusInfo: {statusCode: serverResponse.refreshAccessToken.statusCode, statusDescription: "Couldn't authenticate user correctly"},
                    tokens: null
                }
            }
            return {
                statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
                tokens: serverResponse.refreshAccessToken
            }
    } catch (error) {
        return {
        statusInfo: GraphQLClientExtension.processError(error),
        tokens: null
        };
    }

  }