import { gql, GraphQLClient } from 'graphql-request';

import { IAdleteJSONConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import * as GraphQLClientExtension from '../GraphQLClientExtension';
import { IAnalyticsResponse, ICompetenceInfo, ITask } from '../Types';

/**
 * returns all collected data about the given learner to analysis the learner´s performance
 * statusCodes :
 *    200 OK
 *    402 given request has invalid user input
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @param graphQLClient
 * @param learnerId
 *
 * @returns
 *    statusInfo
 *    analyticsData
 */
export async function fetchLearnerAnalytics(
  graphQLClient: GraphQLClient,
  learnerId: string,
  idCipher?: string
): Promise<{ statusInfo: IStatusInfo; analyticsData: IAnalyticsResponse }> {
  if (!learnerId) {
    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
      analyticsData: null,
    };
  }
  const query = gql`
    query learnerAnalytics($learnerId: String!, $idCipher: String) {
      learnerAnalytics(learnerId: $learnerId, idCipher: $idCipher) {
        serviceConfiguration {
          competenceModel
          globalWeights
          activityNames
          activityObservationWeights
          initialScalarBeliefs
          simulatedLearnerBehaviorTypes
        }
        learner {
          learnerId
          probabilisticBeliefs
          scalarBeliefs
          tendencies
          overAllFinishedActivities
        }
        observations {
          activityName
          activityDifficulty
          activityCorrectness
          timestamp
          additionalInfos
        }
      }
    }
  `;
  const variables = {learnerId, idCipher}

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{learnerAnalytics: IAnalyticsResponse}>(graphQLClient, query, variables);

      const serviceConfiguration = GraphQLClientExtension.parseServiceConfiguration(serverResponse.learnerAnalytics.serviceConfiguration as IAdleteJSONConfiguration);
      const parsedData = GraphQLClientExtension.parseLearnerAnalyticsData(serverResponse.learnerAnalytics.learner);
      serverResponse.learnerAnalytics.learner.probabilisticBeliefs = parsedData.probabilisticBeliefs;
      serverResponse.learnerAnalytics.learner.scalarBeliefs = parsedData.scalarBeliefs;
      serverResponse.learnerAnalytics.learner.tendencies = parsedData.tendencies;

      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        analyticsData: { ...serverResponse.learnerAnalytics, serviceConfiguration },
      };

  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      analyticsData: null,
    };
  }
}

/**
 * use fetchCompetenceInfo to get ScalarBeliefs for all competence names
 *
 * statusCodes :
 *    200 OK
 *    400 given request is incorrect
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @param learnerid
 * @param competenceNames
 *
 * @returns
 *    statusInfo
 *    competenceInfo
 */
export async function fetchCompetenceInfo(
  graphQLClient: GraphQLClient,
  learnerId: string,
  competenceNames: string[],
  idCipher: string
): Promise<{ statusInfo: IStatusInfo; competenceInfo: ICompetenceInfo }> {

  const query = gql`
    query getLearnerCompetence($learnerId: String!, $competenceNames: [String!], $idCipher: String) {
      getLearnerCompetence(learnerId: $learnerId, competenceNames: $competenceNames, idCipher: $idCipher) {
        competenceInfo {
          scalarBeliefs
        }
      }
    }
  `;

  const variables = {
    learnerId,
    competenceNames,
    idCipher
  }

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{getLearnerCompetence: {competenceInfo: {scalarBeliefs: string}} }>(graphQLClient, query, variables);

      const parsedData = JSON.parse(serverResponse.getLearnerCompetence.competenceInfo.scalarBeliefs);

      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
        competenceInfo: {scalarBeliefs: parsedData},
      };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      competenceInfo: null,
    };
  }
}

/**
 * use simulateLearner to simulate a learner by using an agent-based simulation
 *
 * statusCodes :
 *    200 OK
 *    400 given request is incorrect
 *    404 could not Connect to Server
 *    500 internal server error
 *
 * @param initialScalarBeliefSetId
 * @param simulationSteps
 * @param agentBehavior
 *
 * @returns
 *    statusInfo
 *    analyticsData
 */
export async function simulateLearner(
  graphQLClient: GraphQLClient,
  initialScalarBeliefSetId: string,
  simulationSteps: number,
  behavior: string = 'random',
  storeResults: boolean = false,
  learnerId?: string,
  activities?: string[],
  difficulty?: number,
  idCipher?: string
): Promise<{ statusInfo: IStatusInfo; analyticsData: IAnalyticsResponse }> {

  const query = gql`
    query simulate(
      $simulationSteps: Int!
      $initialScalarBeliefSetId: String
      $behavior: String
      $storeResults: Boolean
      $learnerId: String
      $activities: [String!]
      $difficulty: Float
      $idCipher: String
    ) {
      simulate(
        simulationSteps: $simulationSteps
        initialScalarBeliefSetId: $initialScalarBeliefSetId
        behavior: $behavior
        storeResults: $storeResults
        learnerId: $learnerId
        activities: $activities
        difficulty: $difficulty
        idCipher: $idCipher
      ) {
        serviceConfiguration {
          competenceModel
          globalWeights
          activityObservationWeights
          activityNames
          initialScalarBeliefs
          simulatedLearnerBehaviorTypes
        }
        learner {
          learnerId
          probabilisticBeliefs
          scalarBeliefs
          tendencies
          overAllFinishedActivities
        }
        observations {
          activityName
          activityDifficulty
          activityCorrectness
          timestamp
          additionalInfos
        }
      }
    }
  `;
  const variables = {
    simulationSteps,
    initialScalarBeliefSetId,
    behavior,
    storeResults,
    learnerId,
    activities,
    difficulty,
    idCipher
  };

  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{simulate: IAnalyticsResponse}>(graphQLClient, query, variables);

    const serviceConfiguration = GraphQLClientExtension.parseServiceConfiguration(serverResponse.simulate.serviceConfiguration as IAdleteJSONConfiguration);
    const parsedData = GraphQLClientExtension.parseLearnerAnalyticsData(serverResponse.simulate.learner);
    serverResponse.simulate.learner.probabilisticBeliefs = parsedData.probabilisticBeliefs;
    serverResponse.simulate.learner.scalarBeliefs = parsedData.scalarBeliefs;
    serverResponse.simulate.learner.tendencies = parsedData.tendencies;

    const factor = Math.pow(10, 4);
    serverResponse.simulate.observations.forEach((observation) => {
      if (observation.timestamp != null) {
        observation.timestamp = new Date(observation.timestamp);
        observation.activityDifficulty = Math.round(observation.activityDifficulty * factor) / factor;
      }
    });

    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
      analyticsData: { ...serverResponse.simulate, serviceConfiguration },
    };
  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      analyticsData: null,
    };
  }
}

export async function advancedSimulation(
  graphQLClient: GraphQLClient,
  initialScalarBeliefSetId: string,
  taskSet: ITask[],
  maxDifficulty: number,
  minDifficulty: number,
  difficultyStep: number,
  behavior: string,
  storeResults: boolean,
  learnerId: string,
  maxAnsweredCorrect: number,
  idCipher: string
): Promise<{ statusInfo: IStatusInfo; analyticsData: IAnalyticsResponse }> {
  const query = gql`
    query advancedSimulation(
      $initialScalarBeliefSetId: String
      $taskSet: [Task!]!
      $maxDifficulty: Float!
      $minDifficulty: Float!
      $stepsDifficulty: Float!
      $behavior: String
      $storeResults: Boolean
      $learnerId: String
      $maxAnsweredCorrect: Int
      $idCipher: String
    ) {
      advancedSimulation(
        initialScalarBeliefSetId: $initialScalarBeliefSetId
        taskSet: $taskSet
        maxDifficulty: $maxDifficulty
        minDifficulty: $minDifficulty
        stepsDifficulty: $stepsDifficulty
        behavior: $behavior
        storeResults: $storeResults
        learnerId: $learnerId
        maxAnsweredCorrect: $maxAnsweredCorrect
        idCipher: $idCipher
      ) {
        serviceConfiguration {
          competenceModel
          globalWeights
          activityObservationWeights
          activityNames
          initialScalarBeliefs
          simulatedLearnerBehaviorTypes
        }
        learner {
          learnerId
          probabilisticBeliefs
          scalarBeliefs
          tendencies
          overAllFinishedActivities
        }
        observations {
          activityName
          activityDifficulty
          activityCorrectness
          timestamp
          additionalInfos
        }
      }
    }
  `;
  const variables = {
    initialScalarBeliefSetId,
    taskSet,
    maxDifficulty,
    minDifficulty,
    stepsDifficulty: difficultyStep,
    behavior,
    storeResults,
    learnerId,
    maxAnsweredCorrect,
    idCipher
  };
  try {
    const serverResponse = await GraphQLClientExtension.sendRequest<{advancedSimulation: IAnalyticsResponse}>(graphQLClient, query, variables);

    const serviceConfiguration = GraphQLClientExtension.parseServiceConfiguration(serverResponse.advancedSimulation.serviceConfiguration as IAdleteJSONConfiguration);
    const parsedData = GraphQLClientExtension.parseLearnerAnalyticsData(serverResponse.advancedSimulation.learner);
    serverResponse.advancedSimulation.learner.probabilisticBeliefs = parsedData.probabilisticBeliefs;
    serverResponse.advancedSimulation.learner.scalarBeliefs = parsedData.scalarBeliefs;
    serverResponse.advancedSimulation.learner.tendencies = parsedData.tendencies;

    const factor = Math.pow(10, 4);
    serverResponse.advancedSimulation.observations.forEach(
      (observation) => {
        if (observation.timestamp != null) {
          observation.timestamp = new Date(observation.timestamp);
          observation.activityDifficulty = Math.round(observation.activityDifficulty * factor) / factor;
        }
      }
    );

    return {
      statusInfo: GraphQLClientExtension.genSTATUSCODE200(),
      analyticsData: { ...serverResponse.advancedSimulation, serviceConfiguration },
    };

  } catch (error) {
    return {
      statusInfo: GraphQLClientExtension.processError(error),
      analyticsData: null,
    };
  }
}
