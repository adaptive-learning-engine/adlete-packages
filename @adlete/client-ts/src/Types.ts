import { GraphQLClient as TypeGraphQL } from 'graphql-request';

import { IBeliefBundle, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IAccumulatedActivityStatistics } from '@adlete/framework/IAccumulatedActivityStatistics';
import { IAdleteConfiguration, IAdleteJSONConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { ILearner } from '@adlete/framework/ILearner';
import { IObservation } from '@adlete/framework/IObservation';
/**
 * IResponse is used for any response received from GraphQLClient.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type IResponse<T> = Promise<T>;

/**
 * IAnalyticsResponse holds
 * @serviceConfiguration holds the complete service configuration
 * @learner holds all learner information
 * @observations holds all observations about a learner
 */
export interface IAnalyticsResponse {
  serviceConfiguration: IAdleteConfiguration | IAdleteJSONConfiguration;
  learner: ILearner;
  observations: IObservation[];
  learners?: ILearner[]
}

/**
 * ISession holds
 * @startTimestamp which holds the information when the session was initiated
 * @active holds the information with the session is active
 * @activitiesStatistics holds the statistic information for each activity
 */
export interface ISession {
  startTimestamp: Date;
  active: boolean;
  activitiesStatistics: IAccumulatedActivityStatistics[];
}

export interface ICompetenceInfo {
  scalarBeliefs: IBeliefBundle<IScalarBelief>[];
}
/**
 * export GraphQLClient to use GraphQLClientExtension createGraphQLClient without forcing user to install
 * graphql-request dependency.
 */
export type GraphQLClient = TypeGraphQL;

export interface ITask {
  name: string;
  loId: string;
  difficulty: number;
}

export interface ITokenResponse {
  accessToken: string,
  refreshToken: string
}

export interface ILoginResponse extends ITokenResponse {
  statusCode: number
}
