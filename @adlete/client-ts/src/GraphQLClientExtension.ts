import { ClientError, GraphQLClient } from 'graphql-request';

import { IBeliefBundle, IProbabilityBelief, IScalarBelief } from '@adlete/engine-blocks/belief/IBelief';
import { IScalarTendency, ITendencyBundle } from '@adlete/engine-blocks/recommendation/ITendency';
import { IAdleteConfiguration, IAdleteJSONConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { ILearner } from '@adlete/framework/ILearner';
import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';

import { IResponse } from './Types';



/**
 * generates a StatusInfo object to indicate a successful processed request
 * @param statusDescription
 * @returns
 */
export function genSTATUSCODE200(statusDescription = ''): IStatusInfo {
  return { statusCode: 200, statusDescription };
}

/**
 * generates a StatusInfo object to indicate none-authorized calls
 * @returns
 */
export function genSTATUSCODE401(): IStatusInfo {
  return { statusCode: 401, statusDescription: 'User does not exist. Please login user or create a new one!' };
}

/**
 * generates a StatusInfo object to indicate a given invalid user input.
 * @returns
 */
export function genSTATUSCODE402(): IStatusInfo {
  return { statusCode: 402, statusDescription: 'Invalid user input.' };
}

/**
 * generates a StatusInfo object to indicate a server connection failure.
 * @returns
 */
export function genSTATUSCODE404(): IStatusInfo {
  return { statusCode: 404, statusDescription: 'Server connection failed!' };
}

/**
 * generates a StatusInfo object to indicate internal server errors.
 * @param error
 * @returns
 */
export function genSTATUSCODE500(error: Error): IStatusInfo {
  if (error instanceof ClientError) {
    return { statusCode: 500, statusDescription: `Internal Server Error: ${error.response.errors ? error.response.errors[0].message : "Unknown Error"}` };
  }
  return { statusCode: 500, statusDescription: error.message };
}

/**
 * creates a GraphQLClient object which is used to connect to a graphql service
 * @param token
 * @param host
 * @returns
 */
export function createClient(host: string, token?: string): GraphQLClient {
  if(token) {
    const config = {
      headers: {
        'Authorization': `Bearer ${token}`,
      
    }}
    return new GraphQLClient(host, config)
  }
  return new GraphQLClient(host);;
}

/**
 * Returns if a given response is valid, which means if the given response does not contain any error message.
 * @deprecated request should automatically throw an error
 * @param response
 * @returns
 */
export function isResponseValid<T>(response: IResponse<T>): boolean {
  if (response === null || response === undefined) {
    return false;
  }

  return !Object.keys(response).some((value) => value.match('error') || value.match('invalid') || value.match('failed'));
}

/**
 * Processes response errors
 * @param error
 * @returns
 *  IStatusInfo
 */
export function processError(error: ClientError): IStatusInfo {
  if (!error) {
    return genSTATUSCODE404();
  }
  return genSTATUSCODE500(error);
}

/**
 * Sends a query and it input variables to a service
 * @param query
 * @param variables
 * @returns
 */
export async function sendRequest<T>(client: GraphQLClient, query: string, variables: object): Promise<T> {
  return client.request<T,object>(query, variables);
}

/**
 * parsing routine for serialized service configuration data
 * @param serializedServiceConfiguration
 * @returns
 */
export function parseServiceConfiguration(serializedServiceConfiguration: IAdleteJSONConfiguration): IAdleteConfiguration {
  return {
    competenceModel:
      serializedServiceConfiguration.competenceModel !== undefined ? JSON.parse(serializedServiceConfiguration.competenceModel) : null,
    globalWeights:
      serializedServiceConfiguration.globalWeights !== undefined ? JSON.parse(serializedServiceConfiguration.globalWeights) : null,
    activityNames:
      serializedServiceConfiguration.activityNames !== undefined ? JSON.parse(serializedServiceConfiguration.activityNames) : null,
    activityObservationWeights:
      serializedServiceConfiguration.activityObservationWeights !== undefined
        ? JSON.parse(serializedServiceConfiguration.activityObservationWeights)
        : '',
    initialScalarBeliefs:
      serializedServiceConfiguration.initialScalarBeliefs !== undefined
        ? JSON.parse(serializedServiceConfiguration.initialScalarBeliefs)
        : null,
    simulatedLearnerBehaviorTypes:
      serializedServiceConfiguration.simulatedLearnerBehaviorTypes !== undefined
        ? JSON.parse(serializedServiceConfiguration.simulatedLearnerBehaviorTypes)
        : null,
    activityRecommenderTargets:
      serializedServiceConfiguration.activityRecommenderTargets !== undefined
        ? JSON.parse(serializedServiceConfiguration.activityRecommenderTargets)
        : null,
  };
}

export function parseLearnerAnalyticsData(learnerResponse: ILearner): {
  probabilisticBeliefs: IBeliefBundle<IProbabilityBelief>[];
  scalarBeliefs: IBeliefBundle<IScalarBelief>[];
  tendencies: ITendencyBundle<IScalarTendency>[];
} {
  let parsedProbabilisticBeliefs;
  let parsedScalarBeliefs;
  let parsedTendencies;
  const probabilisticBeliefsBundle: IBeliefBundle<IProbabilityBelief>[] = [];
  const scalarBeliefsBundle: IBeliefBundle<IScalarBelief>[] = [];
  const TendenciesBundle: IBeliefBundle<IScalarTendency>[] = [];
  for (let i = 0; i < learnerResponse.probabilisticBeliefs.length; i++) {
    parsedProbabilisticBeliefs = JSON.parse(learnerResponse.probabilisticBeliefs[i] as unknown as string);
    parsedScalarBeliefs = JSON.parse(learnerResponse.scalarBeliefs[i] as unknown as string);
    parsedTendencies = JSON.parse(learnerResponse.tendencies[i] as unknown as string);
    probabilisticBeliefsBundle.push(parsedProbabilisticBeliefs);
    scalarBeliefsBundle.push(parsedScalarBeliefs);
    TendenciesBundle.push(parsedTendencies);
  }

  return {
    probabilisticBeliefs: probabilisticBeliefsBundle,
    scalarBeliefs: scalarBeliefsBundle,
    tendencies: TendenciesBundle,
  };
}


