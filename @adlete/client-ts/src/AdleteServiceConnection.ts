import { GraphQLClient } from 'graphql-request';

import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';
import { IAdleteConfiguration } from '@adlete/framework/IAdleteConfiguration';
import { ILearner } from '@adlete/framework/ILearner';
import { IStatusInfo } from '@adlete/framework/utils/StatusInformation';


import * as GraphQLClientExtension from './GraphQLClientExtension';
import { IAnalyticsResponse, ISession, ITokenResponse } from './Types';
import * as ActivityRoutines from './routines/ActivityRoutines';
import * as AnalysisRoutines from './routines/AnalysisRoutines';
import { loginUser, refreshLogin } from './routines/AuthentificationRoutines';
import * as LearnerRoutines from './routines/LearnerRoutines';
import * as ServiceRoutines from './routines/ServiceRoutines';
import * as SessionRoutines from './routines/SessionRoutines';

export class AdleteServiceConnection {
  /* holds the learnerId of the currently loggedIn learner. Requests will use this Id. */
  private loggedInLearnerId: string;

  /* holds a instance of a GraphQLClient to communicate with a graphql service */
  private graphQLClient: GraphQLClient;

  /* holds a learnerId prefix which is used during creating new learners on the service */
  private newLearnerIdPrefix: string;

  constructor(host: string, newLearnerIdPrefix = '', token?: string) {
    this.graphQLClient = GraphQLClientExtension.createClient(host, token);
    this.newLearnerIdPrefix = newLearnerIdPrefix;
  }

  // #region Service Routines

  /**
   * Checks the server status, and returns also information about the authorization status.
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns IStatusInfo
   */
  public async checkStatus(): Promise<IStatusInfo> {
    return ServiceRoutines.checkStatus(this.graphQLClient);
  }

  public async adleteLoginUser(username: string, password: string): Promise<ITokenResponse> {
    return loginUser(this.graphQLClient, username, password).then((response) => {
      // An error should be thrown if login is false.
        return response.tokens
    })
  }

  public async adleteRefreshLogin(refreshToken: string): Promise<ITokenResponse> {
    return refreshLogin(this.graphQLClient, refreshToken).then(response => {
      return response.tokens
    })
  }

  /**
   * fetching the service configuration
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns IStatusInfo
   */
  public async fetchServiceConfiguration(): Promise<{ statusInfo: IStatusInfo; serviceConfiguration: IAdleteConfiguration }> {
    return ServiceRoutines.fetchServiceConfiguration(this.graphQLClient);
  }

  // #endregion

  // #region Learner Routines

  /**
   * login a user with a given learnerId
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    statusInfo
   */
  public async login(learnerId: string): Promise<{ statusInfo: IStatusInfo; learner: ILearner }> {
    if (!learnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE402(),
        learner: null,
      };
    }

    const response = await LearnerRoutines.fetchLearner(this.graphQLClient, learnerId);

    if (response.statusInfo.statusCode === 200) {
      this.loggedInLearnerId = response.learner.learnerId;
    }

    return response;
  }

  /**
   * Fetching the logged-in learner data from the server
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    statusInfo: IStatusInfo
   *    learner: ILearner
   */
  public async fetchLearner(): Promise<{ statusInfo: IStatusInfo; learner: ILearner }> {
    if (!this.loggedInLearnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
        learner: null,
      };
    }

    return LearnerRoutines.fetchLearner(this.graphQLClient, this.loggedInLearnerId);
  }

  /**
   * creating a learner on server side. A new learner with a unique learnerId starting with the given prefix will be generated.
   * If the creation was successful the created user is logged-in.
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    learnerId
   *    statusInfo
   */
  public async createNewLearnerAndLogin(initialScalarBeliefSetId: string): Promise<{ statusInfo: IStatusInfo; learnerId: string }> {
    const newUniqueLearnerId = this.generateUniqueKey(this.newLearnerIdPrefix);
    const response = await LearnerRoutines.createLearner(this.graphQLClient, newUniqueLearnerId, initialScalarBeliefSetId);
    if (response.statusInfo.statusCode === 200) {
      this.loggedInLearnerId = response.learnerId;
    }

    return response;
  }

  private generateUniqueKey(pre: string): string {
    const newKey = `${pre}_${new Date().getTime()}`;
    return newKey;
  }

  /**
   * delete the logged-in learner. All data according to this user will be deleted.
   * Use login functionality to log-in a user.
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    statusInfo
   */
  public async deleteLearner(): Promise<IStatusInfo> {
    if (!this.loggedInLearnerId) {
      return GraphQLClientExtension.genSTATUSCODE401();
    }

    const response = await LearnerRoutines.deleteLearner(this.graphQLClient, this.loggedInLearnerId);

    if (response.statusCode === 200) {
      this.loggedInLearnerId = null;
    }

    return response;
  }
  // #endregion

  // #region Analysis Routines

  /**
   * returns all collected data from the logged-in learner to analysis the learner´s performance
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    statusInfo
   *    analyticsData
   */
  public async fetchLearnerAnalytics(): Promise<{ statusInfo: IStatusInfo; analyticsData: IAnalyticsResponse }> {
    if (!this.loggedInLearnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
        analyticsData: null,
      };
    }

    return AnalysisRoutines.fetchLearnerAnalytics(this.graphQLClient, this.loggedInLearnerId);
  }

  /**
   * use simulateLearner to simulate a learner by using an agent-based simulation
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @param initialScalarBeliefSetId
   * @param simulationSteps
   * @param agentBehavior
   *
   * @returns
   *    statusInfo
   *    analyticsData
   */
  public async simulateLearner(
    initialScalarBeliefSetId: string,
    simulationSteps: number,
    agentBehavior: string,
    storeResults: boolean = false,
    learnerId?: string,
    activities?: string[],
    difficulty?: number
  ): Promise<{ statusInfo: IStatusInfo; analyticsData: IAnalyticsResponse }> {
    return AnalysisRoutines.simulateLearner(
      this.graphQLClient,
      initialScalarBeliefSetId,
      simulationSteps,
      agentBehavior,
      storeResults,
      learnerId,
      activities,
      difficulty
    );
  }
  // #endregion

  // #region Activity Routines
  /**
   * fetch the next activity recommendation for a specific learner
   * use the parameter activitySubset if your client cannot support
   * all possible activities. Compare the defined activities by
   * fetching the service configuration.
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   *
   * @returns
   *    statusInfo
   *    nextActivityDifficulty
   *    nextActivityName
   */
  public async fetchNextActivityRecommendation(activitySubset?: string[]): Promise<{
    statusInfo: IStatusInfo;
    recommendation: IActivityRecommendation<number>
  }> {
    if (!this.loggedInLearnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
        recommendation: {
          difficulty: 0,
          activityName: '',
          trend: 0,
        }
      };
    }

    return ActivityRoutines.fetchNextActivityRecommendation(this.graphQLClient, this.loggedInLearnerId, activitySubset);
  }

  /**
   * submit Results
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   * @returns
   *    statusInfo
   */
  public async submitActivityResult(activityName: string, activityCorrectness: number, activityDifficulty: number): Promise<IStatusInfo> {
    if (!this.loggedInLearnerId) {
      return GraphQLClientExtension.genSTATUSCODE401();
    }

    return ActivityRoutines.submitActivityResult(
      this.graphQLClient,
      this.loggedInLearnerId,
      activityName,
      activityCorrectness,
      activityDifficulty
    );
  }
  // #endregion

  // #region Session Routines

  /**
   * starts a new session for the logged-in learner
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   * @returns
   *    statusInfo
   *    startTimeStamp
   */
  public async startSession(): Promise<{ statusInfo: IStatusInfo; session: ISession}> {
    if (!this.loggedInLearnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
        session: null,
      };
    }

    return SessionRoutines.startSession(this.graphQLClient, this.loggedInLearnerId);
  }

  /**
   * returns the current active session. If the current session is inactive we return null.
   * @returns
   */
  public async activeSession(): Promise<{ statusInfo: IStatusInfo; activeSession: ISession }> {
    if (!this.loggedInLearnerId) {
      return {
        statusInfo: GraphQLClientExtension.genSTATUSCODE401(),
        activeSession: null,
      };
    }

    return SessionRoutines.activeSession(this.graphQLClient, this.loggedInLearnerId);
  }

  /**
   * stops a session for the logged-in learner
   *
   * statusCodes :
   *    200 OK
   *    400 given request is incorrect
   *    401 if user is not logged in
   *    404 could not Connect to Server
   *    500 internal server error
   * @returns
   *    statusInfo
   *    startTimeStamp
   */
  public async stopSession(): Promise<IStatusInfo> {
    if (!this.loggedInLearnerId) {
      return GraphQLClientExtension.genSTATUSCODE401();
    }

    return SessionRoutines.stopSession(this.graphQLClient, this.loggedInLearnerId);
  }
  // #endregion
}
